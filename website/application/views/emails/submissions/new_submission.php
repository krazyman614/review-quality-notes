<table cellpadding="0" cellspacing="0" border="0">
    <tr>
        <td valign="top"> 
	        <table cellpadding="5" cellspacing="0" border="0" align="center">
	            <tr>
	                <td>
	                    <table cellpadding="5" cellspacing="0" border="1" align="center" style="width: 100%;">
	                        <tr>
	                            <td>
	                                <div style="text-align: center;"><strong>SHIP TO:</strong></div>
	                                <?=$submission['company_name']?><br/>
	                                <?=$submission['name']?><br/>
	                                <?=$submission['address']?><br/>
	                                <?php if (!empty($submission['address2'])):?>
	                                <?=$submission['address2']?><br/>
	                                <?php endif;?>
	                                <?=$submission['city']?>, <?=$submission['state_province']?> <?=$submission['postal_code']?><br/>
	                                <?=$submission['country']?> <br />
	                                <?=$submission['phone_number']?> <br />
	                                <?=$submission['email']?> <br />
	                            </td>
	                            <td>
	                                <div style="text-align: center;"><strong>CUSTOMER ADDRESS:</strong></div>
	                                <?=$user->company_name?><br/>
	                                <?=$user->name?><br/>
	                                <?=$user->address?><br/>
	                                <?php if (!empty($submission['address2'])):?>
	                                <?=$user->address2?><br/>
	                                <?php endif;?>
	                                <?=$user->city?>, <?=$user->state_province?> <?=$user->postal_code?><br/>
	                                <?=$user->country?> <br />
	                                <?=$user->phone_number?> <br />
	                                <?=$user->email?> <br />
                                </td>
	                            <td>
	                                <div style="text-align: center;">Submission ID # <?=$submission['order_number']?></div>
	                                <strong>Service Level</strong> <br />
	                                Level: <?=$level->title?> / Amount: $<?=money_format('%i', $level->per_note)?> <br />
	                                <strong>SHIP VIA:</strong>
	                                <?=$shipping?> <br />
	                                Insurance: <?=money_format('%i', $submission['total_declared_value'])?>
	                            </td>
	                        </tr>
	                    </table>
	                </td>
	            </tr>
	            <tr>
	                <td>
	                    <table cellpadding="3" cellspacing="0" border="1" align="center">
	                        <tr bgcolor="#000000">
	                            <td>#</td>
	                            <td><span style="color: #FFFFFF;"><?=strtoupper(get_column_name('type', $submission['region_id']))?></span></td>
	                            <td><span style="color: #FFFFFF;">DENOM</span></td>
	                            <td><span style="color: #FFFFFF;"><?=strtoupper(get_column_name('catalog_number', $submission['region_id']))?></span></td>
	                            <td><span style="color: #FFFFFF;"><?=strtoupper(get_column_name('pp', $submission['region_id']))?></span></td>
	                            <td><span style="color: #FFFFFF;">SERIAL #</span></td>
	                            <td><span style="color: #FFFFFF;">SVC</span></td>
	                            <td><span style="color: #FFFFFF;">DECLARED VALUE</span></td>
	                            <td><span style="color: #FFFFFF;">SERVICE COST</span></td>
                            </tr>
	                        <?php foreach ($notes as $note):?>
	                        <tr>
	                            <td><?=$note['sequence']?></td>
	                            <td><?=$note['note_type']['type']?></td>
	                            <td>$<?=money_format('%i', $note['denomination'])?></td>
	                            <td><?=$note['catalog_number']?></td>
	                            <td><?=$note['pp']?></td>
	                            <td><?=$note['serial_number']?></td>
	                            <td><?=$note['grading_service']['name']?></td>
	                            <td>$<?=money_format('%i', $note['declared_value'])?></td>
	                            <td>$<?=money_format('%i', $note['service_cost']);?></td>
	                        </tr>
	                        <?php endforeach;?>
	                        <tr>
	                        	<td colspan="8" align="right"><strong>Sub Total</strong></td>
	                        	<td>$<?=$total_service_cost?></td>
	                        </tr>
	                        <tr>
	                        	<td colspan="8" align="right"><strong>Postage</strong></td>
	                        	<td>$<?=$submission['postage']?></td>
	                        </tr>
	                        <tr>
	                        	<td colspan="8" align="right"><strong>Total</strong></td>
	                        	<td>$<?=$total_service_cost + $submission['postage']?></td>
	                        </tr>
                        </table>
	                </td>
	            </tr>
	        </table>
        </td>
    </tr>
</table>