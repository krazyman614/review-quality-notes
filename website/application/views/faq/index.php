<div class="header_row">
	<div class="header_row_inner">
	FAQ
	</div>
</div>

<div style="padding: 40px 70px 70px 70px;">

<fieldset>
	
	<ol>
		<?php if(isset($faqs)){
			foreach($faqs as $faq){?>
				<li><a href="#faq-<?php echo $faq['id'];?>"><?php echo $faq['title'];?></a></li>
			<?php 
			}
		}?>
	</ol>
	<dl>
		<?php if(isset($faqs)){
			foreach($faqs as $faq){?>
				<dt><a name="faq-<?php echo $faq['id'];?>"></a><h4><?php echo $faq['title'];?></h4></dt>
				<dd><p><?php echo $faq['content'];?></p></dd>
			<?php 
			}
		}?>
	</dl>
</fieldset>

</div>