<div class="header_row">
	<div class="header_row_inner">
	FAQ
	</div>
</div>

<div style="padding: 40px 70px 70px 70px;">


<fieldset>

	<style>
<!--
 /* Font Definitions */
@font-face
	{font-family:Arial;
	panose-1:2 11 6 4 2 2 2 2 2 4;
	mso-font-charset:0;
	mso-generic-font-family:auto;
	mso-font-pitch:variable;
	mso-font-signature:3 0 0 0 1 0;}
 /* Style Definitions */
p.MsoNormal, li.MsoNormal, div.MsoNormal
	{mso-style-parent:"";
	margin:0in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";
	mso-bidi-font-family:"Times New Roman";}
h4
	{mso-style-link:"Heading 4 Char";
	mso-margin-top-alt:auto;
	margin-right:0in;
	mso-margin-bottom-alt:auto;
	margin-left:0in;
	mso-pagination:widow-orphan;
	mso-outline-level:4;
	font-size:12.0pt;
	font-family:"Times New Roman";
	font-weight:bold;}
p.MsoHeader, li.MsoHeader, div.MsoHeader
	{mso-style-link:"Header Char";
	margin:0in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	tab-stops:center 3.25in right 6.5in;
	font-size:12.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";
	mso-bidi-font-family:"Times New Roman";}
p.MsoFooter, li.MsoFooter, div.MsoFooter
	{mso-style-link:"Footer Char";
	margin:0in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	tab-stops:center 3.25in right 6.5in;
	font-size:12.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";
	mso-bidi-font-family:"Times New Roman";}
p
	{mso-margin-top-alt:auto;
	margin-right:0in;
	mso-margin-bottom-alt:auto;
	margin-left:0in;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";
	mso-bidi-font-family:"Times New Roman";}
p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:.5in;
	margin-bottom:.0001pt;
	mso-add-space:auto;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";
	mso-bidi-font-family:"Times New Roman";}
p.MsoListParagraphCxSpFirst, li.MsoListParagraphCxSpFirst, div.MsoListParagraphCxSpFirst
	{mso-style-type:export-only;
	margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:.5in;
	margin-bottom:.0001pt;
	mso-add-space:auto;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";
	mso-bidi-font-family:"Times New Roman";}
p.MsoListParagraphCxSpMiddle, li.MsoListParagraphCxSpMiddle, div.MsoListParagraphCxSpMiddle
	{mso-style-type:export-only;
	margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:.5in;
	margin-bottom:.0001pt;
	mso-add-space:auto;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";
	mso-bidi-font-family:"Times New Roman";}
p.MsoListParagraphCxSpLast, li.MsoListParagraphCxSpLast, div.MsoListParagraphCxSpLast
	{mso-style-type:export-only;
	margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:.5in;
	margin-bottom:.0001pt;
	mso-add-space:auto;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";
	mso-bidi-font-family:"Times New Roman";}
span.Heading4Char
	{mso-style-name:"Heading 4 Char";
	mso-style-locked:yes;
	mso-style-link:"Heading 4";
	mso-ansi-font-size:12.0pt;
	mso-bidi-font-size:12.0pt;
	font-weight:bold;}
span.HeaderChar
	{mso-style-name:"Header Char";
	mso-style-locked:yes;
	mso-style-link:Header;
	mso-ansi-font-size:12.0pt;
	mso-bidi-font-size:12.0pt;}
span.FooterChar
	{mso-style-name:"Footer Char";
	mso-style-locked:yes;
	mso-style-link:Footer;
	mso-ansi-font-size:12.0pt;
	mso-bidi-font-size:12.0pt;}
 /* Page Definitions */
@page
	{mso-footnote-separator:url(":FAQ Edits_files:header.htm") fs;
	mso-footnote-continuation-separator:url(":FAQ Edits_files:header.htm") fcs;
	mso-endnote-separator:url(":FAQ Edits_files:header.htm") es;
	mso-endnote-continuation-separator:url(":FAQ Edits_files:header.htm") ecs;}
@page Section1
	{size:8.5in 11.0in;
	margin:1.0in 1.25in 1.0in 1.25in;
	mso-header-margin:.5in;
	mso-footer-margin:.5in;
	mso-header:url(":FAQ Edits_files:header.htm") h1;
	mso-paper-source:0;}
div.Section1
	{page:Section1;}
 /* List Definitions */
@list l0
	{mso-list-id:2014457144;
	mso-list-type:hybrid;
	mso-list-template-ids:456298588 1769898204 67698713 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
@list l0:level1
	{mso-level-start-at:2;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	margin-left:.25in;
	text-indent:-.25in;}
ol
	{margin-bottom:0in;}
ul
	{margin-bottom:0in;}
-->
</style>
<!--[if gte mso 10]>
<style>
 /* Style Definitions */
table.MsoNormalTable
	{mso-style-name:"Table Normal";
	mso-tstyle-rowband-size:0;
	mso-tstyle-colband-size:0;
	mso-style-noshow:yes;
	mso-style-parent:"";
	mso-padding-alt:0in 5.4pt 0in 5.4pt;
	mso-para-margin:0in;
	mso-para-margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	font-family:"Times New Roman";}
table.MsoTableGrid
	{mso-style-name:"Table Grid";
	mso-tstyle-rowband-size:0;
	mso-tstyle-colband-size:0;
	border:solid windowtext 1.0pt;
	mso-border-alt:solid windowtext .5pt;
	mso-padding-alt:0in 5.4pt 0in 5.4pt;
	mso-border-insideh:.5pt solid windowtext;
	mso-border-insidev:.5pt solid windowtext;
	mso-para-margin:0in;
	mso-para-margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	font-family:"Times New Roman";}
</style>
<![endif]--><!--[if gte mso 9]><xml>
 <o:shapedefaults v:ext="edit" spidmax="1027"/>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <o:shapelayout v:ext="edit">
  <o:idmap v:ext="edit" data="1"/>
 </o:shapelayout></xml><![endif]-->

<div class=Section1>

<p class=MsoNormal style='margin-top:.25in;mso-outline-level:4'><b>1. </b><b><span
style='font-family:Arial;mso-bidi-font-family:Arial'>How is RQN different from independent
grading services?<a name="_GoBack"></a><o:p></o:p></span></b></p>

<p class=MsoNormal style='margin-top:6.0pt;margin-right:0in;margin-bottom:0in;
margin-left:13.5pt;margin-bottom:.0001pt'><span style='font-family:Arial;
mso-bidi-font-family:Arial'>Independent grading services use technical grading
as a major consideration when assigning a grade. But technical grading is not a
major factor in a note's visual appearance. <o:p></o:p></span></p>

<p class=MsoNormal style='margin-top:6.0pt;margin-right:0in;margin-bottom:0in;
margin-left:13.5pt;margin-bottom:.0001pt'><span style='font-family:Arial;
mso-bidi-font-family:Arial'>For example, independent grading services count the
number of folds when grading circulated notes. For uncirculated notes, they
give the margins major consideration. RQN goes beyond technical, quantitative
elements and evaluates a note's overall attractiveness, which is usually called
&quot;eye appeal.&quot; See FAQ #2 for more details. <o:p></o:p></span></p>

<p class=MsoListParagraph style='margin-top:.25in;margin-right:0in;margin-bottom:
0in;margin-left:.25in;margin-bottom:.0001pt;text-indent:-.25in;mso-outline-level:
4;mso-list:l0 level1 lfo1'><![if !supportLists]><b><span style='font-family:
Arial;mso-fareast-font-family:Arial;mso-bidi-font-family:Arial'><span
style='mso-list:Ignore'>2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span></b><![endif]><b><span style='font-family:Arial;
mso-bidi-font-family:Arial'>What qualities or elements does RQN consider when
analyzing or reviewing a note? <o:p></o:p></span></b></p>

<p class=MsoNormal style='margin-top:6.0pt;margin-right:0in;margin-bottom:6.0pt;
margin-left:.25in'><span style='font-family:Arial;mso-bidi-font-family:Arial'>RQN
considers 8 qualities when analyzing a note. <o:p></o:p></span></p>

<table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0
 style='margin-left:23.4pt;border-collapse:collapse;border:none;mso-border-alt:
 solid windowtext .5pt;mso-yfti-tbllook:1184;mso-padding-alt:0in 5.4pt 0in 5.4pt;
 mso-border-insideh:.5pt solid windowtext;mso-border-insidev:.5pt solid windowtext'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td width=135 colspan=2 valign=top style='width:135.0pt;border:solid windowtext 1.0pt;
  mso-border-alt:solid windowtext .5pt;background:#4F81BD;mso-background-themecolor:
  accent1;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=center style='margin-top:3.0pt;margin-right:0in;
  margin-bottom:3.0pt;margin-left:0in;text-align:center'><b style='mso-bidi-font-weight:
  normal'><span style='font-family:Arial;mso-bidi-font-family:Arial;color:white'>Quality<o:p></o:p></span></b></p>
  </td>
  <td width=284 valign=top style='width:3.95in;border:solid windowtext 1.0pt;
  border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;background:#4F81BD;mso-background-themecolor:accent1;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-top:3.0pt;margin-right:0in;margin-bottom:
  3.0pt;margin-left:0in'><b style='mso-bidi-font-weight:normal'><span
  style='font-family:Arial;mso-bidi-font-family:Arial;color:white'>RQN looks
  for ~<o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:1'>
  <td width=32 style='width:31.5pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><b
  style='mso-bidi-font-weight:normal'><span style='font-family:Arial;
  mso-bidi-font-family:Arial'>1<o:p></o:p></span></b></p>
  </td>
  <td width=104 style='width:103.5pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><b
  style='mso-bidi-font-weight:normal'><span style='font-family:Arial;
  mso-bidi-font-family:Arial'>Paper<o:p></o:p></span></b></p>
  </td>
  <td width=284 valign=top style='width:3.95in;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-top:3.0pt;margin-right:0in;margin-bottom:
  3.0pt;margin-left:0in'><span style='font-family:Arial;mso-bidi-font-family:
  Arial'>Should be free of any distractions &#8211; any foxing or toning.<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:2'>
  <td width=32 style='width:31.5pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><b
  style='mso-bidi-font-weight:normal'><span style='font-family:Arial;
  mso-bidi-font-family:Arial'>2<o:p></o:p></span></b></p>
  </td>
  <td width=104 style='width:103.5pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><b
  style='mso-bidi-font-weight:normal'><span style='font-family:Arial;
  mso-bidi-font-family:Arial'>Color<o:p></o:p></span></b></p>
  </td>
  <td width=284 valign=top style='width:3.95in;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-top:3.0pt;margin-right:0in;margin-bottom:
  3.0pt;margin-left:0in'><span style='font-family:Arial;mso-bidi-font-family:
  Arial'>Must be vivid and eye catching.<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:3'>
  <td width=32 style='width:31.5pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><b
  style='mso-bidi-font-weight:normal'><span style='font-family:Arial;
  mso-bidi-font-family:Arial'>3<o:p></o:p></span></b></p>
  </td>
  <td width=104 style='width:103.5pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><b
  style='mso-bidi-font-weight:normal'><span style='font-family:Arial;
  mso-bidi-font-family:Arial'>Printing<o:p></o:p></span></b></p>
  </td>
  <td width=284 valign=top style='width:3.95in;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-top:3.0pt;margin-right:0in;margin-bottom:
  3.0pt;margin-left:0in'><span style='font-family:Arial;mso-bidi-font-family:
  Arial'>Should be strong, leaving signs of printing impressions.<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:4'>
  <td width=32 style='width:31.5pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><b
  style='mso-bidi-font-weight:normal'><span style='font-family:Arial;
  mso-bidi-font-family:Arial'>4<o:p></o:p></span></b></p>
  </td>
  <td width=104 style='width:103.5pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><b
  style='mso-bidi-font-weight:normal'><span style='font-family:Arial;
  mso-bidi-font-family:Arial'>Face margins<o:p></o:p></span></b></p>
  </td>
  <td width=284 valign=top style='width:3.95in;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-top:3.0pt;margin-right:0in;margin-bottom:
  3.0pt;margin-left:0in'><span style='font-family:Arial;mso-bidi-font-family:
  Arial'>Should be symmetrical and ample for the series.<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:5'>
  <td width=32 style='width:31.5pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><b
  style='mso-bidi-font-weight:normal'><span style='font-family:Arial;
  mso-bidi-font-family:Arial'>5<o:p></o:p></span></b></p>
  </td>
  <td width=104 style='width:103.5pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><b
  style='mso-bidi-font-weight:normal'><span style='font-family:Arial;
  mso-bidi-font-family:Arial'>Back margins<o:p></o:p></span></b></p>
  </td>
  <td width=284 valign=top style='width:3.95in;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-top:3.0pt;margin-right:0in;margin-bottom:
  3.0pt;margin-left:0in'><span style='font-family:Arial;mso-bidi-font-family:
  Arial'>The margins should be symmetrical and ample for the series.<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:6'>
  <td width=32 style='width:31.5pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><b
  style='mso-bidi-font-weight:normal'><span style='font-family:Arial;
  mso-bidi-font-family:Arial'>6<o:p></o:p></span></b></p>
  </td>
  <td width=104 style='width:103.5pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><b
  style='mso-bidi-font-weight:normal'><span style='font-family:Arial;
  mso-bidi-font-family:Arial'>Face centering<o:p></o:p></span></b></p>
  </td>
  <td width=284 valign=top style='width:3.95in;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-top:3.0pt;margin-right:0in;margin-bottom:
  3.0pt;margin-left:0in'><span style='font-family:Arial;mso-bidi-font-family:
  Arial'>The centering should be even or proportional within the margins.<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:7'>
  <td width=32 style='width:31.5pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><b
  style='mso-bidi-font-weight:normal'><span style='font-family:Arial;
  mso-bidi-font-family:Arial'>7<o:p></o:p></span></b></p>
  </td>
  <td width=104 style='width:103.5pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><b
  style='mso-bidi-font-weight:normal'><span style='font-family:Arial;
  mso-bidi-font-family:Arial'>Back centering<o:p></o:p></span></b></p>
  </td>
  <td width=284 valign=top style='width:3.95in;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-top:3.0pt;margin-right:0in;margin-bottom:
  3.0pt;margin-left:0in'><span style='font-family:Arial;mso-bidi-font-family:
  Arial'>The centering should be even or proportional within the margins.<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:8;mso-yfti-lastrow:yes'>
  <td width=32 style='width:31.5pt;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><b
  style='mso-bidi-font-weight:normal'><span style='font-family:Arial;
  mso-bidi-font-family:Arial'>8<o:p></o:p></span></b></p>
  </td>
  <td width=104 style='width:103.5pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><b
  style='mso-bidi-font-weight:normal'><span style='font-family:Arial;
  mso-bidi-font-family:Arial'>Registration<o:p></o:p></span></b></p>
  </td>
  <td width=284 valign=top style='width:3.95in;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-top:3.0pt;margin-right:0in;margin-bottom:
  3.0pt;margin-left:.3pt'><span style='font-family:Arial;mso-bidi-font-family:
  Arial'>The alignment between the face and back should be complimentary. <o:p></o:p></span></p>
  </td>
 </tr>
</table>

<p class=MsoNormal style='margin-top:.25in;mso-outline-level:4'><a
name=faq-7adc307c-2fc5-5020-9cf5-7e6aaebb1fb3></a><a
name=faq-ae89a6f4-4d57-56fa-8f2a-d5f1f314a05a></a><b>3. </b><b><span
style='font-family:Arial;mso-bidi-font-family:Arial'>Will notes be insured
while in RQN's possession?<o:p></o:p></span></b></p>

<p class=MsoNormal style='margin-top:6.0pt;margin-right:0in;margin-bottom:0in;
margin-left:13.5pt;margin-bottom:.0001pt'><span style='font-family:Arial;
mso-bidi-font-family:Arial'>Yes! While in the possession of RQN, notes are insured based on the declared dollar value the customer assigns when the notes are entered into the system.<o:p></o:p></span></p>



<p class=MsoNormal style='margin-top:.25in;mso-outline-level:4'><a
name=faq-7adc307c-2fc5-5020-9cf5-7e6aaebb1fb3></a><a
name=faq-ae89a6f4-4d57-56fa-8f2a-d5f1f314a05a></a><b>4. </b><b><span
style='font-family:Arial;mso-bidi-font-family:Arial'>Can a customer ask RQN to use FedEx to return their submission?<o:p></o:p></span></b></p>

<p class=MsoNormal style='margin-top:6.0pt;margin-right:0in;margin-bottom:0in;
margin-left:13.5pt;margin-bottom:.0001pt'><span style='font-family:Arial;
mso-bidi-font-family:Arial'>If Submitter is using their FedEx account number for return shipping from RQN, Submitter must be covered by their own insurance for the total amount of the submission. When RQN returns a submission using Submitters FedEx number, RQN will use the ship to and ship from addresses of the Submitter. With the exception of FedEx and pickup, all RQN returned submissions are mailed using USPS registered mail.<o:p></o:p></span></p>


<p class=MsoNormal style='margin-top:.25in;mso-outline-level:4'><a
name=faq-370c4eb3-4f64-5ade-9677-168731de6de9></a><b><span style='font-family:
Arial;mso-bidi-font-family:Arial'>5. Where will the RQN sticker be placed on
the holder?<o:p></o:p></span></b></p>

<p class=MsoNormal style='margin-top:6.0pt;margin-right:0in;margin-bottom:0in;
margin-left:13.5pt;margin-bottom:.0001pt'><span style='font-family:Arial;
mso-bidi-font-family:Arial'>If a note qualifies for a RQN sticker, it will be
placed on the label part of the holder.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-top:.25in;mso-outline-level:4'><a
name=faq-b4092ab9-99b9-5af0-a083-0ede3da2074d></a><b><span style='font-family:
Arial;mso-bidi-font-family:Arial'>6. Is the RQN sticker secure?<o:p></o:p></span></b></p>

<p class=MsoNormal style='margin-top:6.0pt;margin-right:0in;margin-bottom:0in;
margin-left:13.5pt;margin-bottom:.0001pt'><span style='font-family:Arial;
mso-bidi-font-family:Arial'>The RQN sticker is a high quality tamper-proof
hologram. If removed, it will void itself.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-top:12.0pt;mso-outline-level:4'><a
name=faq-104a4413-ad6a-51bc-b369-79d405b806ad></a><b><span style='font-family:
Arial;mso-bidi-font-family:Arial'>7. Can I check to verify whether a note has
received a RQN sticker?<o:p></o:p></span></b></p>

<p class=MsoNormal style='margin-top:6.0pt;margin-right:0in;margin-bottom:0in;
margin-left:13.5pt;margin-bottom:.0001pt;tab-stops:-27.0pt'><span
style='font-family:Arial;mso-bidi-font-family:Arial'>Yes. It's easy for anyone,
at any time, to go to our Web site to verify whether a note has earned a RQN
sticker. Click &quot;Stickered Note Search.&quot; <o:p></o:p></span></p>

<p class=MsoNormal style='margin-left:13.5pt;tab-stops:-27.0pt'><span
style='font-family:Arial;mso-bidi-font-family:Arial'><o:p>&nbsp;</o:p></span></p>

</div>
</fieldset>

</div>