<style>
.bar-empty {
	width:0%;
	transition: width 10s;
	-moz-transition: width 10s; /* Firefox 4 */
	-webkit-transition: width 10s; /* Safari and Chrome */
	-o-transition: width 10s; /* Opera */
}

</style>

<meta http-equiv="REFRESH" content="2;url=<?php echo $url;?>"/>
<div class="container" style="margin-top:5px;">
	<div class="alert alert-success">
		Your order is being processed
		<div class="progress progress-striped active">
		  <div class="bar bar-empty"></div>
		</div>
	</div>
	<script type="text/javascript">
		$(function(){
			$('.bar').css('width', '100%');
		});
	</script>
</div>