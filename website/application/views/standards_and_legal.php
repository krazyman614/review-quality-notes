<div class="header_row">
	<div class="header_row_inner">
	Standards and Legal
	</div>
</div>

<div style="padding: 40px 70px 70px 80px;">

<style>
<!--
/* Style Definitions */
p.MsoNormal, li.MsoNormal, div.MsoNormal
{mso-style-parent:"";
margin:0in;
margin-bottom:.0001pt;
mso-pagination:widow-orphan;
}
@page Section1
{size:8.5in 11.0in;
margin:.7in .8in .7in .8in;
mso-header-margin:.5in;
mso-footer-margin:.5in;
mso-paper-source:0;
}
div.Section1
{page:Section1;
}
-->
</style>
<!--[if gte mso 10]>
<style>
/* Style Definitions */
table.MsoNormalTable
{mso-style-name:"Table Normal";
mso-tstyle-rowband-size:0;
mso-tstyle-colband-size:0;
mso-style-noshow:yes;
mso-style-parent:"";
mso-padding-alt:0in 5.4pt 0in 5.4pt;
mso-para-margin-top:0in;
mso-para-margin-right:0in;
mso-para-margin-bottom:10.0pt;
mso-para-margin-left:0in;
line-height:115%;
mso-pagination:widow-orphan;
}
</style>
<![endif]--><!--[if gte mso 9]><xml>
<o:shapedefaults v:ext="edit" spidmax="1027"/>
</xml><![endif]--><!--[if gte mso 9]><xml>
<o:shapelayout v:ext="edit">
<o:idmap v:ext="edit" data="1"/>
</o:shapelayout></xml><![endif]-->


<div class=Section1>

<p class=MsoNormal align=center style='text-align:center'><span
style="mso-spacerun: yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>UNITED STATES
POSTAL SERVICE INSURANCE.</p>

<p class=MsoNormal>The USPS does provide insurance for some international
shipments, but not to all countries.<span style="mso-spacerun: yes">&nbsp;
</span>Available insurance varies by country and in some cases coverage is
limited or not available.<span style="mso-spacerun: yes">&nbsp; </span>Review
Quality Notes (RQN) will purchase USPS Insurance on return shipments based upon
the Submitter's selection on the Submission Screen, or the maximum amount
provided by the USPS.<span style="mso-spacerun: yes">&nbsp; </span>Insurance
within the US is limited to $25,000.00, unless the Submitter makes prior
arrangements with RQN.<span style="mso-spacerun: yes">&nbsp; </span>RQN assumes
no liability in the event of loss or damage of shipments and no insurance of
any kind is provided for an amount greater than the maximum amount provided by
the USPS.<span style="mso-spacerun: yes">&nbsp; </span>Please contact the USPS
with specific questions regarding the availability of insurance on shipments. </p>

<p class=MsoNormal align=center style='text-align:center'><o:p>&nbsp;</o:p></p>

<p class=MsoNormal align=center style='text-align:center'><o:p>&nbsp;</o:p></p>

<p class=MsoNormal align=center style='text-align:center'><o:p>&nbsp;</o:p></p>

<p class=MsoNormal align=center style='text-align:center'><b style='mso-bidi-font-weight:
normal'>TERMS AND CONDITIONS.<o:p></o:p></b></p>

<p class=MsoNormal>1.<span style="mso-spacerun: yes">&nbsp; </span>RQN (Review
		Quality Notes) will endeavor to provide the services requested in good faith in
		the time frames specified.<span style="mso-spacerun: yes">&nbsp; </span>RQN
		cannot be held accountable for shipping services provided by USPS or any other
		shipper.<span style="mso-spacerun: yes">&nbsp; </span>Shipping disputes are to
		be taken directly to the shipper. </p>

		<p class=MsoNormal><o:p>&nbsp;</o:p></p>

		<p class=MsoNormal>2.<span style="mso-spacerun: yes">&nbsp; </span>The
		Submitter (Customer) shall complete all the information required on the
		Submission Screens, and shall package all items securely and ship items in
		accordance with instructions on the Submission Screens.<span
		style="mso-spacerun: yes">&nbsp; </span>Submitter represents and warrants to
		RQN that the insured value set forth on the Submission Screens<span
		style='color:red'> </span>represents a good faith belief as to the market value
		of the item(s).<span style="mso-spacerun: yes">&nbsp; </span></p>

		<p class=MsoNormal><o:p>&nbsp;</o:p></p>

		<p class=MsoNormal>3.<span style="mso-spacerun: yes">&nbsp; </span>RQN has the
		right in its sole discretion to reject any items submitted for service.<span
		style="mso-spacerun: yes">&nbsp; </span>Because RQN must still receive, track,
		and examine each rejected item, fees for services shall not be refunded for any
		rejected items.<span style="mso-spacerun: yes">&nbsp; </span></p>

		<p class=MsoNormal><o:p>&nbsp;</o:p></p>

		<p class=MsoNormal>4.<span style="mso-spacerun: yes">&nbsp; </span>RQN will use
		reasonable care with respect to items submitted to it for services.<span
		style="mso-spacerun: yes">&nbsp; </span>In the event RQN determines that an
		item was lost or damaged in RQN's possession, RQN will compensate the Submitter
		based upon RQN's good faith determination of the fair market value of the item,
		in respect of current market information.<span style="mso-spacerun: yes">&nbsp;
</span>The amount of compensation will not necessarily be based upon, but in no
event will exceed the Submitter's stated insured value of the item, stated in
the Submission Screen.</p>

<p class=MsoNormal><span style="mso-spacerun: yes">&nbsp;</span></p>

<p class=MsoNormal>5.<span style="mso-spacerun: yes">&nbsp; </span>The Submitter
agrees to pay RQN for all services and shipping fees at the time of
submission.<span style="mso-spacerun: yes">&nbsp; </span>Submitter agrees that
RQN may charge all fees to the credit or debit card upon receipt of the
Submission Screen.<span style="mso-spacerun: yes">&nbsp; </span>Submitter
stipulates they are the sole owner of submitted items and grants RQN a security
interest in all items submitted to secure payment of any fees.</p>

<p class=MsoNormal><span style="mso-spacerun: yes">&nbsp;</span></p>

<p class=MsoNormal>5a.<span style="mso-spacerun: yes">&nbsp; </span>If Submitter is using their FedEx account number for return shipping from RQN, Submitter must be covered by their own insurance for the total amount of the submission. When RQN returns a submission using Submitters FedEx number, RQN will use the ship to and ship from addresses of the Submitter. With the exception of FedEx and pickup, all RQN returned submissions are mailed using USPS registered mail.</p>


<p class=MsoNormal><span style="mso-spacerun: yes">&nbsp; </span></p>

<p class=MsoNormal>6.<span style="mso-spacerun: yes">&nbsp; </span>The
Submitter must inspect all items immediately upon receipt from RQN.<span
style="mso-spacerun: yes">&nbsp; </span>RQN will have no liability for any
damage or errors unless reported to RQN, by e-mail, within 5 days from
Submitter's receipt of the item(s). </p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal>7.<span style="mso-spacerun: yes">&nbsp; </span>Submitter
agrees to return to RQN any item bearing a clerical type error for which
service has been provided.<span style="mso-spacerun: yes">&nbsp; </span>RQN
will correct the clerical error and return the corrected item to the Submitter.<span
style="mso-spacerun: yes">&nbsp; </span>Submitter agrees to indemnify, defend
and hold RQN, any of its affiliates, and all of its and their respective
employees, officers, directors, and agents, harmless from and against all
claims, liabilities and expenses relating to or arising directly or indirectly
from Submitters failure to comply with any part of RQN's <i>Terms and
Conditions</i>.<span style="mso-spacerun: yes">&nbsp; </span></p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal>8.<span style="mso-spacerun: yes">&nbsp; </span>Except for
any express warranties set forth in these <i>Terms and Conditions</i>, RQN disclaims
any and all warranties, express or implied, regarding RQN and or the services,
including but not limited to the warranties of merchantability and fitness for
a particular purpose.<span style="mso-spacerun: yes">&nbsp; </span>The maximum
aggregate liability that RQN shall have to the Submitter arising from any cause,
act, omission, or other circumstance, shall in no event exceed the service fees
or fees paid by the Submitter for the services ordered pursuant to this
Submission Screen.<span style="mso-spacerun: yes">&nbsp; </span>In no event
shall RQN or any of its affiliates, or any of or their respective employees,
officers, directors, or agents be liable to Submitter, or any other party, for
any indirect, incidental special, consequential or exemplary damages even if
advised of the possibility of such damages and in no event shall the aggregate
liability of RQN, its affiliates, and any of or their respective employees,
officers, directors, or agents exceed the fees paid or payable to RQN for the
services. </p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal>9.<span style="mso-spacerun: yes">&nbsp;&nbsp; </span>The
Submitter agrees that RQN has a security interest in items submitted for
services including, but not limited to, shipping fees, and if such fees are not
received in good standing, after 90 days, RQN may at its discretion dispose of
such items in a open market, to recover fees due including related
expenses.<span style="mso-spacerun: yes">&nbsp; </span>Any amounts recovered in
excess of fees due and related expenses shall be returned to the
Submitter.<span style="mso-spacerun: yes">&nbsp; </span></p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal>10.<span style="mso-spacerun: yes">&nbsp; </span>RQN
compiles data regarding, and may make digital images of, items submitted for
services.<span style="mso-spacerun: yes">&nbsp; </span>In consideration for the
performance of services by RQN for services pursuant to this Submission Screen,
Submitter hereby authorizes RQN to compile such data, including images, and
agrees that RQN shall have an irrevocable non-exclusive, perpetual, unlimited,
royalty free and license to use and commercialize such data and images for any
purpose.</p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal>11.<span style="mso-spacerun: yes">&nbsp; </span>The laws of
the State of Florida without regard to its choice of law principles shall apply
to transactions and or disputes between RQN and any Submitter.<span
style="mso-spacerun: yes">&nbsp; </span>Each Submitter agrees to the exclusive
jurisdiction and venue for any dispute of the state and or federal courts
located in or serving Pinellas County, Florida, and shall not challenge such
jurisdiction or venue, and accept service by certified or registered mail.<span
style="mso-spacerun: yes">&nbsp;&nbsp; </span></p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal>12.<span style="mso-spacerun: yes">&nbsp; </span>These
<i>Terms and Conditions</i>, constitute the entire agreement of RQN and the
Submitter regarding, and supersede all prior agreements and understanding,
written or oral, between or among all parties relating to the subject matter
hereof. </p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal><span style="mso-spacerun: yes">&nbsp;</span></p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal align=center style='text-align:center'><b style='mso-bidi-font-weight:
normal'>CREDIT &amp; DEBIT CARD CHARGES AND RELEASE.<o:p></o:p></b></p>

<p class=MsoNormal>The Submitter whose name is on the Submission Form
authorizes RQN to charge the account for the total amount due on the Submission
Form.<span style="mso-spacerun: yes">&nbsp; </span>The Submitter agrees to pay
the full amount of the requested service and shipping fees to be charged to the
card number provided, regardless of the services provided.<span
style="mso-spacerun: yes">&nbsp; </span>In the event the charges to the credit
or debit card are declined the submitter authorizes RQN to hold the submitted
material until all charges, including any additional fess are satisfied in
accordance with item # 9, <i>Terms and Conditions</i>. The Submitter agrees that in
the event of any dispute in regard to the services rendered, they will notify
RQN by e-mail with 14 days of receipt of returned items.<span
style="mso-spacerun: yes">&nbsp; </span>The Submitter agrees that RQN reserves
the right to post additional chares to any card account if extra services are
required to fulfill the services requested.<span style="mso-spacerun:
yes">&nbsp; </span>The Submitter agrees that they are the unrestricted owner of
any items submitted to RQN.<span style="mso-spacerun: yes">&nbsp; </span></p>
<br/><br/>
<center><b>
DEALER BUY/SELL PAGE DISCLAIMER</b></center>
<br/>
The information found on the Buy/Sell web page is generally from independent dealers. Review Quality Notes, Inc. (RQN) has no affiliate or financial relationship other than some dealers may be customers of RQN services.(*) RQN receives no financial benefits from any business transaction that may take place from this information.  RQN makes no warrantees nor assumes any liability regarding dealer postings  including errors on the Buy/Sell portion of the web site. If the contact email information is directed to info@rqnotes.com, we forward all contact requests to the proper dealer.
<br/><br/>*There may be occasions when an RQN employee will post a Buy/Sell transaction.

</div>

</div>