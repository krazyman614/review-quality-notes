<style type="text/css">
	#learn-more {padding:0 100px 0 50px;}
	#learn-more, #learn-more p {font-style:italic; font-size:1.5em;}
	#learn-more-header, #learn-more-footer {text-align:center;}
</style>

<div id="learn-more">
	<div id="learn-more-header" class="center">
		<p>
			<h3 id="learn-more-title">REVIEW QUALITY NOTES INC.</h3>
			<span style="font-size:1.7em;">P.O. Box 571150<br />
			Miami, FL 33257-1150</span>
		</p><br /> <!-- Extra line break before image -->
		<img src="/resources/img/logo-special.png" title="Review Quality Notes" alt="Review Quality Notes" />
		<p>
			<strong>FOR A QUALITY NOTE</strong>,<br />
			within an assigned grade,<br />
			<strong>LOOK FOR THE RQN VERIFICATION STICKER</strong>
		</p>
	</div>
	<br /> <!-- Extra line-break between header and content -->
	<div id="learn-more-content">
		<p>
		In many fields, including collectible paper money, there is a superior few that represents the finest of the fine, the best of the best.  Within an assigned grade, Review Quality Notes (RQN) identifies QUALITY notes.  We are not a grading service.  We strongly support those independent grading services which provide an important service to our industry.   Independent currency grading services use criteria to assign grades to notes that reflect an impartial assessment that represents the condition of a particular note.   Those criteria however must be mainly objective for the grading services to justify the assigned grade.  Anyone looking at notes realizes there can be vast differences in the appearances of notes with the same assigned grade. When the grading services assign a grade to a “circulated” note, a primary factor is the number of folds, not necessarily the margins or centering.  Yet margins, centering, and eye appeal are often considered part of a desirable note.
		</p><p>
		There are numerous factors that affect the condition and appearance of any note.  Note production is a manufacturing process and as such variations often occur.  For example printing plates are known to wear, and are replaced when the wear is excessive.  In the past, the Bureau of Engraving and Printing at times re-engraved portions of printing plates when wear was noted.  Different printing presses are used and can have differences such as pressure settings.  Inks are made in batches which can have differences such as the colors of Treasury Seals in the 1928 and 1934 Federal Reserve Notes. Also a printing press may be low on ink affecting the strength of the colors.  There can also be differences in the type and manufacture of paper used to print notes.  In addition to the above, the distribution of a note from the completion of manufacture to its actual issuance involves much handling
		</p><p>
		RQN will analyze a graded note within its assigned grade to determine if it has superior qualities.  We will look at the paper for any distractions, such as foxing or toning.  The notes colors should be vivid and eye catching.  The printing should be strong and clear with signs of printing impressions.  The face and back margins should be symmetrical and ample for the series. The centering should be even or proportional within the margins, and the registration, the alignment between the face and back, should be complimentary.  Those are factors RQN will look for as it analyzes each note.  
		</p>
	</div>
	<div id="learn-more-footer">
		<p>
			<strong>FOR A QUALITY NOTE</strong>,<br />
			within an assigned grade,<br />
			<strong>LOOK FOR THE RQN VERIFICATION STICKER.</strong>
		</p>
	</div>
</div>