<div class="container hdr-contents" style='background-color:#e3d9c7'>
	<div id="hdr" >
		<div id="logo"><img src="/resources/img/logo.png"/></div>
		<div id="quality"><img src="/resources/img/quality-bkg.png"/></div>
		<div id="logo-special" <?php if($current_user != false){?>style="right:72px;"<?php }?>><img src="/resources/img/logo-special.png"/></div>
		<?php if($current_user == false){?>
		<div id="login">
			<form method="post" class="form-horizontal" action="/login/login_user">
				<div class="login-label">SIGN IN / REGISTER</div>
				<div class="login-content">
					<input type="text" name="email" placeholder="Email"/>
				</div>
				<div class="login-content">
					<input type="password" name="password" placeholder="Password"/>
				</div>
				<div class="login-content">
					<button type="submit" class="btn btn-small login">SIGN IN</button>
					<a href="/users" class="btn btn-small login">REGISTER A NEW ACCOUNT</a>
				</div>
				<div class="login-content">
					<a href="/users/forgot_password" class="forgot-password">Forgot Your Password?</a>
				</div>
			</form>
		</div>
		<?php }?>
	</div>
	<div id="hdr-bar">
	
	</div>
</div>