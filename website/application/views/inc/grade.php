 <?php 
 $grades = array(array('id' => 0, 'name' => 'Other'), array('id' => 50, 'name' => 'Satisfactory'), array('id' => 100, 'name' => 'Quality'));
 ?>
   								<div class="span11">
   									<table  class="table table-bordered">
   									<tr>
   										<th>&nbsp;</th>
   										<th>Paper</th>
   										<th>Color</th>
   										<th>Printing</th>
   										<th>Face Margins</th>
   										<th>Face Centering</th>
   										<th>Back Margins</th>
   										<th>Back Centering</th>
   										<th>Registration</th>
   									</tr>
   									<?php foreach($grades as $grade){?>
   									<tr valign="top">
   										<td><strong><?php echo $grade['name'];?></strong></td>
   										<td><input type="checkbox"<?php echo $grade['id'] == $paper ? ' checked' : '';?> disabled/></td>
   										<td><input type="checkbox"<?php echo $grade['id'] == $color ? ' checked' : '';?> disabled/></td>
   										<td><input type="checkbox"<?php echo $grade['id'] == $printing ? ' checked' : '';?> disabled/></td>
   										<td><input type="checkbox"<?php echo $grade['id'] == $face_margins ? ' checked' : '';?> disabled/></td>
   										<td><input type="checkbox"<?php echo $grade['id'] == $face_centering ? ' checked' : '';?> disabled/></td>
   										<td><input type="checkbox"<?php echo $grade['id'] == $back_margins ? ' checked' : '';?> disabled/></td>
   										<td><input type="checkbox"<?php echo $grade['id'] == $back_centering ? ' checked' : '';?> disabled/></td>
   										<td><input type="checkbox"<?php echo $grade['id'] == $registration ? ' checked' : '';?> disabled/></td>
   									</tr>
   									<?php }?>
   									<tr>
   										<td><strong>Comments</strong>
   										<td colspan="8"><?php echo $comments;?></td>
   									</tr>
   									</table>
   								</div>