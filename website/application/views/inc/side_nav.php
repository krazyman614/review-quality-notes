<div class="side-nav-container">
	<div class="address">
		RQN INC.<br/>
		P.O. Box 571150<br/>
		Miami, FL 33257-1150<br/>
		<a href="mailto:info@rqnotes.com">INFO@RQNOTES.COM</a>
	</div>
	<div><img src="/resources/img/side-nav-bar.png"/></div>
	<div><a href="/">HOME</a></div>
	<div><img src="/resources/img/side-nav-bar.png"/></div>
	<div><a href="/users"><?php if($current_user === false){?>CREATE<?php } else {?>MANAGE<?php }?> ACCOUNT</a></div>
	<div><img src="/resources/img/side-nav-bar.png"/></div>
	<div><a href="/submissions">CUSTOMER ORDER HISTORY</a></div>
	<div><img src="/resources/img/side-nav-bar.png"/></div>
	<div><a href="/pricing">PRICING</a></div>
	<div><img src="/resources/img/side-nav-bar.png"/></div>
	<div><a href="/home/standards_and_legal">STANDARDS &amp; LEGAL</a></div>
	<div><img src="/resources/img/side-nav-bar.png"/></div>	
	<div><a href="/submissions/new_submission">NOTE ENTRY</a></div>
	<div><img src="/resources/img/side-nav-bar.png"/></div>	
	<div><a href="/sticker">STICKERED NOTE SEARCH</a></div>
	<div><img src="/resources/img/side-nav-bar.png"/></div>	
	<div><a href="/accepted_notes">ACCEPTED NOTES</a></div>
	<div><img src="/resources/img/side-nav-bar.png"/></div>	
	<div><a href="/home/directory">DEALER DIRECTORY</a></div>
	<div><img src="/resources/img/side-nav-bar.png"/></div>	
	<div><a href="/bid_asks">DEALER BID ASK</a></div>
	<div><img src="/resources/img/side-nav-bar.png"/></div>	
	<?php /*
	<div><a href="/tour">TAKE A TOUR</a></div>
	<div><img src="/resources/img/side-nav-bar.png"/></div>	
	*/?>
	<div><a href="/faqs">FAQ</a></div>
	<div><img src="/resources/img/side-nav-bar.png"/></div>
	<div><a href="/blog">BLOG</a></div>
	<div><img src="/resources/img/side-nav-bar.png"></div>	
	<?php if($current_user !== false){?>
		<div><a href="/login/logout">LOGOUT</a></div>
		<div><img src="/resources/img/side-nav-bar.png"/></div>	
	<?php }?>
</div>
<div class="side-nav-submission-block">
	<div class="small">CREATE A</div>
	<div class="large">SUBMISSION</div>
	<div>
		<a href="/submissions/new_submission"><img src="/resources/img/submission-btn-nav.png" border="0"/></a>
	</div>
</div>
<div class="side-nav-seal">
	<img src="/resources/img/seal.png"/>
</div>