<div class="header_row">
	<div class="header_row_inner">
	Login
	</div>
</div>

<div style="padding: 40px 70px 70px 80px;">

<script>
	$(function(){
		<?php if(isset($creation_success_alert) && $creation_success_alert) { ?>
			alert("Account creation successful!");
		<?php } ?>
	});
</script>
<fieldset>

	<form method="post" action="/login/login_user">
		<div id="display-error" class="alert alert-error"<?php if(!isset($errors)){?> style="display:none;"<?php }?>>
	    	Please correct the following errors:
	    	<ul id="errors-list">
	    	<?php foreach($errors as $key=>$value){?>
	    		<li><?php echo $value;?></li>
	    	<?php }?>
	    	</ul>
	    </div>
		<div class="control-group<?php if(isset($errors['email'])){ echo ' error';}?>">
			<label class="control-label" for="inputEmail">Email</label>
			<div class="controls">
				<input type="text" name="email" placeholder="Email" value="<?php if(isset($email)){ echo $email; }?>" autofocus/>
			</div>
		</div>
		<div class="control-group<?php if(isset($errors['email'])){ echo ' error';}?>">
			<label class="control-label" for="inputPassword">Password</label>
			<div class="controls">
				<input type="password" name="password" id="inputPassword" placeholder="Password" value="<?php if(isset($password)){ echo $password; }?>"/>
			</div>
		</div>
		<div class="control-group">
			<label class="checkbox"><input type="checkbox"/>Remember me</label>
		</div>
		<div class="control-group">
			<div class="controls">
				<button type="submit" class="btn btn-primary">Sign in</button>
				<a href="/users" class="btn">Register a New  Account</a>
			</div>
		</div>
		<div class="control-group">
			<a href="/users/forgot_password">Forgot Your Password?</a>
		</div>
	</form>
</fieldset>

</div>