<div>

<div class="header_row">
	<div class="header_row_inner">
	Pricing
	</div>
</div>

<div style="padding:60px 60px 60px 78px;">

	<?php if(isset($service_levels)){?>
		<table class="table table-bordered">
			<thead>
				<tr>
	    			<th>Service Level</th>
	    			<th>&nbsp;</th>
	    			<th>Per note</th>
	    			<th>Time to Process</th>
	    		</tr>
    		</thead>
    		<?php foreach($service_levels as $service_level){?>
   			<tr>
 				<td><?php echo $service_level['level'];?></td>
				<td><?php echo $service_level['title'];?></td>
				<td><?php echo $service_level['per_note'] > 0 ? '$' . money_format('%i',$service_level['per_note']) : '' ;?></td>
				<td><?php echo $service_level['time_to_process'];?></td>
    		</tr>		    			
   		<?php }?>
   		</table>
   	<?php } else print "why";?>
</div>
</div>