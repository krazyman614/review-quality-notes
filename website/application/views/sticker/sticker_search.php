
<?php if(false){?>

<div class="header_row">
	<div class="header_row_inner">
	Sticker Search Results
	</div>
</div>

<div style="padding: 40px 70px 70px 70px;">

	<fieldset>
		<?php
		if($note !== false && $note['stickered'] > 0){?>
			<p style="font-size:1.5em;">This note has a RQN sticker and the serial number of the sticker is <strong><?php  echo $note['sticker_serial_number'];?></strong></p>
		<?php } else {?>
			<p>The note information you entered is not a RQN stickered note</p>	
		<?php }?>
		<br/>
		<div>
			<a href="/sticker" class="btn">Search Again</a>
		</div>
	</fieldset>
</div>
<?php } elseif (isset($note)) {?>
<div class="header_row">
	<div class="header_row_inner">
	Sticker Search Results
	</div>
</div>

<div style="padding: 40px 70px 70px 70px;">

	<fieldset>
		<?php
		if ($note !== false && $note['stickered'] > 0){?>
	
			<table id="order-form" class="table table-bordered">
    			<thead>
	    			<tr>
	    				<th>Sticker</th>
	    				<th><?php echo get_column_name('type', @$submission['region_id']);?></th>
	    				<th>Denomination</th>
	    				<th><?php echo get_column_name('catalog_number', @$submission['region_id']);?></th>
	    				<th><?php echo get_column_name('pp', @$submission['region_id']);?></th>
	    				<th>Serial Number</th>
	    				<th>Grading Service</th>
	    				<th>Bar Code</th>
	    			</tr>
    			</thead>
    			<tr>
	    			<td><?php echo get_stickered($note);?></td>
	    			<td><?php echo get_note_type($note, @$submission['region_id']);?></td>
    				<td<?php echo check_history($note, 'denomination');?>>$<?php echo money_format('%i', $note['denomination']);?></td>
    				<td<?php echo check_history($note, 'catalog_number');?>><?php echo $note['catalog_number'];?></td>
    				<td<?php echo check_history($note, 'pp');?>><?php echo $note['pp'];?></td>
    				<td<?php echo check_history($note, 'serial_number');?>><?php echo $note['serial_number'];?></td>
    				<td<?php echo check_history($note, 'grading_service_id');?>><?php echo $note['grading_service']['name'];?></td>
    				<td<?php echo check_history($note, 'barcode');?>><?php echo $note['barcode'];?></td>
   				</tr>
	   			<?php if( $note['status_id'] >= 1000){?>
	   				<tr>
	   					<td colspan="8">
	   						<?php if(isset($note['grades']) && count($note['grades']) > 0){?>
	   						<div class="container-fluid">
	   							<h4>Reviews</h4>
	   							<?php foreach($note['grades'] as $grade){
	   								if($note['status_id'] == 1000){
	   									$this->load->view('inc/grade', $grade);
	   								}
	   								else if($grade['is_final'] >= 1){
	   									$this->load->view('inc/grade', $grade);
	   								}?>
	   							<?php }?>
	   	  					</div>
	   	  					<?php }?>
	   	  				</td>
	   	  			</tr>
	   	  			<?php }?>
   				</table>
		<?php } else {?>
			<p>The note information you entered is not a RQN stickered note</p>	
		<?php }?>
		<br/>
		<div>
			<a href="/sticker" class="btn">Search Again</a>
		</div>
	</fieldset>
</div>
<?php }else {?>

<div class="header_row">
	<div class="header_row_inner">
	Sticker Search
	</div>
</div>

<div style="padding: 40px 70px 70px 70px;">

<fieldset>
	<div>
		<form method="post" id="search-form" action="/sticker/search_results">
			<table>
				<tr>
					<td style="text-align:center;"><h5>CGA</h5></td>
					<td style="text-align:center;"><h5>CGC</h5></td>
				</tr>
				<tr valign="top">
					<td style="text-align:center;"><img src="/resources/img/cga.png" /></td>
					<td style="text-align:center;"><img src="/resources/img/cgc.png" /></td>
				</tr>
				<tr>
					<td style="text-align:center;"><h5>PCGS</h5></td>
					<td style="text-align:center;"><h5>PMG</h5></td>
				</tr>
				<tr valign="top">
					<td style="text-align:center;"><img src="/resources/img/pcgs.png" /></td>
					<td style="text-align:center;"><img src="/resources/img/pmg.png" /></td>
				</tr>
			</table>
			<h4>For the above grading companies you can enter the number noted.</h4>
			<label>Barcode Number (No dashes)</label>
    		<input type="text" name="barcode" placeholder="Barcode" <?php if(isset($opts['barcode'])){ echo ' value="' . $opts['barcode'] . '"';}?>/>
    		<br/>
    		<button type="submit" class="btn">Search</button>
			<br/>
			<h5>OR</h5>
			<h4>For US Notes enter the FR Number and Serial Number</h4>
		 	<label>FR Number/Catalog Number</label>
    		<input type="text" name="catalog_number" placeholder="Catalog Number" <?php if(isset($opts['catalog_number'])){ echo ' value="' . $opts['catalog_number'] . '"';}?>/>
		 	<label>Serial Number</label>
    		<input type="text" name="serial_number" placeholder="Serial Number"<?php if(isset($opts['serial_number'])){ echo ' value="' . $opts['serial_number'] . '"';}?>/>
    		<br/>
    		<button type="submit" class="btn">Search</button>
  		</form>
	</div>
</fieldset>
</div>
<?php }?>