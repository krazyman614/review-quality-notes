<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<title>Review Quality Notes</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link href="/resources/css/default.css" rel="stylesheet" media="screen" />
	<link href="/resources/css/bootstrap.css" rel="stylesheet" media="screen" />
	<link href="/resources/css/global.css" rel="stylesheet" media="screen" />
	<?= $_scripts ?>
	
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	<script type="text/javascript" src="/resources/js/bootstrap.min.js"></script>
	<?= $_styles ?>
	<script type="text/javascript">
		$(function(){
		});
	</script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-98624187-1', 'auto');
        ga('send', 'pageview');
    </script></head>
<body>
	<?php print $header ?>
	<div class="container contents" style="height:100% !important">
		<table width='100%' height="100%"><tr><td style='width:280px;vertical-align: top;height:100% !important;' >
				<div class="side-nav" style="height:inherit;height:100% !important;">
					<?php print $side_nav?>
				</div>
			</td><td style='vertical-align: top;'>
			<div style="padding-left:0px;padding-right:00px;">
				<?php print $content;?>
			</div>
			</td>
		</tr>
		</table>
	</div>
	<?php print $footer ?>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-50248921-1', 'rqnotes.com');
  ga('send', 'pageview');

</script>
	
</body>
</html>
