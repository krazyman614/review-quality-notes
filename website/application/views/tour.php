<div class="header_row">
	<div class="header_row_inner">
	Take a Tour
	</div>
</div>

<div style="padding: 40px 70px 70px 70px;">

<fieldset>

	<div style="text-align:center; width:984px; margin-left:auto; margin-right:auto;">
		<h2>Please Click on a section of the page below to learn more about that feature!</h2>
		<img src="/resources/img/Branching_Graphic.gif" usemap="#links" border="0" width="984" height="773" alt="" />
		<map name="links">

			<area shape="rect" coords="45,346,283,384" href="javascript:loadVideo('tJgZcWjdhVc');" alt="Home" title="Home"    />
			<area shape="rect" coords="44,383,282,417" href="javascript:loadVideo('sQmUVGJ7LBM');" alt="Create Account" title="Create Account"    />
			<area shape="rect" coords="45,418,283,452" href="javascript:loadVideo('PfZY50zFnBw');" alt="Customer Review Submissions" title="Customer Review Submissions"    />
			<area shape="rect" coords="46,451,284,485" href="javascript:loadVideo('hKFhpKfNLoQ');" alt="Pricing" title="Pricing"    />
			<area shape="rect" coords="45,484,283,522" href="javascript:loadVideo('w9ZFNOKvySY');" alt="Standards and Legal" title="Standards and Legal"    />
			<area shape="rect" coords="45,523,283,559" href="javascript:loadVideo('VGvjqUTnHZ0');" alt="Note Entry" title="Note Entry"    />
			<area shape="rect" coords="45,559,283,595" href="javascript:loadVideo('GZlSluR44pk');" alt="Stickered Note Search" title="Stickered Note Search"    />
			<area shape="rect" coords="44,594,282,631" href="javascript:loadVideo('7GsAo5supbA');" alt="Accepted Notes" title="Accepted Notes"    />
			<area shape="rect" coords="45,631,283,668" href="javascript:loadVideo('tpzwp9MwKE0');" alt="Take a Tour" title="Take a Tour"    />
			<area shape="rect" coords="45,667,283,704" href="javascript:loadVideo('4iVl1cWicb4');" alt="FAQ" title="FAQ"    />
			<area shape="rect" coords="45,705,283,742" href="javascript:loadVideo('5GktI8Iaikw');" alt="Logout" title="Logout"    />
			<area shape="rect" coords="465,275,715,506" href="javascript:stop();" alt="Stop Tour" title="Stop Tour"    />

		</map>
		<br/>
		<h2>Or click a link below to learn more about the feature selected. </h2>
		<br/>
		<!-- Image map text links - Start - If you do not wish to have text links under your image map, you can move or delete this DIV -->
		<div style="text-align:center; font-size:12px; font-family:verdana; margin-left:auto; margin-right:auto; width:984px;">
			<a style="text-decoration:none; color:black; font-size:12px; font-family:verdana;" href="javascript:loadVideo('tJgZcWjdhVc');" title="Introduction">Home</a>
			 | 	<a style="text-decoration:none; color:black; font-size:12px; font-family:verdana;" href="javascript:loadVideo('sQmUVGJ7LBM');" title="Create Account">Create Account</a>
			 | 	<a style="text-decoration:none; color:black; font-size:12px; font-family:verdana;" href="javascript:loadVideo('PfZY50zFnBw');" title="Customer Review Submissions">Customer Review Submissions</a>
			 | 	<a style="text-decoration:none; color:black; font-size:12px; font-family:verdana;" href="javascript:loadVideo('hKFhpKfNLoQ');" title="Pricing">Pricing</a>
			 | 	<a style="text-decoration:none; color:black; font-size:12px; font-family:verdana;" href="javascript:loadVideo('w9ZFNOKvySY');" title="Standards and Legal">Standards and Legal</a>
			 | 	<a style="text-decoration:none; color:black; font-size:12px; font-family:verdana;" href="javascript:loadVideo('VGvjqUTnHZ0');" title="Note Entry">Note Entry</a>
			 | 	<a style="text-decoration:none; color:black; font-size:12px; font-family:verdana;" href="javascript:loadVideo('GZlSluR44pk');" title="Stickered Note Search">Stickered Note Search</a>
			 | 	<a style="text-decoration:none; color:black; font-size:12px; font-family:verdana;" href="javascript:loadVideo('7GsAo5supbA');" title="Accepted Notes">Accepted Notes</a>
			 <br/>
			 <a style="text-decoration:none; color:black; font-size:12px; font-family:verdana;" href="javascript:loadVideo('tpzwp9MwKE0');" title="Take a Tour">Take a Tour</a>
			 | 	<a style="text-decoration:none; color:black; font-size:12px; font-family:verdana;" href="javascript:loadVideo('4iVl1cWicb4');" title="FAQ">FAQ</a>
			 | 	<a style="text-decoration:none; color:black; font-size:12px; font-family:verdana;" href="javascript:loadVideo('5GktI8Iaikw');" title="Logout">Logout</a>	</div>
		<br/>
		<!-- Image map text links - End - -->
		<div id="divVideo">

			<div id="ytapiplayer">
		      You will need Flash 8 or better to view this content.
		    </div>
		</div>
	</div>
</fieldset>
    <script type="text/javascript" src="/resources/js/swfobject.js"></script>
    <script type="text/javascript">
      var params = { allowScriptAccess: "always" };
      var atts = { id: "myytplayer" };
      swfobject.embedSWF("//www.youtube.com/v/tJgZcWjdhVc&enablejsapi=1&playerapiid=ytplayer&rel=0", "ytapiplayer", "984", "773", "8", null, null, params, atts);

      function onYouTubePlayerReady(playerId) {
        ytplayer = document.getElementById("myytplayer");
      }

      function play() {
        if (ytplayer) {
          ytplayer.playVideo();
        }
      }

	 function stop() {
		if (ytplayer) {
          ytplayer.stopVideo();
        }
	 }

	  function loadVideo(videoID) {
		  $(document).scrollTop(1000);
		ytplayer = document.getElementById("myytplayer");
	  	ytplayer.loadVideoById(videoID, 0, "large");
	    var videoDiv = document.getElementById("divVideo");
		videoDiv.scrollTop = videoDiv.scrollHeight;
	  }


    </script>

</div>