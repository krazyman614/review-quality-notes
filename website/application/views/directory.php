<?php 
$state_values=array(
                'AL'=>"Alabama",
                'AK'=>"Alaska",
                'AZ'=>"Arizona",
                'AR'=>"Arkansas",
                'CA'=>"California",
                'CO'=>"Colorado",
                'CT'=>"Connecticut",
                'DE'=>"Delaware",
                'DC'=>"District Of Columbia",
                'FL'=>"Florida",
                'GA'=>"Georgia",
                'HI'=>"Hawaii",
                'ID'=>"Idaho",
                'IL'=>"Illinois",
                'IN'=>"Indiana",
                'IA'=>"Iowa",
                'KS'=>"Kansas",
                'KY'=>"Kentucky",
                'LA'=>"Louisiana",
                'ME'=>"Maine",
                'MD'=>"Maryland",
                'MA'=>"Massachusetts",
                'MI'=>"Michigan",
                'MN'=>"Minnesota",
                'MS'=>"Mississippi",
                'MO'=>"Missouri",
                'MT'=>"Montana",
                'NE'=>"Nebraska",
                'NV'=>"Nevada",
                'NH'=>"New Hampshire",
                'NJ'=>"New Jersey",
                'NM'=>"New Mexico",
                'NY'=>"New York",
                'NC'=>"North Carolina",
                'ND'=>"North Dakota",
                'OH'=>"Ohio",
                'OK'=>"Oklahoma",
                'OR'=>"Oregon",
                'PA'=>"Pennsylvania",
                'RI'=>"Rhode Island",
                'SC'=>"South Carolina",
                'SD'=>"South Dakota",
                'TN'=>"Tennessee",
                'TX'=>"Texas",
                'UT'=>"Utah",
                'VT'=>"Vermont",
                'VA'=>"Virginia",
                'WA'=>"Washington",
                'WV'=>"West Virginia",
                'WI'=>"Wisconsin",
                'WY'=>"Wyoming"
    );
function listUSStates($state_values,$dropdown_name,$key_selected) {

    $string="<select name=\"".$dropdown_name."\">\n";
    if (!empty($state_values)) {
        if ($key_selected=="" || !isset($key_selected)) {
            $string.="<option value=\"\">Please select</option>\n";
        }
        foreach($state_values as $state_short=>$state_full) {
            if ($key_selected!="" && $key_selected==$state_short) {
                $additional=" SELECTED";
            }
            else {
                $additional="";
            }
                $string.="<option value=\"".$state_short."\"".$additional.">".$state_full."</option>\n";
        }
    }
    $string.="</select>\n";
    return $string;
}
?>
<style type="text/css">
	#learn-more {padding:0 100px 0 50px;}
	#learn-more, #learn-more p {font-style:italic; font-size:1.5em;}
	#learn-more-header, #learn-more-footer {text-align:center;}
</style>

<div class="header_row">
	<div class="header_row_inner">
	Dealer Directory
	</div>
</div>

<div style="padding: 40px 70px 70px 78px;">

<div >
	<div id="learn-more-header" class="center">
		<p>

			<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
<?php
$select_box_name = "roger";

$key_selected = null;
if (isset($_POST['submit']))
    $key_selected=$_POST[$select_box_name];
echo listUSStates($state_values,$select_box_name,$key_selected);
?><br/>
<input type="submit" name="submit" value="Search" class='btn' />
	<?php
if (isset($_POST['submit']) && $_POST[$select_box_name] != '') {
    $short_name = $_POST[$select_box_name];
    $long_name = $state_values[$short_name];
    echo "<br/><br/>Results for ".$long_name."<br/>";
}
?>	
			</form>
		</p><br /> <!-- Extra line break before image -->
</div>
</i></em>
<?php
if (isset($_POST['submit']) && $_POST[$select_box_name] != '') {
    $short_name = $_POST[$select_box_name];
    $long_name = $state_values[$short_name];
    
    $result = mysql_query("select * from directory where state = '".addslashes($short_name)."'");
    while($row = mysql_fetch_array($result)) {
		echo "<hr>";
		echo $row['name']."<br/>";
		if($row['business'] != '') echo $row['business']."<br/>";
		if($row['address'] != '') echo $row['address']."<br/>";
		if($row['address2'] != '')	echo $row['address2'];
		if($row['city'] != '') echo $row['city'].", "; 
		if($row['state'] != '') echo $row['state']." ";
		echo $row['zip']."<br/><br/>";

		if($row['work_phone'] != '') echo "p:".$row['work_phone']."<br/>";
		if($row['phone'] != '') echo "c:".$row['phone']."<br/>";
		if($row['fax'] != '') echo "f:".$row['fax']."<br/>";
		if($row['website'] != '') echo "<a href='http://".$row['website']."'>".$row['website']."</a><br/>";
		if($row['email'] != '') echo "<a href='http://".$row['email']."'>".$row['email']."</a><br/>";
		if($row['sell'] != '') echo "<br>".nl2br($row['sell'])."<br/>";
		
	}
   if(mysql_num_rows($result) == 0)
   		echo "<h3>No results found</h3>";
} else {
	$result = mysql_query("select * from directory order by name,state limit 20");
	while($row = mysql_fetch_array($result)) {
		echo "<hr>";
		echo $row['name']."<br/>";
		if($row['business'] != '') echo $row['business']."<br/>";
		if($row['address'] != '') echo $row['address']."<br/>";
		if($row['address2'] != '')	echo $row['address2'];
		if($row['city'] != '') echo $row['city'].", "; 
		if($row['state'] != '') echo $row['state']." ";
		echo $row['zip']."<br/><br/>";

		if($row['work_phone'] != '') echo "p:".$row['work_phone']."<br/>";
		if($row['phone'] != '') echo "c:".$row['phone']."<br/>";
		if($row['fax'] != '') echo "f:".$row['fax']."<br/>";
		if($row['website'] != '') echo "<a href='http://".$row['website']."'>".$row['website']."</a><br/>";
		if($row['email'] != '') echo "<a href='http://".$row['email']."'>".$row['email']."</a><br/>";
		if($row['sell'] != '') echo "<br>".nl2br($row['sell'])."<br/>";
	
	}
}
?>	

</div>