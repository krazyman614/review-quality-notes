
<fieldset>
	<legend>RQN News</legend>
	<?php if(isset($news_items)){?>
	<div class="container pull-left" style="background-color:#dfdedb; padding:10px; width:80%;">
		<?php foreach($news_items as $news_item){?>
		<div style="padding-bottom:4px; color:#5e594d;">
			<strong><?php echo $news_item['title'];?></strong>
			<p>
				<?php echo $news_item['news'];?>
				<a href="/news/view/<?php echo $news_item['id'];?>" class="muted">Read More &gt;&gt;</a>
			</p>
		</div>
		<?php }?>
	</div>
	<?php }?>
</fieldset>