<div id="wrap">

<table style='width:100%'>
	<tr>
		<td width='300px;'>
			<img src="/resources/img/header-landing-txt.png" style='margin-left:-40px;margin-bottom:10px;'/><br/>
			<a href="/submissions/new_submission"><img src="/resources/img/header-landing-btn.png" border="0"/></a>
		
		</td>
		<td>
			<div class="feature">
			&nbsp;
			</div>
		</td>
	</tr>
</table>
<div style='float:left;'>
<br style='clear:both;'/>
<div class="brown-bar">

</div>
<div class="container-fluid">
	<div class="row-fluid">
		<div class="span10">
			<div class="headlines">
				<img src="/resources/img/headlines.jpg"/>
			</div>
			<div class="learn-more">
				<a href="/home/learn_more"><img src="/resources/img/learn-more-green-btn.png" border="0"/></a>
			</div>
			<?php if(isset($specials) && count($specials)>0){?>
			<div class="container muted span12" style="background-color:#dfdedb; padding:10px; margin-top:15px;">
				<h4>RQN Specials</h4>
				<?php foreach($specials as $special){?>
				<div style="padding-bottom:4px; color:#5e594d;">
					<strong><?php echo $special['title'];?></strong>
					<p>
						<?php echo $special['description'];?>
					</p>
				</div>
				<?php }?>
			</div>
			<?php }?>
			<?php if(isset($news_items)){?>
			<div class="container muted span12" style="background-color:#dfdedb; padding:10px; margin-top:15px;">
				<h3>RQN News</h3>
				<?php foreach($news_items as $news_item){?>
				<div style="padding-bottom:4px; color:#5e594d; font-size: 14px;">
					<strong><?php echo $news_item['title'];?></strong>
					<p style="font-size: 14px;">
						<?php echo $news_item['news'];?>
						<a href="/news/view/<?php echo $news_item['id'];?>" class="muted">Read More &gt;&gt;</a>
					</p>
				</div>
				<?php }?>
			</div>
			<?php }?>
		</div>
		<div class="span2" style="padding-top:70px;"><!-- img src="/resources/img/add-placeholder.png"/-->
			<script type="text/javascript"><!--
				google_ad_client = "ca-pub-5728399096668118";
				/* home_page_ad */
				google_ad_slot = "6713630299";
				google_ad_width = 160;
				google_ad_height = 600;
			//-->
			</script>
			<script type="text/javascript" src="https://pagead2.googlesyndication.com/pagead/show_ads.js"></script>
		</div>
	</div>
</div>

</div>