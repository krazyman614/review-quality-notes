<!-- script type="text/javascript">
	$(function(){
		$('#frm-search').submit(function(){
			var url = '/note_types';
			var val = $('#search-region_id').val();
			if(val != '') url += '/page/' + val;
			$('#frm-search').attr('action', url);
			return true;
		});

	});
</script-->

<div class="header_row">
	<div class="header_row_inner">
	Accepted Note Types
	</div>
</div>

<div style="padding: 40px 70px 70px 78px;">

<fieldset>

	<div id="note_types">
		<!-- div>
			<?php /* if(isset($regions)){?>
			<form id="frm-search" method="get">
			<div class="input-append pull-left">
				<select class="span2" id="search-region_id">
					<option value="">All</option>
		  			<?php foreach($regions as $region){?>
					<option value="<?php echo $region['id'];?>"<?php if(isset($region_id) && $region['id'] == $region_id){?> selected="selected"<?php }?>><?php echo $region['name'];?></option>
					<?php }?>
			  	</select>
		  		<input type="submit" class="btn" value="Search"/>
			</div>
			</form>
			<?php }*/?>
		</div-->
		<table class="table table-hover">
		<tr>
			<th>Type</th>
			<th>Region</th>
		<tr>
		<?php 
		if(isset($note_types)){
			foreach($note_types as $note_type){
				?>
				<tr>
					<td><?php echo $note_type['type'];?></td>
					<td><?php echo $note_type['region']['name'];?></td>
				</tr>			
			<?php 
			}
		}?>
		</table>		
	</div>
</fieldset>

</div>