<div class="header_row">
	<div class="header_row_inner">
	Password Reset
	</div>
</div>

<div style="padding: 40px 70px 70px 70px;">

<script>
	$(function(){
		/*
		if($(this).find('.error').length == 0)
			return true;
		else{
			$('#display-error').show();
			return false;
		}*/
	});
</script>

<div class="password-reset-confirm">
<?php if(isset($email) && isset($new_password)) {?>

<h4>Your new password is <b><?php print $new_password; ?></b><br /> A reminder has been sent to your account at <?php print urldecode($email); ?>.  Please change it upon logging in.</h4>
<?php } else {?>
You should not be here!  You must leave at once!
<?php }?>
</div>

<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">Terms and Conditions</h3>
	</div>
	<div class="modal-body"></div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
	</div>
</div>

</div>