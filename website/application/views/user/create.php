<script>
	$(function(){
		$('#btn-confirm').click(function(){
			$('#is-confirm-address').val('yes');
		});
		
		$('#create-form').submit(function(){
			$('#display-error').hide();
			$('#errors-list').empty();
			$('.error').removeClass('error');
			$(this).find('input').each(function(){
				var inpt = $(this);
				var type = inpt.attr('type');
				switch(type){
				case 'checkbox':
					if(!inpt.is(':checked')){
						$('#errors-list').append('<li>Did not accept terms</li>');
						inpt.parent().parent().addClass('error');
					}
					break;
				default:
					var name = inpt.attr('name');
					switch(name){
					case 'address2':
					case 'alt_phone_number':
					case 'company_name':
						break;
					default:
						if($.trim(inpt.val()) == ''){
							inpt.parent().addClass('error');
							$('#errors-list').append('<li>Missing ' + name.replace('_', ' ') + '</li>');
						}			
						break;
					}
				}
			});
			if($(this).find('.error').length == 0){
				if($('#is-confirm-address').val() == 'no'){
					$('#addr-confirm').empty();
					$('#addr-confirm').append('<strong>Is this your correct address information</strong><br/>' +
						$('[name=address]').val() + '<br/>' +
						($('[name=address2]').val() != '' ? $('[name=address2]').val() + '<br/>' : '') +
						$('[name=city]').val() + ', ' + $('[name=state_province]').val() + ' ' + $('[name=postal_code]').val() + '<br/>' +
						$('[name=country]').val());
					
					$('#confirm_address').modal('show');				
					return false;
				}
				return true;
			}
			else{
				$('#display-error').show();
				return false;
			}
		});
	});
</script>
<div class="header_row">
	<div class="header_row_inner">
	Create Account
	</div>
</div>

<div style="padding: 40px 70px 70px 70px;">

<form method="post" id="create-form" action="/users/create">
	<input type="hidden" id="is-confirm-address" value="no"/>
	<fieldset style="padding-left:10px;float:left;">

		<div id="display-error" class="alert alert-error"<?php if(!isset($errors)){?> style="display:none;"<?php }?>>
	    	Please correct the following errors:
	    	<ul id="errors-list">
	    	<?php foreach($errors as $key=>$value){?>
	    		<li><?php echo $value;?></li>
	    	<?php }?>
	    	</ul>
	    </div>
		<div class="control-group<?php if(isset($errors['name'])){ ?> error<?php }?>">
			<label>Name</label>
			<input type="text" name="name" placeholder="Name" value="<?php if(isset($data['name'])){ echo $data['name'];}?>"/>
		</div>
		<div class="control-group">
			<label>Company Name</label>
			<input type="text" name="company_name" placeholder="Company Name" value="<?php if(isset($data['company_name'])){ echo $data['company_name'];}?>"/>
		</div>
		<div class="control-group<?php if(isset($errors['address']) || isset($errors['address2']) || isset($errors['city']) || isset($errors['state']) || isset($errors['postal_code']) || isset($errors['country'])){ ?> error<?php }?>">
			<label>Address</label>
			<input type="text" name="address" placeholder="Address" class="span3" value="<?php if(isset($data['address'])){ echo $data['address'];}?>"/>
			<br />
			<label>Address 2</label>
			<input type="text" name="address2" placeholder="Address Line 2" class="span3" value="<?php if(isset($data['address2'])){ echo $data['address2'];}?>"/>
			
					<label>City</label>
					<input type="text" name="city" placeholder="City" value="<?php if(isset($data['city'])){ echo $data['city'];}?>"/>
		
					<label>State</label>
					<input type="text" name="state_province" placeholder="State" class="span3" maxlength="2" value="<?php if(isset($data['state_province'])){ echo $data['state_province'];}?>"/>
			
					<label>Postal Code</label>
					<input type="text" name="postal_code" placeholder="Postal Code" maxlength="10" value="<?php if(isset($data['postal_code'])){ echo $data['postal_code'];}?>"/>
				
			<label>Country</label>
			<select name="country">
				<?php foreach($countries as $country){?>
				<option value="<?php echo $country['name']?>"<?php if(isset($data['country']) && $data['country'] == $country['name'] || (!isset($data['country']) && $country['name']=='UK')){?> selected="selected"<?php }?>><?php echo $country['name']?></option>
				<?php }?>
			</select>
		</div>
	</fieldset>

	<fieldset style="padding-left:10px;">
		<div class="control-group<?php if(isset($errors['phone_number'])){ ?> error<?php }?>">
			<label>Phone Number</label> <input type="text" name="phone_number" placeholder="Phone Number" value="<?php if(isset($data['phone_number'])){ echo $data['phone_number'];}?>"/>
		</div>
		<div class="control-group<?php if(isset($errors['alt_phone_number'])){ ?> error<?php }?>">
			<label>Alt Phone Number</label> <input type="text" name="alt_phone_number" placeholder="Alt Phone Number" value="<?php if(isset($data['alt_phone_number'])){ echo $data['alt_phone_number'];}?>"/>
		</div>
		<div class="control-group<?php if(isset($errors['email'])){ ?> error<?php }?>">
			<label>Email</label>
			<input type="text" name="email" placeholder="Email" value="<?php if(isset($data['email'])){ echo $data['email'];}?>"/>
		</div>
		<div class="control-group<?php if(isset($errors['password'])){ ?> error<?php }?>">
			<label>Password</label>
			<input type="password" name="password" placeholder="Password" value="<?php if(isset($data['password'])){ echo $data['password'];}?>"/>
		</div>
		<div class="control-group<?php if(isset($errors['confirm_password'])){ ?> error<?php }?>">
			<label>Confirm Password</label>
			<input type="password" name="confirm_password" placeholder="Confirm Password" value="<?php if(isset($data['confirm_password'])){ echo $data['confirm_password'];}?>"/>
		</div>
		<div class="control-group<?php if(isset($errors['accepted_terms'])){ ?> error<?php }?>">
			<label class="checkbox">
				<input type="checkbox" name="accepted_terms" value="1" <?php if(isset($data['accepted_terms'])){?>checked="checked"<?php }?>/>
				Accept <a href="/resources/terms.html" data-target="#myModal" data-toggle="modal">terms and conditions</a>
			</label>
		</div>
		<br>
		<button type="submit" id="btn-create" class="btn" style="background:none;border:none;padding:0px;margin:0px;"><img src="/resources/img/btn_submit.png"/></button>
	<div id="confirm_address" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
			<h3>Confirm Address</h3>
		</div>
		<div class="modal-body" id="addr-confirm"></div>
		<div class="modal-footer">
			<button type="submit" id="btn-confirm" class="btn">Yes</button>
			<button type="button" class="btn" data-dismiss="modal" aria-hidden="true">No</button>
		</div>
	</div>
</form>



<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">Terms and Conditions</h3>
	</div>
	<div class="modal-body"></div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
	</div>
</div>

</div>