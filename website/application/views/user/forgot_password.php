<div class="header_row">
	<div class="header_row_inner">
	Forgot Your Password?
	</div>
</div>

<div style="padding: 40px 70px 70px 70px;">

<script>
	$(function(){
		$('#forgot-password-form').submit(function(){
			if ( $.trim( $('#inpt-email').val() ) =='') {
				alert('You did not enter an email!');
				return false;
			}
		});
	});
</script>
<form method="post" id="forgot-password-form" action="/users/send_password_reset">
	<fieldset style="padding-left:10px;">

		<div id="display-error" class="alert alert-error"<?php if(!isset($error)){?> style="display:none;"<?php }?>>
			<?php echo $error;?>
	    </div>
	    <div class="text info">
	    	Enter your email below to recieve a link to reset your password.
	    </div>
		<div class="control-group<?php if(isset($error)){ ?> error<?php }?>">
			<label>Email</label>
			<input type="text" id="inpt-email" name="email" placeholder="Email" value="<?php if(isset($email)){ echo $email;}?>"/>
		</div>
		<button type="submit" class="btn">Reset Password</button>
	</fieldset>
</form>

</div>