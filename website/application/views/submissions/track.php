<div class="header_row">
	<div class="header_row_inner">
	Submissions
	</div>
</div>

<div style="padding: 40px 70px 70px 70px;">

<fieldset style="padding-left:10px;">

	<div id="submissions">
		<div>
			<a href="/submissions/new_submission" class="btn pull-right">New Submission</a>
		</div>	
		<?php if(isset($submissions) && count($submissions) >= 0){?>
		<table class="table table-hover">
		<tr>
			<th>Order #</th>
			<th>Date</th>
			<th>Total Declared Value</th>
			<th>Status</th>
			<th>&nbsp;</th>
		</tr>
		<?php foreach($submissions as $submission){?>
		<tr>
			<td><?php echo $submission['order_number'];?></td>
			<td><?php echo date('D, d M Y g:i A', strtotime($submission['created_date']));?></td>
			<td>$<?php echo money_format('%i', $submission['total_declared_value']);?></td>
			<td><?php echo $submission['status'];?></td>
			<td><a href="/submissions/view/<?php echo $submission['id'];?>" class="btn btn-small">View</a></td>
		</tr>
		<?php }?>
		</table>
		<div>
			<a href="/submissions/new_submission" class="btn pull-right">New Submission</a>
		</div>	
		<?php }?>
	</div>
</fieldset>

</div>