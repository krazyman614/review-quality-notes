<script type="text/javascript">
	$(function(){
		$('.cancel-order').click(function(){
			$('#frm-comment').attr('action', '/submissions/cancel_order/<?php echo $submission['id'];?>');
			$('.comment-btn').val('Cancel Order');
		});

		$('.continue-order').click(function(){
			$('#frm-comment').attr('action', '/submissions/continue_order/<?php echo $submission['id'];?>');
			$('.comment-btn').val('Continue Order');
		});
		
	});
</script>

<fieldset style="padding-left:10px;">
	<legend>Order #<?php echo $submission['order_number']?></legend>
	<?php if(isset($success) && $success > 0){?>
		<div class="alert alert-success">
			<?php switch($success){
				case 1: print 'Your order has been placed';
					break;
				case 2: print 'Your order is canceled'; break;
				case 3: print 'You order is marked to be continued with the received notes'; break;
			}?>
		</div>
	<?php }?>
	<div class="container-fluid">
	  	<?php if($submission['status_id'] <= 100){?>
		<div class="row-fluid">
			<div class="span8">
				<h4 style="color:black;">					
					MAIL YOUR SUBMISSION TO:<br />
					Review Quality Notes INC.<br/>
					P.O. Box 571150<br/>
					Miami, FL 33257-1150<br/>
				</h4>
			</div>
		</div>
		<?php }?>
	  	<?php if(!empty($submission['tracking_number'])){?>
	  	<div class="row-fluid">
		  	<div class="span8">
		  		<strong>Tracking :</strong>
		  		<?php print $submission['tracking_number'];?>
		  	</div>
		</div>
	  	<?php }?>
	  	<?php if(!empty($submission['fedex_account_number'])){?>
	  	<div class="row-fluid">
		  	<div class="span8">
		  		<strong>Fedex Account # :</strong>
		  		<?php print $submission['fedex_account_number'];?>
		  	</div>
		</div>
	  	<?php }?>
	  	<div class="row-fluid">
		  	<div class="span8">
		  		<strong>Status:</strong>
		  		<?php print $submission['status'];?>
		  		<?php if($submission['status_id'] == 200){?>
		  		<form id="frm-comment" method="post">
					<a href="#comments-modal" role="button" class="btn cancel-order" data-toggle="modal">Cancel Order</a>	
					<a href="#comments-modal" role="button" class="btn continue-order" data-toggle="modal">Process Revieved Notes</a>	  		
			  		<div id="comments-modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					  <div class="modal-header">
					    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
					    <h3 id="myModalLabel">Comment</h3>
					  </div>
					  <div class="modal-body">
			  				<textarea style="width:98%" rows="10" name="comments"></textarea>
					  </div>
					  <div class="modal-footer">
			    		<input type="submit" class="btn btn-primary comment-btn" value="Send"/>
					  </div>
					</div>
		  		</form>
		  		<?php }?>
		  	</div>
		</div>
		<div class="row-fluid">
		    <div class="span2">
		    	<strong>Info</strong><br/>
		    	<?php echo $submission['name'];?><br/>
		    	<?php echo $submission['company_name'];?><br/>
		    	<?php echo $submission['phone_number'];?><br/>
		    	<?php echo $submission['email'];?><br/>
		    </div>
		    <div class="span2">
		    	<strong>Shipping Address</strong><br/>
		       	<?php echo $submission['address'];?><br/>
		    	<?php if(trim($submission['address2']) != ''){ ?>
		       	<?php echo $submission['address2'];?><br/>
		       	<?php }?>
		    	<?php echo $submission['city'];?>, <?php echo $submission['state_province'];?> <?php echo $submission['postal_code'];?><br/>
		    	<?php echo $submission['country'];?><br/>
			</div>
			<?php if($submission['payment_type_id'] < 2){?>
		    <div class="span2">
		    	<strong>Billing Address</strong><br/>
		       	<?php echo $submission['billing_address'];?><br/>
		    	<?php if(trim($submission['billing_address2']) != ''){ ?>
		    	<?php echo $submission['billing_address2'];?><br/>
		    	<?php }?>
		    	<?php echo $submission['billing_city'];?>, <?php echo $submission['billing_state_province'];?> <?php echo $submission['billing_postal_code'];?><br/>
		    	<?php echo $submission['billing_country'];?><br/>
			</div>
			<?php }?>
			<?php switch($submission['payment_type_id']){
				case 1:?>
				    <div class="span2">
				    	<strong>Credit Card</strong><br/>
				    	<?php echo $submission['card_type'];?><br/>
				    	<?php echo $submission['name_on_card'];?><br/>
				    	############<?php echo $submission['card_last_four_numbers'];?><br/>
				    	<?php echo $submission['card_exp_month'];?>/<?php echo $submission['card_exp_year'];?><br/>
				    </div>
					<?php 
					break;
				case 2: ?>
				    <div class="span2">
				    	<strong>Check</strong><br/>
				    </div>				
					<?php 
					break;
				case 3: ?>
				    <div class="span2">
				    	<strong>Pickup</strong><br/>
				    </div>				
					<?php 
					break;
			}?>
		</div>
		<div class="row-fluid">
    		<table id="order-form" class="table table-bordered">
    			<thead>
	    			<tr>
	    				<th>Sticker</th>
	    				<th>Line Number</th>
	    				<th>Status</th>
	    				<th>RQN Service</th>
	    				<th><?php echo get_column_name('type', $submission['region_id']);?></th>
	    				<th>Denomination</th>
	    				<th><?php echo get_column_name('catalog_number', $submission['region_id']);?></th>
	    				<th><?php echo get_column_name('pp', $submission['region_id']);?></th>
	    				<th>Serial Number</th>
	    				<th>Grading Service</th>
	    				<th>Bar Code</th>
	    				<th>Declared Value</th>
	    				<th>Service Cost</th>
	    			</tr>
    			</thead>
    			<?php foreach($submission['notes'] as $note){?>
    			<tr>
	    			<td><?php echo get_stickered($note);?></td>
    				<td<?php echo check_history($note, 'sequence');?>><?php echo $note['sequence'];?></td>
    				<td><?php echo $note['status'];?></td>
    				<td<?php echo check_history($note, 'service_level_id');?>><?php echo $note['service_level']['title'];?></td>
	    			<?php echo get_note_type($note, $submission['region_id']);?>
    				<td<?php echo check_history($note, 'denomination');?>>$<?php echo money_format('%i', $note['denomination']);?></td>
    				<td<?php echo check_history($note, 'catalog_number');?>><?php echo $note['catalog_number'];?></td>
    				<td<?php echo check_history($note, 'pp');?>><?php echo $note['pp'];?></td>
    				<td<?php echo check_history($note, 'serial_number');?>><?php echo $note['serial_number'];?></td>
    				<td<?php echo check_history($note, 'grading_service_id');?>><?php echo $note['grading_service']['name'];?></td>
    				<td<?php echo check_history($note, 'barcode');?>><?php echo $note['barcode'];?></td>
      				<td<?php echo check_history($note, 'declared_value');?>>$<?php echo money_format('%i', $note['declared_value']);?></td>
      				<td<?php echo check_history($note, 'service_cost');?>><span <?php echo ($note['status_id'] == 600) ? 'style="text-decoration: line-through;"' : '';?>>$<?php echo money_format('%i', $note['service_cost']);?></span></td>
   				</tr>
	   			<?php if(isset($note['grades']) && count($note['grades']) > 0){?>
	   				<tr>
	   					<td colspan="14">
	   						<div class="container-fluid">
	   							<h4>Reviews</h4>
	   							<?php foreach($note['grades'] as $grade){
	   								if($grade['is_final'] >= 1){
	   									$this->load->view('inc/grade', $grade);
	   								}?>
	   							<?php }?>
	   	  					</div>
	   	  				</td>
	   	  			</tr>
	   	  			<?php }?>
    			<?php }?>
    			<tr id="order-form-footer">
    				<td colspan="11">
    					<strong class="pull-right">Total Declared Value</strong>
    				</td>
    				<td>
    					$<?php echo money_format('%i', $submission['total_declared_value']);?>
    				</td>
    				<td>&nbsp;</td>
    			</tr>
    			<tr>
	    			<td colspan="12">
    					<strong class="pull-right">Sub Total</strong>
    				</td>
    				<td>
    					$<?php echo money_format('%i', $submission['sub_total']);?>
    				</td>
    			</tr>
    			<tr>
	    			<td colspan="12">
    					<strong class="pull-right">Postage</strong>
    				</td>
    				<td>
    					$<?php echo money_format('%i', $submission['postage']);?>
    				</td>
    			</tr>
    			<?php if($submission['discount'] > 0){?>
    			<tr>
	    			<td colspan="12">
    					<strong class="pull-right">Discount (%<?php echo $submission['discount']?>)</strong>
    				</td>
    				<td>
    					$<?php echo money_format('%i', $submission['discount_amount']);?>
    				</td>
    			</tr>
    			<?php }?>
    			<tr>
	    			<td colspan="12">
    					<strong class="pull-right">Total</strong>
    				</td>
    				<td>
    					$<?php echo money_format('%i', $submission['total']);?>
    				</td>
    			</tr>
    		</table>
		</div>
		<?php if(isset($submission['comments']) && count($submission['comments']) > 0){?>
	  	<div class="row-fluid">
			<h4>Comments</h4>
			<?php foreach($submission['comments'] as $comment){?>
				<blockquote>
					<p><?php echo $comment['comment'];?></p>
					<small><?php echo isset($comment['by']) ? $comment['by']->name . ' ' : ''?>@<?php echo date('D, d M Y g:i A', strtotime($comment['created_date'])); ?></small>
				</blockquote>
			<?php }?>
		</div>		
		<?php }?>
	</div>
</fieldset>