<?php 
function get_pp_size($region_id){
	switch($region_id){
		case 'us': return ' maxlength="2" style="width:32px;"';
		default: return ' maxlength="10" style="width:60px;"';
	}
}



function get_type_field($region_id){

	switch($region_id){

		case 'us': return "'<td>' + get_note_types_dropdown(data && data.note_type_id ? data.note_type_id : '') + '</td>'";

		default: return "'<td><input type=\"text\" name=\"note_country[]\" value=\"' + (data && data.note_country ? data.note_country : '') + '\" maxlength=\"255\" style=\"width:120px;\"/></td>'";

	}

}
?>

<script type="text/javascript">
	var access_token = "<?php echo $access_token; ?>";

	<?php if(isset($service_levels)){?>
	var services = [
	   <?php 
	   $itt = 0;
	   foreach($service_levels as $service_level){?>
	   { id: "<?php echo $service_level['id'];?>", level: "<?php echo $service_level['level'];?>", per_note : <?php echo $service_level['per_note'];?> }<?php if($itt++ < count($service_levels) - 1){ echo ', ';}?>
	   <?php }?>
	];
	<?php }?>
	<?php if(isset($note_types)){?>
	var note_types = [
	   <?php 
	   $itt = 0;
	   foreach($note_types as $note_type){?>
	   { id: "<?php echo $note_type['id'];?>", type: "<?php echo $note_type['type'];?>" }<?php if($itt++ < count($note_types) - 1){ echo ', ';}?>
	   <?php }?>
	];
	<?php }?>
	<?php if(isset($grading_services)){?>
	var grading_services = [
	   <?php 
	   $itt = 0;
	   foreach($grading_services as $grading_service){?>
	   { id: "<?php echo $grading_service['id'];?>", type: "<?php echo $grading_service['abbrev'];?>" }<?php if($itt++ < count($grading_services) - 1){ echo ', ';}?>
	   <?php }?>
	];
	<?php }?>
	<?php if(isset($postage)){?>
	var postages = [
	   <?php 
	   $itt = 0;
	   foreach($postage as $p){?>
	   { under_value: <?php echo $p['under_value'];?>, cost: <?php echo $p['cost'];?> }<?php if($itt++ < count($postage) - 1){ echo ', ';}?>
	   <?php }?>
	];
	<?php }?>
	function get_service_per_note(id){
		if(!id) return -1;
		for(var i=0; i < services.length; i++){
			if(services[i].id == id)
				return services[i].per_note;
		}
		return -1;
	}
	function get_note_types_dropdown(type){		
		var str = '<select name="note_type[]" class="note-types" style="width:150px;">';
		str += '<option value=""></option>';
		$.each(note_types, function(index, note_type){
			str += '<option value="' + note_type.id + '"' + (type == note_type.id ? ' selected="selected"' : '') + '>' + note_type.type + '</option>';
		});
		str += '</select>';
		return str; 
	}
	function get_grading_service_dropdown(service){
		var str = '<select name="grading_service[]" class="grading-service" style="width:100px;">';
		str += '<option value=""></option>';
		$.each(grading_services, function(index, grading_service){
			str += '<option value="' + grading_service.id + '"' + (service == grading_service.id ? ' selected="selected"' : '') + '>' + grading_service.type + '</option>';
		});
		str += '</select>';
		return str; 
	}
	
	function add_row(data){
		
		$('#order-form-footer').before('<tr>' +
			'<td><a class="btn btn-mini delete-row" href="javascript:void(0)"><i class="icon-minus"></i></a></td>' +
			<?php echo get_type_field($region['id']);?> + 
			'<td>' +
				'<div class="input-prepend" style="width:100px;">' +
			  		'<span class="add-on">$</span>' +
			  		'<input class="span8" style="width:50px;" name="denomination[]" type="text"' + (data && data.denomination ? ' value="' + data.denomination + '"' : '') + '/>' +
				'</div>' +
			'</td>' +
			'<td><input name="catalog_number[]" maxlength="9" style="width:80px;"' + (data && data.catalog_number ? ' value="' + data.catalog_number + '"' : '') + '/></td>' +
			'<td><input name="pp[]" <?php echo get_pp_size($region['id']);?>' + (data && data.pp ? ' value="' + data.pp + '"' : '') + ' maxlength="2"/></td>' +
			'<td><input name="serial_number[]" maxlength="11" style="width:80px;"' + (data && data.serial_number ? ' value="' + data.serial_number + '"' : '') + '/></td>' +
			'<td>' + get_grading_service_dropdown(data && data.grading_service_id ? data.grading_service_id : '') + '</td>' + 
			'<td>' +
				'<div class="input-prepend control-group" style="width:100px;">' +
			  		'<span class="add-on">$</span>' +
			  		'<input class="span8" style="width:50px;" name="declared_value[]" type="text"' + (data && data.declared_value ? ' value="' + data.declared_value + '"' : '') + '/>' +
				'</div>' +
			'</td>' +
			'<td>' +
				'<div class="input-prepend control-group" style="width:100px;">' +
	  				'<span class="add-on">$</span>' +
	  				'<input class="span8" style="width:50px;" name="service_cost[]" disabled type="text"' + (data && data.declared_value ? ' value="' + data.declared_value + '"' : '') + '/>' +
				'</div>' +
			'</td>' +			
			'</tr>');
	}

	function format_currency(c){
		if($.trim(c) == '') return '---';
		if(typeof(c)=='string' && isNaN(c)){
			c = c.replace(/,/g, '');
			c = parseFloat(c);
			if(isNaN(c)) return "---";
		}
		var num = new Number(c);
		return num.toFixed(2);
	}

	function calculate_declared_total(){
		$('#order-form').find('[name="denomination[]"]').each(function(){
			$(this).parent().removeClass('error');
			if($.trim($(this).val()) != ''){
				var val = format_currency($(this).val());
				var val = parseFloat(val);
				if(isNaN(val)){
					$(this).parent().addClass('error');
				}
				$(this).val(format_currency($(this).val()));
			}
		});
		
		var total = 0.0;
		$('#order-form').find('[name="declared_value[]"]').each(function(){
			$(this).parent().removeClass('error');
			if($.trim($(this).val()) != ''){
				var val = format_currency($(this).val());
				var val = parseFloat(val);
				if(isNaN(val)){
					$(this).parent().addClass('error');
				}
				else {
					total += val;
				}
				$(this).val(format_currency($(this).val()));
			}
		});
		$('#order-form').find('[name="total_declared_value"]').val(format_currency(total));
	}

	function calculate_total(){
		var total = 0.0;
		var postage = -1.0;
		var service_level_id =  $('#service-level-table').find('[name="service_level"]:checked').val();
		var per_note = get_service_per_note(service_level_id);
		
		$('#order-form').find('[name="service_cost[]"]').each(function(){
			if(per_note <= 0 || total < 0){
				$(this).val('---');
				total = -1;
			}
			else {
				total += per_note;
				$(this).val(format_currency(per_note));
			}			
		});


		if(total >= 0){
			$('#order-form').find('[name=sub_total]').val(format_currency(total));

			var postage_total = 0.0;
			if($('#pickup').is(':checked') || $.trim($('#fedex_account_number').val()) != ''){
				postage = 0;
			}
			else {
				$('#order-form').find('[name="declared_value[]"]').each(function(){
					if($.trim($(this).val()) != ''){
						var val = parseFloat($(this).val());
						if(!isNaN(val)){
							postage_total += val;
						}
					}
				});
				
				for(var i = 0; i < postages.length; i++){
					if(postages[i].under_value >= postage_total){
						postage = postages[i].cost;
						break;
					}
				}
			}
		}
		else {
			$('#order-form').find('[name=sub_total]').val('---');
		}

		if(postage < 0){
			$('#order-form').find('[name="postage"]').val('---');
			$('#order-form').find('[name="total"]').val('---');
		}
		else {
			$('#order-form').find('[name="postage"]').val(format_currency(postage));

			total += postage;

			$('#order-form').find('[name="total"]').val(format_currency(total));
		}
	}

	function validate_form(){
		$('#display-error').hide();
		$('#errors-list').empty();
		$('.error').removeClass('error');
		$('#create-form').find('input').each(function(){
			var inpt = $(this);
			var type = inpt.attr('type');
			var name = inpt.attr('name');
			if(name && name.indexOf('[]') < 0){
				switch(name){
				case 'address2':
				case 'total':
				case 'company_name':
				case 'fedex_account_number':
					break;
				case 'service_level':
					if(!$('#service-level').hasClass('error')){
						if(!$('[name=service_level]:checked').val()){
							$('#service-level').addClass('error')
							$('#errors-list').append('<li>No Service Level selected</li>');
						}
					}
					break;
				case 'postage':
					if($.trim(inpt.val()) == '' || $.trim(inpt.val()) == '---'){
						inpt.parent().addClass('error');
						$('#errors-list').append('<li>No Postage for order, please email</li>');
					}		
				default:
					if($.trim(inpt.val()) == ''){
						inpt.parent().addClass('error');
						$('#errors-list').append('<li>Missing ' + name.replace(/_/g, ' ') + '</li>');
					}			
					break;
				}
			}
		});
		if($('#create-form').find('.error').length == 0)
			return true;
		else{
			$(document).scrollTop(0);
			$('#display-error').show();
			return false;
		}
	}

	$(function(){
		
		$('#create-form').submit(function(){
			return validate_form();
		});

	<?php if(isset($notes) && is_array($notes) && count($notes) > 0){?>
		<?php foreach($notes as $note){?>
			add_row(<?php echo json_encode($note);?>);
		<?php }?>
		calculate_declared_total();	
		calculate_total();
	<?php } else {?>
		add_row();
	<?php }?>
		calculate_total();

		$('#order-form').on('click', '.delete-row', function(){
			$(this).parent().parent().remove();
			$('.line-number').each(function(index){
				$(this).empty();
				$(this).append((index+1) + '');
			});
			calculate_declared_total();	
			calculate_total();
			if($('#order-form').find('tr').length < 25){
				$('#order-form').find('.add-row').show();
				$('#order-form .limit-msg').hide();
			}			
		});
		$('#order-form').on('click', '.add-row', function(){
			add_row();
			calculate_declared_total();	
			calculate_total();
			if($('#order-form').find('tr').length >= 25){
				$('#order-form').find('.add-row').hide();
				$('#order-form .limit-msg').show();
			}
		});
		$('#order-form').on('click', '[name=pickup]', function(){
			calculate_declared_total();	
			calculate_total();		
		});
		$('#order-form').on('change', '[name="denomination[]"]', function(){
			calculate_declared_total();	
			calculate_total();
		});

		$('#order-form').on('change', '[name="declared_value[]"]', function(){
			calculate_declared_total();	
			calculate_total();
		});

		$('#create-form').on('change', '[name="fedex_account_number"]', function(){
			calculate_declared_total();	
			calculate_total();
		});

		$('#service-level-table').on('change', '[name="service_level"]', function(){
			calculate_total();
		});
		$('#order-form').find('[name="service_level"]').first().change();

		$('#create-form').on('change', '.postage-country', function(){
			var country = $(this).val();
			$.ajax({
				url : '/postage_rates/get/' + country,
				data: {
					access_token: access_token
				},
				success: function(json){
					if(json.status.code !== 0){
						alert(json.status.message);
					}
					else if(json.postage) {
						postage = [];
						$('#postage').empty();
						var html = '<thead><tr><th>Total Value</th>';
						$.each(json.postage, function(index, p){
							html += '<th>Under $' + p.under_value_formatted + '</th>';

							postage.push({under_value: p.under_value, cost : p.cost});
						});
						html += '<th>Anything Over</th></tr></thead>';
	    				html += '<tr><td>Additional Cost</td>';
						$.each(json.postage, function(index, p){
			    			html += '<td>' + p.cost_formatted + '</td>';
						});
						html += '<th>Please email</th></tr>';
						$('#postage').append(html);	    			
					}
				}
			});
		});
	});
</script>

<div class="header_row">
	<div class="header_row_inner">
	New Submission - <?php echo $region['name']; ?>
	</div>
</div>

<div style="padding: 40px 70px 70px 60px;">

<form method="post" id="create-form" action="/submissions/create/<?php print $region['id'];?>">
	<fieldset style="padding-left:0px;width:1000px;">

		<div id="display-error" class="container alert alert-error"<?php if(!isset($errors)){?> style="display:none;"<?php }?>>
	    	Please correct the following errors:
	    	<ul id="errors-list">
	    	<?php foreach($errors as $key=>$value){?>
	    		<li><?php echo $value;?></li>
	    	<?php }?>
	    	</ul>
	    </div>
		<div class="container-fluid">
		  <div class="row-fluid">
		    <div class="span4">
		    	<div class="control-group<?php if(isset($errors['company_name'])){ ?> error<?php }?>">
					<label>Company Name</label>
					<input type="text" name="company_name" value="<?php if(isset($data['company_name'])){ echo $data['company_name'];}?>"/>
				</div>
				<div class="control-group<?php if(isset($errors['name'])){ ?> error<?php }?>">
					<label>Name</label>
					<input type="text" name="name" class="span12" value="<?php if(isset($data['name'])){ echo $data['name'];}?>"/>
				</div>
				<div class="control-group<?php if(isset($errors['address']) || isset($errors['address2']) || isset($errors['city']) || isset($errors['state']) || isset($errors['postal_code']) || isset($errors['country'])){ ?> error<?php }?>">
					<label>Shipping Address</label>
					<input type="text" name="address" class="span12" class="span12" value="<?php if(isset($data['address'])){ echo $data['address'];}?>"/>
					<br />
					<label>Shipping Address Line 2</label>
					<input type="text" name="address2" class="span12" value="<?php if(isset($data['address2'])){ echo $data['address2'];}?>"/>
				    <div class="controls controls-row">
				    	<div class="span5">
				    		<label>City</label>
							<input type="text" name="city" class="span10" value="<?php if(isset($data['city'])){ echo $data['city'];}?>"/>
						</div>
						<div class="span3">
							<label>State</label>
							<input type="text" name="state_province" class="span8"  maxlength="2" value="<?php if(isset($data['state_province'])){ echo $data['state_province'];}?>" style="min-width:55px;"/>
						</div>
						<div class="span3">
							<label>Zip</label>
							<input type="text" name="postal_code"  maxlength="10" class="span12" value="<?php if(isset($data['postal_code'])){ echo $data['postal_code'];}?>" style="width:70px"/>
						</div>
					</div>
				    <div class="controls controls-row">
				    	<div class="span6">
							<label>Country</label>
							<select name="country" class="postage-country" style="width:120px !important;">
							<?php foreach($countries as $country){?>
								<option value="<?php echo $country['name']?>"<?php if((isset($data['country']) && $data['country'] == $country['name']) || $submission['country'] == $country['name']){?> selected="selected"<?php }?>><?php echo $country['name']?></option>
							<?php }?>
							</select>
						</div>
				    	<div class="span5">
				<div class="control-group<?php if(isset($errors['phone_number'])){ ?> error<?php }?>">
					<label>Phone Number</label> <input type="text" name="phone_number" class="span4" value="<?php if(isset($data['phone_number'])){ echo $data['phone_number'];}?>" style="min-width:130px;"/>
				</div>
				    	</div>
					</div>
				</div>
				<div class="control-group<?php if(isset($errors['email'])){ ?> error<?php }?>">
					<label>Email</label>
					<input type="text" name="email" class="span12" value="<?php if(isset($data['email'])){ echo $data['email'];}?>"/>
				</div>
				
				<div class="control-group<?php if(isset($errors['fedex_account_number'])){ ?> error<?php }?>">
					<label>Fedex Account Number</label>
					<input type="text" name="fedex_account_number" id="fedex_account_number" class="span6" maxlength="255" placeholder="Fedex Account #" value="<?php if(isset($data['fedex_account_number'])){ echo $data['fedex_account_number'];}?>"/>
				</div>
		    </div>
		    <div class="span6">
		    	<div id="service-level" class="control-group">
			    	<label>Select a service level</label>			    	
			    	<?php if(isset($service_levels)){?>
			    		<table id="service-level-table" class="table table-bordered">
			    			<thead>
				    			<tr>
				    				<th align="center">
				    				<center>
				    				Your Service Level
				    				</center>
				    				</th>
				    				<th>
				    				<center>
				    				Service Level
				    				</center>
				    				</th>
				    				<th>Type</th>
				    				<th>Per note</th>
				    				<th>Time to Process</th>
				    			</tr>
			    			</thead>
			    			<?php foreach($service_levels as $service_level){?>
			    				<?php if($service_level['per_note'] > 0){?>
			    				<tr>
			    					<td align="center">
			    					<center>
			    					<input type="radio" name="service_level" class="service_level" value="<?php echo $service_level['id']; ?>" <?php if(isset($data['service_level']) && $data['service_level'] == $service_level['id']){ echo 'checked'; }?>/>
			    					</center>
			    					</td>
									<td>
			    					<center>
									<?php echo $service_level['level'];?>
			    					</center>
									</td>
									<td><?php echo $service_level['title'];?></td>
									<td><?php echo $service_level['per_note'] > 0 ? '$' . money_format('%i', $service_level['per_note']) : '&nbsp;' ;?></td>
									<td><?php echo $service_level['time_to_process'];?></td>
			    				</tr>
			    				<?php }?>
			    			<?php }?>
			    		</table>
			    	<?php }?>
			    </div>
		    </div>
		  </div>
		</div>
		<div class="container-fluid">
    		<table id="order-form" class="table table-bordered">
    			<thead>
	    			<tr>
	    				<th>&nbsp;</th>
	    				<th><?php echo get_column_name('type', $region['id']);?></th>
	    				<th>Denomination</th>
	    				<th><?php echo get_column_name('catalog_number', $region['id']);?></th>
	    				<th><?php echo get_column_name('pp', $region['id']);?></th>
	    				<th>Serial #</th>
	    				<th>Grading Service</th>
	    				<th>Declared Value</th>
	    				<th>Service Cost</th>
	    			</tr>
    			</thead>
    			<tr id="order-form-footer">
    				<td colspan="7">
    					<a class="btn btn-info add-row" href="javascript:void(0)">Add Note</a>
    					<span class="limit-msg" style="display:none;">You can only submit 25 notes at a time.</span>
    					<strong class="pull-right">Total Declared Value</strong>
    				</td>
    				<td>
    					<div class="input-prepend" style="width:100px;">
			  				<span class="add-on">$</span>
			  				<input class="span8" style="width:50px;" name="total_declared_value" type="text" disabled value="<?php if(isset($data['total_declared_value'])){ echo $data['total_declared_value'];}?>"/>
						</div>
    				</td>
    				<td>&nbsp;</td>
    			</tr>
    			<tr>
	    			<td colspan="8">
    					<strong class="pull-right">Sub Total</strong>
    				</td>
    				<td>
    					<div class="input-prepend" style="width:100px;">
			  				<span class="add-on">$</span>
			  				<input class="span8" name="sub_total" style="width:50px;" type="text" disabled value="<?php if(isset($data['sub_total'])){ echo $data['sub_total'];}?>"/>
						</div>
    				</td>
    			</tr>
    			<tr>
	    			<td colspan="8">
    					<strong class="pull-right">Postage</strong>
	    				<span class="pull-right" style="margin-right:15px;">
			    			<label class="checkbox">
								<input type="checkbox" name="pickup" id="pickup"  style="width:50px;" value="yes" <?php if(isset($data['postage']) && empty($data['postage'])){?> checked="checked"<?php }?>/>
								I will pick up this order
							</label>
		    			</span>
    				</td>
    				<td>
    					<div class="input-prepend" style="width:100px;">
			  				<span class="add-on">$</span>
			  				<input class="span8" name="postage" type="text"  style="width:50px;"  disabled value="<?php if(isset($data['postage'])){ echo $data['postage'];}?>"/>
						</div>
    				</td>
    			</tr>
    			<tr>
	    			<td colspan="8">
    					<strong class="pull-right">Total</strong>
    				</td>
    				<td>
    					<div class="input-prepend" style="width:100px;">
			  				<span class="add-on">$</span>
			  				<input class="span8" name="total"  style="width:50px;"  type="text" disabled value="<?php if(isset($data['total'])){ echo $data['total'];}?>"/>
						</div>
    				</td>
    			</tr>
    		</table>
			<strong>Postage</strong>
    		<table id="postage" class="table table-bordered">
    			<thead>
    				<tr>
    					<th>Total Value</th>
		    			<?php foreach($postage as $p){?>
		    				<th>Under $<?php echo money_format('%i', $p['under_value']);?></th>
		    			<?php }?>
		    			<th>Anything Over</th>
    				</tr>
    			</thead>
    			<tr>
    				<td>Additional Cost</td>
	    			<?php foreach($postage as $p){?>
	    				<td>$<?php echo money_format('%i', $p['cost']);?></td>
	    			<?php }?>
	    			<th>Please email</th>
    			</tr>
    		</table>
			<input type="submit" value="Continue" class="btn btn-primary pull-right submit-order"/>
		</div>
	</fieldset>
</form>

</div>