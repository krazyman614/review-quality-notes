<div class="header_row">
	<div class="header_row_inner">
	New Submission
	</div>
</div>

<div style="padding: 40px 70px 70px 70px;">

<fieldset style="padding-left:10px;">
	<?php if(isset($regions)){?>
		<?php foreach($regions as $region){?>
			<a id="goto-<?php echo $region['id']; ?>" href="/submissions/new_submission/<?php echo $region['id'];?>" class="btn btn-large"><?php echo $region['name'];?></a>
		<?php }?>
	<?php }?>
</fieldset>

</div>