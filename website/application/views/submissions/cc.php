<script type="text/javascript">
	$(function(){
		$('#shipping').click(function(){
			if($(this).is(':checked')){
				$('[name="address"]').val("<?php echo $submission['address'];?>");
				$('[name="address2"]').val("<?php echo $submission['address2'];?>");
				$('[name="city"]').val("<?php echo $submission['city'];?>");
				$('[name="state_province"]').val("<?php echo $submission['state_province'];?>");
				$('[name="postal_code"]').val("<?php echo $submission['postal_code'];?>");
				$('[name="country"]').val("<?php echo $submission['country'];?>");
			}
			$('#account').prop('checked', false);
		});

		$('#account').click(function(){
			if($(this).is(':checked')){
				$('[name="address"]').val("<?php echo $user['address'];?>");
				$('[name="address2"]').val("<?php echo $user['address2'];?>");
				$('[name="city"]').val("<?php echo $user['city'];?>");
				$('[name="state_province"]').val("<?php echo $user['state_province'];?>");
				$('[name="postal_code"]').val("<?php echo $user['postal_code'];?>");
				$('[name="country"]').val("<?php echo $user['country'];?>");
			}
			$('#shipping').prop('checked', false);
		});

		$('#select-credit').click(function(){
			if($(this).is(':checked')){
				$('#pay-credit').show();
				$('#pay-check').hide();
				$('#pay-all').show();
			}
		});

		$('#select-check').click(function(){
			if($(this).is(':checked')){
				$('#pay-credit').hide();
				$('#pay-check').show();
				$('#pay-all').show();
			}
		});
		<?php if(!isset($data['payment_type_id']) || $data['payment_type_id'] == 1){?>
			$('#select-credit').attr('checked', true);
			$('#select-credit').click();	
		<?php }?>
		<?php if(isset($data['payment_type_id']) && $data['payment_type_id'] == 2){?>
			$('#select-check').attr('checked', true);
			$('#select-check').click();
		<?php } ?>

		$('#create-form').submit(function(){
			$('#display-error').hide();
			$('#errors-list').empty();
			$('.error').removeClass('error');

			var type =  ($('#pay-check').is(':checked')) ?
				'credit' :
				'check';
			
			$(this).find('input').each(function(){
				var inpt = $(this);				
				var name = inpt.attr('name');
				switch(name){
				case 'address2':
				case 'same_as_shipping':
					break;
				default:
					if($.trim(inpt.val()) == ''){
						if ((type=='credit' && name!='routing_number' && name!='full_name') || 
								(type=='check' && name=='routing_number' && name=='full_name')) { //need to add more for check
						inpt.parent().addClass('error');
						$('#errors-list').append('<li>Missing ' + name.replace('_', ' ') + '</li>');
						}
					}			
					break;
				}
			});
			if($(this).find('.error').length == 0)
				return true;
			else{
				$('#display-error').show();
				return false;
			}
		});

		<?php if(isset($data['same_as_shipping'])&&$data['same_as_shipping']===true) {?>
			$('[name="address"]').val("<?php echo $submission['address'];?>");
			$('[name="address2"]').val("<?php echo $submission['address2'];?>");
			$('[name="city"]').val("<?php echo $submission['city'];?>");
			$('[name="state_province"]').val("<?php echo $submission['state_province'];?>");
			$('[name="postal_code"]').val("<?php echo $submission['postal_code'];?>");
			$('[name="country"]').val("<?php echo $submission['country'];?>");
			$('#shipping').click();
			$('#shipping').val('checked');
		<?php }?>
	});

</script>

<div class="header_row">
	<div class="header_row_inner">
	New Submission
	</div>
</div>

<div style="padding: 40px 70px 70px 70px;">

<form method="post" id="create-form" action="/submissions/charge/<?php echo $submission['id'];?>">
	<fieldset style="padding-left:20px;">

		<div id="display-error"  class="container alert alert-error" style="margin-left:0px;<?php if(!isset($errors) || count($errors) <= 0){?> display:none;<?php }?>">
	    	Please correct the following errors:
	    	<ul id="errors-list">
	    	<?php if(isset($errors) && count($errors) > 0){?>
	    	<?php foreach($errors as $key=>$value){?>
	    		<li><?php echo $value;?></li>
	    	<?php }?>
	    	<?php }?>
	    	</ul>
	    </div>
		<div class="container" style="margin-left:0px;">
			<div class="row">
				<a class="btn btn-primary pull-right" style="margin-right:5px;" href="/submissions/new_submission/<?php echo $submission['region_id'] ?>/<?php echo $submission['id'] ?>">Back</a>
			</div>
			<?php if ($can_pay_by_check) {?>
		  	<div id="selection" class="row" style="margin-bottom:15px;border-bottom:1px solid #ccc;padding-bottom:15px;margin-left:0px;">
		  		<label style="font-size: 21px;line-height:35px;">Payment Type</label>
		  		<label class="radio inline"><input id="select-credit" type="radio" name="payment_type_id" value="1"/><strong>Credit</strong></label>
		  		<label class="radio inline"><input id="select-check" type="radio" name="payment_type_id" value="2"/><strong>Check</strong></label>
		  	</div>
		  	<?php } else {?>
		  	<input type="hidden" name="payment_type_id" value="1" />
		  	<?php }?>
		    <div id="pay-credit" class="row">
			    <div class="row">
			    	<div class="span8">
						<div class="control-group<?php if(isset($errors['address']) || isset($errors['address2']) || isset($errors['city']) || isset($errors['state']) || isset($errors['postal_code']) || isset($errors['country'])){ ?> error<?php }?>">
							<label style="font-size: 21px;">Billing Credit Card Address</label>
							<label class="checkbox">
							<br>
								<input type="checkbox" name="same_as_shipping" id="shipping" value="">
								Same as Shipping
							</label>
							<label class="checkbox">
								<input type="checkbox" name="same_as_account" id="account" value="">
								Same as Account
							</label>
							<br>
						    <div class="controls controls-row" style="padding:0px !important;">
						    	<div class="span2">
								<label>Address</label>
								<input type="text" name="address" class="span12" class="span12" value="<?php if(isset($data['address'])){ echo $data['address'];}?>" style="width:360px"/>
								</div>
							</div>
						    <div class="controls controls-row" style="padding:0px !important;">
						    	<div class="span2">
								<label>Address Line 2</label>
								<input type="text" name="address2" class="span12" value="<?php if(isset($data['address2'])){ echo $data['address2'];}?>" style="width:360px"/>
								</div>
							</div>

						    <div class="controls controls-row" style="padding:0px !important;">
						    	<div class="span2">
						    		<label>City</label>
									<input type="text" name="city" class="span6" value="<?php if(isset($data['city'])){ echo $data['city'];}?>" style="width:130px"/>
								</div>
								<div class="span1">
									<label>State</label>
									<input type="text" name="state_province" class="span5"  maxlength="2" value="<?php if(isset($data['state_province'])){ echo $data['state_province'];}?>" style="width:60px"/>
								</div>
								<div class="span3">
									<label>Zip</label>
									<input type="text" name="postal_code"  maxlength="10" class="span4" value="<?php if(isset($data['postal_code'])){ echo $data['postal_code'];}?>" style="width:120px;"/>
								</div>
							</div>
						    <div class="controls controls-row" style="padding:0px !important;">
						    	<div class="span2">
								<label>Country</label>
								<select name="country" class="postage-country">
								<?php foreach($countries as $country){?>
									<option value="<?php echo $country['name']?>"<?php if((isset($data['country']) && $data['country'] == $country['name']) || $submission['country'] == $country['name']){?> selected="selected"<?php }?>><?php echo $country['name']?></option>
								<?php }?>
								</select>
								</div>
							</div>
						</div>
			   		</div>
			   </div>
				<div class="controls controls-row" style="padding:0px !important;min-width:650px !important">
			    	<div class="span4" style="padding:0px !important;min-width:650px !important">
						<div class="control-group<?php if(isset($errors['type'])){ ?> error<?php }?>">
							<label>Credit Card Type</label>
							<select name="type">
								<option value="VISA" <?php if(isset($data['type']) && $data['type'] == 'Visa'){ echo 'selected';}?>>Visa</option>
								<option value="MasterCard" <?php if(isset($data['type']) && $data['type'] == 'MasterCard'){ echo 'selected';}?>>Master Card</option>
								<option value="Discover" <?php if(isset($data['type']) && $data['type'] == 'Discover'){ echo 'selected';}?>>Discover</option>
								<option value="Amex" <?php if(isset($data['type']) && $data['type'] == 'Amex'){ echo 'selected';}?>>American Express</option>
							</select>
						</div>
						<div class="control-group<?php if(isset($errors['name'])){ ?> error<?php }?>">
							<label>Name on Card</label>
							<input type="text" name="name" class="span8" placeholder="Name on Card" value="<?php if(isset($data['name'])){ echo $data['name'];}?>" style="width:300px;"/>
						</div>
						<br><br>
						<div class="control-group<?php if(isset($errors['cc'])){ ?> error<?php }?>">
							<label>Credit Card Number</label>
							<input type="text" name="cc" class="span6" maxLength="16" placeholder="Credit Card Number"  value="<?php if(isset($data['cc'])){ echo $data['cc'];}?>" style="width:300px;"/>
						</div>
						<br><br>
						<div class="control-group<?php if(isset($errors['cvv'])){ ?> error<?php }?>" style="float:left;margin-right:5px;">
							<label>CVV</label>
							<input type="text" name="cvv" class="span5" maxLength="4" placeholder="Credit Card Code"  value="<?php if(isset($data['cvv'])){ echo $data['cvv'];}?>" style="max-width:90px;"/>
						</div>
						<div class="control-group<?php if(isset($errors['exp_month']) || isset($errors['exp_year'])){ ?> error<?php }?>" style="width:200px;float:left;">
							<label>Expiration Date</label>
							<select name="exp_month" style="width:90px;">
								<?php for($itt=1; $itt <= 12; $itt++){?>
								<option value="<?php echo $itt;?>" <?php if(isset($data['exp_month']) && $data['exp_month'] == $itt){ echo 'selected';}?>><?php echo $itt;?></option>						
								<?php }?>
							</select>
							&nbsp;<font style="font-size:14px;">/<font>
							<select name="exp_year" style="width:90px;">
								<?php 
								$y = date('Y');
								for($itt = $y; $itt <= $y + 12; $itt++){?>
								<option value="<?php echo $itt;?>" <?php if(isset($data['exp_year']) && $data['exp_year'] == $itt){ echo 'selected';}?>><?php echo $itt;?></option>						
								<?php }?>
							</select>					
						</div>
						<div style="clear:both;"></div>
					</div>
				</div>		
				<div class="row">
					<input type="submit" value="Place Order" class="btn btn-primary pull-right"/>
					<a class="btn btn-primary pull-right" style="margin-right:5px;" href="/submissions/new_submission/<?php echo $submission['region_id'] ?>/<?php echo $submission['id'] ?>">Back</a>
				</div>
			</div>
	    	<?php if ($can_pay_by_check) {?>
			<div id="pay-check" class="row">
		    	<div class="alert alert-info">
		    		Don't forget to send your check with your submission.
		    	</div>
				<div class="row">
					<input type="submit" value="Place Order" class="btn btn-primary pull-right"/>
					<a class="btn btn-primary pull-right" style="margin-right:5px;" href="/submissions/new_submission/<?php echo $submission['region_id'] ?>/<?php echo $submission['id'] ?>">Back</a>
				</div>
			</div>
	    	<?php }?>
			<div id="pay-all">
			</div>
		</div>
	</fieldset>
</form>

</div>