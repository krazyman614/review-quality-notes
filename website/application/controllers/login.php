<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends PMA_Controller {
	
	function __construct(){
			parent::__construct();
	}

	public function index()
	{
		$this->template->write_view('content', 'login', array());
		$this->template->render();
	}
	
	public function login_user(){
		$errors = array();
		$email = $this->input->post('email');
		if(empty($email)) $errors['email'] = 'Missing email';
		$password = $this->input->post('password');
		if(empty($password)) $errors['password'] = 'Missing password';
		
		$this->load->model('user');
		$user = $this->user->by_email_password($email, $password);
		if($user !== false){
			$this->load->model('User_login','userlogin',TRUE); //...Why do I always have issues that appear randomly?  Didn't edit any files, simply rebuilt db, and this model broke (no other model did).  --James
			$user_login = $this->userlogin->create($user);
			if($user_login !== false){
				$this->session->set_userdata('login_id', $user_login->id);
				$this->load->helper('url');
				redirect('/submissions');
				exit();
			}
			else {
				$errors['user_login'] = 'Invalid user login';				
			}
		}
		else {
			$errors['user'] = 'Invalid user/password';
		}
		
		if(count($errors) > 0){
			$this->template->write_view('content', 'login', array('errors' => $errors, 'data' => array('email' => $email, 'password' => $password)));
		}
		$this->template->render();		
	}
	
	public function logout(){
		$this->session->unset_userdata('login_id');
		$this->load->helper('url');
		redirect('/home');		
	}
}

/* End of file login.php */
/* Location: ./application/controllers/login.php */