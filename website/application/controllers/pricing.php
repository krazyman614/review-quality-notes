<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pricing extends PMA_Controller {
	
	function __construct(){
			parent::__construct();
	}
	
	public function index($region_id='') {
		$region_id = strtolower($region_id);
	
		$this->load->model('region');
		$region = $this->region->by_id(empty($region_id) ? array() : $region_id);
	
		$this->load->model('service_level');
		$service_levels = $this->service_level->get();
		/*
			$this->load->model('note_type');
		$note_types = $this->note_type->get($region_id);
	
		$this->load->model('grading_service');
		$grading_services = $this->grading_service->get();
	
		$this->load->model('postage_country');
		$countries = $this->postage_country->get();
	
		$data =  (array) $this->get_user();
	
		$this->load->model('postage');
		$postage = $this->postage->get_postage($this->get_user()->country);
		*/
		$page_data = array(
				'access_token' => $this->get_access_token(),
				'region' => $region,
				'service_levels' => $service_levels/*,
				'note_types' => $note_types,
		'grading_services' => $grading_services,
		'data' => $data,
		'countries' => $countries,
		'postage' => $postage*/
		);
			
		//$data = (array)$this->get_user();
		$this->template->write_view('content', 'pricing', $page_data);
	
		$this->template->render();/*
		//		if(empty($country)) $this->return_json(1, "Invalid country");
		$this->load->model('postage');
		$postage = $this->postage->get_postage($country);
		$formatted = array();
		foreach($postage as $p){
		$p['under_value_formatted'] = money_format('%i', $p['under_value']);
		$p['cost_formatted'] = money_format('%i', $p['cost']);
		array_push($formatted, $p);
		}
		$this->return_json(0, array('postage' => $formatted));		*/
	}
}

/* End of file pricing.php */
/* Location: ./application/controllers/pricing.php */