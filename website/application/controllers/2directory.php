<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Directory extends PMA_Controller {
	
	function __construct(){
			parent::__construct();
	}

	public function index()
	{
		$this->template->write_view('content', 'directory');
		$this->template->render();
	}
}

