<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Accepted_Notes extends PMA_Controller {
	
	function __construct(){
			parent::__construct();
	}
	
	public function index($region_id = '', $page_size = -1, $page_number = -1) {
		//$this->load->model('/admin/application/models/note_type');
		$this->load->model('accepted_note');
		$count = $this->accepted_note->count(array('region_id' => $region_id));
		$note_types = $this->accepted_note->search(array('region_id' => $region_id, 'page_size' => $page_size, 'page_number' => $page_number));
		$this->load->model('region');
		$regions = $this->region->search(array());
		$this->template->write_view('content', 'accepted_notes', array('region_id' => $region_id, 'count' => $count, 'note_types' => $note_types, 'page_size' => $page_size, 'page_number' => $page_number, 'regions' => $regions));
		$this->template->render();
	}
}