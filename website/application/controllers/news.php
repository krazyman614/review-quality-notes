<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News extends PMA_Controller {
	
	function __construct(){
			parent::__construct();
	}

	public function index()
	{
		$this->load->model('news_item');
		$news_items = $this->news_item->get(-1);
		
		$this->template->write_view('content', 'news/index', array('news_items' => $news_items));
		$this->template->render();
	}
	
	public function view($news_item_id){
		$this->load->model('news_item');
		$news_item = $this->news_item->news_item->by_id($news_item_id);
		if($news_item == false)
			$this->template->write_view('content', 'news/index', array('news_items' => $news_items));
		else {
			$this->template->write_view('content', 'news/view', array('news_item' => $news_item));			
		}
		$this->template->render();
	}
}

/* End of file news.php */
/* Location: ./application/controllers/news.php */