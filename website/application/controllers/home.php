<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends PMA_Controller {
	
	function __construct(){
			parent::__construct();
	}

	public function index()
	{
		$this->load->model('news_item');
		$news_items = $this->news_item->get(5);
		
		$this->load->model('special');
		$specials = $this->special->get(5);
		
		$this->template->write_view('content', 'home', array('news_items' => $news_items, 'specials'=>$specials));
		$this->template->render();
	}
	
//Didn't feel these were worthy of own controller
	public function standards_and_legal() {
		$this->template->write_view('content', 'standards_and_legal');
		$this->template->render();
	}

	public function learn_more() {
		$this->template->write_view('content','learn_more',array());
		$this->template->render();
	}
	public function directory() {
		
		
		$this->template->write_view('content','directory',array());
		$this->template->render();
		
		
	}
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */