<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sticker extends PMA_Controller {
	
	function __construct(){
			parent::__construct();
	}

	public function index(){
		$this->template->write_view('content', 'sticker/sticker_search', array('opts' => array(), 'notes' => array()));
		$this->template->render();
	}
	
	
	public function search_results(){
		$opts = array(
			'barcode' => $this->input->get_post('barcode'),
			'catalog_number' => $this->input->get_post('catalog_number'),
			'serial_number' => $this->input->get_post('serial_number')
		);
		
		$note = false;
		$this->load->model('submission_note');
		if(!empty($opts['barcode'])){
			$note = $this->submission_note->by_barcode($opts['barcode'], true);
		}
		else if(!empty($opts['catalog_number']) && !empty($opts['serial_number'])){
			$note = $this->submission_note->by_catalog_serial($opts['catalog_number'], $opts['serial_number'], true); 
		}
		
		$submission = false;
		
		if (!empty($note['submission_id'])) {
			$this->load->model('submission');
			$submission = $this->submission->by_id($note['submission_id']);
		}
		
		$this->template->write_view('content', 'sticker/sticker_search', compact('opts', 'note', 'submission'));
		$this->template->render();
	}
}

/* End of file Sticker.php */
/* Location: ./application/controllers/sticker.php */
