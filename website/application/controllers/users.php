<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends PMA_Controller {
	
	function __construct(){
			parent::__construct(self::VALIDATION_OPTIONAL);
	}
	
//views
	public function index()
	{
		$this->load->model('postage_country');
		$countries = $this->postage_country->get();
		
		$user = $this->get_user();
		if(empty($user))
			$this->template->write_view('content', 'user/create', array('data' => array('country' => 'USA'), 'countries' => $countries));
		else {
			$user = (array) $user;
			unset($user['password']);
			$this->template->write_view('content', 'user/maintain', array('data' => $user, 'countries' => $countries));
		}
		$this->template->render();
	}
	
	public function forgot_password(){
		$this->template->write_view('content', 'user/forgot_password');
		$this->template->render();
	}
	
//Modifiers
	public function create(){
		//Everything until next comment is form validation and data population
		$errors = array();
		$name = $this->input->post('name');
		if(empty($name))
			$errors['name'] = 'Missing name';
		
		$email = $this->input->post('email');
		if(empty($email))
			$errors['email'] = 'Missing email';
		else if(!filter_var($email, FILTER_VALIDATE_EMAIL))
			$errors['email'] = 'Invalid email';
		else {
			$this->load->model('user');
			if($this->user->by_email($email) !== false)
				$errors['email'] = 'Email already exists';
		}
		$password = $this->input->post('password');
		$confirm_password = $this->input->post('confirm_password');
		if(empty($password) || empty($confirm_password)){
			if(empty($password))
				$errors['password'] = 'Missing password';
			if(empty($confirm_password))
				$errors['confirm_password'] = 'Missing confirmation password';
		}
		elseif($password != $confirm_password){
				$errors['password'] = "Passwords don't match";
				$errors['confirm_password'] = "Passwords don't match";
		}
		
		$company_name = $this->input->post('company_name');
		if(empty($company_name))
			$company_name = "";
		
		$address = $this->input->post('address');
		if(empty($address))
			$errors['address'] = 'Missing address';
		
		$address2 = $this->input->post('address2');
		
		$city = $this->input->post('city');
		if(empty($city))
			$errors['city'] = 'Missing city';
		
		$state_province = $this->input->post('state_province');
		if(empty($state_province))
			$errors['state_province'] = 'Missing state';
		
		$postal_code = $this->input->post('postal_code');
		if(empty($postal_code))
			$errors['postal_code'] = 'Missing postal code';
		
		$country = $this->input->post('country');
		if(empty($country))
			$errors['country'] = 'Missing country';
		
		$phone_number = $this->input->post('phone_number');
		if(empty($phone_number))
			$errors['phone_number'] = 'Missing phone number';
		else if(strlen($phone_number) < 10)		
			$errors['phone_number'] = 'Invalid phone number';
				
		$alt_phone_number = $this->input->post('alt_phone_number');
			
		$accepted_terms = $this->input->post('accepted_terms');
		if(empty($accepted_terms))
			$errors['accepted_terms'] = 'Did not accept terms';
		
		$data = array(
			'name' => $name,
			'email' => $email,
			'password' => $password,
			'company_name' => $company_name,
			'address' => $address,
			'address2' => $address2,
			'city' => $city,
			'state_province' => $state_province,
			'postal_code' => $postal_code,
			'country' => $country,
			'phone_number' => $phone_number,
			'alt_phone_number' => $alt_phone_number,
			'accepted_terms' =>$accepted_terms
		);
		//Done grabbing input
			
		if(count($errors) === 0){
			$user = $this->user->create($data);
			if($user === false){
				$errors['user'] = 'Error creating user';
			}
		}
		
		if(count($errors) === 0){
			//send email confirmation of account creation
			$message = "Thank you ".$name." for registering with Review Quality Notes.  If you did not register this account, please contact <a href=\"mailto:info@rqnotes.com\">info@rqnotes.com</a>";
			$this->send_email($email, $message, 'Registration for Review Quality Notes');
			$this->template->write_view('content', 'login', array('email' => $email, 'password' => $password, 'creation_success_alert'=>true));
		}
		else {
			$this->load->model('postage_country');
			$countries = $this->postage_country->get();
			$data['confirm_password'] = $confirm_password;
			$this->template->write_view('content', 'user/create', array('errors' => $errors, 'data' => $data, 'countries' => $countries));
		}
		$this->template->render();
	}
	
	public function edit(){
		//Everything until the next comment is validation and populating the data to be updated.
		//Front end is pre-populated with the data, so we update every field with the incoming data. 
		$errors = array();
		$id = $this->input->post('id');
		if(empty($id))
			$errors['id'] = "Missing id, cannot edit";
		
		$name = $this->input->post('name');
		if(empty($name))
			$errors['name'] = 'Missing name';
	
		$email = $this->input->post('email');
		if(empty($email))
			$errors['email'] = 'Missing email';
		else if(!filter_var($email, FILTER_VALIDATE_EMAIL))
			$errors['email'] = 'Invalid email';
		else {
			$this->load->model('user');
			if($this->user->by_email($email, $id) !== false)
				$errors['email'] = 'Email already exists';
		}
		$password = $this->input->post('password');
		$confirm_password = $this->input->post('confirm_password');
		if(!empty($password) || !empty($confirm_password)){
			if(empty($password))
				$errors['password'] = 'Missing password';
			if(empty($confirm_password))
				$errors['confirm_password'] = 'Missing confirmation password';
			if($password != $confirm_password){
				$errors['password'] = "Passwords don't match";
				$errors['confirm_password'] = "Passwords don't match";
			}
		}
	
		$company_name = $this->input->post('company_name');
		if(empty($company_name))
			$company_name = "";
	
		$address = $this->input->post('address');
		if(empty($address))
			$errors['address'] = 'Missing address';
	
		$address2 = $this->input->post('address2');
	
		$city = $this->input->post('city');
		if(empty($city))
			$errors['city'] = 'Missing city';
	
		$state_province = $this->input->post('state_province');
		if(empty($state_province))
			$errors['state_province'] = 'Missing state';
	
		$postal_code = $this->input->post('postal_code');
		if(empty($postal_code))
			$errors['postal_code'] = 'Missing postal code';
	
		$country = $this->input->post('country');
		if(empty($country))
			$errors['country'] = 'Missing country';
	
		$phone_number = $this->input->post('phone_number');
		if(empty($phone_number))
			$errors['phone_number'] = 'Missing phone number';
	
		$alt_phone_number = $this->input->post('alt_phone_number');
			
		$accepted_terms = $this->input->post('accepted_terms');
		if(empty($accepted_terms))
			$errors['accepted_terms'] = 'Did not accept terms';
	
		//We could wrap these following two giant assignments into the first "if errors===0" block
		$data = array(
				'id' => $id,
				'name' => $name,
				'email' => $email,
				'company_name' => $company_name,
				'address' => $address,
				'address2' => $address2,
				'city' => $city,
				'state_province' => $state_province,
				'postal_code' => $postal_code,
				'country' => $country,
				'phone_number' => $phone_number,
				'alt_phone_number' => $alt_phone_number,
				'accepted_terms' =>$accepted_terms,
				'updated_uid' => $this->get_user()->id
		);
		/*
		$updates_string = "Your account has been updated to the following.<br />".
				'name' .": " . $name.',<br /> '.
				'email' .": " . $email.',<br /> '.
				'password' .": " . $password.',<br /> '.
				'company_name' .": " . $company_name.',<br /> '.
				'address' .": " . $address.',<br /> '.
				'address2' .": " . $address2.',<br /> '.
				'city' .": " . $city.',<br /> '.
				'state_province' .": " . $state_province.',<br /> '.
				'postal_code' .": " . $postal_code.',<br /> '.
				'country' .": " . $country.',<br /> '.
				'phone_number' .": " . $phone_number.',<br /> '.
				'alt_phone_number' .": " . $alt_phone_number;*/
		
		if(!empty($password))
			$data['password'] = $password;
	
	
		if(count($errors) === 0){
			$user = $this->user->edit($data);
			if($user === false){
				$errors['user'] = 'Error creating user';
			}
		}
	
		$this->load->model('postage_country');
		$countries = $this->postage_country->get();
		
		if(count($errors) === 0){
			$message="This is a notification that changes have been made to your Review Quality Notes account.  If you did not make these changes, please contact <a href=\"mailto:info@rqnotes.com\">info@rqnotes.com</a>";
			$this->send_email($data["email"],$message,'Changes to your Review Quality Notes Account');
			$this->template->write_view('content', 'user/maintain', array('success' => true, 'data' => $data, 'countries' => $countries));
		}
		else {
			$data['confirm_password'] = $confirm_password;
			$this->template->write_view('content', 'user/maintain', array('errors' => $errors, 'data' => $data, 'countries' => $countries));
		}
		$this->template->render();
	}
	
	public function send_password_reset() {
		$email = $this->get_input('email', true);
		
		$this->load->model('user');
		$ret = $this->user->send_password_reset($email);
		if(!empty($ret)){
			$this->send_email($ret['email'], $ret['message'], 'Forgot Password');
			$this->template->write_view('content','user/forgot_password_sent',$ret); //I'll clean this up once it's working, but right now 0 is code 1 is msg, and if 3 exists its data
		}
		else {
			$this->template->write_view('content', 'user/forgot_password', array('email'=> $email, 'error' => 'User not found, please check your email address'));
		}
		$this->template->render();
	}
	
	public function reset_password($user_id,$hashed_password,$email) {
		$email = urldecode($email);
		$this->load->model('user');
		$ret = $this->user->reset_password($user_id,$hashed_password,$email);
		if($ret['new_password']){
			$this->send_email($ret['email'], 'Your new password is '.$ret['new_password'],'Review Quality Notes Password Reset');
		}
		$this->template->write_view('content','user/password_reset_confirm',$ret);
		$this->template->render();
	}
	
	
}

/* End of file user.php */
/* Location: ./application/controllers/user.php */