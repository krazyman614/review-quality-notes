<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tour extends PMA_Controller {
	
	function __construct(){
			parent::__construct();
	}

	public function index()
	{
		$this->template->write_view('content', 'tour', array());
		$this->template->render();
		
	}
}

/* End of file faq.php */
/* Location: ./application/controllers/faq.php */