<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Postage_Rates extends PMA_Controller {
	
	function __construct(){
		parent::__construct();
	}

	public function get($country)
	{
		if(empty($country)) $this->return_json(1, "Invalid country");
		$this->load->model('postage');
		$postage = $this->postage->get_postage($country);
		$formatted = array();
		foreach($postage as $p){
			$p['under_value_formatted'] = money_format('%i', $p['under_value']);
			$p['cost_formatted'] = money_format('%i', $p['cost']);
			array_push($formatted, $p);
		}
		$this->return_json(0, array('postage' => $formatted));
	}
}

/* End of file postage_rates.php */
/* Location: ./application/controllers/postage_rates.php */