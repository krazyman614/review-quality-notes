<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Submissions extends PMA_Controller {
	const EMAIL = 'info@rqnotes.com';
	
	function __construct(){
			parent::__construct(self::VALIDATION_YES);
	}

	private function send_recieved_email($submission){
		/*
		$html = "Your order has been placed<br/>";						
		$html .= "MAIL YOUR SUBMISSION TO:<br/>";
		$html .= "RQN INC.<br/>";
		$html .= "PO BOX 40418<br/>";
		$html .= "St, Petersburg, Fl 33743-0418<br/>";
		$html .= "<br/>";
		
		$title = 'Order #' . $submission['order_number'];
		*/
			
		$this->load->model('user');
		$user = $this->user->by_id($submission['created_uid']);
		
		// retrieve the submission formatted so we can build our email with it
		$notes = &$submission['notes'];
			
		$total_service_cost = 0;
			
		foreach ($notes as $note) {
			$total_service_cost += $note['service_cost'];
		}
			
		$shipping = 'Pickup';
			
		if (!empty($submission['fedex_account_number'])) {
			$shipping = 'Fedex Acct: ' . $submission['fedex_account_number'];
		} elseif (!empty($submission['postage'])) {
			$shipping = 'Registered Mail: $' . money_format('%i', $submission['postage']);
		}
			
		$this->load->model('service_level');
		$level = $this->service_level->by_id($notes[0]['service_level_id']);
			
		$page_data = compact('submission', 'notes', 'user', 'shipping', 'level', 'total_service_cost');
			
		// get the content to load into the template
		$content = $this->load->view('emails/submissions/new_submission', $page_data, true);
		$title = 'NOTE SUBMITTED';
			
		$page_data = compact('content', 'title');
			
		// load and get the template
		$html = $this->load->view('emails/template', $page_data, true);
		
		$this->load->library('email');
		
		$this->email->from(self::EMAIL, 'Review Quality Notes');
		$this->email->to($user->email);
		$this->email->subject($title);
		$this->email->message($html);
		$this->email->send();
	}

	public function index(){
		$this->load->model('submission');		
		$submissions = $this->submission->by_user($this->get_user());
		
		$this->template->write_view('content', 'submissions/track', array('submissions' => $submissions));
		$this->template->render();
	}
	
	public function view($submission_id){
		$success = $this->input->get_post('success');
		$this->load->model('submission');		
		$submission = $this->submission->by_id($submission_id, true);
		$this->template->write_view('content', 'submissions/view', array('success' => $success, 'submission' => $submission));
		$this->template->render();
	}
	
	public function new_submission($region_id = '', $id = '')
	{
		$region_id = strtolower($region_id);
		switch($region_id){
			case '':
				$this->load->model('region');
				$regions = $this->region->search(array());
				$this->template->write_view('content', 'submissions/new_submission', array('regions' => $regions));
				break;
			default:
				
				$this->load->model('region');
				$region = $this->region->by_id($region_id);
				
				$this->load->model('service_level');
				$service_levels = $this->service_level->get();
				
				$this->load->model('note_type');
				$note_types = $this->note_type->get($region_id);
				
				$this->load->model('grading_service');
				$grading_services = $this->grading_service->get();
				
				$this->load->model('postage_country');
				$countries = $this->postage_country->get();
				
				$notes = array();
				if(empty($id)){
					$data =  (array) $this->get_user();
				}
				else {
					$this->load->model('submission');
					$data = $this->submission->by_id($id, true, 50);
					if(isset($data['notes']) && count($data['notes']) > 0){
						$notes = $data['notes'];
						$note = $notes[0];
						$data['service_level'] = $note['service_level']['id'];
					}
				}
				
				$this->load->model('postage');
				$postage = $this->postage->get_postage($this->get_user()->country);
				
				$page_data = array(
					'access_token' => $this->get_access_token(),
					'region' => $region,
					'service_levels' => $service_levels, 
					'note_types' => $note_types, 
					'grading_services' => $grading_services,
					'data' => $data,
					'notes' => $notes,
					'countries' => $countries,
					'postage' => $postage
				);
			
				$data = (array)$this->get_user();
				$this->template->write_view('content', 'submissions/new_submission_form', $page_data);
				break;
		}
		$this->template->render();
	}
	
	public function create($region_id){
		$errors = array();
		$name = $this->input->post('name');
		if(empty($name))
			$errors['name'] = 'Missing name';
		
		$email = $this->input->post('email');
		if(empty($email))
			$errors['email'] = 'Missing email';
		else if(!filter_var($email, FILTER_VALIDATE_EMAIL))
			$errors['email'] = 'Invalid email';
		
		
		$company_name = $this->input->post('company_name');
		
		$address = $this->input->post('address');
		if(empty($address))
			$errors['address'] = 'Missing address';
		
		$address2 = $this->input->post('address2');
		
		$city = $this->input->post('city');
		if(empty($city))
			$errors['city'] = 'Missing city';
		
		$state_province = $this->input->post('state_province');
		if(empty($state_province))
			$errors['state_province'] = 'Missing state';
		
		$postal_code = $this->input->post('postal_code');
		if(empty($postal_code))
			$errors['postal_code'] = 'Missing postal code';
		
		$country = $this->input->post('country');
		if(empty($country))
			$errors['country'] = 'Missing country';
		
		$phone_number = $this->input->post('phone_number');
		if(empty($phone_number))
			$errors['phone_number'] = 'Missing phone number';
		else if(strlen($phone_number) < 10)
			$errors['phone_number'] = 'Invalid phone number';
		
		$fedex_account_number = $this->input->post('fedex_account_number');
		
		$sub_total = 0;
		$postage = 0;
		$total_declared_value = 0;
		
		$this->load->model('region');
		$region = $this->region->by_id($region_id);
		
		$data = array(
			'region_id' => $region_id,
			'name' => $name,
			'email' => $email,
			'company_name' => $company_name,
			'address' => $address,
			'address2' => $address2,
			'city' => $city,
			'state_province' => $state_province,
			'postal_code' => $postal_code,
			'country' => $country,
			'phone_number' => $phone_number,
			'fedex_account_number' => $fedex_account_number,
			'created_uid' => $this->get_user()->id,
			'updated_uid' => $this->get_user()->id
		);
		
		$notes = array();
		
		$service_level_id = $this->input->post('service_level');
		$this->load->model('service_level');
		$service_level = $this->service_level->by_id($service_level_id);
		if($service_level === false){
			$errors['service_level'] = 'Missing service level';
		}
		
		
		$note_types = $this->input->post('note_type');
		$note_countries = $this->input->post('note_country');
		$denominations = $this->input->post('denomination');
		$catalog_numbers = $this->input->post('catalog_number');
		$pps = $this->input->post('pp');
		$serial_numbers = $this->input->post('serial_number');
		$grading_services = $this->input->post('grading_service');
		$declared_values = $this->input->post('declared_value');
		if(count($denominations) > 0){
			$this->load->model('note_type');
			$this->load->model('grading_service');
			
			
			for($itt =0; $itt < count($denominations); $itt++){
				$note_type_id = null;
				$note_country = null;
				
				switch($region['id']){
					case 'us':
						$note_type_id = $note_types[$itt];
						$note_type = $this->note_type->by_id($note_types[$itt]);
						if($note_type === false){
							$errors['note_type-' . $itt] = 'Missing note type';
						}
						break;
					default:
						$note_country = $note_countries[$itt];
						if(empty($note_country)){
							$errors['note_country-' . $itt] = 'Missing note country';							
						}						
						break;
				}
				$denomination = $denominations[$itt];
				if(!is_numeric($denominations[$itt]) || doubleval($denomination) <= 0){
					$errors['denomination-' . $itt] = 'Missing denomination, denomination must be greater than 0';
				}
				$catalog_number = $catalog_numbers[$itt];
				if(empty($catalog_number)){
					$catalog_number = "";
				}
				$pp = $pps[$itt];
				if ($region_id == 'us') {
					if (!is_numeric($pp)) {
						$errors['pp-' . $itt] = 'The Grade must be a numeric value.';
					}
					if (strlen($pp) > 2) {
						$errors['pp-' . $itt] = 'The Grade can only be up to 2 digits.';
					}
				}
				
				$serial_number = $serial_numbers[$itt];
				if(empty($serial_number)){
					$serial_number = "";
				}
				if(empty($grading_services[$itt])){
					$errors['grading_service-' . $itt] ='Missing grading service';
				}
				else {
					$grading_service = $this->grading_service->by_id($grading_services[$itt]);
					if($grading_services === false){
						$errors['grading_service-' . $itt] ='Missing grading service';
					}
				}
				
				$declared_value = $declared_values[$itt];
				if(!is_numeric($declared_values[$itt]) || doubleval($declared_value) <= 0){
					$errors['declared_value-' . $itt] = 'Missing declared value, declared value must be greater than 0';
				}
				else {
					$total_declared_value += doubleval($declared_value);
				}
				
				$service_cost = 0;
				if($service_level == false || $service_level->per_note <= 0)
					$errors['service_cost-' . $itt] = 'Missing service cost, service cost must be greater than 0';
				else {
					$service_cost = $service_level->per_note;
				}
				
				$note = array(
					'sequence' => $itt+1,
					'service_level_id' => $service_level_id,
					'note_type_id' => $note_type_id,
					'note_country' => $note_country,
					'denomination' => $denomination,
					'catalog_number' => $catalog_number,
					'pp' => $pp,
					'serial_number' => $serial_number,
					'grading_service_id' => $grading_services[$itt],
					'declared_value' => $declared_value,
					'service_cost' => $service_cost,
					'created_uid' => $this->get_user()->id,
					'updated_uid' => $this->get_user()->id
				);
				
				array_push($notes, $note);
				
				$sub_total += $service_cost;
			}
			$data['total_declared_value'] = $total_declared_value;
		}
		else {
			$errors['notes'] = 'Please enter one note to submit';			
		}
		
		$postage = 0.0;
		$pickup = $this->input->get_post('pickup');
		if($pickup != 'yes' && empty($fedex_account_number)){
			$this->load->model('postage');
			$postage = $this->postage->get_postage_amount($country, $total_declared_value);
		}
		
		if($postage === false) {
			$errors['postage'] = 'Please email for postage';
		}
		else {
			$total = $sub_total + $postage;		
			$data['sub_total'] = $sub_total;
			$data['postage'] = $postage;
			$data['total'] = $total;
		}
		
		if(count($errors) === 0){
			$this->load->model('submission');
			$submission = $this->submission->create($data);
			if($submission === false){
				$errors['submission'] = 'Error creating submissions';
			}
			else {
				$this->load->model('submission_note');
				$submission['notes'] = $this->submission_note->create($submission, $notes);
				if($submission['notes'] === false){
					$errors['notes'] = 'Error creating submission notes';
				}
				else {
					$submission = $this->submission->by_id($submission['id'], true, 50);
				}
			}
		}
		
		if(count($errors) === 0){
			$this->load->model('postage_country');
			$countries = $this->postage_country->get();
			$can_pay_by_check = (bool) $this->get_user()->pay_by_check;
			//$url = '/submissions/view/' . $submission['id'] . '?success=1';
			//$this->template->write_view('content', 'redirect', array('url' => $url));
			$user = (array) $this->get_user();
			$this->template->write_view('content', 'submissions/cc', compact('submission', 'countries', 'can_pay_by_check', 'user'));
		}
		else {
			$this->load->model('service_level');
			$service_levels = $this->service_level->get();
			
			$this->load->model('note_type');
			$note_types = $this->note_type->get($region_id);
			
			$this->load->model('grading_service');
			$grading_services = $this->grading_service->get();
			
			$this->load->model('postage_country');
			$countries = $this->postage_country->get();
			
			if(!isset($data))
				$data =  (array) $this->get_user();
			else if(isset($service_level) && $service_level != false)
				$data['service_level'] = $service_level->id;	

			$this->load->model('postage');
			$postage = $this->postage->get_postage($this->get_user()->country);
			
			$page_data = array(
				'errors' => $errors,
				'access_token' => $this->get_access_token(),
				'region' => $region,
				'service_levels' => $service_levels, 
				'note_types' => $note_types, 
				'grading_services' => $grading_services,
				'data' => $data,
				'notes' => $notes,
				'countries' => $countries,
				'postage' => $postage,
				'service_level' => $service_level_id
			);
			$this->template->write_view('content', 'submissions/new_submission_form', $page_data);
		}
		$this->template->render();
	}
	
	public function charge($submission_id){
		$errors = array();
		
		$this->load->model('submission');
		$submission = $this->submission->by_id($submission_id, true, 50);
		
		$can_pay_by_check = (bool) $this->get_user()->pay_by_check;
		
		$payment_type = $this->input->post('payment_type_id');
		if($payment_type == 3){	
				$data = array(
					'total' => $submission['total'] - $submission['postage'],
					'postage' => 0,
					'payment_type_id' => $payment_type,
					'status_id' => 100,
					'updated_uid' => $this->get_user()->id		
				);
				
				$this->submission->update($submission, $data);
				$this->send_recieved_email($submission);
				
				$url = '/submissions/view/' . $submission['id'] . '?success=1';
				$this->template->write_view('content', 'redirect', array('url' => $url));	
		} elseif ($payment_type == 2) {
			if (!$can_pay_by_check) {
				$errors['payment_type_id'] = 'Not authorized to pay by check';
			}
		} else {
			
			$address = $this->input->post('address');
			if(empty($address))
				$errors['address'] = 'Missing address';
				
			$address2 = $this->input->post('address2');
				
			$city = $this->input->post('city');
			if(empty($city))
				$errors['city'] = 'Missing city';
				
			$state_province = $this->input->post('state_province');
			if(empty($state_province))
				$errors['state_province'] = 'Missing state';
				
			$postal_code = $this->input->post('postal_code');
			if(empty($postal_code))
				$errors['postal_code'] = 'Missing postal code';
				
			$country = $this->input->post('country');
			if(empty($country))
				$errors['country'] = 'Missing country';
			
			$type = $this->input->post('type');
			if(empty($type))
				$errors['type'] = 'Missing card type';
			
			$name = $this->input->post('name');
			if(empty($name))
				$errors['name'] = 'Missing name on card';
			
			$cc = $this->input->post('cc');
			if(empty($cc))
				$errors['cc'] = 'Missing credit card number';
			
			$cvv = $this->input->post('cvv');
			if(empty($cvv))
				$errors['cvv'] = 'Missing cvv';
			
			$exp_month = $this->input->post('exp_month');
			if(empty($exp_month))
				$errors['exp_month'] = 'Missing expiration month';
			elseif(!is_numeric($exp_month))
				$errors['exp_month'] = 'Expiration month must be a number';
			elseif($exp_month < 1 || $exp_month > 12)
				$errors['exp_month'] = 'Expiration month must be between 1 and 12';
			
			$exp_year = $this->input->post('exp_year');
			if(empty($exp_year))
				$errors['exp_year'] = 'Missing expiration year';
			elseif(!is_numeric($exp_year))
				$errors['exp_year'] = 'Expiration year must be a number';
			elseif($exp_year < date('Y', time()))
				$errors['exp_year'] = 'Expiration year must be greater than ' . date('Y', time());
			
			$y = intval(date('Y'));
			if($exp_year < $y)
				$errors['exp_year'] = 'Expiration year cannot be before today';
			else if($exp_year == $y){
				$m = date('n');
				if($exp_month < $m)
					$errors['exp_month'] = 'Expiration month cannot be before today';
			}
			
			
			if(count($errors) == 0){
				$cc = trim(str_ireplace(' ', '', str_ireplace('-', '', $cc)));
				
				if($cc != '1111111111111111'){
					// Load helpers
					$this->load->helper('url');
					
					// Load PayPal library
					$this->config->load('paypal');
					
					$config = array(
							'Sandbox' => $this->config->item('Sandbox'), 			// Sandbox / testing mode option.
							'APIUsername' => $this->config->item('APIUsername'), 	// PayPal API username of the API caller
							'APIPassword' => $this->config->item('APIPassword'), 	// PayPal API password of the API caller
							'APISignature' => $this->config->item('APISignature'), 	// PayPal API signature of the API caller
							'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
							'APIVersion' => $this->config->item('APIVersion')		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
					);
					
					// Show Errors
					if($config['Sandbox'])
					{
						error_reporting(E_ALL);
						ini_set('display_errors', '1');
					}
					
					$this->load->library('paypal/Paypal_pro', $config);
					
					//Charge credit card
					$DPFields = array(
							'paymentaction' => 'Sale',                         // How you want to obtain payment.  Authorization indidicates the payment is a basic auth subject to settlement with Auth & Capture.  Sale indicates that this is a final sale for which you are requesting payment.  Default is Sale.
							'ipaddress' => $_SERVER['REMOTE_ADDR'],                             // Required.  IP address of the payer's browser.
							'returnfmfdetails' => '1'                     // Flag to determine whether you want the results returned by FMF.  1 or 0.  Default is 0.
					);
					$CCDetails = array(
							'creditcardtype' => $type,                     // Required. Type of credit card.  Visa, MasterCard, Discover, Amex, Maestro, Solo.  If Maestro or Solo, the currency code must be GBP.  In addition, either start date or issue number must be specified.
							'acct' => $cc,                                 // Required.  Credit card number.  No spaces or punctuation.
							'expdate' => $exp_month . '' . $exp_year,      // Required.  Credit card expiration date.  Format is MMYYYY
							'cvv2' => $cvv,                                 // Requirements determined by your PayPal account settings.  Security digits for credit card.
							'startdate' => '',                             // Month and year that Maestro or Solo card was issued.  MMYYYY
							'issuenumber' => ''                            // Issue number of Maestro or Solo card.  Two numeric digits max.
					);
					
					$PayerInfo = array(
							'email' => $submission['email'],                                 // Email address of payer.
							'payerid' => '',                             // Unique PayPal customer ID for payer.
							'payerstatus' => '',                         // Status of payer.  Values are verified or unverified
							'business' => $submission['company_name']                             // Payer's business name.
					);
					
					list($fname, $lname) = explode(' ', $submission['name'], 2);
					$PayerName = array(
							'salutation' => '',                         // Payer's salutation.  20 char max.
							'firstname' => $fname,                             // Payer's first name.  25 char max.
							'middlename' => '',                         // Payer's middle name.  25 char max.
							'lastname' => $lname,                             // Payer's last name.  25 char max.
							'suffix' => ''                                // Payer's suffix.  12 char max.
					);
					
					$BillingAddress = array(
							'street' => $address,                         // Required.  First street address.
							'street2' => $address2,                         // Second street address.
							'city' => $city,                             // Required.  Name of City.
							'state' => $state_province,                             // Required. Name of State or Province.
							'countrycode' => substr($country, 0, 2),                     // Required.  Country code.
							'zip' => $postal_code,                             // Required.  Postal code of payer.
							'phonenum' => $submission['phone_number']                         // Phone Number of payer.  20 char max.
					);
					
					$ShippingAddress = array(
							'shiptoname' => $submission['name'],                     // Required if shipping is included.  Person's name associated with this address.  32 char max.
							'shiptostreet' => $submission['address'],                     // Required if shipping is included.  First street address.  100 char max.
							'shiptostreet2' => $submission['address2'],                     // Second street address.  100 char max.
							'shiptocity' => $submission['city'],                     // Required if shipping is included.  Name of city.  40 char max.
							'shiptostate' => $submission['state_province'],                     // Required if shipping is included.  Name of state or province.  40 char max.
							'shiptozip' => $submission['postal_code'],                         // Required if shipping is included.  Postal code of shipping address.  20 char max.
							'shiptocountry' => substr($submission['country'], 0, 2),                     // Required if shipping is included.  Country code of shipping address.  2 char max.
							'shiptophonenum' => $submission['phone_number']                    // Phone number for shipping address.  20 char max.
					);
					
					
					$OrderItems = array();

					$PaymentDetails = array(
							'amt' => $submission['total'],                            // Required.  Total amount of order, including shipping, handling, and tax.
							'currencycode' => 'USD',                     // Required.  Three-letter currency code.  Default is USD.
							'itemamt' => $submission['sub_total'],                         // Required if you include itemized cart details. (L_AMTn, etc.)  Subtotal of items not including S&H, or tax.
							'shippingamt' => $submission['postage'],                     // Total shipping costs for the order.  If you specify shippingamt, you must also specify itemamt.
							'shipdiscamt' => '',                     // Shipping discount for the order, specified as a negative number.
							'handlingamt' => '',                     // Total handling costs for the order.  If you specify handlingamt, you must also specify itemamt.
							'taxamt' => '',                         // Required if you specify itemized cart tax details. Sum of tax for all items on the order.  Total sales tax.
							'desc' => 'Review Quality Notes Reviewing',                             // Description of the order the customer is purchasing.  127 char max.
							'custom' => '',                         // Free-form field for your own use.  256 char max.
							'invnum' => $submission['order_number'],                         // Your own invoice or tracking number
							'notifyurl' => ''                        // URL for receiving Instant Payment Notifications.  This overrides what your profile is set to use.
					);
					
					foreach($submission['notes'] as $note){
						$Item  = array(
								'l_name' => '$' . money_format('%i', $note['denomination']) . ' note',                         // Item Name.  127 char max.
								'l_desc' => '',                         // Item description.  127 char max.
								'l_amt' => $note['service_cost'],                             // Cost of individual item.
								'l_number' => $note['id'],                         // Item Number.  127 char max.
								'l_qty' => '1',                             // Item quantity.  Must be any positive integer.
								'l_taxamt' => '',                         // Item's sales tax amount.
								'l_ebayitemnumber' => '',                 // eBay auction number of item.
								'l_ebayitemauctiontxnid' => '',         // eBay transaction ID of purchased item.
								'l_ebayitemorderid' => ''                 // eBay order ID for the item.
						);
						array_push($OrderItems, $Item);
					}
					
					$Secure3D = array(
			                      'authstatus3d' => '', 
			                      'mpivendor3ds' => '', 
			                      'cavv' => '', 
			                      'eci3ds' => '', 
			                      'xid' => ''
			                      );
			                      
				    $PayPalRequestData = array(
				                            'DPFields' => $DPFields, 
				                            'CCDetails' => $CCDetails, 
				                            'PayerInfo' => $PayerInfo, 
				                            'PayerName' => $PayerName, 
				                            'BillingAddress' => $BillingAddress, 
				                            'ShippingAddress' => $ShippingAddress, 
				                            'PaymentDetails' => $PaymentDetails, 
				                            'OrderItems' => $OrderItems, 
				                            'Secure3D' => $Secure3D
				                        );
				                        
				    $PayPalResult = $this->paypal_pro->DoDirectPayment($PayPalRequestData);
				    
				    if(!$this->paypal_pro->APICallSuccessful($PayPalResult['ACK']))
				    {
				    	$itt = 0;
				    	foreach($PayPalResult['ERRORS'] as $err){
				    		$errors['paypal-' . $itt++] = $err['L_LONGMESSAGE'];
				    	}
				    }
				}
			}
		}
				
		switch($payment_type){
			case 1: //CC
				if(count($errors) == 0){
					$data = array(
						'billing_address' => $address,
						'billing_address2' => $address2,
						'billing_city' => $city,
						'billing_state_province' => $state_province,
						'billing_postal_code' => $postal_code,
						'billing_country' => $country,
						'payment_type_id' => $payment_type,
						'card_type' => $type,
						'name_on_card' => $name,
						'card_last_four_numbers' => (strlen($cc) > 4 ? substr($cc, strlen($cc)-4, 4) : $cc),
						'card_exp_month' => $exp_month,
						'card_exp_year' => $exp_year,
						'status_id' => 100,
						'updated_uid' => $this->get_user()->id		
					);
					
					$this->submission->update($submission, $data);
					
					$this->send_recieved_email($submission);
					
					$url = '/submissions/view/' . $submission['id'] . '?success=1';
					$this->template->write_view('content', 'redirect', array('url' => $url));
				}
				else {
					$same_as_shipping = $this->input->post('same_as_shipping');
					if(!empty($same_as_shipping))
						$same_as_shipping=true;
					else
						$same_as_shipping=false;
					
					
					$data = array(
						'address' => $address,
						'address2' => $address2,
						'city' => $city,
						'state_province' => $state_province,
						'same_as_shipping'=>$same_as_shipping,
						'payment_type_id' => $payment_type,
						'postal_code' => $postal_code,
						'country' => $country,
						'type' => $type,
						'name' => $name,
						'cc' => $cc,
						'cvv' => $cvv,
						'exp_month' => $exp_month,
						'exp_year' => $exp_year
					);
					
					$this->load->model('postage_country');
					$countries = $this->postage_country->get();
					$this->template->write_view('content', 'submissions/cc', array('errors' => $errors, 'data' => $data, 'submission' => $submission, 'countries' => $countries));
				}
				break;
			case 2: //Check
				
				if(count($errors) == 0){
					$data = array(
						'billing_address' => '',
						'billing_address2' => '',
						'billing_city' => '',
						'billing_state_province' => '',
						'billing_postal_code' => '',
						'payment_type_id' => $payment_type,
						'billing_country' => '',
						'status_id' => 100,
						'updated_uid' => $this->get_user()->id		
					);
					
					$this->submission->update($submission, $data);
					$this->send_recieved_email($submission);
					
					$url = '/submissions/view/' . $submission['id'] . '?success=1';
					$this->template->write_view('content', 'redirect', array('url' => $url));
				}
				else {
					$same_as_shipping = $this->input->post('same_as_shipping');
					if(!empty($same_as_shipping))
						$same_as_shipping=true;
					else
						$same_as_shipping=false;
					
					$data = array(
						'address' => 'test',
						'address2' => '',
						'city' => '',
						'state_province' => '',
						'same_as_shipping'=>$same_as_shipping,
						'postal_code' => '',
						'payment_type_id' => $payment_type,
						'country' => ''
					);
					
					$user = (array) $this->get_user();
					
					$this->load->model('postage_country');
					$countries = $this->postage_country->get();
					$this->template->write_view('content', 'submissions/cc', compact('errors', 'data', 'submission',
							'countries', 'can_pay_by_check', 'user'));
				}
				break;
			case 3: // PICKUP
				break;
			default:
				$errors['payment_type'] = "Invalid or Missing Payment Type";
				$this->template->write_view('content', 'submissions/cc', array('errors' => $errors));
				break;
		}

		$this->template->render();		
	}
	
	public function cancel_order($submission_id){
		$this->load->model('submission');
		$submission = $this->submission->by_id($submission_id);
		
		$this->submission->update_status($submission, $this->get_user(), 500);
		
		$comment = $this->input->post('comments');
		if(!empty($comment)){
			$this->load->model('submission_comment');
			
			$this->submission_comment->create(array(
				'submission_id' => $submission_id,
				'comment' => $comment,
				'created_uid' => $this->get_user()->id
			));
		}	
		$url = '/submissions/view/' . $submission['id'] . '?success=2';
		$this->template->write_view('content', 'redirect', array('url' => $url));
		$this->template->render();
	}
	
	public function continue_order($submission_id){
		$this->load->model('submission');
		$submission = $this->submission->by_id($submission_id, true);
		
		$remove_amount = 0.0;
		$this->load->model('submission_note');
		foreach($submission['notes'] as $note){
			if($note['status_id'] != 300){
				$remove_amount += $note['service_cost'];
				$this->submission_note->update_status($note, $this->get_user(), 600);
			}
		}
		
		$this->submission->update_amount($submission, $this->get_user(), $remove_amount);
		$this->submission->update_status($submission, $this->get_user(), 400);
		
		$comment = $this->input->post('comments');
		if(!empty($comment)){
			$this->load->model('submission_comment');
				
			$this->submission_comment->create(array(
					'submission_id' => $submission_id,
					'comment' => $comment,
					'created_uid' => $this->get_user()->id
			));
		}
		
		$url = '/submissions/view/' . $submission['id'] . '?success=3';
		$this->template->write_view('content', 'redirect', array('url' => $url));
		$this->template->render();
	}
	
}

/* End of file submissions.php */
/* Location: ./application/controllers/submissions.php */