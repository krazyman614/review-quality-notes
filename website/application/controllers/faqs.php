<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class FAQs extends PMA_Controller {
	
	function __construct(){
			parent::__construct();
	}

	public function index()
	{
		/*$this->load->model('faq');
		$faqs = $this->faq->search(array());
		$this->template->write_view('content', 'faq/index', array('faqs' => $faqs));
		$this->template->render();
		*/
		$this->template->write_view('content', 'faq/static', array());
		$this->template->render();
		
	}
}

/* End of file faq.php */
/* Location: ./application/controllers/faq.php */