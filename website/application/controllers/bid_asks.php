<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bid_Asks extends PMA_Controller
{
	public function index()
	{
		$this->page();
	}
	
	public function page($page_size = 20, $page_number = 1){		
		$this->load->model('bid_ask');
		$opts = array('page_size' => $page_size, 'page_number' => $page_number, 'order' => 'LENGTH(fr_num), fr_num asc');
		$count = $this->bid_ask->count($opts);
		$bid_ask_items = $this->bid_ask->search($opts);
		$this->template->write_view('content', 'bid_asks/index', array('count' => $count, 'bid_ask_items' => $bid_ask_items, 'opts' => $opts));
		$this->template->render();
	}
}
