<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class TBD extends PMA_Controller {
	
	function __construct(){
			parent::__construct();
	}

	public function index()
	{
		$this->template->write_view('content', 'tbd', array());
		$this->template->render();
	}
}

/* End of file tbd.php */
/* Location: ./application/controllers/tbd.php */