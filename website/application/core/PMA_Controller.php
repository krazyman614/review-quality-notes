<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PMA_Controller extends CI_Controller {
	const VALIDATION_NO = 'no';
	const VALIDATION_YES = 'yes';
	const VALIDATION_OPTIONAL = 'optional';
	
	const RESPONSE_TYPE_REDIRECT = 'redirect';
	const RESPONSE_TYPE_JSON = 'json';
	
	//Just delete these if we're not using error codes
	const SUCCESS_CODE = 0;
	const UNKNOWN_ERROR = -1;
	const MISSING_PARAMETER_CODE = 301; //Reassign if you want, they're arbitrary
	const INVALID_PARAMETER_CODE = 302; //Reassign if you want, they're arbitrary
	
	var $login_id;
	var $user_login;
	var $current_user = false;
	
	function __construct($validation = self::VALIDATION_OPTIONAL, $response_type = self::RESPONSE_TYPE_REDIRECT)
	{
		parent::__construct();
		
		switch($validation){
			case self::VALIDATION_OPTIONAL:
				$this->login_id = $this->session->userdata('login_id');
				if(!empty($this->login_id)){
					$this->load->model('user_login');
					$this->user_login = $this->user_login->get_login($this->login_id);
					if($this->user_login !== false){
						$this->load->model('user');
						$this->current_user = $this->user->by_id($this->user_login->user_id);
					}
				}				
				break;
			case self::VALIDATION_YES:
				$this->login_id = $this->session->userdata('login_id');
				if(empty($this->login_id)) $this->redirect_login($response_type);
				$this->load->model('user_login');
				$this->user_login = $this->user_login->get_login($this->login_id);
				if($this->user_login === false) $this->redirect_login($response_type);
				$this->load->model('user');
				$this->current_user = $this->user->by_id($this->user_login->user_id);
				if($this->current_user === false) $this->redirect_login($response_type);
				break;			
		}
		
		$this->load->library('template');
		$this->template->write_view('header', 'inc/header.php', array('current_user' => $this->current_user)); 
		$this->template->write_view('side_nav', 'inc/side_nav.php', array('current_user' => $this->current_user)); 
		$this->template->write_view('footer', 'inc/footer.php', array());
		
	}
	
	public function redirect_login($response_type){
		switch($response_type){
			case self::RESPONSE_TYPE_JSON:
				$this->return_json(401, "Not authorized");
				break;
			default:
				$this->load->helper('url');
				redirect('/login');
				exit();
		}
	}
	
	public function get_access_token(){
		return $this->login_id;
	}
	
	public function get_user(){
		return $this->current_user;
	}
	
	public function return_json($code, $msg, $data = null){
		if(!is_string($msg) && $data === null){
			$data = $msg;	
			$msg = '';
		}
		if(empty($data)) $data = array();
		
		$data['status'] = array('code' => $code, 'message' => $msg);
		$this->output_json($data);
	}
	
	
	private function output_json($json, $status_code = 200){
		$this->output->set_status_header($status_code);
		$this->output->set_header("Expires: Sun, 19 Nov 1978 05:00:00 GMT");
		$this->output->set_header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
		$this->output->set_header("Cache-Control: post-check=0, pre-check=0", false);
		$this->output->set_header("Pragma: no-cache");
		$callback = $this->input->get_post('callback');
		$include_script_tags = $this->input->get_post('include_script_tags') == 'true';
		if($include_script_tags){
			$this->output->set_header('Content-type: text/html');
		}
		elseif(!empty($callback)){
			$this->output->set_header('Content-type: text/javascript');
		}
		else {
			$this->output->set_header('Content-type: application/json');
		}
	
		$out = '';
		if(!empty($callback)){
			if($include_script_tags === true) $out .= '<script type="text/javascript">';
			$out .= $callback . '(';
		}
		$out .= json_encode($json);
		if(!empty($callback)){
			$out .= ')';
			if($include_script_tags === true) $out .= '</script>';
		}
		$this->output->set_output($out);
		$this->output->_display();
		exit();
	}
	
	//I feel this functionality is needed often enough to warrant a custom function
	public function get_input($field_name=null,$is_required=false,$default=null, $filter_xss=true) {
		$data = $this->input->get_post($field_name,$filter_xss); //Null fieldname will grab everything
		if ($data===null)
			$data=$default;
		if ($data===null && $is_required) //
			$this->return_json(PMA_Controller::MISSING_PARAMETER_CODE, 'Missing paramater in input', array('field_names'=>$field_name));
		else return $data;
	}
	
	public function get_multiple_inputs($field_names=array(), $filter_xss=true) {
		$ret=array();
		$empty_fields=array();
		foreach ($field_names as $field_name) {
			if(!is_string($field_name))
				continue;
			$data = $this->input->get_post($field_name,$filter_xss); //Null fieldname will grab everything
			$ret[$field_name] = $data;
			if ($data===null) $empty_fields[] = $field_name;
		}
		return array('values'=>$ret,'missing_input'=>$empty_fields);
	}
	
	public function send_email($email, $message, $subject="Review Quality Notes") {
		$this->load->library('email');
	
		$this->email->from('info@rqnotes.com', 'Review Quality Notes');
		$this->email->to($email);
		$this->email->subject($subject);			
		$this->email->message($message);
		$this->email->send();
	}
	
}
?>