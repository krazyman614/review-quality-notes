<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Special extends CI_Model {
	const TABLE = "specials";
	const FIELDS = "specials.id, specials.title, UNIX_TIMESTAMP(specials.date) as date, specials.description";

	function __construct()
	{
		parent::__construct();
	}
	
	public function by_id($special_id){
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		$this->db->where(self::TABLE.'.status_id >= 1');
		$this->db->where(self::TABLE.'.id', $special_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return (array)$query->row();
		}
		else {
			return false;
		}
	}
	
	public function get($limit){
		$this->db->select(self::TABLE.".*");
		$this->db->from(self::TABLE);
		$this->db->where(self::TABLE.'.status_id >= 1');
		$date = ceil(time()/86400) * 86400;		
		$this->db->where(self::TABLE.'.expires_date >= ', date('Y-m-d', $date));
		if($limit > 0)
			$this->db->limit($limit);	
		$this->db->order_by(self::TABLE.'.expires_date', 'desc');
		$query = $this->db->get();
		return $query->result_array();
	}
}
