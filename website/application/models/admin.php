<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin extends CI_Model {
	const TABLE = "admins";
	const FIELDS = "admins.id, admins.name";

	function __construct()
	{
		parent::__construct();
	}
	
	public function by_id($admin_id){
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		$this->db->where('admins.id', $admin_id);
		$this->db->where('status_id >= 1');
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
		else {
			return false;
		}
	}
}
