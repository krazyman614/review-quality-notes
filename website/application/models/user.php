<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User extends CI_Model {
	const TABLE = "users";
	const FIELDS = "users.id, users.name, users.email, users.password, users.company_name, users.address, users.address2, users.city, users.state_province, users.postal_code, users.country, users.phone_number, users.alt_phone_number, users.accepted_terms, users.status_id, users.created_uid, users.created_date, users.updated_uid, users.updated_date, users.pay_by_check";

	function __construct()
	{
		parent::__construct();
	}
	
	public function by_email($email, $not_id = null){
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		$this->db->where('users.email', $email);
		if(!empty($not_id))
			$this->db->where('users.id !=', $not_id);
		$this->db->where('status_id >= 1');
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
		else {
			return false;
		}		
	}
	
	
	
	public function by_email_password($email, $password){
		$this->load->library('encrypt');
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		$this->db->where('users.email', $email);
		$this->db->where('users.password', $this->encrypt->sha1($password));
		$this->db->where('status_id >= 1');
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
		else {
			return false;
		}
	}
	
	public function by_id($user_id){
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		$this->db->where('users.id', $user_id);
		$this->db->where('status_id >= 1');
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
		else {
			return false;
		}
	}
	
	public function create($user){
		$this->load->library('encrypt');
		$this->load->library('uuid');
		$user['id'] = $this->uuid->v5('PMA.user');
		$user['password'] = $this->encrypt->sha1($user['password']);
		$user['status_id'] = 1;
		$user['created_date'] = date('Y-m-d H:i:s');
		$user['updated_date'] = date('Y-m-d H:i:s');
		$this->db->insert(self::TABLE, $user);
		return $this->by_id($user['id']);
	}
	
	public function edit($user){
		$this->load->library('encrypt');
		$user['updated_date'] = date('Y-m-d H:i:s');
		if(isset($user['password']))
			$user['password'] = $this->encrypt->sha1($user['password']);
		$this->db->where('users.id', $user['id']);
		$this->db->update(self::TABLE, $user);
		return $this->by_id($user['id']);
	}
	
	public function send_password_reset($email) {
		
		//Wait, we're keeping email unique, right?
		$this->db->select('users.id, users.name, users.password')->from('users')->where('users.email',$email);
		$query = $this->db->limit(1)->get();
		$password = false;
		$username = false;
		$user_id = false;
		if ($query->num_rows() == 1) {
			$query = $query->row();
			$password = $query->password;
			$username = $query->name;
			$user_id = $query->id;
			$message = $username.', to reset your password for rqnotes.com, please <a href="http://www.rqnotes.com/users/reset_password/'.$user_id.'/'.$password.'/'.$email.'">click here.</a>';
			$message = str_replace("@","%40",$message);
			return array('code'=>PMA_Controller::SUCCESS_CODE, 'message'=>$message,'email'=>$email);
		} else {
			return false;
		}
	}
	
	//After demo on 2012-12-20, need to update this and send reset email function
	//This could also make it only 1 param by using lookup
	public function reset_password($user_id, $old_pass_hash, $email){
		$this->load->library('encrypt');
		$this->load->helper('string');
		
		$new_pwd = random_string('alnum', 16);
		
		$this->db->set('password', $this->encrypt->sha1($new_pwd));
		$this->db->set('updated_date', 'NOW()', FALSE);
		$this->db->where('id', $user_id); //Oh god do we need a more secure way to do this
		$this->db->update('users');
		//Yeah, we literally can't fail in this function until we put in validation
		return array('code'=>PMA_Controller::SUCCESS_CODE, 'new_password'=>$new_pwd, 'email'=>$email);
	}
}
