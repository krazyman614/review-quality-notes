<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Submission extends CI_Model {
	const TABLE = "submissions";
	const FIELDS = "submissions.*";
		
	function __construct()
	{
		parent::__construct();
	}
	
	private function format_all($submissions){
		$formatted = array();
		foreach($submissions as $submission){
			array_push($formatted, $this->format($submission));
		}		
		return $formatted;
	}
	
	private function format($submission){
		switch($submission['status_id']){
			case 100:
				$submission['status'] = 'New Submission';
				break;
			case 200:
				$submission['status'] = 'Incomplete';
				break;
			case 300:
				$submission['status'] = 'Received';
				break;
			case 400:
				$submission['status'] = 'Customer Approved';
				break;
			case 500:
				$submission['status'] = 'Customer Canceled';
				break;
			case 1000:
				$submission['status'] = 'In Process';
				break;
			case 1100:
				$submission['status'] = 'Reviewing Complete';
				break;
			case 2000:
				$submission['status'] = 'Mailed';
				break;
			default:
				$submission['status'] = 'Unknown [' . $submission['status_id'] . ']';
				break;
		}
		
		$this->load->model('submission_note');
		$submission['notes'] = $this->submission_note->by_submission($submission);
		$this->load->model('submission_comment');
		$submission['comments'] = $this->submission_comment->by_submission($submission);
		return $submission;
	}
	
	public function by_user($user){
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		$this->db->where('submissions.created_uid', $user->id);
		$this->db->where('status_id >= 100');
		$this->db->order_by('created_date', 'desc');
		$query = $this->db->get();
		return $this->format_all($query->result_array());
	}
	
	public function by_id($submission_id, $formatted = false, $status_id = 100){
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		$this->db->where('submissions.id', $submission_id);
		$this->db->where('status_id >=', $status_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			if($formatted)
				return $this->format((array)$query->row());
			else
				return (array)$query->row();
		}
		else {
			return false;
		}
	}
	
	public function create($submission){
		$this->load->library('uuid');
		$submission['id'] = $this->uuid->v5('PMA.submission');
		$submission['order_number'] = time();
		$submission['status_id'] = 50;
		$submission['created_date'] = date('Y-m-d H:i:s');
		$submission['updated_date'] = date('Y-m-d H:i:s');
		$this->db->insert(self::TABLE, $submission);
		$submission = $this->by_id($submission['id'], false, 50);
		$this->submission_update_status($submission);
		return $submission;
	}
	
	public function update_status($submission, $user, $status_id){
		$data = array(
			'status_id' => $status_id,
			'updated_uid' => $user->id,
			'updated_date' => date('Y-m-d H:i:s')	
		);
		$this->db->where('submissions.id', $submission['id']);
		$this->db->update(self::TABLE, $data);
		
		$submission = $this->by_id($submission['id']);
		$this->submission_update_status($submission);
	}
	
	public function update($submission, $data){
		$data['updated_date'] = date('Y-m-d H:i:s');
		$this->db->where('submissions.id', $submission['id']);
		$this->db->update(self::TABLE, $data);
		
		$submission = $this->by_id($submission['id']);
		$this->submission_update_status($submission);
	}
	
	public function update_amount($submission, $user, $remove_amount){
		$data = array(
			'sub_total' => $submission['sub_total'] - $remove_amount,
			'total' => $submission['total'] - $remove_amount,
			'updated_uid' => $user->id,
			'updated_date' => date('Y-m-d H:i:s')	
		);
		$this->db->where('submissions.id', $submission['id']);
		$this->db->update(self::TABLE, $data);
	}
	
	private function submission_update_status($submission){
		if(isset($submission['status_id'])){
			$uid = false;
			if(isset($submission['created_uid']))
				$uid = $submission['created_uid'];
			elseif(isset($submission['updated_uid']))
				$uid = $submission['updated_uid'];
			if($uid == false) return;
			
			
			$this->load->library('uuid');
			$this->db->set('id', $this->uuid->v5('PMA.submission'));
			$this->db->set('submission_id', $submission['id']);
			$this->db->set('status_id', $submission['status_id']);
			$this->db->set('created_uid', $uid);
			$this->db->set('created_date',  date('Y-m-d H:i:s'));
			$this->db->insert('submission_status_change');
		}
		
	}
	
}
