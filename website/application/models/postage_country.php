<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Postage_Country extends CI_Model {
	const TABLE = "postage_countries";
	const FIELDS = "postage_countries.id, postage_countries.name";

	function __construct()
	{
		parent::__construct();
	}
	
	public function get(){
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		$this->db->where('postage_countries.status_id > 0');
		$this->db->order_by('postage_countries.name', 'asc');
		$query = $this->db->get();
		return $query->result_array();
	}
	
}
