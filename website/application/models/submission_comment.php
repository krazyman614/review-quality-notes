<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Submission_Comment extends CI_Model {
	const TABLE = "submission_comments";
	const FIELDS = "submission_comments.id, submission_comments.submission_id, submission_comments.comment, submission_comments.created_uid, submission_comments.created_date";
		
	function __construct()
	{
		parent::__construct();
	}
	
	private function format_all($comments){
		$formatted = array();
		foreach($comments as $comment){
			array_push($formatted, $this->format($comment));
		}		
		return $formatted;
	}
	
	private function format($comment){
		if(isset($comment['created_uid'])){
			$this->load->model('admin');
			$admin = $this->admin->by_id($comment['created_uid']);
			if($admin !== false){
				$comment['by'] = $admin;
			}
			else {
				$this->load->model('user');
				$user = $this->user->by_id($comment['created_uid']);
				if($user !== false)
					$comment['by'] = $user;
			}
		}
		return $comment;
	}
	
	public function by_submission($submission){
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		$this->db->where('submission_comments.submission_id', $submission['id']);
		$query = $this->db->get();
		return $this->format_all($query->result_array());
	}
	
	public function create($comment){
		$this->load->library('uuid');
		$comment['id'] = $this->uuid->v5('PMA.submission_comments');
		$comment['created_date'] = date('Y-m-d H:i:s');
		
		$this->db->insert(self::TABLE, $comment);
	}
	
}
