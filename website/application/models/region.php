<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Region extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}
	
	public function by_id($region_id){
		switch($region_id){
			case 'row':
				return array('id' => 'row', 'name' => 'World Notes');
			case 'us':
				return array('id' => 'us', 'name' => 'U.S. Notes');
			default:
				return null;
		}
	}
	
	public function search($opts){
		return array(array('id' => 'us', 'name' => 'U.S. Notes'), array('id' => 'row', 'name' => 'World Notes'));		
	}
}
