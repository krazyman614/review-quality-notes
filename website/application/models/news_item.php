<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class News_Item extends CI_Model {
	const TABLE = "news_items";
	const FIELDS = "news_items.id, news_items.title, UNIX_TIMESTAMP(news_items.date) as date, news_items.news";

	function __construct()
	{
		parent::__construct();
	}
	
	public function by_id($news_item_id){
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		$this->db->where('news_items.status_id >= 1');
		$this->db->where('news_items.id', $news_item_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return (array)$query->row();
		}
		else {
			return false;
		}
	}
	
	public function get($limit){
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		$this->db->where('news_items.status_id >= 1');
		//$this->db->where('news_items.date <= NOW()');
		if($limit > 0)
			$this->db->limit($limit);	
		$this->db->order_by('news_items.date', 'desc');
		$query = $this->db->get();
		return $query->result_array();
	}
}
