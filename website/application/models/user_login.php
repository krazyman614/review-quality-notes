<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User_login extends CI_Model {
	const TABLE = "user_logins";
	const FIELDS = "user_logins.id, user_logins.user_id, user_logins.status_id, user_logins.created_date";
	/*
	var $id;
	var $user_id;
	var $status_id;
	var $created_date;
*/
	function __construct()
	{
		parent::__construct();
	}
	
	public function get_login($login_id){
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		$this->db->where('id', $login_id);
		$this->db->where('status_id >= 1');
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
		else {
			return false;
		}
	}
	
	public function create($user){
		$this->load->library('uuid');
		$data = array(
			'id' => $this->uuid->v5('PMA.user_login'),
			'user_id' => $user->id,
			'status_id' => 1,
			'created_date' => date('Y-m-d H:i:s')
		);
		$this->db->insert(self::TABLE, $data);
		return $this->get_login($data['id']);
	}
	
	public function logout($user) {
		$this->db->where('user_id',$user->id)->update('logins',  array('status_id'=>0) );
	}
}
