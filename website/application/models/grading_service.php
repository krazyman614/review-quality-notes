<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Grading_Service extends CI_Model {
	const TABLE = "grading_services";
	const FIELDS = "grading_services.id, grading_services.abbrev, grading_services.name";
	
	function __construct()
	{
		parent::__construct();
	}
	
	public function by_id($grading_service_id){
		if(empty($grading_service_id)) return false;
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		$this->db->where('grading_services.status_id > 0');
		$this->db->where('grading_services.id', $grading_service_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
		else {
			return false;
		}	
	}
	
	public function get(){
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		$this->db->where('grading_services.status_id > 0');
		$this->db->order_by('grading_services.abbrev', 'asc');
		$query = $this->db->get();
		return $query->result_array();
	}
}
