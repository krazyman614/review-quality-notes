<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Note_Type extends CI_Model {
	const TABLE = "note_types";
	const FIELDS = "note_types.id, note_types.type, note_types.region_id";

	function __construct()
	{
		parent::__construct();
	}
	
	private function format_all($notes){
		$n_notes = array();
		foreach($notes as $note){
			array_push($n_notes, $this->format($note));
		}
		return $n_notes;
	}
	
	
	private function format($note){
		if(isset($note['region_id'])){
			$this->load->model('region');
			$note['region'] = $this->region->by_id($note['region_id']);
			unset($note['region_id']);
		}
		return $note;
	}
	
	
	public function by_id($note_type_id){
		if(empty($note_type_id)) return false;
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		$this->db->where('note_types.status_id > 0');
		$this->db->where('note_types.id', $note_type_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
		else {
			return false;
		}
	}
	
	public function get($region_id){
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		$this->db->where('note_types.region_id', $region_id);
		$this->db->where('note_types.status_id > 0');
		$this->db->order_by('note_types.type', 'asc');
		$query = $this->db->get();
		return $this->format_all($query->result_array());
	}
}
