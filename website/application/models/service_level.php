<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Service_Level extends CI_Model {
	const TABLE = "service_levels";
	const FIELDS = "service_levels.id, service_levels.level, service_levels.title, service_levels.per_note, service_levels.time_to_process";

	function __construct()
	{
		parent::__construct();
	}
	
	public function by_id($service_level_id){
		if(empty($service_level_id)) return false;
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		$this->db->where('service_levels.status_id > 0');
		$this->db->where('service_levels.id', $service_level_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
		else {
			return false;
		}	
	}
	
	public function get(){
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		$this->db->where('service_levels.status_id > 0');
		$this->db->order_by('service_levels.level', 'asc');
		$query = $this->db->get();
		return $query->result_array();
	}
}
