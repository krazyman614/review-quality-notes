<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Submission_note extends CI_Model {
	const TABLE = "submission_notes";
	const FIELDS = "submission_notes.id, submission_notes.submission_id, submission_notes.sequence, submission_notes.service_level_id, submission_notes.note_country, submission_notes.note_type_id, submission_notes.denomination, submission_notes.catalog_number, submission_notes.pp, submission_notes.serial_number, submission_notes.grading_service_id, submission_notes.barcode, submission_notes.declared_value, submission_notes.service_cost, submission_notes.status_id, submission_notes.created_uid, submission_notes.created_date, submission_notes.updated_uid, submission_notes.updated_date, submission_notes.stickered, submission_notes.sticker_serial_number";
		
	function __construct()
	{
		parent::__construct();
	}
	
	private function format_all($notes){
		$formatted = array();
		foreach($notes as $note){
			array_push($formatted, $this->format($note));
		}		
		return $formatted;
	}
	
	private function format($note){
		switch($note['status_id']){
			case 100:
				$note['status'] = 'New Submission';
				break;
			case 200:
				$note['status'] = 'Not Received';
				break;
			case 300:
				$note['status'] = 'Received';
				break;
			case 600:
				$note['status'] = 'Customer Removed';
				break;
			case 1000:
				$note['status'] = 'In Process';
				break;
			case 1100:
				$note['status'] = 'Reviewing Complete';
				break;
			case 2000:
				$note['status'] = 'Mailed';
				break;
			default:
				$note['status'] = 'Unknown [' . $note['status_id'] . ']';
				break;
		}
		$this->load->model('service_level');
		$note['service_level'] = (array)$this->service_level->by_id($note['service_level_id']);
		if(!empty($note['note_type_id'])){
			$this->load->model('note_type');
			$note['note_type'] = (array)$this->note_type->by_id($note['note_type_id']);		
		}
		$this->load->model('grading_service');
		$note['grading_service'] = (array)$this->grading_service->by_id($note['grading_service_id']);
		
		$this->db->from('submission_note_history')->where('note_id', $note['id'])->where('submission_id', $note['submission_id']);
		$this->db->order_by('updated_date', 'desc')->limit(1);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			$note['history'] = (array)$query->row();
		}
		
		$this->load->model('submission_note_grade');
		$note['grades'] = $this->submission_note_grade->by_note($note);
		
		return $note;
	}
	
	public function by_submission($submission){
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		$this->db->where('submission_notes.submission_id', $submission['id']);
		$this->db->where('status_id >= 1');
		$this->db->order_by('sequence', 'asc');
		$query = $this->db->get();
		return $this->format_all($query->result_array());
	}
	
	public function by_barcode($barcode, $formatted = false){
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		$this->db->where('barcode', $barcode);
		$this->db->where('status_id >= 1');
		$this->db->where('stickered', 1);
		$this->db->order_by('sequence', 'asc');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$note = $query->row_array();
			
			if ($formatted) {
				return $this->format($note);
			} else {
				return $note;
			}
		}
		else {
			return false;
		}
	}
	
	public function by_catalog_serial($catalog_number, $serial_number, $formatted = false){
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		$this->db->where('catalog_number', $catalog_number);
		$this->db->like('serial_number', $serial_number, 'none');
		$this->db->where('stickered', 1);
		$this->db->where('status_id >= 1');
		$this->db->order_by('sequence', 'asc');
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			$note = $query->row_array();
			
			if ($formatted) {
				return $this->format($note);
			} else {
				return $note;
			}
		}
		else {
			return false;
		}
	}

	public function by_id($submission_id){
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		$this->db->where('submission_notes.id', $submission_id);
		$this->db->where('status_id >= 1');
		$this->db->order_by('sequence', 'asc');
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return (array)$query->row();
		}
		else {
			return false;
		}
	}
	
	public function create($submission, $notes){
		$this->load->library('uuid');
		$saved = array();
		foreach($notes as $note){
			$note['submission_id'] = $submission['id'];
			$note['id'] = $this->uuid->v5('PMA.submission_notes');
			$note['status_id'] = 100;
			$note['created_date'] = date('Y-m-d H:i:s');
			$note['updated_date'] = date('Y-m-d H:i:s');
			$this->db->insert(self::TABLE, $note);
			$note = $this->by_id($note['id']);
			if($note === false)
				return false;
			array_push($saved, $note);
		}
		return $saved;
	}
	
	public function update_status($note, $user, $status_id){
		$data = array(
			'status_id' => $status_id,
			'updated_uid' => $user->id,
			'updated_date' => date('Y-m-d H:i:s')	
		);
		$this->db->where('submission_notes.id', $note['id']);
		$this->db->update(self::TABLE, $data);
	}
	
	public function search($opts){
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		$this->db->where('submission_notes.status_id >= 1100');
		$this->db->order_by('submission_notes.barcode', 'desc');
		foreach($opts as $key=>$value){
			if(empty($value)) continue;
			switch($key){
				default:
					$this->db->where('submission_notes.' . $key, $value);
					break;
			}
		}
		$query = $this->db->get();
		return $query->result_array();
	}
	
}
