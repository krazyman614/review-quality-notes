<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bid_Ask extends CI_Model
{
	const TABLE = "bid_ask";	
	const FIELDS = "*";

	public function count($opts){
		$this->db->from(self::TABLE);
		foreach($opts as $key=>$value){
			switch($key){
				case 'page_size':
				case 'page_number': break;
			}
		}
		return $this->db->count_all_results();
	}
	
	public function search($opts){
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		foreach($opts as $key=>$value){
			switch($key){
				case 'page_size':
						$page_size = 0;
						if(is_numeric($value)){
							$page_size = intval($value);
						}
						if($page_size > 0){
							$page_number = 1;
							if(isset($opts['page_number']) && is_numeric($opts['page_number']) && intval($opts['page_number']) > 0){
								$page_number = intval($opts['page_number']);
							}
							$this->db->limit($page_size, ($page_number - 1) * $page_size);	
						}
						break;
				case 'page_number': break;
				case 'order':
					$this->db->order_by($value);
					break;
			}
		}
		$query = $this->db->get();
		return $query->result_array();
	}
	
	public function by_id($id, $active = true){
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		$this->db->where(self::TABLE . '.id', $id);
		if ($active) {
			$this->db->where('is_active = 1');
		}
		
		$query = $this->db->get();
		
		if ($query->num_rows() > 0){
			return $query->row_array();
		}
		else {
			return false;
		}
	}	
}

