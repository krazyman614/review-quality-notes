<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Submission_Note_Grade extends CI_Model {
	const TABLE = "submission_note_grades";
	const FIELDS = "*";
		
	function __construct()
	{
		parent::__construct();
	}
	
	public function format_all($grades){
		$gs = array();
		foreach($grades as $grade){
			array_push($gs, $this->format($grade));
		}
		return $gs;
	}
	
	public function format($grade){
		if(isset($grade['created_uid'])){
			$this->load->model('admin');
			$grade['by'] = $this->admin->by_id($grade['created_uid']);
		}
		return $grade;
	}
		
	public function by_note($note){
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		$this->db->where('submission_note_grades.note_id', $note['id']);
		$this->db->where('status_id >= 1');
		$this->db->where('is_final >= 1');
		$query = $this->db->get();
		return $this->format_all($query->result_array());
	}
	
}
