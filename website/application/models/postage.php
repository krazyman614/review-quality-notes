<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Postage extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}
	
	public function get_postage_amount($country, $total_declared_value){
		$this->db->select('postage_rates.cost');
		$this->db->from('postage_rates');
		$this->db->join('postage_countries', 'postage_countries.id = postage_rates.postage_country_id AND postage_countries.status_id > 0');
		$this->db->where('postage_rates.status_id > 0');
		$this->db->where('postage_countries.name', $country);
		$this->db->where('postage_rates.under_value >=', $total_declared_value);
		$this->db->order_by('postage_rates.under_value', 'asc');
		$this->db->limit(1);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row()->cost;
		}
		else {
			return false;
		}
		
	}
	
	public function get_postage($country){
		$this->db->select('postage_rates.id, postage_rates.name, postage_rates.under_value, postage_rates.cost');
		$this->db->from('postage_rates');
		$this->db->join('postage_countries', 'postage_countries.id = postage_rates.postage_country_id AND postage_countries.status_id > 0');
		$this->db->where('postage_rates.status_id > 0');
		$this->db->where('postage_countries.name', $country);		
		$this->db->order_by('postage_rates.under_value', 'asc');
		$query = $this->db->get();
		return $query->result_array();
	}
}
