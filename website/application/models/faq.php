<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class FAQ extends CI_Model {
	const TABLE = "faqs";
	const FIELDS = "faqs.id, faqs.title, faqs.order_by, faqs.content, faqs.status_id, faqs.created_uid, faqs.created_date, faqs.updated_uid, faqs.updated_date";

	function __construct()
	{
		parent::__construct();
	}
	
	public function search($opts){
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		foreach($opts as $key=>$value){
			switch($key){
				case 'page_size':
						$page_size = 0;
						if(is_numeric($value)){
							$page_size = intval($value);
						}
						if($page_size > 0){
							$page_number = 1;
							if(isset($opts['page_number']) && is_numeric($opts['page_number']) && intval($opts['page_number']) > 0){
								$page_number = intval($opts['page_number']);
							}
							$this->db->limit($page_size, ($page_number - 1) * $page_size);	
						}
						break;
				case 'page_number': break;
			}
		}
		$this->db->where('faqs.status_id >= 1');
		$this->db->order_by('faqs.order_by asc');
		$query = $this->db->get();
		return $query->result_array();
	}
}
