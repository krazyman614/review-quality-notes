<?php 

if (!function_exists('format_grade_score')) {
	function format_grade_score($score){
		switch($score){
			case 0: return 'Other';
			case 50: return 'Satisfactory';
			case 100: return 'Superior';
			default: return 'Unknown';
		}
	}
}

if (!function_exists('get_stickered')) {
	function get_stickered($note) {
		if ($note['status_id'] < 1100) {
			return '---';
		} elseif ($note['stickered'] > 0) {
			return $note['sticker_serial_number'];
		} else {
			return 'Not Stickered';
		}
	}
}

if (!function_exists('check_history')) {
	function check_history($note, $name){
		if(isset($note['history'])){
			if($note['history'][$name] != $note[$name]) return ' class="text-error" style="font-weight:bold;"';
		}
		return '';
	}
}

if (!function_exists('get_note_type')) {
	function get_note_type($note, $region_id){
		switch($region_id){
			case 'us': return '<td' . check_history($note, 'note_type_id') . '>' . $note['note_type']['type'] . '</td>';
			default: return '<td' . check_history($note, 'note_country') . '>' . $note['note_country'] . '</td>';
		}
	}
}

if (!function_exists('get_column_name')) {
	function get_column_name($field, $region_id){
		switch($field){
			case 'type':
				switch($region_id){
					case 'us': return 'Type';
					default: return "Country";
				}
				break;
			case 'catalog_number':
				switch($region_id){
					case 'us': return 'FR Number/Catalog Number';
					default: return "Pick/Charlton";
				}
				break;
			case 'pp':
				switch($region_id){
					case 'us': return 'Grade';
					default: return "Date/Series";
				}
				break;
		}
		return "Unknown";
	}
}