<?php
/**
 * The main template file.
 */

				get_header(); ?><div class="container contents" style="height:100% !important">
		<table width="100%" height="100%"><tbody><tr><td style="width:280px;vertical-align: top;height:100% !important;">
				<div class="side-nav" style="height:inherit;height:100% !important;">
					<div class="side-nav-container">
	<div class="address">
		RQN INC.<br>
		P.O. Box 571150<br>
		Miami, FL 33257-1150<br>
		<a href="mailto:info@rqnotes.com">INFO@RQNOTES.COM</a>
	</div>
	<div><img src="/resources/img/side-nav-bar.png"></div>
	<div><a href="/">HOME</a></div>
	<div><img src="/resources/img/side-nav-bar.png"></div>
	<div><a href="/users">CREATE ACCOUNT</a></div>
	<div><img src="/resources/img/side-nav-bar.png"></div>
	<div><a href="/submissions">CUSTOMER ORDER HISTORY</a></div>
	<div><img src="/resources/img/side-nav-bar.png"></div>
	<div><a href="/pricing">PRICING</a></div>
	<div><img src="/resources/img/side-nav-bar.png"></div>
	<div><a href="/home/standards_and_legal">STANDARDS &amp; LEGAL</a></div>
	<div><img src="/resources/img/side-nav-bar.png"></div>	
	<div><a href="/submissions/new_submission">NOTE ENTRY</a></div>
	<div><img src="/resources/img/side-nav-bar.png"></div>	
	<div><a href="/sticker">STICKERED NOTE SEARCH</a></div>
	<div><img src="/resources/img/side-nav-bar.png"></div>	
	<div><a href="/accepted_notes">ACCEPTED NOTES</a></div>
	<div><img src="/resources/img/side-nav-bar.png"></div>	
	<div><a href="/home/directory">DEALER DIRECTORY</a></div>
	<div><img src="/resources/img/side-nav-bar.png"></div>	
		
	<div><a href="/faqs">FAQ</a></div>
	<div><img src="/resources/img/side-nav-bar.png"></div>
	<div><a href="/blog">BLOG</a></div>
	<div><img src="/resources/img/side-nav-bar.png"></div>	
	</div>
<div class="side-nav-submission-block">
	<div class="small">CREATE A</div>
	<div class="large">SUBMISSION</div>
	<div>
		<a href="/submissions/new_submission"><img src="/resources/img/submission-btn-nav.png" border="0"></a>
	</div>
</div>
<div class="side-nav-seal">
	<img src="/resources/img/seal.png">
</div>				</div>
			</td><td style="vertical-align: top;">
			<div style="padding-left:0px;padding-right:00px;">
				<div>

<div class="header_row">
	<div class="header_row_inner">
		Blog
	</div>
</div>

<div style="padding:60px 60px 60px 78px;">
                    	<div class="blog-content">
							<?php if ( have_posts() ) : ?>
                                <?php 
								if(is_author()){
									the_post();
									$title = '<h1 class="blog-top">Posts by: '.get_the_author().'</h1>';
								}elseif(is_tag()){
									$title = '<h1 class="blog-top">Posts in:'.single_tag_title( '', false ).'</h1>';
								}elseif(is_category()){
									$title = '<h1 class="blog-top">Posts in:'.single_cat_title( '', false ).'</h1>';
								}elseif(is_archive()){
									$title = '<h1 class="blog-top">Posts in:'.get_the_date('F, Y').'</h1>';
								}else{
									$title = false;
								}
								
								echo $title;			
								?>
                                <div class="clearfix"></div>
                                <?php while ( have_posts() ) : the_post(); ?>
                                    <?php
									if(is_single()){
										$content = '
										<div class="post">
											<h1>'.get_the_title().'</h1>
											<span class="post-details">Posted '.get_the_date('F j, Y').' by '.get_the_author().'</span>'
											.apply_filters( 'the_content',get_the_content()).getComments().'				
										</div>';
									}else{
										$content = '
										<div class="post">
											<h1><a href="'.get_permalink().'" title="'.get_the_title().'">'.get_the_title().'</a></h1>
											<span class="post-details">Posted '.get_the_date('F j, Y').' by '.get_the_author().'</span>'
											.apply_filters( 'the_content',get_the_excerpt()).getCommentsLinks().'
										</div>';
									}
									
									echo $content;		
									?>                                    
                                <?php endwhile; // end of the loop. ?>
                            <?php endif;?>
                        </div>
                        <div class="clearfix"></div>
                        <?php 
						 $pages = '';
						 $range = 2;
						 $showitems = ($range * 2)+1;  
					
						 global $paged;
						 if(empty($paged)) $paged = 1;
					
						 if($pages == ''){
							 global $wp_query;
							 $pages = $wp_query->max_num_pages;
							 if(!$pages){
								 $pages = 1;
							 }
						 }   
					
						 if(1 != $pages){
							 echo "<div class='pagination'>";
							 if($paged > 2 && $paged > $range+1) echo "<a href='".get_pagenum_link(1)."'>&laquo;</a>";
							 if($paged > 1) echo "<a href='".get_pagenum_link($paged - 1)."'>&lt;</a>";
					
							 for ($i=1; $i <= $pages; $i++){
								 if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )){
									 echo ($paged == $i)? "<span class='current'>".$i."</span>":"<a href='".get_pagenum_link($i)."' class='inactive' >".$i."</a>";
								 }
							 }
					
							 if ($paged < $pages ) echo "<a href='".get_pagenum_link($paged + 1)."'>&gt;</a>";  
							 if ($paged < $pages-1 &&  $paged+$range-1 < $pages) echo "<a href='".get_pagenum_link($pages)."'>&raquo;</a>";
							 echo "</div>\n";
						 }
						?>
   	</div>
</div>			</div>
			</td>
		</tr>
		</tbody></table>
	</div>
				<?php get_footer(); ?>