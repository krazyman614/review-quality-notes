<?php
/**
 * The Header for our theme.
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 9]>
<html class="ie ie9" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) | !(IE 9)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<title>Blog | Review Quality Notes</title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="/resources/css/default.css" rel="stylesheet" media="screen">
<link href="/resources/css/bootstrap.css" rel="stylesheet" media="screen">
<link href="/resources/css/global.css" rel="stylesheet" media="screen">
		
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script type="text/javascript" src="/resources/js/bootstrap.min.js"></script>
<?php wp_head(); ?>
</head>

<body <?php body_class();?>>
<div class="container hdr-contents" style="background-color:#e3d9c7">
	<div id="hdr">
		<div id="logo"><img src="/resources/img/logo.png"></div>
		<div id="quality"><img src="/resources/img/quality-bkg.png"></div>
		<div id="logo-special"><img src="/resources/img/logo-special.png"></div>
				<div id="login">
			<form method="post" class="form-horizontal" action="/login/login_user">
				<div class="login-label">SIGN IN / REGISTER</div>
				<div class="login-content">
					<input type="text" name="email" placeholder="Email">
				</div>
				<div class="login-content">
					<input type="password" name="password" placeholder="Password">
				</div>
				<div class="login-content">
					<button type="submit" class="btn btn-small login">SIGN IN</button>
					<a href="/users" class="btn btn-small login">REGISTER A NEW ACCOUNT</a>
				</div>
				<div class="login-content">
					<a href="/users/forgot_password" class="forgot-password">Forgot Your Password?</a>
				</div>
			</form>
		</div>
	</div>
	<div id="hdr-bar">	
	</div>
</div>	