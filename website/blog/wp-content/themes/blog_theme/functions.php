<?php
//Define constants
define('SITE_URL', home_url().'/');
define('THEME_PATH', get_template_directory().'/');
define('THEME_URI', get_template_directory_uri().'/');
define('FRAMEWORK_PATH', THEME_PATH.'framework/');
define('FRAMEWORK_URI', THEME_URI.'framework/');

wp_enqueue_style( 'blog-style', get_stylesheet_uri() );

function getCommentsLinks(){
		if ( comments_open() ) :
			ob_start();
			comments_popup_link( '<span class="leave-reply">' . __( 'Leave a comment' ) . '</span>', __( '1 Comment' ), __( '% Comments' ),'comments-link' );
		 
			return ob_get_clean();
		else:
			return false;
		endif;
	}
	 function getComments(){
		if ( comments_open() ) :
			ob_start();
			comments_template( '', true );
			
			$comments = ob_get_clean();
		 
			return '<div class="comment list">'.$comments.'</div>';
		else:
			return false;
		endif;
	}
?>
