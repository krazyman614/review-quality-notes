<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admins extends PMA_Controller {
	
	function __construct(){
			parent::__construct();
	}

	public function index()
	{
		$this->page();
	}
	
	public function page($page_size = 10, $page_number = 0){		
		$this->load->model('admin');
		$opts = array('page_size' => $page_size, 'page_number' => $page_number);
		$count = $this->admin->count($opts);
		$admins = $this->admin->search($opts);
		$this->template->write_view('content', 'admin/index', array('count' => $count, 'admins' => $admins, 'opts' => $opts));
		$this->template->render();
	}
	
	public function add(){
		$this->template->write_view('content', 'admin/create', array());
		$this->template->render();		
	}
	
	public function edit($admin_id){
		$this->load->model('admin');
		$admin = $this->admin->by_id($admin_id);
		if(empty($admin)){
			$this->template->write_view('content', 'admin/create', array());
		}
		else {
			$admin = (array) $admin;
			unset($admin['password']);
			$this->template->write_view('content', 'admin/edit', array('data' => $admin));
		}			
		$this->template->render();				
	}
	
	public function create(){
		$errors = array();
		$name = $this->input->post('name');
		if(empty($name))
			$errors['name'] = 'Missing name';
		
		$email = $this->input->post('email');
		if(empty($email))
			$errors['email'] = 'Missing email';
		else {
			$this->load->model('admin');
			if($this->admin->by_email($email) !== false)
				$errors['email'] = 'Email already exists';
		}
		$password = $this->input->post('password');
		$confirm_password = $this->input->post('confirm_password');
		if(empty($password) || empty($confirm_password)){
			if(empty($password))
				$errors['password'] = 'Missing password';
			if(empty($confirm_password))
				$errors['confirm_password'] = 'Missing confirmation password';
		}
		elseif($password != $confirm_password){
				$errors['password'] = "Passwords don't match";
				$errors['confirm_password'] = "Passwords don't match";
		}
		
		$admin_type_id = $this->input->post('admin_type_id');
		$status_id = $this->input->post('status_id');
		
		$data = array(
			'name' => $name,
			'email' => $email,
			'password' => $password,
			'status_id' => $status_id,
			'admin_type_id' => $admin_type_id,
			'created_uid' => $this->get_admin()->id,
			'updated_uid' => $this->get_admin()->id				
		);
		
		
		if(count($errors) === 0){	
			$this->load->model('admin');
			$admin = $this->admin->create($data);
			if($admin === false){
				$errors['admin'] = 'Error creating admin';
			}
		}
		
		if(count($errors) === 0){
			$this->template->write_view('content', 'admin/edit', array('data' => $data, 'success'=>true));
		}
		else {
			$data['confirm_password'] = $confirm_password;
			$this->template->write_view('content', 'admin/create', array('errors' => $errors, 'data' => $data));
		}
		$this->template->render();
	}
	
	public function edit_admin(){
		$errors = array();
		$id = $this->input->post('id');
		if(empty($id))
			$errors['id'] = "Missing id, cannot edit";
		
		$name = $this->input->post('name');
		if(empty($name))
			$errors['name'] = 'Missing name';
	
		$email = $this->input->post('email');
		if(empty($email))
			$errors['email'] = 'Missing email';
		else {
			$this->load->model('admin');
			if($this->admin->by_email($email, $id) !== false)
				$errors['email'] = 'Email already exists';
		}
		$password = $this->input->post('password');
		$confirm_password = $this->input->post('confirm_password');
		if(!empty($password) || !empty($confirm_password)){
			if(empty($password))
				$errors['password'] = 'Missing password';
			if(empty($confirm_password))
				$errors['confirm_password'] = 'Missing confirmation password';
			if($password != $confirm_password){
				$errors['password'] = "Passwords don't match";
				$errors['confirm_password'] = "Passwords don't match";
			}
		}
	
		$status_id = $this->input->post('status_id');
		$admin_type_id = $this->input->post('admin_type_id');
		
		$data = array(
				'id' => $id,
				'name' => $name,
				'email' => $email,
				'status_id' => $status_id,
				'admin_type_id' => $admin_type_id,
				'updated_uid' => $this->get_admin()->id
		);
		
		if(!empty($password))
			$data['password'] = $password;
	
	
		if(count($errors) === 0){
			$this->load->model('admin');
			$admin = $this->admin->edit($data);
			if($admin === false){
				$errors['admin'] = 'Error creating admin';
			}
		}
	
		if(count($errors) === 0){
			$this->template->write_view('content', 'admin/edit', array('success' => true, 'data' => $data));
		}
		else {
			$data['confirm_password'] = $confirm_password;
			$this->template->write_view('content', 'admin/edit', array('errors' => $errors, 'data' => $data));
		}
		$this->template->render();
	}
}

/* End of file admin.php */
/* Location: ./application/controllers/admin.php */