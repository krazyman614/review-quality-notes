<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bid_Asks extends PMA_Controller
{
	public function index()
	{
		$this->page();
	}
	
	public function page($page_size = 10, $page_number = 1){		
		$this->load->model('bid_ask');
		$opts = array('page_size' => $page_size, 'page_number' => $page_number, 'order' => 'LENGTH(fr_num), fr_num asc');
		$count = $this->bid_ask->count($opts);
		$bid_ask_items = $this->bid_ask->search($opts);
		$this->template->write_view('content', 'bid_asks/index', array('count' => $count, 'bid_ask_items' => $bid_ask_items, 'opts' => $opts));
		$this->template->render();
	}
	
	public function add() {
		$this->template->write_view('content', 'bid_asks/create', array());
		$this->template->render();		
	}
	
	public function edit($id) {
		$this->load->model('bid_ask');
		$bid_ask = $this->bid_ask->by_id($id);
		if(empty($bid_ask)){
			$this->template->write_view('content', 'bid_asks/create', array());
		}
		else {
			$bid_ask = (array) $bid_ask;
			$this->template->write_view('content', 'bid_asks/edit', array('data' => $bid_ask));
		}			
		$this->template->render();				
	}
	
	public function create(){
		$errors = array();

		$fr_num = $this->input->post('fr_num');
		if (empty($fr_num)) {
			$errors['fr_num'] = 'Missing FR #';
		}
		
		$grade = $this->input->post('grade');
		$rqn_bid = $this->input->post('rqn_bid');
		$rqn_ask = $this->input->post('rqn_ask');
		$email = $this->input->post('email');
		$comments = $this->input->post('comments');
		
		$data = array(
				'fr_num' => $fr_num,
				'grade' => $grade,
				'rqn_bid' => $rqn_bid,
				'rqn_ask' => $rqn_ask,
				'email' => $email,
				'comments' => $comments,
				'created_uid' => $this->get_admin()->id,
				'updated_uid' => $this->get_admin()->id				
				);
		
		if (count($errors) === 0){
			$this->load->model('bid_ask');
			$bid_ask = $this->bid_ask->create($data);
			if ($bid_ask === false){
				$errors['bid_ask_item'] = 'Error creating bid/ask';
			}
		}
		
		if (count($errors) === 0){
			$this->load->helper('url');
			redirect('/bid_asks/edit/' . $bid_ask['id']);
		} else {
			$this->template->write_view('content', 'bid_asks/create', array('errors' => $errors, 'data' => $data));
		}
		
		$this->template->render();
	}
	
	public function edit_bid_ask_item(){
		$errors = array();
		
		$id = $this->input->post('id');
		if(empty($id))
			$errors['id'] = "Missing id, cannot edit";
		
		$fr_num = $this->input->post('fr_num');
		if (empty($fr_num)) {
			$errors['fr_num'] = 'Missing FR #';
		}
		
		$grade = $this->input->post('grade');
		$rqn_bid = $this->input->post('rqn_bid');
		$rqn_ask = $this->input->post('rqn_ask');
		$email = $this->input->post('email');
		$comments = $this->input->post('comments');
		
		$data = array(
				'id' => $id,
				'fr_num' => $fr_num,
				'grade' => $grade,
				'rqn_bid' => $rqn_bid,
				'rqn_ask' => $rqn_ask,
				'email' => $email,
				'comments' => $comments,
				'updated_uid' => $this->get_admin()->id				
				);
	
		if(count($errors) === 0){
			$this->load->model('bid_ask');
			$bid_ask = $this->bid_ask->edit($data);
			if($bid_ask === false){
				$errors['bid_ask_item'] = 'Error creating bid/ask item';
			}
		}
		
		if (count($errors) === 0) {
			$this->template->write_view('content', 'bid_asks/edit', array('success' => true, 'data' => $data));
		} else {
			$this->template->write_view('content', 'bid_asks/edit', array('errors' => $errors, 'data' => $data));
		}
		
		$this->template->render();
	}
	
	//could probably shorten this function and pass to page
	public function delete($id, $page_size=10, $page_number=1) {
		$errors = array();
		if(empty($id))
			$errors['id'] = "Missing id, cannot delete";
		
		$this->load->model('bid_ask');
		$model_bounce = $this->bid_ask->delete($id);
		if (!empty($model_bounce['code']))
			$errors['delete']="Error deleting ".$id."-- ".$model_bounce['code'];
		
		$opts = array('page_number'=>$page_number, 'page_size'=>$page_size);
		$count = $this->bid_ask->count($opts);
		$bid_ask_items = $this->bid_ask->search(array('page_size' => $page_size, 'page_number' => $page_number));
		if (count($errors)===0)
			$this->template->write_view('content', 'bid_asks/index', array('count' => $count, 'bid_ask_items' => $bid_ask_items, 'page_size' => $page_size, 'page_number' => $page_number, 'opts'=>$opts));
		else
			$this->template->write_view('content', 'bid_asks/index', array('count' => $count, 'bid_ask_items' => $bid_ask_items, 'page_size' => $page_size, 'page_number' => $page_number, 'opts'=>$opts, 'errors'=>$errors));
		$this->template->render();
	}
}