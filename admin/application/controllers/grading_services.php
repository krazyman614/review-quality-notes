<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Grading_Services extends PMA_Controller {
	
	function __construct(){
			parent::__construct();
	}

	public function index()
	{
		$this->page();
	}
	
	public function page($page_size = 10, $page_number = 1){		
		$this->load->model('grading_service');
		$opts = array('page_size' => $page_size, 'page_number' => $page_number);
		$count = $this->grading_service->count($opts);
		$grading_services = $this->grading_service->search($opts);
		$this->template->write_view('content', 'grading_service/index', array('count' => $count, 'grading_services' => $grading_services, 'opts' => $opts));
		$this->template->render();
	}
	
	public function add(){
		$this->template->write_view('content', 'grading_service/create', array());
		$this->template->render();		
	}
	
	public function edit($grading_service_id){
		$this->load->model('grading_service');
		$grading_service = $this->grading_service->by_id($grading_service_id);
		if(empty($grading_service)){
			$this->template->write_view('content', 'grading_service/create', array());
		}
		else {
			$grading_service = (array) $grading_service;
			unset($grading_service['password']);
			$this->template->write_view('content', 'grading_service/edit', array('data' => $grading_service));
		}			
		$this->template->render();				
	}
	
	public function delete($grading_service_id){
		$this->load->model('grading_service');
		$grading_service = $this->grading_service->by_id($grading_service_id, false);
		if(!empty($grading_service)){
			$this->grading_service->delete($grading_service);
		}
		$this->index();
	}
	
	public function create(){
		$errors = array();
		$name = $this->input->post('name');
		if(empty($name))
			$errors['name'] = 'Missing name';
		
		$abbrev = $this->input->post('abbrev');
		if(empty($abbrev))
			$errors['abbrev'] = 'Missing abbrev';
		
		$status_id = $this->input->post('status_id');
		
		$data = array(
			'name' => $name,
			'abbrev' => $abbrev,
			'status_id' => $status_id,
			'created_uid' => $this->get_admin()->id,
			'updated_uid' => $this->get_admin()->id				
		);
		
		if(count($errors) === 0){
			$this->load->model('grading_service');
			$grading_service = $this->grading_service->create($data);
			if($grading_service === false){
				$errors['grading_service'] = 'Error creating grading_service';
			}
		}
		
		if(count($errors) === 0){
			$this->template->write_view('content', 'grading_service/edit', array('success' => true, 'data' => $data));
		}
		else {
			$this->template->write_view('content', 'grading_service/create', array('errors' => $errors, 'data' => $data));
		}
		$this->template->render();
	}
	
	public function edit_grading_service(){
		$errors = array();
		$id = $this->input->post('id');
		if(empty($id))
			$errors['id'] = "Missing id, cannot edit";
		
		$name = $this->input->post('name');
		if(empty($name))
			$errors['name'] = 'Missing name';
		
		$abbrev = $this->input->post('abbrev');
		if(empty($abbrev))
			$errors['abbrev'] = 'Missing abbrev';
	
		$status_id = $this->input->post('status_id');
		
		$data = array(
				'id' => $id,
				'name' => $name,
				'abbrev' => $abbrev,
				'status_id' => $status_id,
				'updated_uid' => $this->get_admin()->id
		);
	
		if(count($errors) === 0){
			$this->load->model('grading_service');
			$grading_service = $this->grading_service->edit($data);
			if($grading_service === false){
				$errors['grading_service'] = 'Error creating grading_service';
			}
		}
	
		if(count($errors) === 0){
			$this->template->write_view('content', 'grading_service/edit', array('success' => true, 'data' => $data));
		}
		else {
			$this->template->write_view('content', 'grading_service/edit', array('errors' => $errors, 'data' => $data));
		}
		$this->template->render();
	}
}

/* End of file grading_services.php */
/* Location: ./application/controllers/grading_services.php */