<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Note_Types extends PMA_Controller {
	
	function __construct(){
			parent::__construct();
	}

	public function index()
	{
		$this->page();
	}
	
	public function page($region_id = '', $page_size = 10, $page_number = 1){	
		if($region_id==='null') $region_id=""; //can't send a null region id (aka, no search) over server
		
		$this->load->model('note_type');
		$opts = array('region_id' => $region_id, 'page_size' => $page_size, 'page_number' => $page_number);
		$count = $this->note_type->count($opts);
		$note_types = $this->note_type->search($opts);
		$this->load->model('region');
		$regions = $this->region->search(array());
		$this->template->write_view('content', 'note_type/index', array('region_id' => $region_id, 'count' => $count, 'note_types' => $note_types, 'opts' => $opts, 'regions' => $regions));
		$this->template->render();
	}
	
	public function add(){
		$this->load->model('region');
		$regions = $this->region->search(array());
		$this->template->write_view('content', 'note_type/create', array('regions' => $regions));
		$this->template->render();		
	}
	
	public function edit($note_type_id){
		$this->load->model('note_type');
		$note_type = $this->note_type->by_id($note_type_id, false);
		$this->load->model('region');
		$regions = $this->region->search(array());
		if(empty($note_type)){
			$this->template->write_view('content', 'note_type/create', array('regions' => $regions));
		}
		else {
			$note_type = (array) $note_type;
			unset($note_type['password']);
			$this->template->write_view('content', 'note_type/edit', array('data' => $note_type, 'regions' => $regions));
		}			
		$this->template->render();				
	}
	
	public function delete($note_type_id){
		$this->load->model('note_type');
		$note_type = $this->note_type->by_id($note_type_id, false);
		if(!empty($note_type)){
			$this->note_type->delete($note_type);
		}
		$this->index();
	}
	
	public function create(){
		$errors = array();
		$type = $this->input->post('type');
		if(empty($type))
			$errors['type'] = 'Missing type';
		
		$region_id = $this->input->post('region_id');
		$status_id = $this->input->post('status_id');
		
		$data = array(
			'type' => $type,
			'region_id' => $region_id,
			'status_id' => $status_id,
			'created_uid' => $this->get_admin()->id,
			'updated_uid' => $this->get_admin()->id				
		);
		
		if(count($errors) === 0){
			$this->load->model('note_type');
			$note_type = $this->note_type->create($data);
			if($note_type === false){
				$errors['note_type'] = 'Error creating note_type';
			}
		}
		
		
		$this->load->model('region');
		$regions = $this->region->search(array());
		if(count($errors) === 0){
			$this->template->write_view('content', 'note_type/edit', array('success' => true, 'data' => $data, 'regions' => $regions));
		}
		else {
			$this->template->write_view('content', 'note_type/create', array('errors' => $errors, 'data' => $data, 'regions' => $regions));
		}
		$this->template->render();
	}
	
	public function edit_note_type(){
		$errors = array();
		$id = $this->input->post('id');
		if(empty($id))
			$errors['id'] = "Missing id, cannot edit";
		
		$type = $this->input->post('type');
		if(empty($type))
			$errors['type'] = 'Missing type';
		
		$region_id = $this->input->post('region_id');
		$status_id = $this->input->post('status_id');
		
		$data = array(
				'id' => $id,
				'type' => $type,
				'region_id' => $region_id,
				'status_id' => $status_id,
				'updated_uid' => $this->get_admin()->id
		);
	
		if(count($errors) === 0){
			$this->load->model('note_type');
			$note_type = $this->note_type->edit($data);
			if($note_type === false){
				$errors['note_type'] = 'Error creating note_type';
			}
		}
	
		
		$this->load->model('region');
		$regions = $this->region->search(array());
		if(count($errors) === 0){
			$this->template->write_view('content', 'note_type/edit', array('success' => true, 'data' => $data, 'regions' => $regions));
		}
		else {
			$this->template->write_view('content', 'note_type/edit', array('errors' => $errors, 'data' => $data, 'regions' => $regions));
		}
		$this->template->render();
	}
}

/* End of file note_types.php */
/* Location: ./application/controllers/note_types.php */