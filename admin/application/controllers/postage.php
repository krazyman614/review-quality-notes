<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Postage extends PMA_Controller {
	
	function __construct(){
			parent::__construct();
	}

	public function index()
	{
		$this->page();
	}
	
	public function page($page_size = 10, $page_number = 1){				
		$this->load->model('postage_country');
		$opts = array('page_size' => $page_size, 'page_number' => $page_number);
		$count = $this->postage_country->count($opts);
		$postages = $this->postage_country->search($opts);
		$this->template->write_view('content', 'postage/index', array('count' => $count, 'postages' => $postages, 'opts' => $opts));
		$this->template->render();
	}
	
	public function add(){
		$this->template->write_view('content', 'postage/create', array());
		$this->template->render();		
	}
	
	public function edit($postage_id){
		$this->load->model('postage_country');
		$postage = $this->postage_country->by_id($postage_id);
		if(empty($postage)){
			$this->template->write_view('content', 'postage/create', array());
		}
		else {
			$postage = (array) $postage;
			$this->template->write_view('content', 'postage/edit', array('data' => $postage));
		}			
		$this->template->render();				
	}
	
	public function delete($postage_id){
		$this->load->model('postage_country');
		$postage = $this->postage_country->by_id($postage_id);
		if(!empty($postage)){
			$this->postage_country->delete($postage);
		}			
		$this->index();
	}
	
	public function create(){
		$errors = array();
		$full_name = $this->input->post('full_name');
		if(empty($full_name))
			$errors['full_name'] = 'Missing full name';
		$name = $this->input->post('name');
		if(empty($name))
			$errors['name'] = 'Missing abbrev';
		$status_id = $this->input->post('status_id');
		
		$data = array(
			'full_name' => $full_name,
			'name' => $name,
			'status_id' => $status_id,
			'created_uid' => $this->get_admin()->id,
			'updated_uid' => $this->get_admin()->id				
		);
		
		if(count($errors) === 0){
			$this->load->model('postage_country');
			$postage = $this->postage_country->create($data);
			if($postage === false){
				$errors['postage'] = 'Error creating postage';
			}
		}
		
		if(count($errors) === 0){
			$this->template->write_view('content', 'postage/edit', array('success' => true, 'data' => $data));
		}
		else {
			$this->template->write_view('content', 'postage/create', array('errors' => $errors, 'data' => $data));
		}
		$this->template->render();
	}
	
	public function edit_postage(){
		$errors = array();
		$id = $this->input->post('id');
		if(empty($id))
			$errors['id'] = "Missing id, cannot edit";
		$full_name = $this->input->post('full_name');
		if(empty($full_name))
			$errors['full_name'] = 'Missing full name';
		$name = $this->input->post('name');
		if(empty($name))
			$errors['name'] = 'Missing abbrev';
		
		$status_id = $this->input->post('status_id');
		
		$data = array(
			'id' => $id,
			'full_name' => $full_name,
			'name' => $name,
			'status_id' => $status_id,
			'updated_uid' => $this->get_admin()->id
		);
	
		if(count($errors) === 0){
			$this->load->model('postage_country');
			$postage = $this->postage_country->edit($data);
			if($postage === false){
				$errors['postage'] = 'Error creating postage';
			}
		}
	
		if(count($errors) === 0){
			$this->template->write_view('content', 'postage/edit', array('success' => true, 'data' => $data));
		}
		else {
			$this->template->write_view('content', 'postage/edit', array('errors' => $errors, 'data' => $data));
		}
		$this->template->render();
	}
	
	public function rates($postage_country_id)
	{
		$this->rates_page($postage_country_id);
	}
	
	public function rates_page($postage_country_id, $page_size = 10, $page_number = 1){
		$this->load->model('postage_country');
		$postage_country = $this->postage_country->by_id($postage_country_id);
		$this->load->model('postage_rate');
		$opts = array('postage_country_id' => $postage_country_id, 'page_size' => $page_size, 'page_number' => $page_number);
		$count = $this->postage_rate->count($opts);
		$rates = $this->postage_rate->search($opts);
		$this->template->write_view('content', 'postage/rates', array('count' => $count, 'rates' => $rates, 'opts' => $opts, 'postage_country' => $postage_country));
		$this->template->render();
	}
	
	public function add_rate($postage_country_id){
		$this->load->model('postage_country');
		$postage_country = $this->postage_country->by_id($postage_country_id);
		$this->template->write_view('content', 'postage/create_rate', array('postage_country' => $postage_country));
		$this->template->render();
	}
	
	public function edit_rate($postage_country_id, $rate_id){
		$this->load->model('postage_country');
		$postage_country = $this->postage_country->by_id($postage_country_id);
		$this->load->model('postage_rate');
		$rate = $this->postage_rate->by_id($rate_id);
		if(empty($rate)){
			$this->template->write_view('content', 'postage/create', array('postage_country' => $postage_country));
		}
		else {
			$this->template->write_view('content', 'postage/edit_rate', array('data' => $rate, 'postage_country' => $postage_country));
		}
		$this->template->render();
	}
	
	public function create_rate($postage_country_id){
		$this->load->model('postage_country');
		$postage_country = $this->postage_country->by_id($postage_country_id);
		$errors = array();
		$name = $this->input->post('name');
		if(empty($name))
			$errors['name'] = 'Missing name';
		
		$under_value = intval($this->input->post('under_value'));
		if($under_value <= 0)
			$errors['under_value'] = 'Missing under value, must be greater than zero';
		
		$cost = intval($this->input->post('cost'));
		if($cost <= 0)
			$errors['cost'] = 'Missing cost, must be greater than zero';
		
		$status_id = $this->input->post('status_id');
	
		$data = array(
				'name' => $name,
				'under_value' => $under_value,
				'cost' => $cost,
				'postage_country_id' => $postage_country['id'],
				'status_id' => $status_id,
				'created_uid' => $this->get_admin()->id,
				'updated_uid' => $this->get_admin()->id
		);
	
		if(count($errors) === 0){
			$this->load->model('postage_rate');
			$postage = $this->postage_rate->create($data);
			if($postage === false){
				$errors['postage'] = 'Error creating postage';
			}
		}
	
		if(count($errors) === 0){
			$this->template->write_view('content', 'postage/edit_rate', array('success' => true, 'data' => $data, 'postage_country' => $postage_country));
		}
		else {
			$this->template->write_view('content', 'postage/create_rate', array('errors' => $errors, 'data' => $data, 'postage_country' => $postage_country));
		}
		$this->template->render();
	}
	
	public function post_edit_rate($postage_country_id){
		$this->load->model('postage_country');
		$postage_country = $this->postage_country->by_id($postage_country_id);
		$errors = array();
		$id = $this->input->post('id');
		if(empty($id))
			$errors['id'] = "Missing id, cannot edit";
	
		$name = $this->input->post('name');
		if(empty($name))
			$errors['name'] = 'Missing name';
		
		$under_value = intval($this->input->post('under_value'));
		if($under_value <= 0)
			$errors['under_value'] = 'Missing under value, must be greater than zero';
		
		$cost = intval($this->input->post('cost'));
		if($cost <= 0)
			$errors['cost'] = 'Missing cost, must be greater than zero';
		
		$status_id = $this->input->post('status_id');
	
		$data = array(
				'id' => $id,
				'name' => $name,
				'under_value' => $under_value,
				'cost' => $cost,
				'postage_country_id' => $postage_country['id'],
				'status_id' => $status_id,
				'updated_uid' => $this->get_admin()->id
		);
	
		if(count($errors) === 0){
			$this->load->model('postage_rate');
			$postage = $this->postage_rate->edit($data);
			if($postage === false){
				$errors['postage'] = 'Error creating postage';
			}
		}
	
		if(count($errors) === 0){
			$this->template->write_view('content', 'postage/edit_rate', array('success' => true, 'data' => $data, 'postage_country' => $postage_country));
		}
		else {
			$this->template->write_view('content', 'postage/edit_rate', array('errors' => $errors, 'data' => $data, 'postage_country' => $postage_country));
		}
		$this->template->render();
	}
}

/* End of file postage.php */
/* Location: ./application/controllers/postage.php */