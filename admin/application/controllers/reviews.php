<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reviews extends PMA_Controller {
	
	public function __construct(){
			parent::__construct(self::VALIDATION_YES);
	}
	
	public function edit($id = null)
	{
		$this->load->helper('url');

		if ($id === null) {
			$id = $this->input->post('id');
		}
		
		if (empty($id)) { 
			redirect('submissions');
		}
		
		$this->load->model('Submission_Note_Grade', 'review');
		
		$review = $this->review->by_id($id);
		
		$this->load->helper(array('form'));
		
		$errors = array();
		
		if ($this->input->post('id')) {
			$this->load->library('form_validation');
			
			$this->form_validation->set_rules('paper', 'Paper', 'required');
			$this->form_validation->set_rules('color', 'Color', 'required');
			$this->form_validation->set_rules('printing', 'Printing', 'required');
			$this->form_validation->set_rules('face_margins', 'Face Margins', 'required');
			$this->form_validation->set_rules('face_centering', 'Face Centering', 'required');
			$this->form_validation->set_rules('back_margins', 'Back Margins', 'required');
			$this->form_validation->set_rules('back_centering', 'Back Centering', 'required');
			$this->form_validation->set_rules('registration', 'Registration', 'required');
			$this->form_validation->set_rules('comments', 'Comments', 'xss');
			
			if ($this->form_validation->run() !== false) {
				$data = array(
						'id' => $review->id,
						'paper' => $this->input->post('paper'),
						'color' => $this->input->post('color'),
						'printing' => $this->input->post('printing'),
						'face_margins' => $this->input->post('face_margins'),
						'face_centering' => $this->input->post('face_centering'),
						'back_margins' => $this->input->post('back_margins'),
						'back_centering' => $this->input->post('back_centering'),
						'registration' => $this->input->post('registration'),
						'comments' => $this->input->post('comments'),
						);
				
				$this->review->edit($data);
				
				$this->load->model('Submission_note');
				
				$note = $this->Submission_note->by_id($review->note_id);
				
				redirect('submissions/view/' . $note['submission_id']);
			}
		}
		
		$this->template->write_view('content', 'reviews/edit', compact('review', 'errors'));
		$this->template->render();
	}

}