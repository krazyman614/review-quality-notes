<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reports extends PMA_Controller {
	
	function __construct(){
			parent::__construct();
	}
	
	function submissions($page_size = 10, $page_number = 1){
		$opts = array(
			'historical' => true,
			'name' => $this->input->get_post('name'),
			'order_number' => $this->input->get_post('order_number'),
			'catalog_number' => $this->input->get_post('catalog_number'),
			'serial_number' => $this->input->get_post('serial_number'),
			'submission_status_change.start_date' => $this->input->get_post('start_date'),
			'submission_status_change.end_date' => $this->input->get_post('end_date'),
			'submission_status_change.status_id' => $this->input->get_post('status_id'),
			'stickered' => $this->input->get_post('stickered') == 'yes',
			'sticker_number' => $this->input->get_post('sticker_number'),
			'page_size' => $page_size,
			'page_number' => $page_number
		);
		
		if(!empty($opts['submission_status_change.end_date']))
			$opts['submission_status_change.end_date'] = strtotime($opts['submission_status_change.end_date']);
		else
			$opts['submission_status_change.end_date'] = time();
		
		if(!empty($opts['submission_status_change.start_date']))
			$opts['submission_status_change.start_date'] = strtotime($opts['submission_status_change.start_date']);
		else 
			$opts['submission_status_change.start_date'] = $opts['submission_status_change.end_date'] - (7*24*3600);
		
		
		$this->load->model('submission');
		$count = $this->submission->count($opts);
		$submissions = $this->submission->search($opts);
		$submission_status = $this->submission->get_status();
		
		$this->template->add_css('/resources/css/datepicker.css');
		$this->template->add_js('/resources/js/bootstrap-datepicker.js');
		$this->template->write_view('content', 'reports/submission', array('opts' => $opts, 'submissions' => $submissions, 'count' => $count, 'submission_status' => $submission_status));
		$this->template->render();		
	}
	
	function submissions_download(){
		$opts = array(
			'historical' => true,
			'name' => $this->input->get_post('name'),
			'order_number' => $this->input->get_post('order_number'),
			'catalog_number' => $this->input->get_post('catalog_number'),
			'serial_number' => $this->input->get_post('serial_number'),
			'submission_status_change.start_date' => $this->input->get_post('start_date'),
			'submission_status_change.end_date' => $this->input->get_post('end_date'),
			'submission_status_change.status_id' => $this->input->get_post('status_id'),
			'stickered' => $this->input->get_post('stickered') == 'yes',
			'sticker_number' => $this->input->get_post('sticker_number')
		);
		
		if(!empty($opts['submission_status_change.end_date']))
			$opts['submission_status_change.end_date'] = strtotime($opts['submission_status_change.end_date']);
		else
			$opts['submission_status_change.end_date'] = time();
		
		if(!empty($opts['submission_status_change.start_date']))
			$opts['submission_status_change.start_date'] = strtotime($opts['submission_status_change.start_date']);
		else 
			$opts['submission_status_change.start_date'] = $opts['submission_status_change.end_date'] - (7*24*3600);
		
		
		$this->load->model('submission');
		$submissions = $this->submission->search($opts);
		
		$this->output->set_header('Content-Type: application/vnd.ms-excel');
		$this->output->set_header('Content-Disposition: attachment; filename="submission_report_' . date('d_m_Y_G_i_s') . '.xls"');
		$this->output->set_header("Content-Transfer-Encoding: binary");
		$this->output->set_header('Expires: 0');
		$this->output->set_header('Pragma: no-cache');
		
		$this->load->view('reports/submission_download', array('submissions' => $submissions));
	}
	
	function notes($page_size = 10, $page_number = 1){
		$opts = array(
				'include_submissions' => true,
				'submissions.name' => $this->input->get_post('name'),
				'submissions.order_number' => $this->input->get_post('order_number'),
				'catalog_number' => $this->input->get_post('catalog_number'),
				'serial_number' => $this->input->get_post('serial_number'),
				'start_date' => $this->input->get_post('start_date'),
				'end_date' => $this->input->get_post('end_date'),
				'status_id' => $this->input->get_post('status_id'),
				'stickered' => $this->input->get_post('stickered') == 'yes',
				'sticker_serial_number' => $this->input->get_post('sticker_serial_number'),
				'page_size' => $page_size,
				'page_number' => $page_number
		);
	
		if(!empty($opts['end_date']))
			$opts['end_date'] = strtotime($opts['end_date']);
		else
			$opts['end_date'] = time();
	
		if(!empty($opts['start_date']))
			$opts['start_date'] = strtotime($opts['start_date']);
		else
			$opts['start_date'] = $opts['end_date'] - (7*24*3600);
	
	
		$this->load->model('submission_note');
		$count = $this->submission_note->count($opts);
		$submissions = $this->submission_note->search($opts);
		$this->load->model('submission');
		$submission_status = $this->submission->get_status();
	
		$this->template->add_css('/resources/css/datepicker.css');
		$this->template->add_js('/resources/js/bootstrap-datepicker.js');
		$this->template->write_view('content', 'reports/notes', array('opts' => $opts, 'submissions' => $submissions, 'count' => $count, 'submission_status' => $submission_status));
		$this->template->render();
	}
	
	function notes_download(){
		$opts = array(
				'include_submissions' => true,
				'submissions.name' => $this->input->get_post('name'),
				'submissions.order_number' => $this->input->get_post('order_number'),
				'catalog_number' => $this->input->get_post('catalog_number'),
				'serial_number' => $this->input->get_post('serial_number'),
				'start_date' => $this->input->get_post('start_date'),
				'end_date' => $this->input->get_post('end_date'),
				'status_id' => $this->input->get_post('status_id'),
				'stickered' => $this->input->get_post('stickered') == 'yes',
				'sticker_serial_number' => $this->input->get_post('sticker_serial_number')
		);
	
		if(!empty($opts['end_date']))
			$opts['end_date'] = strtotime($opts['end_date']);
		else
			$opts['end_date'] = time();
	
		if(!empty($opts['start_date']))
			$opts['start_date'] = strtotime($opts['start_date']);
		else
			$opts['start_date'] = $opts['end_date'] - (7*24*3600);
	
	
		$this->load->model('submission_note');
		$submissions = $this->submission_note->search($opts);
	
		$this->output->set_header('Content-Type: application/vnd.ms-excel');
		$this->output->set_header('Content-Disposition: attachment; filename="note_report_' . date('d_m_Y_G_i_s') . '.xls"');
		$this->output->set_header("Content-Transfer-Encoding: binary");
		$this->output->set_header('Expires: 0');
		$this->output->set_header('Pragma: no-cache');
		$this->load->view('reports/notes_download', array('submissions' => $submissions));
	}
	
	function note_status_history($page_size = 10, $page_number = 1){
		$opts = array(
				'include_submissions' => true,
				'include_submission_status_change' => true,
				'submissions.name' => $this->input->get_post('name'),
				'submissions.order_number' => $this->input->get_post('order_number'),
				'catalog_number' => $this->input->get_post('catalog_number'),
				'serial_number' => $this->input->get_post('serial_number'),
				'submission_status_change.start_date' => $this->input->get_post('start_date'),
				'submission_status_change.end_date' => $this->input->get_post('end_date'),
				'submission_status_change.status_id' => $this->input->get_post('status_id'),
				'stickered' => $this->input->get_post('stickered') == 'yes',
				'sticker_serial_number' => $this->input->get_post('sticker_serial_number'),
				'page_size' => $page_size,
				'page_number' => $page_number
		);
	
		if(!empty($opts['submission_status_change.end_date']))
			$opts['submission_status_change.end_date'] = strtotime($opts['submission_status_change.end_date']);
		else
			$opts['submission_status_change.end_date'] = time();
	
		if(!empty($opts['submission_status_change.start_date']))
			$opts['submission_status_change.start_date'] = strtotime($opts['submission_status_change.start_date']);
		else
			$opts['submission_status_change.start_date'] = $opts['submission_status_change.end_date'] - (7*24*3600);
	
	
		$this->load->model('submission_note');
		$count = $this->submission_note->count($opts);
		$submissions = $this->submission_note->search($opts);
		$this->load->model('submission');
		$submission_status = $this->submission->get_status();
	
		$this->template->add_css('/resources/css/datepicker.css');
		$this->template->add_js('/resources/js/bootstrap-datepicker.js');
		$this->template->write_view('content', 'reports/note_status_history', array('opts' => $opts, 'submissions' => $submissions, 'count' => $count, 'submission_status' => $submission_status));
		$this->template->render();
	}
	
	function note_status_history_download(){
		$opts = array(
				'include_submissions' => true,
				'include_submission_status_change' => true,
				'submissions.name' => $this->input->get_post('name'),
				'submissions.order_number' => $this->input->get_post('order_number'),
				'catalog_number' => $this->input->get_post('catalog_number'),
				'serial_number' => $this->input->get_post('serial_number'),
				'submission_status_change.start_date' => $this->input->get_post('start_date'),
				'submission_status_change.end_date' => $this->input->get_post('end_date'),
				'submission_status_change.status_id' => $this->input->get_post('status_id'),
				'stickered' => $this->input->get_post('stickered') == 'yes',
				'sticker_serial_number' => $this->input->get_post('sticker_serial_number')
		);
	
		if(!empty($opts['submission_status_change.end_date']))
			$opts['submission_status_change.end_date'] = strtotime($opts['submission_status_change.end_date']);
		else
			$opts['submission_status_change.end_date'] = time();
	
		if(!empty($opts['submission_status_change.start_date']))
			$opts['submission_status_change.start_date'] = strtotime($opts['submission_status_change.start_date']);
		else
			$opts['submission_status_change.start_date'] = $opts['submission_status_change.end_date'] - (7*24*3600);
	
	
		$this->load->model('submission_note');
		$submissions = $this->submission_note->search($opts);
	
		$this->output->set_header('Content-Type: application/vnd.ms-excel');
		$this->output->set_header('Content-Disposition: attachment; filename="note_report_' . date('d_m_Y_G_i_s') . '.xls"');
		$this->output->set_header("Content-Transfer-Encoding: binary");
		$this->output->set_header('Expires: 0');
		$this->output->set_header('Pragma: no-cache');
		$this->load->view('reports/note_status_history_download', array('submissions' => $submissions));
	}
}

/* End of file reports.php */
/* Location: ./application/controllers/reports.php */