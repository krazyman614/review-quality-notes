<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dealer extends PMA_Controller {
	
	function __construct(){
			parent::__construct();
	}

	public function index()
	{
		$this->page();
	}
	
	public function page($page_size = 10, $page_number = 1){		

		$this->template->write_view('content', 'dealer/index');
		$this->template->render();
	}
	
	public function add(){
		$this->template->add_css('/resources/css/datepicker.css');
		$this->template->add_js('/resources/js/bootstrap-datepicker.js');
		$this->template->write_view('content', 'dealer/create', array());
		$this->template->render();		
	}

	public function edit($special_id){
	
		$this->template->write_view('content', 'dealer/edit', array('id' => $special_id));
		$this->template->render();
	}
	public function save(){
		if($_REQUEST['id'] == '') {
			mysql_query("insert into directory (name) values ('')");
			$_REQUEST['id'] = mysql_insert_id();
		}
		mysql_query("update directory set name='".addslashes($_REQUEST['name'])."', address='".addslashes($_REQUEST['address'])."', city='".addslashes($_REQUEST['city'])."', state='".addslashes($_REQUEST['state'])."', zip='".addslashes($_REQUEST['zip'])."', phone='".addslashes($_REQUEST['phone'])."', website='".addslashes($_REQUEST['website'])."', 
				
				business='".addslashes($_REQUEST['business'])."', 
				work_phone='".addslashes($_REQUEST['work_phone'])."', 
				fax='".addslashes($_REQUEST['fax'])."', 
				email='".addslashes($_REQUEST['email'])."', 
				sell='".addslashes($_REQUEST['sell'])."'
				where id='".addslashes($_REQUEST['id'])."' ");

		header("Location: /dealer/index");die();			
	}
	
	
	public function delete($id) { //These page defaults are the parameters that index calls on page
		mysql_query("delete from directory where id = '".$id."'");
		header("Location: /dealer/index");die();	
	}
}

/* End of file specials.php */
/* Location: ./application/controllers/specials.php */