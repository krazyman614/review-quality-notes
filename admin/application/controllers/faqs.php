<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class FAQs extends PMA_Controller {
	
	function __construct(){
			parent::__construct();
	}

	public function index()
	{
		$this->page();
	}
	
	public function page($page_size = 10, $page_number = 1){		
		$this->load->model('faq');
		$opts = array('page_size' => $page_size, 'page_number' => $page_number, 'order_by'=>"ASC");
		$count = $this->faq->count($opts);
		$faqs = $this->faq->search($opts);
		$this->template->write_view('content', 'faq/index', array('count' => $count, 'faqs' => $faqs, 'opts' => $opts));
		$this->template->render();
	}
	
	public function add(){
		$this->template->write_view('content', 'faq/create', array());
		$this->template->render();		
	}
	
	public function edit($faq_id){
		$this->load->model('faq');
		$faq = $this->faq->by_id($faq_id);
		if(empty($faq)){
			$this->template->write_view('content', 'faq/create', array());
		}
		else {
			$faq = (array) $faq;
			unset($faq['password']);
			$this->template->write_view('content', 'faq/edit', array('data' => $faq));
		}			
		$this->template->render();				
	}
	
	public function create(){
		$errors = array();
		$title = $this->input->post('title');
		if(empty($title))
			$errors['title'] = 'Missing title';
		
		$order_by = $this->input->post('order_by');
		if(empty($order_by))
			$errors['order_by'] = 'Missing order_by';
		$order_by = intval($order_by);
		if($order_by === false)
			$errors['order_by'] = 'Invalid order by, must be an integer';
		
		$content = $this->input->post('content');
		if(empty($content))
			$errors['content'] = 'Missing content';
		
		$status_id = $this->input->post('status_id');
		
		$data = array(
			'title' => $title,
			'order_by' => $order_by,
			'content' => $content,
			'status_id' => $status_id,
			'created_uid' => $this->get_admin()->id,
			'updated_uid' => $this->get_admin()->id				
		);
		
		if(count($errors) === 0){
			$this->load->model('faq');
			$faq = $this->faq->create($data);
			if($faq === false){
				$errors['faq'] = 'Error creating faq';
			}
		}
		
		if(count($errors) === 0){
			$this->template->write_view('content', 'faq/edit', array('success' => true, 'data' => $data));
		}
		else {
			$this->template->write_view('content', 'faq/create', array('errors' => $errors, 'data' => $data));
		}
		$this->template->render();
	}
	
	public function edit_faq(){
		$errors = array();
		$id = $this->input->post('id');
		if(empty($id))
			$errors['id'] = "Missing id, cannot edit";
		
		$title = $this->input->post('title');
		if(empty($title))
			$errors['title'] = 'Missing title';
		
		$order_by = $this->input->post('order_by');
		if(empty($order_by))
			$errors['order_by'] = 'Missing order_by';
		$order_by = intval($order_by);
		if($order_by === false)
			$errors['order_by'] = 'Invalid order by, must be an integer';
	
		$content = $this->input->post('content');
		if(empty($content))
			$errors['content'] = 'Missing content';
		
		$status_id = $this->input->post('status_id');
		
		$data = array(
				'id' => $id,
				'title' => $title,
				'order_by' => $order_by,
				'content' => $content,
				'status_id' => $status_id,
				'updated_uid' => $this->get_admin()->id
		);
	
		if(count($errors) === 0){
			$this->load->model('faq');
			$faq = $this->faq->edit($data);
			if($faq === false){
				$errors['faq'] = 'Error creating faq';
			}
		}
	
		if(count($errors) === 0){
			$this->template->write_view('content', 'faq/edit', array('success' => true, 'data' => $data));
		}
		else {
			$this->template->write_view('content', 'faq/edit', array('errors' => $errors, 'data' => $data));
		}
		$this->template->render();
	}
}

/* End of file faq.php */
/* Location: ./application/controllers/faq.php */