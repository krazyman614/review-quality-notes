<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notes extends PMA_Controller {
	
	public function __construct(){
			parent::__construct();
	}
	
	public function edit($id = null)
	{
		$this->load->helper(array('url', 'form'));

		if ($id === null) {
			$id = $this->input->post('id');
		}
		
		if (empty($id)) { 
			redirect('submissions');
		}
		
		$this->load->model('Submission_Note', 'note');
		
		$note = (array) $this->note->by_id($id);
		
		$this->load->model('Submission');
		
		$submission = $this->Submission->by_id($note['submission_id'], true);
		
		$errors = array();
		
		if ($this->input->post('id')) {
			$this->load->library('form_validation');
			
			$this->form_validation->set_rules('sticker_serial_number', 'Sticker Number', 'xss_clean');
			$this->form_validation->set_rules('catalog_number', get_column_name('catalog_number', $submission['region_id']), 'required');
			$this->form_validation->set_rules('pp', get_column_name('pp', $submission['region_id']), 'required');
			$this->form_validation->set_rules('serial_number', 'Serial Number', 'required');
			$this->form_validation->set_rules('barcode', 'Bar Code', 'required');
			
			if ($submission['region_id'] !== 'us') {
				$this->form_validation->set_rules('grade', 'Grade', 'xss_clean');
			}
			
			if ($this->form_validation->run() !== false) {
				$data = array(
						'id' => $note['id'],
						'catalog_number' => $this->input->post('catalog_number'),
						'pp' => $this->input->post('pp'),
						'sticker_serial_number' => $this->input->post('sticker_serial_number'),
						'serial_number' => $this->input->post('serial_number'),
						'barcode' => $this->input->post('barcode'),
						'stickered' => ($this->input->post('sticker_serial_number') === '') ? 0 : 1,
						'grade' => $this->input->post('grade'),
						);
				
				$this->note->edit($data);
				
				redirect('submissions/view/' . $note['submission_id']);
			}
		}
		
		foreach ($submission['notes'] as $submission_note) {
			if ($note['id'] === $submission_note['id']) {
				$note = $submission_note;
				break;
			}
		}
		
		$this->template->write_view('content', 'notes/edit', compact('note', 'submission', 'errors'));
		$this->template->render();
	}
	
	// @todo this has to be finished before using
	// it needs to recalculate the total prices for the submission
	public function delete($id)
	{
		$this->load->model('Submission_note');
		$this->load->helper('url');
		
		$note = $this->Submission_note->by_id($id);
		
		if ($note === false) {
			redirect('submissions');
		}
		
		// we just change the status to 0 instead of deleting
		$note['status_id'] = 0;
		
		$this->Submission_note->edit($note);
		
		redirect('submissions/view/' . $note['submission_id']);
	}
	
	public function check_barcode_exists($barcode)
	{
		$this->load->model('Submission_note');
		
		$notes = $this->Submission_note->find_barcode($barcode);
		
		$json = json_encode(array('exists' => (count($notes) > 0)));
		
		$this->load->view('json_view', compact('json'));
	}

}
