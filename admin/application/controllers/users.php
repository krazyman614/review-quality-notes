<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends PMA_Controller {
	
	function __construct(){
			parent::__construct();
	}

	public function index()
	{
		$this->page();
	}
	
	public function page($page_size = 10, $page_number = 1){		
		$this->load->model('user');
		$opts = array('page_size' => $page_size, 'page_number' => $page_number);
		$count = $this->user->count($opts);
		$users = $this->user->search($opts);
		$this->template->write_view('content', 'user/index', array('count' => $count, 'users' => $users, 'opts' => $opts));
		$this->template->render();
	}
	
	public function add(){
		$this->template->write_view('content', 'user/create', array());
		$this->template->render();		
	}
	
	public function edit($user_id){
		$this->load->model('user');
		$user = $this->user->by_id($user_id);
		if(empty($user)){
			$this->template->write_view('content', 'user/create', array());
		}
		else {
			$user = (array) $user;
			unset($user['password']);
			$this->template->write_view('content', 'user/edit', array('data' => $user));
		}			
		$this->template->render();				
	}
	
	public function create(){
		$errors = array();
		$name = $this->input->post('name');
		if(empty($name))
			$errors['name'] = 'Missing name';
		
		$email = $this->input->post('email');
		if(empty($email))
			$errors['email'] = 'Missing email';
		else {
			$this->load->model('user');
			if($this->user->by_email($email) !== false)
				$errors['email'] = 'Email already exists';
		}
		$password = $this->input->post('password');
		$confirm_password = $this->input->post('confirm_password');
		if(empty($password) || empty($confirm_password)){
			if(empty($password))
				$errors['password'] = 'Missing password';
			if(empty($confirm_password))
				$errors['confirm_password'] = 'Missing confirmation password';
		}
		elseif($password != $confirm_password){
				$errors['password'] = "Passwords don't match";
				$errors['confirm_password'] = "Passwords don't match";
		}
		
		$company_name = $this->input->post('company_name');
		if(empty($company_name))
			$errors['company_name'] = 'Missing company name';
		
		$address = $this->input->post('address');
		if(empty($address))
			$errors['address'] = 'Missing address';
		
		$address2 = $this->input->post('address2');
		
		$city = $this->input->post('city');
		if(empty($city))
			$errors['city'] = 'Missing city';
		
		$state_province = $this->input->post('state_province');
		if(empty($state_province))
			$errors['state_province'] = 'Missing state';
		
		$postal_code = $this->input->post('postal_code');
		if(empty($postal_code))
			$errors['postal_code'] = 'Missing postal code';
		
		$country = $this->input->post('country');
		if(empty($country))
			$errors['country'] = 'Missing country';
		
		$phone_number = $this->input->post('phone_number');
		if(empty($phone_number))
			$errors['phone_number'] = 'Missing phone number';
		
		$alt_phone_number = $this->input->post('alt_phone_number');
		
		$pay_by_check = $this->input->post('pay_by_check') === '1' ? '1' : '0';
			
		$accepted_terms = $this->input->post('accepted_terms');
		if(empty($accepted_terms))
			$errors['accepted_terms'] = 'Did not accept terms';
		
		$status_id = $this->input->post('status_id');
		
		$data = array(
			'name' => $name,
			'email' => $email,
			'password' => $password,
			'company_name' => $company_name,
			'address' => $address,
			'address2' => $address2,
			'city' => $city,
			'state_province' => $state_province,
			'postal_code' => $postal_code,
			'country' => $country,
			'phone_number' => $phone_number,
			'alt_phone_number' => $alt_phone_number,
			'accepted_terms' =>$accepted_terms,
			'status_id' => $status_id,
			'created_uid' => $this->get_admin()->id,
			'updated_uid' => $this->get_admin()->id,
			'pay_by_check' => $pay_by_check,			
		);
		
		if(count($errors) === 0){
			$this->load->model('user');
			$user = $this->user->create($data);
			if($user === false){
				$errors['user'] = 'Error creating user';
			}
		}
		
		if(count($errors) === 0){
			$this->template->write_view('content', 'user/edit', array('data' => $data));
		}
		else {
			$data['confirm_password'] = $confirm_password;
			$this->template->write_view('content', 'user/create', array('errors' => $errors, 'data' => $data));
		}
		$this->template->render();
	}
	
	public function edit_user(){
		$errors = array();
		$id = $this->input->post('id');
		if(empty($id))
			$errors['id'] = "Missing id, cannot edit";
		
		$name = $this->input->post('name');
		if(empty($name))
			$errors['name'] = 'Missing name';
	
		$email = $this->input->post('email');
		if(empty($email))
			$errors['email'] = 'Missing email';
		else {
			$this->load->model('user');
			if($this->user->by_email($email, $id) !== false)
				$errors['email'] = 'Email already exists';
		}
		$password = $this->input->post('password');
		$confirm_password = $this->input->post('confirm_password');
		if(!empty($password) || !empty($confirm_password)){
			if(empty($password))
				$errors['password'] = 'Missing password';
			if(empty($confirm_password))
				$errors['confirm_password'] = 'Missing confirmation password';
			if($password != $confirm_password){
				$errors['password'] = "Passwords don't match";
				$errors['confirm_password'] = "Passwords don't match";
			}
		}
	
		$company_name = $this->input->post('company_name');
		if(empty($company_name))
			$errors['company_name'] = 'Missing company name';
	
		$address = $this->input->post('address');
		if(empty($address))
			$errors['address'] = 'Missing address';
	
		$address2 = $this->input->post('address2');
	
		$city = $this->input->post('city');
		if(empty($city))
			$errors['city'] = 'Missing city';
	
		$state_province = $this->input->post('state_province');
		if(empty($state_province))
			$errors['state_province'] = 'Missing state';
	
		$postal_code = $this->input->post('postal_code');
		if(empty($postal_code))
			$errors['postal_code'] = 'Missing postal code';
	
		$country = $this->input->post('country');
		if(empty($country))
			$errors['country'] = 'Missing country';
	
		$phone_number = $this->input->post('phone_number');
		if(empty($phone_number))
			$errors['phone_number'] = 'Missing phone number';
	
		$alt_phone_number = $this->input->post('alt_phone_number');
		
		$pay_by_check = $this->input->post('pay_by_check') === '1' ? '1' : '0';
			
		$accepted_terms = $this->input->post('accepted_terms');
		if(empty($accepted_terms))
			$errors['accepted_terms'] = 'Did not accept terms';
	
		$status_id = $this->input->post('status_id');
		
		$data = array(
				'id' => $id,
				'name' => $name,
				'email' => $email,
				'company_name' => $company_name,
				'address' => $address,
				'address2' => $address2,
				'city' => $city,
				'state_province' => $state_province,
				'postal_code' => $postal_code,
				'country' => $country,
				'phone_number' => $phone_number,
				'alt_phone_number' => $alt_phone_number,
				'accepted_terms' =>$accepted_terms,
				'status_id' => $status_id,
				'updated_uid' => $this->get_admin()->id,
				'pay_by_check' => $pay_by_check,
		);
		
		if(!empty($password))
			$data['password'] = $password;
	
	
		if(count($errors) === 0){
			$this->load->model('user');
			$user = $this->user->edit($data);
			if($user === false){
				$errors['user'] = 'Error creating user';
			}
		}
	
		if(count($errors) === 0){
			$this->template->write_view('content', 'user/edit', array('success' => true, 'data' => $data));
		}
		else {
			$data['confirm_password'] = $confirm_password;
			$this->template->write_view('content', 'user/edit', array('errors' => $errors, 'data' => $data));
		}
		$this->template->render();
	}

    public function delete($id, $page_size = 10, $page_number = 1)
    {
        $errors = array();
        if (empty($id))
            $errors['id'] = "Missing id, cannot delete";

        $this->load->model('user');
        $model_bounce = $this->user->delete($id);
        if (!empty($model_bounce['code']))
            $errors['delete'] = "Error deleting " . $id . "-- " . $model_bounce['code'];

        $opts = array('page_number' => $page_number, 'page_size' => $page_size);
        $count = $this->user->count($opts);
        $users = $this->user->search(array('page_size' => $page_size, 'page_number' => $page_number));
        if (count($errors) === 0)
            $this->template->write_view('content', 'user/index', array('count' => $count, 'users' => $users, 'page_size' => $page_size, 'page_number' => $page_number, 'opts' => $opts));
        else
            $this->template->write_view('content', 'user/index', array('count' => $count, 'users' => $users, 'page_size' => $page_size, 'page_number' => $page_number, 'opts' => $opts, 'errors' => $errors));
        $this->template->render();
    }
}

/* End of file user.php */
/* Location: ./application/controllers/user.php */