<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends PMA_Controller {
	
	function __construct(){
			parent::__construct();
	}
	
	public function discount(){
		$this->load->model('system_settings');
		$this->template->write_view('content', 'settings/discount', array('discount' => $this->system_settings->get_not_stickered_discount()));
		$this->template->render();
	}
	
	public function edit_discount(){
		$errors = array();
		$discount = $this->input->post('discount');
		if(!is_numeric($discount) || doubleval($discount) < 0){
			$errors['discount'] = "Discount must be greater than or equal to zero";
		}
		else {
			$this->load->model('system_settings');
			$this->system_settings->update_not_stickered_discount($discount);		
		}
		
		$this->template->write_view('content', 'settings/discount', array('discount' => $discount, 'errors' => $errors));
		$this->template->render();
	}
}

/* End of file system.php */
/* Location: ./application/controllers/system.php */