<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Specials extends PMA_Controller {
	
	function __construct(){
			parent::__construct();
	}

	public function index()
	{
		$this->page();
	}
	
	public function page($page_size = 10, $page_number = 1){		
		$this->load->model('special');
		$opts = array('page_size' => $page_size, 'page_number' => $page_number);
		$count = $this->special->count($opts);
		$special = $this->special->search($opts);
		$this->template->write_view('content', 'specials/index', array('count' => $count, 'specials' => $special, 'opts' => $opts));
		$this->template->render();
	}
	
	public function add(){
		$this->template->add_css('/resources/css/datepicker.css');
		$this->template->add_js('/resources/js/bootstrap-datepicker.js');
		$this->template->write_view('content', 'specials/create', array());
		$this->template->render();		
	}
	
	public function edit($special_id){
		$this->template->add_css('/resources/css/datepicker.css');
		$this->template->add_js('/resources/js/bootstrap-datepicker.js');
		
		$this->load->model('special');
		$special = $this->special->by_id($special_id, false);
		if(empty($special)){
			$this->template->write_view('content', 'specials/create', array());
		}
		else {
			$special = (array) $special;
			unset($special['password']);
			$this->template->write_view('content', 'specials/edit', array('data' => $special));
		}			
		$this->template->render();				
	}
	
	public function create(){
		$errors = array();
		$title = $this->input->post('title');
		if(empty($title))
			$errors['title'] = 'Missing title';
		
		$expires_date = $this->input->post('expires_date');
		if(empty($expires_date))
			$errors['expires_date'] = 'Missing expiration date';
		$expires_date = strtotime($expires_date);
		if($expires_date === false)
			$errors['expires_date'] = 'Invalid date';
		
		$description = $this->input->post('description');
		if(empty($description))
			$errors['description'] = 'Missing description';
		
		$status_id = $this->input->post('status_id');
		
		$data = array(
			'title' => $title,
			'expires_date' => $expires_date,
			'description' => $description,
			'status_id' => $status_id,
			'created_uid' => $this->get_admin()->id,
			'updated_uid' => $this->get_admin()->id				
		);
		
		if(count($errors) === 0){
			$this->load->model('special');
			$special = $this->special->create($data);
			if($special === false){
				$errors['special'] = 'Error creating special';
			}
		}
		
		$this->template->add_css('/resources/css/datepicker.css');
		$this->template->add_js('/resources/js/bootstrap-datepicker.js');
		if(count($errors) === 0){
			$this->template->write_view('content', 'specials/edit', array('success' => true, 'data' => $data));
		}
		else {
			$this->template->write_view('content', 'specials/create', array('errors' => $errors, 'data' => $data));
		}
		$this->template->render();
	}
	
	public function edit_special(){
		$errors = array();
		$id = $this->input->post('id');
		if(empty($id))
			$errors['id'] = "Missing id, cannot edit";
		
		$title = $this->input->post('title');
		if(empty($title))
			$errors['title'] = 'Missing title';
		
		$expires_date = $this->input->post('expires_date');
		if(empty($expires_date))
			$errors['$expires_date'] = 'Missing experation date';
		$expires_date = strtotime($expires_date);
		if($expires_date === false)
			$errors['expires_date'] = 'Invalid date';
	
		$description = $this->input->post('description');
		if(empty($description))
			$errors['description'] = 'Missing description';
		
		$status_id = $this->input->post('status_id');
		
		$data = array(
				'id' => $id,
				'title' => $title,
				'expires_date' => $expires_date,
				'description' => $description,
				'status_id' => $status_id,
				'updated_uid' => $this->get_admin()->id
		);
	
		if(count($errors) === 0){
			$this->load->model('special');
			$special = $this->special->edit($data);
			if($special === false){
				$errors['special'] = 'Error creating special';
			}
		}
		
		$this->template->add_css('/resources/css/datepicker.css');
		$this->template->add_js('/resources/js/bootstrap-datepicker.js');
		if(count($errors) === 0){
			$this->template->write_view('content', 'specials/edit', array('success' => true, 'data' => $data));
		}
		else {
			$this->template->write_view('content', 'specials/edit', array('errors' => $errors, 'data' => $data));
		}
		$this->template->render();
	}
	
	public function delete($id,$page_size=10,$page_number=1) { //These page defaults are the parameters that index calls on page
		$errors = array();
		if(empty($id))
			$errors['id'] = "Missing id, cannot delete";		
		
		$this->load->model('special');
		$model_bounce = $this->special->delete($id);
		if (!empty($model_bounce['code']))
			$errors['delete']="Error deleting ".$id."-- ".$model_bounce['code'];
		
		$this->page($page_size, $page_number);
	}
}

/* End of file specials.php */
/* Location: ./application/controllers/specials.php */