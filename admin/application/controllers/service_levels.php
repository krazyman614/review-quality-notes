<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Service_Levels extends PMA_Controller {
	
	function __construct(){
			parent::__construct();
	}

	public function index()
	{
		$this->page();
	}
	
	public function page($page_size = 10, $page_number = 1){		
		$this->load->model('service_level');
		$opts = array('page_size' => $page_size, 'page_number' => $page_number);
		$count = $this->service_level->count($opts);
		$service_levels = $this->service_level->search($opts);
		$this->template->write_view('content', 'service_level/index', array('count' => $count, 'service_levels' => $service_levels, 'opts' => $opts));
		$this->template->render();
	}
	
	public function add(){
		$this->template->write_view('content', 'service_level/create', array());
		$this->template->render();		
	}
	
	public function edit($service_level_id){
		$this->load->model('service_level');
		$service_level = $this->service_level->by_id($service_level_id);
		if(empty($service_level)){
			$this->template->write_view('content', 'service_level/create', array());
		}
		else {
			$service_level = (array) $service_level;
			unset($service_level['password']);
			$this->template->write_view('content', 'service_level/edit', array('data' => $service_level));
		}			
		$this->template->render();				
	}
	
	public function delete($service_level_id){
		$this->load->model('service_level');
		$service_level = $this->service_level->by_id($service_level_id, false);
		if(!empty($service_level)){
			$this->service_level->delete($service_level);
		}
		$this->index();
	}
	
	public function create(){
		$errors = array();
		$level = $this->input->post('level');
		if(empty($level))
			$errors['level'] = "Missing level";
		else {
			$level = intval($level);
			if($level <= 0)
				$errors['level'] = "Level must be greater than zero";
		}
		
		$title = $this->input->post('title');
		if(empty($title))
			$errors['title'] = 'Missing title';
		
		$per_note = $this->input->post('per_note');
		if(!empty($per_note)){
			$per_note = doubleval($per_note);
			if($per_note < 0)
				$errors['per_note']  =" Per note must be greater than or equal to zero";
		}
		else {
			$per_note = 0;
		}
		
		$time_to_process = $this->input->post('time_to_process');
		
		$status_id = $this->input->post('status_id');
		
		$data = array(
			'level' => $level,
			'title' => $title,
			'per_note' => $per_note,
			'time_to_process' => $time_to_process,
			'status_id' => $status_id,
			'created_uid' => $this->get_admin()->id,
			'updated_uid' => $this->get_admin()->id				
		);
		
		if(count($errors) === 0){
			$this->load->model('service_level');
			$service_level = $this->service_level->create($data);
			if($service_level === false){
				$errors['service_level'] = 'Error creating service_level';
			}
		}
		
		if(count($errors) === 0){
			$this->template->write_view('content', 'service_level/edit', array('success' => true, 'data' => $data));
		}
		else {
			$this->template->write_view('content', 'service_level/create', array('errors' => $errors, 'data' => $data));
		}
		$this->template->render();
	}
	
	public function edit_service_level(){
		$errors = array();
		$id = $this->input->post('id');
		if(empty($id))
			$errors['id'] = "Missing id, cannot edit";
		
		$level = $this->input->post('level');
		if(empty($level))
			$errors['level'] = "Missing level";
		else {
			$level = intval($level);
			if($level <= 0)
				$errors['level'] = "Level must be greater than zero";
		}
		
		$title = $this->input->post('title');
		if(empty($title))
			$errors['title'] = 'Missing title';
		
		$per_note = $this->input->post('per_note');
		if(!empty($per_note)){
			$per_note = doubleval($per_note);
			if($per_note < 0)
				$errors['per_note']  =" Per note must be greater than or equal to zero";
		}
		else {
			$per_note = 0;
		}
		
		$time_to_process = $this->input->post('time_to_process');
		
		$status_id = $this->input->post('status_id');
		
		$data = array(
			'id' => $id,
			'level' => $level,
			'title' => $title,
			'per_note' => $per_note,
			'time_to_process' => $time_to_process,
			'status_id' => $status_id,
			'updated_uid' => $this->get_admin()->id				
		);
	
		if(count($errors) === 0){
			$this->load->model('service_level');
			$service_level = $this->service_level->edit($data);
			if($service_level === false){
				$errors['service_level'] = 'Error creating service_level';
			}
		}
	
		if(count($errors) === 0){
			$this->template->write_view('content', 'service_level/edit', array('success' => true, 'data' => $data));
		}
		else {
			$this->template->write_view('content', 'service_level/edit', array('errors' => $errors, 'data' => $data));
		}
		$this->template->render();
	}
}

/* End of file service_level.php */
/* Location: ./application/controllers/service_level.php */