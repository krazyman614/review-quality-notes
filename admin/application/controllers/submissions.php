<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Submissions extends PMA_Controller {
	const EMAIL = 'info@rqnotes.com';
	
	function __construct(){
			parent::__construct(self::VALIDATION_YES);
	}

	public function index()
	{
		$this->search();
	}
	
	public function view($submission_id, $msg = '', $notes = null){
		$page_data = array();
		
		$this->load->model('submission');		
		$submission = $this->submission->by_id($submission_id, true);
		
		$page = '';
		switch($submission["status_id"]){
			case 100:
				$this->load->model('region');
				$region = $this->region->by_id($submission['region_id']);
				
				$this->load->model('service_level');
				$service_levels = $this->service_level->search(array());
				
				$this->load->model('note_type');
				$note_types = $this->note_type->search(array('region_id' => $submission['region_id']));
				
				$this->load->model('grading_service');
				$grading_services = $this->grading_service->search(array());
				
				$this->load->model('postage_country');
				$countries = $this->postage_country->search(array());
				
				$this->load->model('postage_rate');
				$postage = $this->postage_rate->get_postage($submission['country']);
				
				$page_data = array(
						'access_token' => $this->get_access_token(),
						'region' => $region,
						'service_levels' => $service_levels,
						'note_types' => $note_types,
						'grading_services' => $grading_services,
						'countries' => $countries,
						'postage' => $postage
				);
				
				$page = 'submissions/new_submission_form';
				break;				
			case 300:
			case 400:
				$page = 'submissions/pre-process';
				break;
			case 1000:
				$page = 'submissions/in-process';
				break;
			case 1100:
				$page = 'submissions/mailing';
				break;
			default:
				$page = 'submissions/view';
				break;
		}
		
		if(!empty($notes))
			$submission['notes'] = $notes;
		
		$page_data['msg'] = $msg;
		$page_data['submission'] = $submission;
		$page_data['current_admin'] = $this->get_admin();
		
		$this->template->write_view('content', $page, $page_data);
		$this->template->render();
	}
	
	public function search($page_size = 10, $page_number = 1){
		$opts = array(
			'name' => $this->input->get_post('name'),
			'order_number' => $this->input->get_post('order_number'),
			'catalog_number' => $this->input->get_post('catalog_number'),
			'serial_number' => $this->input->get_post('serial_number'),
			'start_date' => $this->input->get_post('start_date'),
			'end_date' => $this->input->get_post('end_date'),
			'status_id' => $this->input->get_post('status_id'),
			'stickered' => $this->input->get_post('stickered') == 'yes',
			'sticker_number' => $this->input->get_post('sticker_number'),
			'page_size' => $page_size,
			'page_number' => $page_number
		);
		
		if(!empty($opts['end_date']))
			$opts['end_date'] = strtotime($opts['end_date']);
		else
			$opts['end_date'] = time();
		
		if(!empty($opts['start_date']))
			$opts['start_date'] = strtotime($opts['start_date']);
		else 
			$opts['start_date'] = $opts['end_date'] - (7*24*3600);
		
		
		$this->load->model('submission');
		$count = $this->submission->count($opts);
		$submissions = $this->submission->search($opts);
		$submission_status = $this->submission->get_status();
		
		$this->template->add_css('/resources/css/datepicker.css');
		$this->template->add_js('/resources/js/bootstrap-datepicker.js');
		$this->template->write_view('content', 'submissions/track', array('opts' => $opts, 'submissions' => $submissions, 'count' => $count, 'submission_status' => $submission_status));
		$this->template->render();
		
	}
	
	public function edit_submission($submission_id) {
		$result = $this->save_edits($submission_id);
		if($result !== true){
			$this->view($submission_id, $result['errors'], $result['notes']);
		}
		else {	
			$this->view($submission_id);
		}
	}
	
	public function mark_incomplete($submission_id){
		$result = $this->save_edits($submission_id);
		if($result !== true){
			$this->view($submission_id, $result['errors'], $result['notes']);
		}
		else {
			$this->load->model('submission');
			$submission = $this->submission->by_id($submission_id, false);
			$this->submission->update_status($submission, $this->get_admin(), 200);
							
			$html = 'The received submission is missing some notes';
			$html .= ', click <a href="http://www.rqnotes.com/submissions/view/' . $submission['id'] . '">here</a> to manage the submission';
			$comments = $this->input->post('comments');
			if(!empty($comments)){
				$html .= '<br/><br/><strong>Comments</strong><br/>' . $comments;
			}
			
			$this->load->model('user');
			$user = $this->user->by_id($submission['created_uid']);
			
			$this->load->library('email');
			
			$this->email->from(self::EMAIL, 'Review Quality Notes');
			$this->email->to($user->email);
			$this->email->subject('Incomplete Submission');
			$this->email->message($html);		
			$this->email->send();
			
			$this->view($submission_id, 'Submission is marked as incomplete, the user has been notified.');
		}
	}
	
	public function mark_received($submission_id){
		$result = $this->save_edits($submission_id);
		if($result !== true){
			$this->view($submission_id, $result['errors'], $result['notes']);
		}
		else {
			$this->load->model('submission');
			$submission = $this->submission->by_id($submission_id, false);
		
			$this->submission->update_status($submission, $this->get_admin(), 300);
		
			$html = 'The submission of your note(s) to Review Quality Notes for your order #'.$submission['order_number'].' has been recieved';
		
			$this->load->model('user');
			$user = $this->user->by_id($submission['created_uid']);
		
			// retrieve the submission formatted so we can build our email with it
			$submission = $this->submission->by_id($submission_id, true);
			$notes = &$submission['notes'];
			
			$total_service_cost = 0;
			
			foreach ($notes as $note) {
				$total_service_cost += $note['service_cost'];
			}
			
			$shipping = 'Pickup';
			
			if (!empty($submission['fedex_account_number'])) {
				$shipping = 'Fedex Acct: ' . $submission['fedex_account_number'];
			} elseif (!empty($submission['postage'])) {
				$shipping = 'Registered Mail: $' . money_format('%i', $submission['postage']);
			}
			
			$this->load->model('service_level');
			$level = $this->service_level->by_id($notes[0]['service_level_id']);
			
			$page_data = compact('submission', 'notes', 'user', 'shipping', 'level', 'total_service_cost');
			
			// get the content to load into the template
			$content = $this->load->view('emails/submissions/received', $page_data, true);
			$title = 'NOTE RECEIVED';
			
			$page_data = compact('content', 'title');
			
			// load and get the template
			$html = $this->load->view('emails/template', $page_data, true);
		
			$this->load->library('email');
		
			$this->email->from(self::EMAIL, 'Review Quality Notes');
			$this->email->to($user->email);
			$this->email->subject($title);
			$this->email->message($html);
			$this->email->send();
		
			$this->view($submission_id);
		}
	}
	
	public function mark_in_process($submission_id) {
		$this->load->model('submission');
		$submission = $this->submission->by_id($submission_id, true);
		
		$this->load->model('submission_note');
		foreach($submission['notes'] as $note){
			if($note['status_id'] != 600){
				$this->submission_note->update_status($note, $this->get_admin(), 1000);
			}
		}
		$this->submission->update_status($submission, $this->get_admin(), 1000);
		
		$this->load->model('user');
		$user = $this->user->by_id($submission['created_uid']);
		
		/*
		$html = 'Your notes for order #'.$submission['order_number'].' are currently being reviewed.';
		*/
		
		// get the content to load into the template
		$content = $this->load->view('emails/submissions/in_process', array(), true);
		
		$page_data = array('content' => $content, 'title' => 'In Process');
		
		// load and get the template
		$html = $this->load->view('emails/template', $page_data, true);
	
		$this->load->library('email');
		
		$this->email->from(self::EMAIL, 'Review Quality Notes');
		$this->email->to($user->email);
		$this->email->subject('Reviewing In Process');
		$this->email->message($html);
		$this->email->send();
		
		$this->view($submission_id);
	}
	
	public function mark_mailed($submission_id) {
		$this->load->model('submission');
		$submission = $this->submission->by_id($submission_id, true);
		
		$tracking_number = $this->input->post('tracking_number');
		if(empty($tracking_number)){
			$this->view($submission_id, 'Missing Tracking number');
			return;			
		}
		$this->submission->update_tracking_number($submission, $this->get_admin(), $tracking_number);
		
		$this->load->model('submission_note');
		foreach($submission['notes'] as $note){
			if($note['status_id'] == 1100){
				$this->submission_note->update_status($note, $this->get_admin(), 2000);
			}
		}
		$this->submission->update_status($submission, $this->get_admin(), 2000);
		
		$html = 'The submissions is completed and reviewed, it has been placed in the mail';
		$html .= '<br/><br/>The tracking number is: <strong>' . $tracking_number . '</strong>';
		
		$this->load->model('user');
		$user = $this->user->by_id($submission['created_uid']);
		
		$this->load->library('email');
		
		$this->email->from(self::EMAIL, 'Review Quality Notes');
		$this->email->to($user->email);
		$this->email->subject('Reviewed and Complete Submission has been Mailed');
		$this->email->message($html);
		$this->email->send();
		
		$this->view($submission_id);
	}
	
	public function grade($submission_id) {
		$note_id = $this->input->post('note_id');
		$this->load->model('submission_note');
		$this->load->model('submission_note_grade');
		if(!empty($note_id)){
			$note = $this->submission_note->by_id($note_id);
			if($note['status_id'] < 1100){
				$paper = $this->input->post('paper');
				$paper = empty($paper) ? 0 : $paper;
				$color = $this->input->post('color');
				$color = empty($color) ? 0 : $color;
				$printing = $this->input->post('printing');
				$printing = empty($printing) ? 0 : $printing;
				$face_margins = $this->input->post('face_margins');
				$face_margins = empty($face_margins) ? 0 : $face_margins;
				$face_centering = $this->input->post('face_centering');
				$face_centering = empty($face_centering) ? 0 : $face_centering;
				$back_margins = $this->input->post('back_margins');
				$back_margins = empty($back_margins) ? 0 : $back_margins;
				$back_centering = $this->input->post('back_centering');
				$back_centering = empty($back_centering) ? 0 : $back_centering;
				$registration = $this->input->post('registration');
				$registration = empty($registration) ? 0 : $registration;
				$comments = $this->input->post('comments');
				$final = $this->input->post('final');
				
				$data = array(
						'note_id' => $note_id,
						'paper' => $paper,
						'color' => $color,
						'printing' => $printing,
						'face_margins' => $face_margins,
						'face_centering' => $face_centering,
						'back_margins' => $back_margins,
						'back_centering' => $back_centering,
						'registration' => $registration,
						'comments' => $comments,
						'is_final' => $final,
						'created_uid' => $this->get_admin()->id
				);
				
				$this->submission_note_grade->create($data);			
				
				if($final > 0){
					$this->load->model('submission_note');
					$note = $this->submission_note->by_id($note_id);
					$this->submission_note->update_status($note, $this->get_admin(), 1100);
					$stickered = $this->input->post('sticker');
					if($stickered > 0){
						$sticker_serial = $this->input->post('sticker_serial_number');
						$this->submission_note->update_stickered($note, $this->get_admin(), $sticker_serial);
					}
				}
			}
		}
		
		$this->finalize_submission($submission_id);
		
		$this->view($submission_id);
	}
	
	private function save_edits($submission_id){
		$this->load->model('submission');
		$submission = $this->submission->by_id($submission_id,true);
		
		$this->load->model('submission_note');
		
		$errors = array();
		$notes = array();
		
		
		$total = 0;
		$total_declared_value = 0;
		$sub_total = 0;
		
		$receiveds = $this->input->post('received');
		$note_ids = $this->input->post('note_id');
		$service_levels = $this->input->post('service_level');
		$note_types = $this->input->post('note_type');
		$note_countries = $this->input->post('note_country');
		$denominations = $this->input->post('denomination');
		$catalog_numbers = $this->input->post('catalog_number');
		$pps = $this->input->post('pp');
		$serial_numbers = $this->input->post('serial_number');
		$grading_services = $this->input->post('grading_service');
		$barcodes = $this->input->post('barcode');
		$declared_values = $this->input->post('declared_value');
		if(count($note_ids) > 0){
			$this->load->model('service_level');
			$this->load->model('note_type');
			$this->load->model('grading_service');
		
			$current_service_level = '';
			for($itt =0; $itt < count($service_levels); $itt++){
				$status_id = 200;
		
				$received = $receiveds[$itt];
				if($received){
					$status_id = 300;
				}
		
				$service_level = $this->service_level->by_id($service_levels[$itt]);
				if($service_level === false){
					$errors['service_level-' . $itt] = 'Missing service level';
				}
				else {
					if($current_service_level == ''){
						$current_service_level = $service_level;
					}
					else if($service_level->id != $current_service_level->id)
						$errors['service_level-' . $itt] = 'Order must be of one service level';
				}
				
				$note_type_id = null;
				$note_country = null;				
				switch($submission['region_id']){
					case 'us':
						$note_type_id = $note_types[$itt];
						$note_type = $this->note_type->by_id($note_types[$itt]);
						if($note_type === false){
							$errors['note_type-' . $itt] = 'Missing note type';
						}
						break;
					default:
						$note_country = $note_countries[$itt];
						if(empty($note_country)){
							$errors['note_country-' . $itt] = 'Missing note country';							
						}						
						break;
				}
				$denomination = $denominations[$itt];
				if(!is_numeric($denominations[$itt]) || doubleval($denomination) <= 0){
					$errors['denomination-' . $itt] = 'Missing denomination, denomination must be greater than 0';
				}
				$catalog_number = $catalog_numbers[$itt];
				$pp = $pps[$itt];
				
				$serial_number = $serial_numbers[$itt];
				
				$grading_service = $this->grading_service->by_id($grading_services[$itt]);
				if($grading_services === false){
					$errors['grading_service-' . $itt] ='Missing grading service';
				}
				$barcode = $barcodes[$itt];
				if(empty($barcode)){
					$errors['barcode-' . $itt] ='Missing barcode';
				}
		
				$declared_value = $declared_values[$itt];
				if(!is_numeric($declared_values[$itt]) || doubleval($declared_value) <= 0){
					$errors['declared_value-' . $itt] = 'Missing declared value, declared value must be greater than 0';
				}
				else {
					$total_declared_value += doubleval($declared_value);
				}
		
				$service_cost = 0;
				if($current_service_level != ''){
					if($current_service_level->per_note <= 0)
						$errors['service_cost-' . $itt] = 'Missing service cost, service cost must be greater than 0';
					else {
						$service_cost = $current_service_level->per_note;
					}
				}
		
				$note = array(
						'submission_id' => $submission['id'],
						'id' => $note_ids[$itt],
						'sequence' => $itt+1,
						'service_level_id' => $service_levels[$itt],
						'note_type_id' => $note_type_id,
						'note_country' => $note_country,
						'denomination' => $denomination,
						'catalog_number' => $catalog_number,
						'pp' => $pp,
						'serial_number' => $serial_number,
						'grading_service_id' => $grading_services[$itt],
						'barcode' => $barcode,
						'declared_value' => $declared_value,
						'service_cost' => $service_cost,
						'status_id' => $status_id,
						'created_uid' => $this->get_admin()->id,
						'updated_uid' => $this->get_admin()->id
				);
		
				array_push($notes, $note);
		
				$sub_total += $service_cost;
			}
			$data['total_declared_value'] = $total_declared_value;
		}
		else {
			$errors['notes'] = 'Please enter one note to submit';
		}
		
		$postage = 0.0;
		$pickup = $this->input->get_post('pickup');
		if($pickup != 'yes' && empty($submission['fedex_account_number'])){
			$this->load->model('postage_rate');
			$postage = $this->postage_rate->get_postage_amount($submission['country'], $total_declared_value);
		}
		
		if($postage === false) {
			$errors['postage'] = 'Please email for postage';
		}
		else {
			$total = $sub_total + $postage;		
			$data['sub_total'] = $sub_total;
			$data['postage'] = $postage;
			$data['total'] = $total;
		}
		
		if(count($errors) > 0){
			return array('errors' => $errors, 'notes' => $notes);
		}
		else {
			$this->submission->update($submission, array(
					'postage' => $postage,
					'sub_total' => $sub_total,
					'total' => $total,
					'total_declared_value' => $total_declared_value,
					'updated_uid' => $this->get_admin()->id));
			foreach($notes as $note){
				$this->submission_note->update($note);
			}
		
			$comments = $this->input->post('comments');
			if(!empty($comments)){ //could be a bug here
				$this->load->model('submission_comments');
				$this->submission_comments->create(array('comment' => $comments, 'submission_id' => $submission_id, 'created_uid' => $this->get_admin()->id));
			}
			
			// set the items deleted on the UI to status of 0
			$notes = $this->submission_note->by_submission($submission, false);
			
			foreach ($notes as $note) {
				if ($note['status_id'] == '100') {
					$note['status_id'] = 0;
					$this->submission_note->edit($note);
				}
			}
			
			return true;
		}
	}
	
	private function finalize_submission($submission_id){
		$this->load->model('submission');
		$submission = $this->submission->by_id($submission_id, true);
		
		$finalized_note_count = 0;
		$received_note_count = 0;
		foreach($submission['notes'] as $note){
			switch($note['status_id']){
				case 1100:
					$finalized_note_count++;
				case 1000:
					$received_note_count++;
					break;
			}
		}
		if($finalized_note_count === $received_note_count){
			$this->submission->update_status($submission, $this->get_admin(), 1100);
			//Calculate discount
			$this->load->model('system_settings');
			$discount = doubleval($this->system_settings->get_not_stickered_discount()) / 100.0;
				
			$total_discount = 0.0;
			foreach($submission['notes'] as $note){
				if($note['stickered'] < 1){
					$total_discount += doubleval($note['service_cost']) * $discount;
				}
			}
			$this->submission->update_discount($submission, $this->get_admin(), $this->system_settings->get_not_stickered_discount(), $total_discount);
		
			$this->load->model('user');
			$user = $this->user->by_id($submission['created_uid']);
			
			/* the old email option if we need to revert 
			$html = 'One or more of your notes for order #'.$submission['order_number'].' has been reviewed';
			$title = 'New Review for Your Submission';
			*/
			
			// retrieve the submission formatted so we can build our email with it
			$submission = $this->submission->by_id($submission_id, true);
			$notes = &$submission['notes'];
			
			$total_service_cost = 0;
			
			foreach ($notes as &$note) {
				$total_service_cost += $note['service_cost'];
				
				if ($note['status_id'] < 1100) {
					$note['sticker_serial_number'] = '---';
				} elseif ($note['stickered'] > 0) {
					$note['sticker_serial_number'] = $note['sticker_serial_number'];
				} else {
					$note['sticker_serial_number'] = 'Not Stickered';
				}
			}
			unset($note);
			
			$shipping = 'Pickup';
			
			if (!empty($submission['fedex_account_number'])) {
				$shipping = 'Fedex Acct: ' . $submission['fedex_account_number'];
			} elseif (!empty($submission['postage'])) {
				$shipping = 'Registered Mail: $' . money_format('%i', $submission['postage']);
			}
			
			$this->load->model('service_level');
			$level = $this->service_level->by_id($notes[0]['service_level_id']);
			
			$page_data = compact('submission', 'notes', 'user', 'shipping', 'level', 'total_service_cost');
			
			// get the content to load into the template
			$content = $this->load->view('emails/submissions/finalized', $page_data, true);
			$title = 'COMPLETED NOTE REVIEW';
			
			$page_data = compact('content', 'title');
			
			// load and get the template
			$html = $this->load->view('emails/template', $page_data, true);
					
			$this->load->library('email');
			
			$this->email->from(self::EMAIL, 'Review Quality Notes');
			$this->email->to($user->email);
			$this->email->subject($title);
			$this->email->message($html);
			$this->email->send();
		}
	}
	
}

/* End of file submissions.php */
/* Location: ./application/controllers/submissions.php */