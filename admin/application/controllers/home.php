<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends PMA_Controller {
	
	function __construct(){
			parent::__construct(self::VALIDATION_OPTIONAL);
	}

	public function index()
	{
		if($this->get_admin() == false){
			$this->template->write_view('content', 'login', array());
			$this->template->render();
		}
		else {
			$this->load->helper('url');
			redirect('/submissions');
			exit();
		}
	}
	
	public function login(){
		$errors = array();
		$email = $this->input->post('email');
		if(empty($email)) $errors['email'] = 'Missing email';
		$password = $this->input->post('password');
		if(empty($password)) $errors['password'] = 'Missing password';
		
		$this->load->model('admin');
		$admin = $this->admin->by_email_password($email, $password);
		if($admin !== false){
			$this->load->model('admin_login');
			$admin_login = $this->admin_login->create($admin);
			if($admin_login !== false){
				$this->session->set_userdata('login_id', $admin_login->id);
				$this->load->helper('url');
				redirect('/submissions');
				exit();
			}
			else {
				$errors['admin_login'] = 'Invalid admin login';
			}
		}
		else {
			$errors['admin'] = 'Invalid email/password';
		}
		
		if(count($errors) > 0){
			$this->template->write_view('content', '/login', array('errors' => $errors, 'data' => array('email' => $email, 'password' => $password)));
		}
		$this->template->render();
	}
	
	public function create_default(){
		$data = array(
			'email' => 'tim.dennis@seisan.com',
			'password' => 'letmein',
			'name' => 'Tim Dennis',
			'status_id' => 1	
		);
		
		$this->load->model('admin');
		$this->admin->create($data);
		
	}
	
	public function logout(){
		$this->session->unset_userdata('login_id');
		$this->load->helper('url');
		redirect('/home');		
	}
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */