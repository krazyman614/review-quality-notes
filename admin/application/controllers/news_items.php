<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News_Items extends PMA_Controller {
	
	function __construct(){
			parent::__construct();
	}

	public function index()
	{
		$this->page();
	}
	
	public function page($page_size = 10, $page_number = 1){		
		$this->load->model('news_item');
		$opts = array('page_size' => $page_size, 'page_number' => $page_number);
		$count = $this->news_item->count($opts);
		$news_items = $this->news_item->search($opts);
		$this->template->write_view('content', 'news_item/index', array('count' => $count, 'news_items' => $news_items, 'opts' => $opts));
		$this->template->render();
	}
	
	public function add(){
		$this->template->add_css('/resources/css/datepicker.css');
		$this->template->add_js('/resources/js/bootstrap-datepicker.js');
		$this->template->write_view('content', 'news_item/create', array());
		$this->template->render();		
	}
	
	public function edit($news_item_id){
		$this->template->add_css('/resources/css/datepicker.css');
		$this->template->add_js('/resources/js/bootstrap-datepicker.js');
		
		$this->load->model('news_item');
		$news_item = $this->news_item->by_id($news_item_id);
		if(empty($news_item)){
			$this->template->write_view('content', 'news_item/create', array());
		}
		else {
			$news_item = (array) $news_item;
			unset($news_item['password']);
			$this->template->write_view('content', 'news_item/edit', array('data' => $news_item));
		}			
		$this->template->render();				
	}
	
	public function create(){
		$errors = array();
		$title = $this->input->post('title');
		if(empty($title))
			$errors['title'] = 'Missing title';
		
		$date = $this->input->post('date');
		if(empty($date))
			$errors['date'] = 'Missing date';
		$date = strtotime($date);
		if($date === false)
			$errors['date'] = 'Invalid date';
		
		$news = $this->input->post('news');
		if(empty($news))
			$errors['news'] = 'Missing news';
		
		$status_id = $this->input->post('status_id');
		
		$data = array(
			'title' => $title,
			'date' => $date,
			'news' => $news,
			'status_id' => $status_id,
			'created_uid' => $this->get_admin()->id,
			'updated_uid' => $this->get_admin()->id				
		);
		
		if(count($errors) === 0){
			$this->load->model('news_item');
			$news_item = $this->news_item->create($data);
			if($news_item === false){
				$errors['news_item'] = 'Error creating news_item';
			}
		}
		
		$this->template->add_css('/resources/css/datepicker.css');
		$this->template->add_js('/resources/js/bootstrap-datepicker.js');
		if(count($errors) === 0){
			$this->template->write_view('content', 'news_item/edit', array('success' => true, 'data' => $data));
		}
		else {
			$this->template->write_view('content', 'news_item/create', array('errors' => $errors, 'data' => $data));
		}
		$this->template->render();
	}
	
	public function edit_news_item(){
		$errors = array();
		$id = $this->input->post('id');
		if(empty($id))
			$errors['id'] = "Missing id, cannot edit";
		
		$title = $this->input->post('title');
		if(empty($title))
			$errors['title'] = 'Missing title';
		
		$date = $this->input->post('date');
		if(empty($date))
			$errors['date'] = 'Missing date';
		$date = strtotime($date);
		if($date === false)
			$errors['date'] = 'Invalid date';
	
		$news = $this->input->post('news');
		if(empty($news))
			$errors['news'] = 'Missing news';
		
		$status_id = $this->input->post('status_id');
		
		$data = array(
				'id' => $id,
				'title' => $title,
				'date' => $date,
				'news' => $news,
				'status_id' => $status_id,
				'updated_uid' => $this->get_admin()->id
		);
	
		if(count($errors) === 0){
			$this->load->model('news_item');
			$news_item = $this->news_item->edit($data);
			if($news_item === false){
				$errors['news_item'] = 'Error creating news_item';
			}
		}
		
		$this->template->add_css('/resources/css/datepicker.css');
		$this->template->add_js('/resources/js/bootstrap-datepicker.js');
		if(count($errors) === 0){
			$this->template->write_view('content', 'news_item/edit', array('success' => true, 'data' => $data));
		}
		else {
			$this->template->write_view('content', 'news_item/edit', array('errors' => $errors, 'data' => $data));
		}
		$this->template->render();
	}
	
	//could probably shorten this function and pass to page
	public function delete($id, $page_size=10, $page_number=1) {
		$errors = array();
		if(empty($id))
			$errors['id'] = "Missing id, cannot delete";
		
		$this->load->model('news_item');
		$model_bounce = $this->news_item->delete($id);
		if (!empty($model_bounce['code']))
			$errors['delete']="Error deleting ".$id."-- ".$model_bounce['code'];
		
		$opts = array('page_number'=>$page_number, 'page_size'=>$page_size);
		$count = $this->news_item->count($opts);
		$news_items = $this->news_item->search(array('page_size' => $page_size, 'page_number' => $page_number));
		if (count($errors)===0)
			$this->template->write_view('content', 'news_item/index', array('count' => $count, 'news_items' => $news_items, 'page_size' => $page_size, 'page_number' => $page_number, 'opts'=>$opts));
		else
			$this->template->write_view('content', 'news_item/index', array('count' => $count, 'news_items' => $news_items, 'page_size' => $page_size, 'page_number' => $page_number, 'opts'=>$opts, 'errors'=>$errors));
		$this->template->render();
	}
	
}

/* End of file news_item.php */
/* Location: ./application/controllers/news_item.php */