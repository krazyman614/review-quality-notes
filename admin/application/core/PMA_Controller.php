<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PMA_Controller extends CI_Controller {
	const VALIDATION_NO = 'no';
	const VALIDATION_OPTIONAL = 'optional';
	const VALIDATION_YES = 'yes';
	
	var $login_id;
	var $login;
	var $current_admin = false;
	
	function __construct($validation = 'yes')
	{
		parent::__construct();
		
		switch($validation){
			case self::VALIDATION_OPTIONAL:
				$this->login_id = $this->session->userdata('login_id');
				if(!empty($this->login_id)){
					$this->load->model('admin_login');
					$this->login = $this->admin_login->get_login($this->login_id);
					if(!$this->login === false){
						$this->load->model('admin');
						$this->current_admin = $this->admin->by_id($this->login->admin_id);
					}
				}
				break;			
			case self::VALIDATION_YES:
				$this->login_id = $this->session->userdata('login_id');
				if(empty($this->login_id)) $this->redirect_login();
				$this->load->model('admin_login');
				$this->login = $this->admin_login->get_login($this->login_id);
				if($this->login === false) $this->redirect_login();
				$this->load->model('admin');
				$this->current_admin = $this->admin->by_id($this->login->admin_id);
				if($this->current_admin === false) $this->redirect_login();
				break;			
		}
		
		$this->load->library('template');
		$this->template->write_view('header', 'inc/header.php', array('current_admin' => $this->current_admin)); 
		$this->template->write_view('footer', 'inc/footer.php', array());
		
	}
	
	public function redirect_login(){
		$this->load->helper('url');
		redirect('/');
		exit();
	}
	
	public function get_admin(){
		return $this->current_admin;
	}
	
	public function get_access_token(){
		return $this->login_id;
	}
}
?>