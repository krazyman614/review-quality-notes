<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class News_Item extends CI_Model {
	const TABLE = "news_items";
	const FIELDS = "news_items.id, news_items.title, UNIX_TIMESTAMP(news_items.date) as date, news_items.news, news_items.status_id, news_items.created_uid, news_items.created_date, news_items.updated_uid, news_items.updated_date";

	function __construct()
	{
		parent::__construct();
	}
	
	public function count($opts){
		$this->db->from(self::TABLE);
		foreach($opts as $key=>$value){
			switch($key){
				case 'page_size':
				case 'page_number': break;
			}
		}
		return $this->db->count_all_results();
	}
	
	public function search($opts){
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		foreach($opts as $key=>$value){
			switch($key){
				case 'page_size':
						$page_size = 0;
						if(is_numeric($value)){
							$page_size = intval($value);
						}
						if($page_size > 0){
							$page_number = 1;
							if(isset($opts['page_number']) && is_numeric($opts['page_number']) && intval($opts['page_number']) > 0){
								$page_number = intval($opts['page_number']);
							}
							$this->db->limit($page_size, ($page_number - 1) * $page_size);	
						}
						break;
				case 'page_number': break;
			}
		}
		$query = $this->db->get();
		return $query->result_array();
	}
	
	public function by_id($news_item_id, $active=true){
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		$this->db->where('news_items.id', $news_item_id);
		if ($active)
			$this->db->where('status_id >= 1');
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return (array)$query->row();
		}
		else {
			return false;
		}
	}
	
	public function create($news_item){
		$this->load->library('uuid');
		$news_item['id'] = $this->uuid->v5('PMA.news_item');
 		$news_item['created_date'] = date('Y-m-d H:i:s');
		$news_item['updated_date'] = date('Y-m-d H:i:s');
		$news_item['date'] = date('Y-m-d H:i:s', $news_item['date']);
		
		$this->db->insert(self::TABLE, $news_item);
		return $this->by_id($news_item['id'],false);
	}
	
	public function edit($news_item){
		$news_item['updated_date'] = date('Y-m-d H:i:s');
		$news_item['date'] = date('Y-m-d H:i:s', $news_item['date']);
		
		$this->db->where('news_items.id', $news_item['id']);
		$this->db->update(self::TABLE, $news_item);
		return $this->by_id($news_item['id'],false);
	}
	
	public function mark_inactive($id) {
		return $this->db->where('id',$id)->update('news_items', array('status_id'=>0) );
	}
	
	public function delete($id) {
		return $this->db->delete('news_items', array('id'=>$id) );
	}
}
