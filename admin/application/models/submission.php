<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Submission extends CI_Model {
	const TABLE = "submissions";
	const FIELDS = "submissions.*";
	
	function __construct()
	{
		parent::__construct();
	}
	
	private function format_all($submissions){
		$formatted = array();
		foreach($submissions as $submission){
			array_push($formatted, $this->format($submission));
		}		
		return $formatted;
	}
	
	public function get_status(){
		return array(
			array('id' => 100, 'name' => 'New Submission'),
			array('id' => 200, 'name' => 'Incomplete'),
			array('id' => 300, 'name' => 'Received'),
			array('id' => 400, 'name' => 'Customer Approved'),
			array('id' => 500, 'name' => 'Customer Canceled'),
			array('id' => 1000, 'name' => 'In Progress'),
			array('id' => 1100, 'name' => 'Reviewing Complete'),
			array('id' => 2000, 'name' => 'Mailed')	
		);
	}
	
	private function format($submission){
		switch($submission['status_id']){
			case 100:
				$submission['status'] = 'New Submission';
				break;
			case 200:
				$submission['status'] = 'Incomplete';
				break;
			case 300:
				$submission['status'] = 'Received';
				break;
			case 400:
				$submission['status'] = 'Customer Approved';
				break;
			case 500:
				$submission['status'] = 'Customer Canceled';
				break;
			case 1000:
				$submission['status'] = 'In Progress';
				break;
			case 1100:
				$submission['status'] = 'Reviewing Complete';
				break;
			case 2000:
				$submission['status'] = 'Mailed';
				break;
			default:
				$submission['status'] = 'Unknown [' . $submission['status_id'] . ']';
				break;
		}
		
		$this->load->model('submission_note');
		$submission['notes'] = $this->submission_note->by_submission($submission);
		return $submission;
	}
	
	public function by_id($submission_id, $formatted = false){
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		$this->db->where('submissions.id', $submission_id);
		$this->db->where('status_id >= 100');
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			if($formatted)
				return $this->format((array)$query->row());
			else
				return (array)$query->row();
		}
		else {
			return false;
		}
	}
	
	public function count($opts){
		$this->db->from(self::TABLE);
		$this->db->where('submissions.status_id >= 100');
		if(isset($opts['historical'])){
			$this->db->join('submission_status_change', 'submission_status_change.submission_id = submissions.id');
			$this->db->where('submission_status_change.status_id >= 100');
		}
		else {
			$this->db->where('submissions.status_id >= 100');
		}
			
		$notes_query = '';
		foreach($opts as $key=>$value){
			if(empty($value)) continue;
			switch($key){
				case 'historical':
				case 'page_size':
				case 'page_number': break;
				case 'start_date':
					$this->db->where('submissions.created_date >= ', date('Y-m-d H:i:s', $value));
					break;
				case 'end_date':
					$this->db->where('submissions.created_date <= ', date('Y-m-d H:i:s', $value + (24 * 3600 - 1)));
					break;			
				case 'status_id':
					$this->db->where('submissions.status_id', $value);
					break;
				case 'submission_status_change.start_date':
					$this->db->where('submission_status_change.created_date >= ', date('Y-m-d H:i:s', $value));
					break;
				case 'submission_status_change.end_date':
					$this->db->where('submission_status_change.created_date <= ', date('Y-m-d H:i:s', $value + (24 * 3600 - 1)));
					break;			
				case 'submission_status_change.status_id':
					$this->db->where('submission_status_change.status_id', $value);
					break;
				case 'sticker_number':
				case 'sticker_serial_number':
				case 'stickered':
				case 'serial_number':
				case 'catalog_number':
					if(!empty($value)){
						if($notes_query == '')
							$notes_query = 'SELECT submission_notes.id FROM submission_notes WHERE submission_notes.status_id > 0 AND submission_notes.submission_id = submissions.id';
						switch($key){
							case 'sticker_serial_number':
							case 'sticker_number':
								$notes_query .= " AND submission_notes.sticker_serial_number like '%" . $this->db->escape_like_str($value) . "%'";
								break;
								case 'stickered':
								$notes_query .= ' AND submission_notes.stickered >= 1';
								break;
							case 'serial_number':
								$notes_query .= " AND submission_notes.serial_number like '%" . $this->db->escape_like_str($value) . "%'";
								break;
							case 'catalog_number':
								$notes_query .= " AND submission_notes.catalog_number like '%" . $this->db->escape_like_str($value) . "%'";
								break;
						}
					}
					break;
				default:
					$this->db->like('submissions.' . $key, $value);
					break;
			}
		}
		if(!empty($notes_query)){
			$this->db->where('EXISTS (' . $notes_query . ')', null, false);
		}
		return $this->db->count_all_results();
	}
	
	public function search($opts){
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		if(isset($opts['historical'])){
			$this->db->select('submissions.id, submissions.order_number, submissions.name, submissions.created_date, submissions.total, submission_status_change.status_id, submission_status_change.created_date AS updated_date');
			$this->db->join('submission_status_change', 'submission_status_change.submission_id = submissions.id');
			$this->db->where('submission_status_change.status_id >= 100');
			$this->db->order_by('submission_status_change.created_date', 'desc');
		}
		else {
			$this->db->where('submissions.status_id >= 100');
			$this->db->order_by('submissions.created_date', 'desc');
		}
		
		$notes_query = '';
		foreach($opts as $key=>$value){
			if(empty($value)) continue;
			switch($key){
				case 'page_size':
					$page_size = 0;
					if(is_numeric($value)){
						$page_size = intval($value);
					}
					if($page_size > 0){
						$page_number = 1;
						if(isset($opts['page_number']) && is_numeric($opts['page_number']) && intval($opts['page_number']) > 0){
							$page_number = intval($opts['page_number']);
						}
						$this->db->limit($page_size, ($page_number - 1) * $page_size);
					}
					break;
				case 'historical':
				case 'page_number': break;
				case 'start_date':
					$this->db->where('submissions.created_date >= ', date('Y-m-d H:i:s', $value));
					break;
				case 'end_date':
					$this->db->where('submissions.created_date <= ', date('Y-m-d H:i:s', $value + (24 * 3600 - 1)));
					break;	
				case 'status_id':
					$this->db->where('submissions.status_id', $value);
					break;
				case 'submission_status_change.start_date':
					$this->db->where('submission_status_change.created_date >= ', date('Y-m-d H:i:s', $value));
					break;
				case 'submission_status_change.end_date':
					$this->db->where('submission_status_change.created_date <= ', date('Y-m-d H:i:s', $value + (24 * 3600 - 1)));
					break;			
				case 'submission_status_change.status_id':
					$this->db->where('submission_status_change.status_id', $value);
					break;	
				case 'sticker_number':
				case 'sticker_serial_number':
				case 'stickered':
				case 'serial_number':
				case 'catalog_number':
					if(!empty($value)){
						if($notes_query == '')
							$notes_query = 'SELECT submission_notes.id FROM submission_notes WHERE submission_notes.status_id > 0 AND submission_notes.submission_id = submissions.id';
						switch($key){
							case 'sticker_serial_number':
							case 'sticker_number':
								$notes_query .= " AND submission_notes.sticker_serial_number like '%" . $this->db->escape_like_str($value) . "%'";
								break;
								case 'stickered':
								$notes_query .= ' AND submission_notes.stickered >= 1';
								break;
							case 'serial_number':
								$notes_query .= " AND submission_notes.serial_number like '%" . $this->db->escape_like_str($value) . "%'";
								break;
							case 'catalog_number':
								$notes_query .= " AND submission_notes.catalog_number like '%" . $this->db->escape_like_str($value) . "%'";
								break;
						}
					}
					break;
				default:
					$this->db->like('submissions.' . $key, $value);
					break;				
			}
		}
		if(!empty($notes_query)){
			$this->db->where('EXISTS (' . $notes_query . ')', null, false);
		}
		$query = $this->db->get();
		return $this->format_all($query->result_array());
	}
	
	public function update($submission, $data){
		$data['updated_date'] = date('Y-m-d H:i:s');
		$this->db->where('submissions.id', $submission['id']);
		$this->db->update(self::TABLE, $data);
		
		if(isset($data['status_id'])){
			if($data['status_id'] > $submission['status_id']){
				$data['id'] = $submission['id'];
				$this->submission_update_status($data);
			}
		}
	}
	
	public function update_status($submission, $user, $status_id){
		$data = array(
			'status_id' => $status_id,
			'updated_uid' => $user->id,
			'updated_date' => date('Y-m-d H:i:s')	
		);
		$this->db->where('submissions.id', $submission['id']);
		$this->db->update(self::TABLE, $data);
		
		if($data['status_id'] > $submission['status_id']){
			$data['id'] = $submission['id'];
			$this->submission_update_status($data);
		}
	}
	
	public function update_discount($submission, $user, $discount, $discount_amount){
		$data = array(
			'discount' => $discount,
			'discount_amount' => $discount_amount,
			'total' => $submission['sub_total'] + $submission['postage'] - $discount_amount,
			'updated_uid' => $user->id,
			'updated_date' => date('Y-m-d H:i:s')	
		);
		$this->db->where('submissions.id', $submission['id']);
		$this->db->update(self::TABLE, $data);
	}
	
	public function update_tracking_number($submission, $user, $tracking_number){
		$data = array(
			'tracking_number' => $tracking_number,
			'updated_uid' => $user->id,
			'updated_date' => date('Y-m-d H:i:s')	
		);
		$this->db->where('submissions.id', $submission['id']);
		$this->db->update(self::TABLE, $data);
	}
	
	private function submission_update_status($submission){
		if(isset($submission['status_id'])){
			$uid = false;
			if(isset($submission['created_uid']))
				$uid = $submission['created_uid'];
			elseif(isset($submission['updated_uid']))
				$uid = $submission['updated_uid'];
			if($uid == false) return;
			
			
			$this->load->library('uuid');
			$this->db->set('id', $this->uuid->v5('PMA.submission'));
			$this->db->set('submission_id', $submission['id']);
			$this->db->set('status_id', $submission['status_id']);
			$this->db->set('created_uid', $uid);
			$this->db->set('created_date',  date('Y-m-d H:i:s'));
			$this->db->insert('submission_status_change');
		}
		
	}
	
}
