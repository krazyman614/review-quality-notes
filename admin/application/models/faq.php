<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class FAQ extends CI_Model {
	const TABLE = "faqs";
	const FIELDS = "faqs.id, faqs.title, faqs.order_by, faqs.content, faqs.status_id, faqs.created_uid, faqs.created_date, faqs.updated_uid, faqs.updated_date";

	function __construct()
	{
		parent::__construct();
	}
	
	public function count($opts){
		$this->db->from(self::TABLE);
		foreach($opts as $key=>$value){
			switch($key){
				case 'order_by':
				case 'page_size':
				case 'page_number': break;
			}
		}
		return $this->db->count_all_results();
	}
	
	public function search($opts){
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		foreach($opts as $key=>$value){
			switch($key){
				case 'page_size':
						$page_size = 0;
						if(is_numeric($value)){
							$page_size = intval($value);
						}
						if($page_size > 0){
							$page_number = 1;
							if(isset($opts['page_number']) && is_numeric($opts['page_number']) && intval($opts['page_number']) > 0){
								$page_number = intval($opts['page_number']);
							}
							$this->db->limit($page_size, ($page_number - 1) * $page_size);	
						}
						break;
				case 'page_number': break;
				case 'order_by': $this->db->order_by('order_by',$value);
			}
		}
		$query = $this->db->get();
		return $query->result_array();
	}
	
	public function by_id($faq_id, $active_only=false){
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		$this->db->where('faqs.id', $faq_id);
		if ($active_only)
			$this->db->where('status_id >= 1');
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
		else {
			return false;
		}
	}
	
	public function create($faq){
		$this->load->library('uuid');
		$faq['id'] = $this->uuid->v5('PMA.faq');
 		$faq['created_date'] = date('Y-m-d H:i:s');
		$faq['updated_date'] = date('Y-m-d H:i:s');
		
		$this->db->insert(self::TABLE, $faq);
		return $this->by_id($faq['id']);
	}
	
	public function edit($faq){
		$faq['updated_date'] = date('Y-m-d H:i:s');
		
		$this->db->where('faqs.id', $faq['id']);
		$this->db->update(self::TABLE, $faq);
		return $this->by_id($faq['id']);
	}
}
