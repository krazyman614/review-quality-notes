<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User extends CI_Model {
	const TABLE = "users";
	const FIELDS = "users.id, users.name, users.email, users.password, users.company_name, users.address, users.address2, users.city, users.state_province, users.postal_code, users.country, users.phone_number, users.alt_phone_number, users.accepted_terms, users.status_id, users.created_uid, users.created_date, users.updated_uid, users.updated_date, users.pay_by_check";

	function __construct()
	{
		parent::__construct();
	}
	
	public function count($opts){
		$this->db->from(self::TABLE);
		foreach($opts as $key=>$value){
			switch($key){
				case 'page_size':
				case 'page_number': break;
			}
		}
		return $this->db->count_all_results();
	}
	
	public function search($opts){
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		$this->db->order_by('created_date', 'desc');
		foreach($opts as $key=>$value){
			switch($key){
				case 'page_size':
						$page_size = 0;
						if(is_numeric($value)){
							$page_size = intval($value);
						}
						if($page_size > 0){
							$page_number = 1;
							if(isset($opts['page_number']) && is_numeric($opts['page_number']) && intval($opts['page_number']) > 0){
								$page_number = intval($opts['page_number']);
							}
							$this->db->limit($page_size, ($page_number - 1) * $page_size);	
						}
						break;
				case 'page_number': break;
			}
		}
		$query = $this->db->get();
		return $query->result_array();
	}
	
	public function by_email($email, $not_id = null){
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		$this->db->where('users.email', $email);
		if(!empty($not_id))
			$this->db->where('users.id !=', $not_id);
		$this->db->where('status_id >= 1');
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
		else {
			return false;
		}		
	}
	
	public function by_id($user_id){
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		$this->db->where('users.id', $user_id);
		$this->db->where('status_id >= 1');
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
		else {
			return false;
		}
	}
	
	public function create($user){
		$this->load->library('encrypt');
		$this->load->library('uuid');
		$user['id'] = $this->uuid->v5('PMA.user');
		$user['password'] = $this->encrypt->sha1($user['password']);
		$user['status_id'] = 1;
		$user['created_date'] = date('Y-m-d H:i:s');
		$user['updated_date'] = date('Y-m-d H:i:s');
		$this->db->insert(self::TABLE, $user);
		return $this->by_id($user['id']);
	}
	
	public function edit($user){
		$this->load->library('encrypt');
		$user['updated_date'] = date('Y-m-d H:i:s');
		if(isset($user['password']))
			$user['password'] = $this->encrypt->sha1($user['password']);
		$this->db->where('users.id', $user['id']);
		$this->db->update(self::TABLE, $user);
		return $this->by_id($user['id']);
	}

    public function delete($id)
    {
        return $this->db->delete(self::TABLE, array('id' => $id));
    }
}
