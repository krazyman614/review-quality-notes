<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin extends CI_Model {
	const TABLE = "admins";
	const FIELDS = "admins.id, admins.name, admins.email, admins.password, admins.status_id, admins.created_uid, admins.created_date, admins.updated_uid, admins.updated_date, admins.admin_type_id";

	function __construct()
	{
		parent::__construct();
	}
	
	public function count($opts){
		$this->db->from(self::TABLE);
		foreach($opts as $key=>$value){
			switch($key){
				case 'page_size':
				case 'page_number': break;
			}
		}
		return $this->db->count_all_results();
	}
	
	public function search($opts){
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		foreach($opts as $key=>$value){
			switch($key){
				case 'page_size':
						$page_size = 0;
						if(is_numeric($value)){
							$page_size = intval($value);
						}
						if($page_size > 0){
							$page_number = 1;
							if(isset($opts['page_number']) && is_numeric($opts['page_number']) && intval($opts['page_number']) > 0){
								$page_number = intval($opts['page_number']);
							}
							$this->db->limit($page_size, ($page_number - 1) * $page_size);	
						}
						break;
				case 'page_number': break;
			}
		}
		$query = $this->db->get();
		return $query->result_array();
	}
	
	public function by_email($email, $not_id = null){
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		$this->db->where('admins.email', $email);
		if(!empty($not_id))
			$this->db->where('admins.id !=', $not_id);
		$this->db->where('status_id >= 1');
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
		else {
			return false;
		}		
	}
	
	public function by_email_password($email, $password){
		$this->load->library('encrypt');
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		$this->db->where('admins.email', $email);
		$this->db->where('admins.password', $this->encrypt->sha1($password));
		$this->db->where('status_id >= 1');
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
		else {
			return false;
		}
	}
	
	public function by_id($admin_id, $active_only=false){
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		$this->db->where('admins.id', $admin_id);
		if ($active_only)
			$this->db->where('status_id >= 1');
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
		else {
			return false;
		}
	}
	
	public function create($admin){
		$this->load->library('uuid');
		$this->load->library('encrypt');
		$admin['id'] = $this->uuid->v5('PMA.admin');
		$admin['password'] = $this->encrypt->sha1($admin['password']);
 		$admin['created_date'] = date('Y-m-d H:i:s');
		$admin['updated_date'] = date('Y-m-d H:i:s');
		
		$this->db->insert(self::TABLE, $admin);
		return $this->by_id($admin['id']);
	}
	
	public function edit($admin){
		$this->load->library('encrypt');
		if(isset($admin['password']))
			$admin['password'] = $this->encrypt->sha1($admin['password']);
		$admin['updated_date'] = date('Y-m-d H:i:s');
		
		$this->db->where('admins.id', $admin['id']);
		$this->db->update(self::TABLE, $admin);
		return $this->by_id($admin['id']);
	}
}
