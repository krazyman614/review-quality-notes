<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Postage_Country extends CI_Model {
	const TABLE = "postage_countries";
	const FIELDS = "postage_countries.id, postage_countries.name, postage_countries.full_name, postage_countries.status_id, postage_countries.created_uid, postage_countries.created_date, postage_countries.updated_uid, postage_countries.updated_date";

	function __construct()
	{
		parent::__construct();
	}
	
	public function count($opts){
		$this->db->from(self::TABLE);
		foreach($opts as $key=>$value){
			switch($key){
				case 'page_size':
				case 'page_number': break;
			}
		}
		$this->db->where('status_id >= 0');
		return $this->db->count_all_results();
	}
	
	public function search($opts){
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		foreach($opts as $key=>$value){
			switch($key){
				case 'page_size':
						$page_size = 0;
						if(is_numeric($value)){
							$page_size = intval($value);
						}
						if($page_size > 0){
							$page_number = 1;
							if(isset($opts['page_number']) && is_numeric($opts['page_number']) && intval($opts['page_number']) > 0){
								$page_number = intval($opts['page_number']);
							}
							$this->db->limit($page_size, ($page_number - 1) * $page_size);	
						}
						break;
				case 'page_number': break;
			}
		}
		$this->db->where('status_id >= 0');
		$this->db->order_by('name', 'asc');
		$query = $this->db->get();
		return $query->result_array();
	}
	
	public function by_id($postage_country_id, $active_only=false){
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		$this->db->where('postage_countries.id', $postage_country_id);
		if ($active_only)
			$this->db->where('status_id >= 1');
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return (array)$query->row();
		}
		else {
			return false;
		}
	}
	
	public function by_abbrev($postage_country_abbrev, $active_only=false) {
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		$this->db->where('postage_countries.name', $postage_country_abbrev);
		if ($active_only)
			$this->db->where('status_id >= 1');
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return (array)$query->row();
		}
		else {
			return false;
		}
	}
	
	public function by_full_name($postage_country_full, $active_only=false) {
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		$this->db->where('postage_countries.full_name', $postage_country_full);
		if ($active_only)
			$this->db->where('status_id >= 1');
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return (array)$query->row();
		}
		else {
			return false;
		}
	}
	
	public function create($postage_country){
		if ($this->by_full_name($postage_country['full_name']) || $this->by_abbrev($postage_country['name']))
			return false;
		$this->load->library('uuid');
		$postage_country['id'] = $this->uuid->v5('PMA.postage_country');
 		$postage_country['created_date'] = date('Y-m-d H:i:s');
		$postage_country['updated_date'] = date('Y-m-d H:i:s');
		
		$this->db->insert(self::TABLE, $postage_country);
		return $this->by_id($postage_country['id']);
	}
	
	public function edit($postage_country){
		$postage_country['updated_date'] = date('Y-m-d H:i:s');
		
		$this->db->where('postage_countries.id', $postage_country['id']);
		$this->db->update(self::TABLE, $postage_country);
		return $this->by_id($postage_country['id']);
	}
	
	public function delete($postage_country){
		$this->db->set('updated_date', date('Y-m-d H:i:s'));
		$this->db->set('status_id', -1);
		$this->db->where('postage_countries.id', $postage_country['id']);
		$this->db->update(self::TABLE);
	}
}
