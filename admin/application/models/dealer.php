<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Dealer extends CI_Model {
	const TABLE = "directory";	
	const FIELDS = "*";


	function __construct()
	{
		parent::__construct();
	}
	
	public function count($opts){
		$this->db->from(self::TABLE);
		foreach($opts as $key=>$value){
			switch($key){
				case 'page_size':
				case 'page_number': break;
			}
		}
		return $this->db->count_all_results();
	}
	
	public function search($opts){
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		$query = $this->db->get();
		return $query->result_array();
	}
	
	public function by_id($special_id, $active=true){
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		$this->db->where('specials.id', $special_id);
		if ($active)
			$this->db->where('status_id >= 1');
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return (array)$query->row();
		}
		else {
			return false;
		}
	}
	
	public function create($special){
		$this->load->library('uuid');
		$special['id'] = $this->uuid->v5('PMA.special');
 		$special['created_date'] = date('Y-m-d H:i:s');
		$special['updated_date'] = date('Y-m-d H:i:s');
		$special['expires_date'] = date('Y-m-d H:i:s', $special['expires_date']);
		
		$this->db->insert(self::TABLE, $special);
		return $this->by_id($special['id'],false);
	}
	
	public function edit($special){
		$special['updated_date'] = date('Y-m-d H:i:s');
		$special['expires_date'] = date('Y-m-d H:i:s', $special['expires_date']);
		
		$this->db->where('specials.id', $special['id']);
		$this->db->update(self::TABLE, $special);
		return $this->by_id($special['id'],false);
	}
	
	public function mark_inactive($id) {
		return $this->db->where('id',$id)->update('specials', array('status_id'=>0) );
	}
	
	public function delete($id) {
		return $this->db->delete('specials', array('id' => $id) );
	}
}
