<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class System_Settings extends CI_Model {
	const TABLE = "system";
	const FIELDS = "system.id, system.not_stickered_discount";

	function __construct()
	{
		parent::__construct();
	}
	
	public function update_not_stickered_discount($discount){
		
		$sys = $this->get();
		if($sys === false){
			$this->create();
			$sys = $this->get();
		}
		
		$this->db->set('not_stickered_discount', $discount);
		$this->db->update(self::TABLE);
	}
	
	public function get_not_stickered_discount(){
		$discount = 20;
		
		$sys = $this->get();
		if($sys !== false){
			$discount = $sys->not_stickered_discount;
		}
		
		return $discount;
	}
	
	private function get(){
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
		else {
			return false;
		}
	}
	
	private function create(){
		$this->load->library('uuid');
		$id = $this->uuid->v5('PMA.id');
		$this->db->set('id', $id);
		$this->db->insert(self::TABLE);
	}
}
