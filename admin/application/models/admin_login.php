<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin_Login extends CI_Model {
	const TABLE = "admin_logins";
	const FIELDS = "admin_logins.id, admin_logins.admin_id, admin_logins.status_id, admin_logins.created_date";
	var $id;
	var $admin_id;
	var $status_id;
	var $created_date;

	function __construct()
	{
		parent::__construct();
	}
	
	public function get_login($login_id){
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		$this->db->where('id', $login_id);
		$this->db->where('status_id >= 1');
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
		else {
			return false;
		}
	}
	
	public function create($admin){
		$this->load->library('uuid');
		$data = array(
			'id' => $this->uuid->v5('PMA.admin_login'),
			'admin_id' => $admin->id,
			'status_id' => 1,
			'created_date' => date('Y-m-d H:i:s')
		);
		$this->db->insert(self::TABLE, $data);
		return $this->get_login($data['id']);
	}
}
