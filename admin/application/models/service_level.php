<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Service_Level extends CI_Model {
	const TABLE = "service_levels";
	const FIELDS = "service_levels.id, service_levels.level, service_levels.title, service_levels.per_note, service_levels.time_to_process, service_levels.status_id, service_levels.created_uid, service_levels.created_date, service_levels.updated_uid, service_levels.updated_date";

	function __construct()
	{
		parent::__construct();
	}
	
	public function count($opts){
		$this->db->from(self::TABLE);
		foreach($opts as $key=>$value){
			switch($key){
				case 'page_size':
				case 'page_number': break;
			}
		}
		$this->db->where("status_id >= 0");
		return $this->db->count_all_results();
	}
	
	public function search($opts){
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		foreach($opts as $key=>$value){
			switch($key){
				case 'page_size':
						$page_size = 0;
						if(is_numeric($value)){
							$page_size = intval($value);
						}
						if($page_size > 0){
							$page_number = 1;
							if(isset($opts['page_number']) && is_numeric($opts['page_number']) && intval($opts['page_number']) > 0){
								$page_number = intval($opts['page_number']);
							}
							$this->db->limit($page_size, ($page_number - 1) * $page_size);	
						}
						break;
				case 'page_number': break;
			}
		}
		$this->db->where("status_id >= 0");
		$this->db->order_by('service_levels.level', 'asc');
		$query = $this->db->get();
		return $query->result_array();
	}
	
	public function by_id($service_level_id){
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		$this->db->where('service_levels.id', $service_level_id);
		$this->db->where('status_id >= 1');
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
		else {
			return false;
		}
	}
	
	public function create($service_level){
		$this->load->library('uuid');
		$service_level['id'] = $this->uuid->v5('PMA.service_level');
 		$service_level['created_date'] = date('Y-m-d H:i:s');
		$service_level['updated_date'] = date('Y-m-d H:i:s');
		
		$this->db->insert(self::TABLE, $service_level);
		return $this->by_id($service_level['id']);
	}
	
	public function edit($service_level){
		$service_level['updated_date'] = date('Y-m-d H:i:s');
		
		$this->db->where('service_levels.id', $service_level['id']);
		$this->db->update(self::TABLE, $service_level);
		return $this->by_id($service_level['id']);
	}
	
	public function delete($service_level){
		$this->db->set('updated_date', date('Y-m-d H:i:s'));
		$this->db->set('status_id', -1);
		$this->db->where('service_levels.id', $service_level->id);
		$this->db->update(self::TABLE);
	}
}
