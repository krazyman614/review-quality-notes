<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Postage_Rate extends CI_Model {
	const TABLE = "postage_rates";
	const FIELDS = "postage_rates.id, postage_rates.name, postage_rates.under_value, postage_rates.cost, postage_rates.postage_country_id, postage_rates.status_id, postage_rates.created_uid, postage_rates.created_date, postage_rates.updated_uid, postage_rates.updated_date";

	function __construct()
	{
		parent::__construct();
	}
	
	public function count($opts){
		$this->db->from(self::TABLE);
		foreach($opts as $key=>$value){
			switch($key){
				case 'postage_country_id':
					$this->db->where('postage_rates.postage_country_id', $value);
					break;
				case 'page_size':
				case 'page_number': break;
			}
		}
		return $this->db->count_all_results();
	}
	
	public function search($opts){
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		foreach($opts as $key=>$value){
			switch($key){
				case 'postage_country_id':
					$this->db->where('postage_rates.postage_country_id', $value);
					break;
				case 'page_size':
						$page_size = 0;
						if(is_numeric($value)){
							$page_size = intval($value);
						}
						if($page_size > 0){
							$page_number = 1;
							if(isset($opts['page_number']) && is_numeric($opts['page_number']) && intval($opts['page_number']) > 0){
								$page_number = intval($opts['page_number']);
							}
							$this->db->limit($page_size, ($page_number - 1) * $page_size);	
						}
						break;
				case 'page_number': break;
			}
		}
		$this->db->order_by('postage_rates.under_value', 'desc');
		$query = $this->db->get();
		return $query->result_array();
	}
	
	public function by_id($postage_rate_id, $active_only=false){
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		$this->db->where('postage_rates.id', $postage_rate_id);
		if ($active_only)
			$this->db->where('status_id >= 1');
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return (array)$query->row();
		}
		else {
			return false;
		}
	}
	
	public function create($postage_rate){
		$this->load->library('uuid');
		$postage_rate['id'] = $this->uuid->v5('PMA.postage_rate');
 		$postage_rate['created_date'] = date('Y-m-d H:i:s');
		$postage_rate['updated_date'] = date('Y-m-d H:i:s');
		
		$this->db->insert(self::TABLE, $postage_rate);
		return $this->by_id($postage_rate['id']);
	}
	
	public function edit($postage_rate){
		$postage_rate['updated_date'] = date('Y-m-d H:i:s');
		
		$this->db->where('postage_rates.id', $postage_rate['id']);
		$this->db->update(self::TABLE, $postage_rate);
		return $this->by_id($postage_rate['id']);
	}
	
	public function get_postage($country){
		$this->db->select('postage_rates.id, postage_rates.name, postage_rates.under_value, postage_rates.cost');
		$this->db->from('postage_rates');
		$this->db->join('postage_countries', 'postage_countries.id = postage_rates.postage_country_id AND postage_countries.status_id > 0');
		$this->db->where('postage_rates.status_id > 0');
		$this->db->where('postage_countries.name', $country);		
		$this->db->order_by('postage_rates.under_value', 'asc');
		$query = $this->db->get();
		return $query->result_array();
	}
	
	public function get_postage_amount($country, $total_declared_value){
		$this->db->select('postage_rates.cost');
		$this->db->from('postage_rates');
		$this->db->join('postage_countries', 'postage_countries.id = postage_rates.postage_country_id AND postage_countries.status_id > 0');
		$this->db->where('postage_rates.status_id > 0');
		$this->db->where('postage_countries.name', $country);
		$this->db->where('postage_rates.under_value >=', $total_declared_value);
		$this->db->order_by('postage_rates.under_value', 'asc');
		$this->db->limit(1);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row()->cost;
		}
		else {
			return false;
		}
	
	}
}
