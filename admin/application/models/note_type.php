<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Note_Type extends CI_Model {
	const TABLE = "note_types";
	const FIELDS = "note_types.id, note_types.type, note_types.region_id, note_types.status_id, note_types.created_uid, note_types.created_date, note_types.updated_uid, note_types.updated_date";

	function __construct()
	{
		parent::__construct();
	}
	
	private function format_all($notes){
		$n_notes = array();
		foreach($notes as $note){
			array_push($n_notes, $this->format($note));
		}
		return $n_notes;
	}
	
	
	private function format($note){
		if(isset($note['region_id'])){
			$this->load->model('region');
			$note['region'] = $this->region->by_id($note['region_id']);
			unset($note['region_id']);
		}
		return $note;
	}
	
	public function count($opts){
		$this->db->from(self::TABLE);
		foreach($opts as $key=>$value){
			switch($key){
				case 'page_size':
				case 'page_number': break;
				case 'region_id':
					if(!empty($value))
						$this->db->where('note_types.region_id', $value);
					break;
			}
		}
		$this->db->where('status_id >= 0');
		return $this->db->count_all_results();
	}
	
	public function search($opts){
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		foreach($opts as $key=>$value){
			switch($key){
				case 'page_size':
						$page_size = 0;
						if(is_numeric($value)){
							$page_size = intval($value);
						}
						if($page_size > 0){
							$page_number = 1;
							if(isset($opts['page_number']) && is_numeric($opts['page_number']) && intval($opts['page_number']) > 0){
								$page_number = intval($opts['page_number']);
							}
							$this->db->limit($page_size, ($page_number - 1) * $page_size);	
						}
						break;
				case 'page_number': break;
				case 'region_id':
					if(!empty($value))
						$this->db->where('note_types.region_id', $value);
					break;				
			}
		}
		$this->db->where('note_types.status_id >= 0');
		$this->db->order_by('note_types.type', 'asc');
		$query = $this->db->get();
		return $this->format_all($query->result_array());
	}
	
	public function by_id($note_type_id, $active=true){
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		$this->db->where('note_types.id', $note_type_id);
		if ($active)
			$this->db->where('status_id >= 1'); //we should probably change this to be >=0, and allow for -1 to mean deleted
		else 
			$this->db->where('status_id >= 0');
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
		else {
			return false;
		}
	}
	
	public function create($note_type){
		$this->load->library('uuid');
		$note_type['id'] = $this->uuid->v5('PMA.note_type');
 		$note_type['created_date'] = date('Y-m-d H:i:s');
		$note_type['updated_date'] = date('Y-m-d H:i:s');
		
		$this->db->insert(self::TABLE, $note_type);
		return $this->by_id($note_type['id'], false);
	}
	
	public function edit($note_type){
		$note_type['updated_date'] = date('Y-m-d H:i:s');
		
		$this->db->where('note_types.id', $note_type['id']);
		$this->db->update(self::TABLE, $note_type);
		return $this->by_id($note_type['id'], false);
	}
	
	public function delete($note_type){
		$this->db->set('updated_date', date('Y-m-d H:i:s'));
		$this->db->set('status_id', -1);
		$this->db->where('note_types.id', $note_type->id);
		$this->db->update(self::TABLE);
	}
}
