<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Grading_Service extends CI_Model {
	const TABLE = "grading_services";
	const FIELDS = "grading_services.id, grading_services.abbrev, grading_services.name, grading_services.status_id, grading_services.created_uid, grading_services.created_date, grading_services.updated_uid, grading_services.updated_date";

	function __construct()
	{
		parent::__construct();
	}
	
	public function count($opts){
		$this->db->from(self::TABLE);
		foreach($opts as $key=>$value){
			switch($key){
				case 'page_size':
				case 'page_number': break;
			}
		}
		$this->db->where('status_id >= 0');
		return $this->db->count_all_results();
	}
	
	public function search($opts){
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		foreach($opts as $key=>$value){
			switch($key){
				case 'page_size':
						$page_size = 0;
						if(is_numeric($value)){
							$page_size = intval($value);
						}
						if($page_size > 0){
							$page_number = 1;
							if(isset($opts['page_number']) && is_numeric($opts['page_number']) && intval($opts['page_number']) > 0){
								$page_number = intval($opts['page_number']);
							}
							$this->db->limit($page_size, ($page_number - 1) * $page_size);	
						}
						break;
				case 'page_number': break;
			}
		}
		$this->db->where('status_id >= 0');
		$query = $this->db->get();
		return $query->result_array();
	}
	
	public function by_id($grading_service_id){
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		$this->db->where('grading_services.id', $grading_service_id);
		$this->db->where('status_id >= 1');
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
		else {
			return false;
		}
	}
	
	public function create($grading_service){
		$this->load->library('uuid');
		$grading_service['id'] = $this->uuid->v5('PMA.grading_service');
 		$grading_service['created_date'] = date('Y-m-d H:i:s');
		$grading_service['updated_date'] = date('Y-m-d H:i:s');
		
		$this->db->insert(self::TABLE, $grading_service);
		return $this->by_id($grading_service['id']);
	}
	
	public function edit($grading_service){
		$grading_service['updated_date'] = date('Y-m-d H:i:s');
		
		$this->db->where('grading_services.id', $grading_service['id']);
		$this->db->update(self::TABLE, $grading_service);
		return $this->by_id($grading_service['id']);
	}
	
	public function delete($grading_service){
		$this->db->set('updated_date', date('Y-m-d H:i:s'));
		$this->db->set('status_id', -1);
		$this->db->where('grading_services.id', $grading_service->id);
		$this->db->update(self::TABLE);
	}
}
