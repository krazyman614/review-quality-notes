<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Submission_Comments extends CI_Model {
	const TABLE = "submission_comments";
		
	function __construct()
	{
		parent::__construct();
	}
	
	public function create($comment){
		$this->load->library('uuid');
		$comment['id'] = $this->uuid->v5('PMA.submission_comments');
		$comment['created_date'] = date('Y-m-d H:i:s');
		
		$this->db->insert(self::TABLE, $comment);
	}
	
}
