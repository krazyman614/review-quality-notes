<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Submission_note extends CI_Model {
	const TABLE = "submission_notes";
	const FIELDS = "submission_notes.id, submission_notes.submission_id, submission_notes.sequence, submission_notes.service_level_id, submission_notes.note_type_id, submission_notes.note_country, submission_notes.denomination, submission_notes.catalog_number, submission_notes.pp, submission_notes.serial_number, submission_notes.grading_service_id, submission_notes.barcode, submission_notes.declared_value, submission_notes.status_id, submission_notes.created_uid, submission_notes.created_date, submission_notes.updated_uid, submission_notes.updated_date, submission_notes.service_cost, submission_notes.stickered, submission_notes.sticker_serial_number, submission_notes.grade";
		
	function __construct()
	{
		parent::__construct();
	}
	
	private function format_all($notes){
		$formatted = array();
		foreach($notes as $note){
			array_push($formatted, $this->format($note));
		}		
		return $formatted;
	}
	
	private function format($note){
		switch($note['status_id']){
			case 100:
				$note['status'] = 'New Submission';
				break;
			case 200:
				$note['status'] = 'Not Recieved';
				break;
			case 300:
				$note['status'] = 'Recieved';
				break;
			case 600:
				$note['status'] = 'Customer Removed';
				break;
			case 1000:
				$note['status'] = 'In Process';
				break;
			case 1100:
				$note['status'] = 'Reviewing Complete';
				break;
			case 2000:
				$note['status'] = 'Mailed';
				break;
			default:
				$note['status'] = 'Unknown [' . $note['status_id'] . ']';
				break;
		}
		if(isset($note['status_change_id'])){
			switch($note['status_change_id']){
				case 100:
					$note['status_change'] = 'New Submission';
					break;
				case 200:
					$note['status_change'] = 'Incomplete';
					break;
				case 300:
					$note['status_change'] = 'Received';
					break;
				case 400:
					$note['status_change'] = 'Customer Approved';
					break;
				case 500:
					$note['status_change'] = 'Customer Canceled';
					break;
				case 1000:
					$note['status_change'] = 'In Progress';
					break;
				case 1100:
					$note['status_change'] = 'Reviewing Complete';
					break;
				case 2000:
					$note['status_change'] = 'Mailed';
					break;
				default:
					$note['status_change'] = 'Unknown [' . $note['status_id'] . ']';
					break;
			}
		}
		
		if(isset($note['discount']) && isset($note['service_cost']) && $note['stickered'] == 0){
			$note['note_discount'] = (doubleval($note['discount']) /100) * doubleval($note['service_cost']);
		}
		
		$this->load->model('service_level');
		$note['service_level'] = (array)$this->service_level->by_id($note['service_level_id']);
		if(!empty($note['note_type_id'])){
			$this->load->model('note_type');
			$note['note_type'] = (array)$this->note_type->by_id($note['note_type_id']);
		}
		$this->load->model('grading_service');
		$note['grading_service'] = (array)$this->grading_service->by_id($note['grading_service_id']);
		$this->load->model('submission_note_grade');
		$note['grades'] = $this->submission_note_grade->by_note($note);
		
		$this->load->model('submission');
		$note['submission'] = $this->submission->by_id($note['submission_id']);
		
		return $note;
	}
	
	public function by_submission($submission, $format = true){
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		$this->db->where('submission_notes.submission_id', $submission['id']);
		$this->db->where('status_id >= 1');
		$this->db->order_by('sequence', 'asc');
		$query = $this->db->get();
		
		if ($format) {
			return $this->format_all($query->result_array());
		} else {
			return $query->result_array();
		}
	}
	

	public function by_id($submission_note_id){
		$this->db->select(self::FIELDS);
		$this->db->from(self::TABLE);
		$this->db->where('submission_notes.id', $submission_note_id);
		$this->db->where('status_id >= 1');
		$this->db->order_by('sequence', 'asc');
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return (array)$query->row();
		}
		else {
			return false;
		}
	}
	
	public function find_barcode($barcode)
	{
		return $this->db
				->select(self::FIELDS)
				->from(self::TABLE)
				->where('submission_notes.barcode', $barcode)
				->order_by('sequence', 'asc')
				->get()
				->result();
	}
	
	public function update_status($note, $user, $status_id){
		$data = array(
			'status_id' => $status_id,
			'updated_uid' => $user->id,
			'updated_date' => date('Y-m-d H:i:s')	
		);
		$this->db->where('submission_notes.id', $note['id']);
		$this->db->update(self::TABLE, $data);
	}
	
	public function update_stickered($note, $user, $sticker_serial = ''){
		$data = array(
			'stickered' => 1,
			'sticker_serial_number' => $sticker_serial,
			'updated_uid' => $user->id,
			'updated_date' => date('Y-m-d H:i:s')	
		);
		$this->db->where('submission_notes.id', $note['id']);
		$this->db->update(self::TABLE, $data);
	}
	
	public function update($note){
		$this->load->library('uuid');
		if(!empty($note['id'])){
			$sql = "INSERT INTO submission_note_history (id, note_id, submission_id, sequence, service_level_id, note_type_id, denomination, catalog_number" .
					", pp, serial_number, grading_service_id, barcode, declared_value, status_id, created_uid, created_date, updated_uid, updated_date" .
					", service_cost, stickered)" .
					" SELECT '" . $this->uuid->v5('PMA.submission_note_history') . "', id, submission_id, sequence, service_level_id, note_type_id, denomination, catalog_number" .
					", pp, serial_number, grading_service_id, barcode, declared_value, status_id, created_uid, created_date, '" . $note['created_uid'] . "', '" . date('Y-m-d H:i:s') . "'" .
					", service_cost, stickered FROM submission_notes WHERE id = '" . $note['id'] . "'";
			$this->db->query($sql);
			$this->db->delete(self::TABLE, array('id' => $note['id']));
		}
		else {
			$note['id'] = $this->uuid->v5('PMA.submission_notes');
		}

        // force serial numbers to always be upper-case
        if (isset($note['serial_number'])) {
            $note['serial_number'] = strtoupper($note['serial_number']);
        }
		
		$note['created_date'] = date('Y-m-d H:i:s');
		$note['updated_date'] = date('Y-m-d H:i:s');
		$this->db->insert(self::TABLE, $note);			
	}
	
	public function count($opts){
		$this->db->from(self::TABLE);
		if(isset($opts['include_submissions'])){
			$this->db->join('submissions', 'submission_notes.submission_id = submissions.id');
			$this->db->where('submissions.status_id >= 100');
			if(isset($opts['include_submission_status_change'])){
				$this->db->join('submission_status_change', 'submission_status_change.submission_id = submissions.id');
				$this->db->where('submission_status_change.status_id >= 100');
			}
			$this->db->where('submission_notes.status_id >= 100');
		}
		else {
			$this->db->where('submission_notes.status_id >= 100');
		}
			
		$notes_query = '';
		foreach($opts as $key=>$value){
			if(empty($value)) continue;
			switch($key){
				case 'include_submissions':
				case 'include_submission_status_change':
				case 'page_size':
				case 'page_number': break;
				case 'start_date':
					$this->db->where('submission_notes.created_date >= ', date('Y-m-d H:i:s', $value));
					break;
				case 'end_date':
					$this->db->where('submission_notes.created_date <= ', date('Y-m-d H:i:s', $value + (24 * 3600 - 1)));
					break;
				case 'status_id':
					$this->db->where('submission_notes.status_id', $value);
					break;
				case 'submission_status_change.start_date':
					$this->db->where('submission_status_change.created_date >= ', date('Y-m-d H:i:s', $value));
					break;
				case 'submission_status_change.end_date':
					$this->db->where('submission_status_change.created_date <= ', date('Y-m-d H:i:s', $value + (24 * 3600 - 1)));
					break;
				case 'submission_status_change.status_id':
					$this->db->where('submission_status_change.status_id', $value);
					break;
				case 'stickered':
					if($value){
						$this->db->where('submission_notes.stickered > 0');
					}
					break;
				case 'submissions.name':
				case 'submissions.order_number':
					$this->db->like($key, $value);
					break;
				default:
					$this->db->like('submission_notes.' . $key, $value);
					break;
			}
		}
		if(!empty($notes_query)){
			$this->db->where('EXISTS (' . $notes_query . ')', null, false);
		}
		return $this->db->count_all_results();
	}
	
	public function search($opts){
		$this->db->from(self::TABLE);
		$fields = self::FIELDS;
		if(isset($opts['include_submissions'])){
			$fields .= ', submissions.postage, submissions.order_number, submissions.payment_type_id, submissions.discount, submissions.name, submissions.discount_amount, submissions.discount';
			$this->db->join('submissions', 'submission_notes.submission_id = submissions.id');
			$this->db->where('submissions.status_id >= 100');
			
			if(isset($opts['include_submission_status_change'])){
				$fields .= ', submission_status_change.status_id as status_change_id, submission_status_change.created_date as status_change_date';		
			
				$this->db->join('submission_status_change', 'submission_status_change.submission_id = submissions.id');
				$this->db->where('submission_status_change.status_id >= 100');
				$this->db->order_by('submission_status_change.created_date', 'desc');
			}
			else {
				$fields .= ', submission_notes.status_id as status_change_id, submissions.created_date as status_change_date';
				$this->db->order_by('submissions.created_date', 'desc');
			}
			$this->db->where('submission_notes.status_id >= 100');
		}
		else {
			$this->db->where('submission_notes.status_id >= 100');
		}
		$this->db->select($fields);
	
		$notes_query = '';
		foreach($opts as $key=>$value){
			if(empty($value)) continue;
			switch($key){
				case 'page_size':
					$page_size = 0;
					if(is_numeric($value)){
						$page_size = intval($value);
					}
					if($page_size > 0){
						$page_number = 1;
						if(isset($opts['page_number']) && is_numeric($opts['page_number']) && intval($opts['page_number']) > 0){
							$page_number = intval($opts['page_number']);
						}
						$this->db->limit($page_size, ($page_number - 1) * $page_size);
					}
					break;
				case 'include_submissions':
				case 'include_submission_status_change':
				case 'page_size':
				case 'page_number': break;
				case 'start_date':
					$this->db->where('submission_notes.created_date >= ', date('Y-m-d H:i:s', $value));
					break;
				case 'end_date':
					$this->db->where('submission_notes.created_date <= ', date('Y-m-d H:i:s', $value + (24 * 3600 - 1)));
					break;
				case 'status_id':
					$this->db->where('submission_notes.status_id', $value);
					break;
				case 'submission_status_change.start_date':
					$this->db->where('submission_status_change.created_date >= ', date('Y-m-d H:i:s', $value));
					break;
				case 'submission_status_change.end_date':
					$this->db->where('submission_status_change.created_date <= ', date('Y-m-d H:i:s', $value + (24 * 3600 - 1)));
					break;
				case 'submission_status_change.status_id':
					$this->db->where('submission_status_change.status_id', $value);
					break;
				case 'stickered':
					if($value){
						$this->db->where('submission_notes.stickered > 0');
					}
					break;
				case 'submissions.name':
				case 'submissions.order_number':
					$this->db->like($key, $value);
					break;
				default:
					$this->db->like('submission_notes.' . $key, $value);
					break;
			}
		}
		if(!empty($notes_query)){
			$this->db->where('EXISTS (' . $notes_query . ')', null, false);
		}
		$query = $this->db->get();
		return $this->format_all($query->result_array());
	}
	
	public function edit($note){
		$this->db->where(self::TABLE . '.id', $note['id']);
		$this->db->update(self::TABLE, $note);
		return $this->by_id($note['id']);
	}
	
}
