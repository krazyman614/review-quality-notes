<script>
	$(function(){
		$('#create-form').submit(function(){
			$('#display-error').hide();
			$('#errors-list').empty();
			$('.error').removeClass('error');
			$(this).find('input').each(function(){
				var inpt = $(this);
				var name = inpt.attr('name');
				if($.trim(inpt.val()) == ''){
					inpt.parent().addClass('error');
					$('#errors-list').append('<li>Missing ' + name.replace('_', ' ') + '</li>');
				}			
			});
			if($(this).find('.error').length == 0)
				return true;
			else{
				$('#display-error').show();
				return false;
			}
		});


		$('.date').datepicker({
			format: 'mm/dd/yyyy'
		});
	});
</script>
<form method="post" id="create-form" action="/news_items/edit_news_item">
	<input type="hidden" name="id" value="<?php if(isset($data['id'])){ echo $data['id'];}?>"/>
	<fieldset>
		<legend>Edit News Item</legend>
		<?php if(isset($success) && $success){?>
			<div class="alert alert-success">
				Your admin has been updated
			</div>
		<?php }?>
		<div id="display-error" class="alert alert-error"<?php if(!isset($errors)){?> style="display:none;"<?php }?>>
	    	Please correct the following errors:
	    	<ul id="errors-list">
	    	<?php foreach($errors as $key=>$value){?>
	    		<li><?php echo $value;?></li>
	    	<?php }?>
	    	</ul>
	    </div>
		<div class="control-group<?php if(isset($errors['title'])){ ?> error<?php }?>">
			<label>Title</label>
			<input type="text" name="title" placeholder="Title" value="<?php if(isset($data['title'])){ echo $data['title'];}?>" class="span5" maxlength="1000"/>
		</div>
		<div class="control-group<?php if(isset($errors['date'])){ ?> error<?php }?>">
			<label>Date</label>
			<div class="input-append date" data-date="<?php echo date('m/d/Y', isset($data['date']) ? $data['date'] : time());?>" data-date-format="dd/mm/yyyy">
			  	<input class="span2" size="16" type="text" value="<?php echo date('m/d/Y', isset($data['date']) ? $data['date'] : time());?>" name="date"/>
  				<span class="add-on"><i class="icon-th"></i></span>
			</div>
		</div>
		<div class="control-group<?php if(isset($errors['news'])){ ?> error<?php }?>">
			<label>News</label>
			<textarea name="news" rows="5" class="span5"><?php if(isset($data['news'])){ echo $data['news'];}?></textarea>
		</div>
		<div class="control-group<?php if(isset($errors['status_id'])){ ?> error<?php }?>">
			<label>Status</label>
			<select name="status_id">
				<option value="1" <?php if(isset($data['status_id']) && $data['status_id'] == '1'){?>selected="selected"<?php }?>>Active</option>
				<option value="0" <?php if(isset($data['status_id']) && $data['status_id'] == '0'){?>selected="selected"<?php }?>>Inactive</option>
			</select>
		</div>
		<button type="submit" class="btn">Edit News Item</button><a href="/news_items/" class="btn">Back</a>
	</fieldset>
</form>