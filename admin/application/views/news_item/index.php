<script type="text/javascript">
	function change_page(page_number){
		self.location.href = '/news_items/page/<? echo $opts['page_size'] . '/'?>' + page_number;
	}
</script>
<fieldset>
	<legend>News Items</legend>
	<div id="news_items">
		<table class="table table-hover">
		<tr>
			<th>Date</th>
			<th>Title</th>
			<th>Status</th>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
		<tr>
		<?php 
		if(isset($news_items)){
			foreach($news_items as $news_item){
				?>
				<tr>
					<td><?php echo date("F j, Y", $news_item['date']);?></td>
					<td><?php echo $news_item['title'];?></td>
					<td><?php echo ($news_item['status_id'] > 0 ? 'Active' : 'Inactive');?></td>
					<td><a href="/news_items/edit/<?php echo $news_item['id'];?>" class="btn btn-mini">Edit</a></td>
					<td><a href="/news_items/delete/<?php echo $news_item['id'];?>" class="btn btn-mini">Delete</a></td>
				</tr>			
			<?php } ?>
			<?php if($count > $opts['page_size']){?>
			<tr>
				<td colspan="5">
					<div class="pagination pagination-right">
					  <ul>
					    <li <?php if($opts['page_number'] == 1){?>class='disabled'<?php }?>><a href="javascript:change_page(1);">Prev</a></li>
					    <?php 
					    $end_page = ceil($count / $opts['page_size']);
					    for($i = 1; $i <= $end_page; $i++){?>
					    <li <?php if($opts['page_number'] == $i){?>class='active'<?php }?>><a href="javascript:change_page(<?php echo $i;?>);"><?php echo $i;?></a></li>
					    <?php }?>
					    <li <?php if($opts['page_number'] == $end_page){?>class='disabled'<?php }?>><a href="javascript:change_page(<?php echo $end_page;?>);">Next</a></li>
					  </ul>
					</div>
				</td>
			</tr>
			<?php }?>
		</table>
		<?php }?>
		<div>
			<a href="/news_items/add" class="btn pull-right">New News Item</a>
		</div>
		
	</div>
</fieldset>