<div id="grade-modal" class="modal-grades">
  <div class="modal-header">
    <h3 id="myModalLabel">Edit Note Review</h3>
  </div>
  	<?php if (validation_errors()):?>
	<div id="display-error" class="alert alert-error">
    	Please correct the following errors:
    	<ul id="errors-list">
    		<?=validation_errors('<li>', '</li>')?>
    	</ul>
    </div>
    <?php endif;?>
  <form id="frm-grade" method="post" action="/reviews/edit" style="margin:0px;">
  	<input type="hidden" name="id" value="<?=$review->id?>"/>
	  <div class="modal-body">
			<fieldset>
		    	<legend>Score</legend>
		    	<div id="scores" style="width:800px;"></div>
		  		<table class="table table-bordered">
		  		<tr>
		  			<th>&nbsp;</th>
		  			<th>Paper</th>
		  			<th>Color</th>
		  			<th>Printing</th>
		  			<th>Face Margins</th>
		  			<th>Face Centering</th>
		  			<th>Back Margins</th>
		  			<th>Back Centering</th>
		  			<th>Registration</th>
		  		</tr>
		  		<tr>
		  			<td><strong>Other</strong></td>
		  			<td><input type="radio" name="paper" id="paper" value="0" <?=set_radio('paper', '0', $review->paper == 0)?>/></td>
		  			<td><input type="radio" name="color" id="color" value="0" <?=set_radio('color', '0', $review->color == 0)?>/></td>
		  			<td><input type="radio" name="printing" id="printing" value="0" <?=set_radio('printing', '0', $review->printing == 0)?>/></td>
		  			<td><input type="radio" name="face_margins" id="face-margins" value="0" <?=set_radio('face_margins', '0', $review->face_margins == 0)?>/></td>
		  			<td><input type="radio" name="face_centering" id="face-centering" value="0" <?=set_radio('face_centering', '0', $review->face_centering == 0)?>/></td>
		  			<td><input type="radio" name="back_margins" id="back-margins" value="0" <?=set_radio('back_margins', '0', $review->back_margins == 0)?>/></td>
		  			<td><input type="radio" name="back_centering" id="back-centering" value="0" <?=set_radio('back_centering', '0', $review->back_centering == 0)?>/></td>
		  			<td><input type="radio" name="registration" id="registration" value="0" <?=set_radio('registration', '0', $review->registration == 0)?>/></td>
		  		</tr>
		  		<tr>
			  		<td><strong>Satisfactory</strong></td>
		  			<td><input type="radio" name="paper" id="paper" value="50" <?=set_radio('paper', '50', $review->paper == 50)?>/></td>
		  			<td><input type="radio" name="color" id="color" value="50" <?=set_radio('color', '50', $review->color == 50)?>/></td>
					<td><input type="radio" name="printing" id="printing" value="50" <?=set_radio('printing', '50', $review->printing == 50)?>/></td>
		  			<td><input type="radio" name="face_margins" id="face-margins" value="50" <?=set_radio('face_margins', '50', $review->face_margins == 50)?>/></td>
		  			<td><input type="radio" name="face_centering" id="face-centering" value="50" <?=set_radio('face_centering', '50', $review->face_centering == 50)?>/></td>
		  			<td><input type="radio" name="back_margins" id="back-margins" value="50" <?=set_radio('back_margins', '50', $review->back_margins == 50)?>/></td>
		  			<td><input type="radio" name="back_centering" id="back-centering" value="50" <?=set_radio('back_centering', '50', $review->back_centering == 50)?>/></td>
		  			<td><input type="radio" name="registration" id="registration" value="50" <?=set_radio('registration', '50', $review->registration == 50)?>/></td>
		  		</tr>
		  		<tr>
			  		<td><strong>Quality</strong>
		  			<td><input type="radio" name="paper" id="paper" value="100" <?=set_radio('paper', '100', $review->paper == 100)?>/></td>
		  			<td><input type="radio" name="color" id="color" value="100" <?=set_radio('color', '100', $review->color == 100)?>/></td>
		  			<td><input type="radio" name="printing" id="printing" value="100" <?=set_radio('printing', '100', $review->printing == 100)?>/></td>
		  			<td><input type="radio" name="face_margins" id="face-margins" value="100" <?=set_radio('face_margins', '100', $review->face_margins == 100)?>/></td>
		  			<td><input type="radio" name="face_centering" id="face-centering" value="100" <?=set_radio('face_centering', '100', $review->face_centering == 100)?>/></td>
		  			<td><input type="radio" name="back_margins" id="back-margins" value="100" <?=set_radio('back_margins', '100', $review->back_margins == 100)?>/></td>
		  			<td><input type="radio" name="back_centering" id="back-centering" value="100" <?=set_radio('back_centering', '100', $review->back_centering == 100)?>/></td>
		  			<td><input type="radio" name="registration" id="registration" value="100" <?=set_radio('registration', '100', $review->registration == 100)?>/></td>
		  		</tr>
			  	</table>
			</fieldset>
			<fieldset>
		    	<legend>Comments</legend>
		    	<textarea name="comments" rows="5" style="width:98%;"><?=set_value('comments', $review->comments)?></textarea>
		    </fieldset>
		</div>
	  <div class="modal-footer">
	    <input type="submit" class="btn btn-primary" value="Save Review" />
	  </div>
</form>
</div>