<form method="post" action="/notes/edit">
	<input type="hidden" name="id" value="<?=$note['id']?>"/>
	<fieldset>
		<legend>Edit Note</legend>
	  	<?php if (validation_errors()):?>
		<div id="display-error" class="alert alert-error">
	    	Please correct the following errors:
	    	<ul id="errors-list">
	    		<?=validation_errors('<li>', '</li>')?>
	    	</ul>
	    </div>
	    <?php endif;?>
		<div class="control-group">
			<label>Sticker Number</label>
			<input type="text" name="sticker_serial_number" value="<?=set_value('sticker_serial_number', $note['sticker_serial_number'])?>"/>
		</div>
		<div class="control-group">
			<label><?php echo get_column_name('catalog_number', $submission['region_id']);?></label>
			<input type="text" name="catalog_number" value="<?=set_value('catalog_number', $note['catalog_number'])?>"/>
		</div>
		<div class="control-group">
			<label><?php echo get_column_name('pp', $submission['region_id']);?></label>
			<input type="text" name="pp" value="<?=set_value('pp', $note['pp'])?>"/>
		</div>
		<div class="control-group">
			<label>Serial Number</label>
			<input type="text" name="serial_number" value="<?=set_value('serial_number', $note['serial_number'])?>"/>
		</div>
		<?php /*
		<div class="control-group">
			<label>Grading Service</label>
			<input type="text" value="<?=$note['grading_service']['name']?>" disabled/>
		</div>
		*/?>
		<div class="control-group">
			<label>Bar Code</label>
			<input type="text" name="barcode" value="<?=set_value('barcode', $note['barcode'])?>"/>
		</div>
		<?php if ($submission['region_id'] !== 'us') {?>
		<div class="control-group">
			<label>Grade</label>
			<input type="text" name="grade" value="<?=set_value('grade', $note['grade'])?>"/>
		</div>
		<?php }?>
		<button type="submit" class="btn">Edit Note</button>
	</fieldset>
</form>