<?php if(isset($submissions) && count($submissions) >= 0){?>
	<table>
	<tr>
		<th># Stickered</th>
		<th># Notes</th>
		<th>Order #</th>
		<th>Service Level</th>
		<th>User</th>
		<th>Date</th>
		<th>Order Total</th>
		<th>Status</th>
		<th>Changed Date</th>
	</tr>
	<?php foreach($submissions as $submission){
		$service_level = (count($submission['notes']) > 0 ? $submission['notes'][0]['service_level'] : null);
		$stickered = 0;
		foreach($submission['notes'] as $note){
			if($note['stickered']) $stickered++;
		}
	?>
	<tr>
		<td><?php echo $stickered ?></td>
		<td><?php echo count($submission['notes'])?></td>
		<td><?php echo $submission['order_number'];?></td>
		<td><?php echo $service_level != null ? $service_level['title'] : '---';?></td>
		<td><?php echo $submission['name']?></td>
		<td><?php echo date('D, d M Y g:i A', strtotime($submission['created_date']));?></td>
		<td>$<?php echo money_format('%i', $submission['total']);?></td>
		<td><?php echo $submission['status'];?></td>
		<td><?php echo date('D, d M Y g:i A', strtotime($submission['updated_date']));?></td>
	</tr>
	<?php }?>
	</table>
<?php }?>
