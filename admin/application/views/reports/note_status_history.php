<script type="text/javascript">
	$(function(){
		$('.date').datepicker({
			format: 'mm/dd/yyyy'
		});

		$('.download').click(function(){
			$('#search-form').attr('action', '/reports/note_status_history_download');
			$('#search-form').submit();		
		});
		
		$('.search').click(function(){
			$('#search-form').attr('action', '/reports/note_status_history');
		});

	});

	function change_page(page_number){
		$('#search-form').attr('action', '/reports/note_status_history/<? echo $opts['page_size'] . '/'?>' + page_number);
		$('#search-form').submit();
	}
</script>
<fieldset>
	<legend>Notes Report</legend>
	<div>
		<form method="post" id="search-form" action="/reports/note_status_history">
		 	<div class="row-fluid">
				<div class="span2">
					<label>Order Number</label>
			 		<input type="text" name="order_number" class="input-small span12" value="<?php echo $opts['submissions.order_number'];?>" placeholder="Order Number">
			 	</div>
			 	<div class="span2">
					<label>Catalog Number</label>
				 	<input type="text" name="catalog_number" class="input-small span12" value="<?php echo $opts['catalog_number'];?>" placeholder="Catalog Number">
			 	</div>
			 	<div class="span2">
					<label>Serial Number</label>
				 	<input type="text" name="serial_number" class="input-small span12" value="<?php echo $opts['serial_number'];?>" placeholder="Serial Number">
			 	</div>
			 	<div class="span2">
					<label>Customer Name</label>
				 	<input type="text" name="name" class="input-small span12" value="<?php echo $opts['submissions.name'];?>" placeholder="Customer Name">
			 	</div>
			</div>
			<div class="row-fluid">
			 	<div class="span2">
					<label>Order Date After</label>
				 	<div class="input-append date" data-date="<?php echo date('m/d/Y', $opts['submission_status_change.start_date']);?>" data-date-format="dd/mm/yyyy">
					  	<input class="span12" size="16" type="text" value="<?php echo date('m/d/Y', $opts['submission_status_change.start_date']);?>" name="start_date"/>
		  				<span class="add-on"><i class="icon-th"></i></span>
					</div>
			 	</div>
			 	<div class="span2">
					<label>Order Date Before</label>
				 	<div class="input-append date" data-date="<?php echo date('m/d/Y', $opts['submission_status_change.end_date']);?>" data-date-format="dd/mm/yyyy">
					  	<input class="span12" size="16" type="text" value="<?php echo date('m/d/Y', $opts['submission_status_change.end_date']);?>" name="end_date">
	  					<span class="add-on"><i class="icon-th"></i></span>
					</div>
		    	</div>
			 	<div class="span4">
					<label>Status</label>
					<select name="status_id">
		    		<option value="">Any Status</option>
		    		<?php foreach($submission_status as $s){?>
		    			<option value="<?php echo $s['id']; ?>"<?php if($opts['submission_status_change.status_id'] == $s['id']){?> selected<?php }?>><?php echo $s['name'];?></option>
		    		<?php }?>
		    		</select>
			 	</div>
			</div>
			<div class="row-fluid">
			 	<div class="span2">
					<label>Sticker Number</label>
				 	<input type="text" name="sticker_serial_number" class="input-small span12" value="<?php echo $opts['sticker_serial_number'];?>" placeholder="Sticker Number">
			 	</div>
			 	<div class="span2">
			    	<label>Stickered</label>
					<input type="checkbox" name="stickered" value="yes"<?php if($opts['stickered']){ echo ' checked';}?>>
				</div>
			</div>
			<div class="control-group" style="margin:5px; height:20px;">
			    <div class="controls">
					<button type="submit" class="btn download pull-right" style="margin-left:5px;">Download To Excel</button>
					<button type="submit" class="btn search btn-primary pull-right">Search</button>
				</div>
			</div>
		</form>
	</div>
	<div id="submissions">
		<?php if(isset($submissions) && count($submissions) >= 0){?>
			<table class="table table-hover">
			<tr>
				<th>Order #</th>
				<th>Name</th>
				<th>Pmt Mode</th>
				<th>FR/CAT</th>
				<th>Serial #</th>
				<th>Reviewing Svc</th>
				<th>Barcode</th>
				<th>Decl Value</th>
				<th>Mail Fee</th>
				<th>Svc Fee</th>
				<th>Sticker</th>
				<th>Sticker #</th>
				<th>Ret $</th>
				<th>Reviewer</th>
				<th>Status</th>
				<th>Status Date</th>
				<th>&nbsp;</th>
			</tr>
			<?php foreach($submissions as $submission){
				$c = '';
				$service_level = $submission['service_level'];
				if($submission['status_id'] < 2000){
					if($service_level['id'] <= 3)
						$c = 'error';
					else if($service_level['id'] < 4)
						$c = 'warning';
				}
			?>
			<tr class="<?php echo $c;?>">
				<td><?php echo $submission['order_number'];?></td>
				<td><?php echo $submission['name'];?></td>
				<td><?php echo $submission['payment_type_id'] == 1 ? 'CC' : 'Check';?></td>
				<td><?php echo $submission['catalog_number'];?></td>
				<td><?php echo $submission['serial_number'];?></td>
				<td><?php echo $submission['grading_service']['abbrev'];?></td>
				<td><?php echo $submission['barcode'];?></td>
				<td>$<?php echo money_format('%i', $submission['declared_value']);?></td>
				<td>$<?php echo money_format('%i', $submission['postage']);?></td>
				<td>$<?php echo money_format('%i', $submission['service_level']['per_note']);?></td>
				<td><?php echo $submission['stickered'] > 0 ? 'yes' : 'no' ?></td>
				<td><?php echo get_stickered($submission);?></td>
				<td><?php echo !empty($submission['note_discount']) ? '$' . money_format('%i', $submission['note_discount']) : '$0.00';?></td>
				<td>
				<?php if(isset($submission['grades'])){
					foreach($submission['grades'] as $grade){
						echo isset($grade['by']) ? $grade['by']->name : 'unknown';
						if($grade['is_final'] > 0) echo ' - final';
						echo '<br/>';
					}
				}?>
				</td>
				<td><?php echo $submission['status_change']?></td>
				<td><?php echo date(PMA_DATE_FORMAT, strtotime($submission['status_change_date']));?></td>
				<td><a href="/submissions/view/<?php echo $submission['submission_id'];?>" class="btn btn-small">View</a></td>
			</tr>
			<?php }?>
			<?php if($count > $opts['page_size']){?>
			<tr>
				<td colspan="10">
					<div class="pagination pagination-right">
					  <ul>
					    <li <?php if($opts['page_number'] == 1){?>class='disabled'<?php }?>><a href="javascript:change_page(1);">Prev</a></li>
					    <?php 
					    $end_page = ceil($count / $opts['page_size']);
					    for($i = 1; $i <= $end_page; $i++){?>
					    <li <?php if($opts['page_number'] == $i){?>class='active'<?php }?>><a href="javascript:change_page(<?php echo $i;?>);"><?php echo $i;?></a></li>
					    <?php }?>
					    <li <?php if($opts['page_number'] == $end_page){?>class='disabled'<?php }?>><a href="javascript:change_page(<?php echo $end_page;?>);">Next</a></li>
					  </ul>
					</div>
				</td>
			</tr>
			<?php }?>
			</table>
		<?php }?>
	</div>
</fieldset>