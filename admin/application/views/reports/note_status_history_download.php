		<?php if(isset($submissions) && count($submissions) >= 0){?>
			<table class="table table-hover">
			<tr>
				<th>Order #</th>
				<th>Name</th>
				<th>Pmt Mode</th>
				<th>FR/CAT</th>
				<th>Serial #</th>
				<th>Reviewing Svc</th>
				<th>Barcode</th>
				<th>Decl Value</th>
				<th>Mail Fee</th>
				<th>Svc Fee</th>
				<th>Sticker</th>
				<th>Sticker #</th>
				<th>Ret $</th>
				<th>Reviewer</th>
				<th>Status</th>
				<th>Status Date</th>
			</tr>
			<?php foreach($submissions as $submission){
				$c = '';
				$service_level = $submission['service_level'];
				if($submission['status_id'] < 2000){
					if($service_level['id'] <= 3)
						$c = 'error';
					else if($service_level['id'] < 4)
						$c = 'warning';
				}
					
			?>
			<tr class="<?php echo $c;?>">
				<td><?php echo $submission['order_number'];?></td>
				<td><?php echo $submission['name'];?></td>
				<td><?php echo $submission['payment_type_id'] == 1 ? 'CC' : 'Check';?></td>
				<td><?php echo $submission['catalog_number'];?></td>
				<td><?php echo $submission['serial_number'];?></td>
				<td><?php echo $submission['grading_service']['abbrev'];?></td>
				<td><?php echo $submission['barcode'];?></td>
				<td>$<?php echo money_format('%i', $submission['declared_value']);?></td>
				<td>$<?php echo money_format('%i', $submission['postage']);?></td>
				<td>$<?php echo money_format('%i', $submission['service_level']['per_note']);?></td>
				<td><?php echo $submission['stickered'] > 0 ? 'yes' : 'no' ?></td>
				<td><?php echo get_stickered($submission);?></td>
				<td><?php echo !empty($submission['note_discount']) ? '$' . money_format('%i', $submission['note_discount']) : '$0.00';?></td>
				<td>
				<?php if(isset($submission['grades'])){
					$itt = 0;
					foreach($submission['grades'] as $grade){
						if($itt++ > 0) echo ', ';
						echo isset($grade['by']) ? $grade['by']->name : 'unknown';
						if($grade['is_final'] > 0) echo ' - final';
					}
				}?>
				</td>
				<td><?php echo $submission['status_change']?></td>
				<td><?php echo date(PMA_DATE_FORMAT, strtotime($submission['status_change_date']));?></td>
			</tr>
			<?php }?>
			</table>
		<?php }?>