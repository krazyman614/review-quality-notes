<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<title>Review Quality Notes - Admin</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link href="/resources/css/bootstrap.min.css" rel="stylesheet" media="screen" />
	<link href="/resources/css/bootstrap-responsive.min.css" rel="stylesheet" />
	<?= $_styles ?>
	
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	<script type="text/javascript" src="/resources/js/bootstrap.min.js"></script>
	<?= $_scripts ?>
</head>
<body>
	<?php print $header ?>
	
    <div class="container" style="margin-left:10px; margin-right:10px;">
		<?php print $content ?>
	</div>
	<?php print $footer ?>
</body>
</html>
