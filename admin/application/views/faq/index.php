<script type="text/javascript">
	function change_page(page_number){
		self.location.href = '/faqs/page/<? echo $opts['page_size'] . '/'?>' + page_number;
	}
</script>
<fieldset>
	<legend>FAQs</legend>
	<div id="faqs">
		<div>
			<a href="/faqs/add" class="btn pull-right">New FAQs</a>
		</div>
		<table class="table table-hover">
		<tr>
			<th>Order</th>
			<th>Title</th>
			<th>Status</th>
			<th>&nbsp;</th>
		<tr>
		<?php 
		if(isset($faqs)){
			foreach($faqs as $faq){
				?>
				<tr>
					<td><?php echo $faq['order_by'];?></td>
					<td><?php echo $faq['title'];?></td>
					<td><?php echo ($faq['status_id'] > 0 ? 'Active' : 'Inactive');?></td>
					<td><a href="/faqs/edit/<?php echo $faq['id'];?>" class="btn btn-mini">Edit</a></td>
				</tr>			
			<?php } ?>
			<?php if($count > $opts['page_size']){?>
			<tr>
				<td colspan="4">
					<div class="pagination pagination-right">
					  <ul>
					    <li <?php if($opts['page_number'] == 1){?>class='disabled'<?php }?>><a href="javascript:change_page(1);">Prev</a></li>
					    <?php 
					    $end_page = ceil($count / $opts['page_size']);
					    for($i = 1; $i <= $end_page; $i++){?>
					    <li <?php if($opts['page_number'] == $i){?>class='active'<?php }?>><a href="javascript:change_page(<?php echo $i;?>);"><?php echo $i;?></a></li>
					    <?php }?>
					    <li <?php if($opts['page_number'] == $end_page){?>class='disabled'<?php }?>><a href="javascript:change_page(<?php echo $end_page;?>);">Next</a></li>
					  </ul>
					</div>
				</td>
			</tr>
			<?php }?>
		</table>
		<?php }?>
		<div>
			<a href="/faqs/add" class="btn pull-right">New FAQs</a>
		</div>
		
	</div>
</fieldset>