<script>
	$(function(){
		$('#create-form').submit(function(){
			$('#display-error').hide();
			$('#errors-list').empty();
			$('.error').removeClass('error');
			$(this).find('input').each(function(){
				var inpt = $(this);
				var name = inpt.attr('name');
				if($.trim(inpt.val()) == ''){
					inpt.parent().addClass('error');
					$('#errors-list').append('<li>Missing ' + name.replace('_', ' ') + '</li>');
				}			
			});
			if($(this).find('.error').length == 0)
				return true;
			else{
				$('#display-error').show();
				return false;
			}
		});
	});
</script>
<form method="post" id="create-form" action="/faqs/edit_faq">
	<input type="hidden" name="id" value="<?php if(isset($data['id'])){ echo $data['id'];}?>"/>
	<fieldset>
		<legend>Edit FAQ</legend>
		<?php if(isset($success) && $success){?>
			<div class="alert alert-success">
				Your admin has been updated
			</div>
		<?php }?>
		<div id="display-error" class="alert alert-error"<?php if(!isset($errors)){?> style="display:none;"<?php }?>>
	    	Please correct the following errors:
	    	<ul id="errors-list">
	    	<?php foreach($errors as $key=>$value){?>
	    		<li><?php echo $value;?></li>
	    	<?php }?>
	    	</ul>
	    </div>
		<div class="control-group<?php if(isset($errors['title'])){ ?> error<?php }?>">
			<label>Title</label>
			<input type="text" name="title" placeholder="Title" value="<?php if(isset($data['title'])){ echo $data['title'];}?>" class="span5" maxlength="1000"/>
		</div>
		<div class="control-group<?php if(isset($errors['order_by'])){ ?> error<?php }?>">
			<label>Order By</label>
			<input type="text" name="order_by" placeholder="Order By" value="<?php if(isset($data['order_by'])){ echo $data['order_by'];}?>" class="span2" maxlength="2"/>
		</div>
		<div class="control-group<?php if(isset($errors['content'])){ ?> error<?php }?>">
			<label>Content</label>
			<textarea name="content" rows="5" class="span5"><?php if(isset($data['content'])){ echo $data['content'];}?></textarea>
		</div>
		<div class="control-group<?php if(isset($errors['status_id'])){ ?> error<?php }?>">
			<label>Status</label>
			<select name="status_id">
				<option value="1" <?php if(isset($data['status_id']) && $data['status_id'] == '1'){?>selected="selected"<?php }?>>Active</option>
				<option value="0" <?php if(isset($data['status_id']) && $data['status_id'] == '0'){?>selected="selected"<?php }?>>Inactive</option>
			</select>
		</div>
		<button type="submit" class="btn">Edit FAQ</button><a href="/faqs/" class="btn">Back</a>
	</fieldset>
</form>