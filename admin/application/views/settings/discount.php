<script>
	$(function(){
		$('#create-form').submit(function(){
			$('#display-error').hide();
			$('#errors-list').empty();
			$('.error').removeClass('error');
			$(this).find('input').each(function(){
				var inpt = $(this);
				var name = inpt.attr('name');
				if($.trim(inpt.val()) == ''){
					inpt.parent().addClass('error');
					$('#errors-list').append('<li>Missing ' + name.replace('_', ' ') + '</li>');
				}			
			});
			if($(this).find('.error').length == 0)
				return true;
			else{
				$('#display-error').show();
				return false;
			}
		});
	});
</script>
<form method="post" id="create-form" action="/settings/edit_discount">
	<fieldset>
		<legend>Set Discount</legend>
		<div id="display-error" class="alert alert-error"<?php if(!isset($errors) || count($errors) == 0){?> style="display:none;"<?php }?>>
	    	Please correct the following errors:
	    	<ul id="errors-list">
	    	<?php foreach($errors as $key=>$value){?>
	    		<li><?php echo $value;?></li>
	    	<?php }?>
	    	</ul>
	    </div>
		<div class="control-group<?php if(isset($errors['discount'])){ ?> error<?php }?>">
			<label>Discount</label>
			<div class="input-prepend">
				<span class="add-on">%</span>
				<input type="text" name="discount" placeholder="Discount" value="<?php if(isset($discount)){ echo $discount;}?>"/>
			</div>
		</div>
		<button type="submit" class="btn">Save</button>
	</fieldset>
</form>