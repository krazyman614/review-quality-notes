<form method="post" id="edit-form" action="/bid_asks/edit_bid_ask_item">
	<input type="hidden" name="id" value="<?=$data['id']?>">
	<fieldset>
		<legend>Edit Bid/Ask Item</legend>
		<div id="display-error" class="alert alert-error"<?php if(!isset($errors)){?> style="display:none;"<?php }?>>
	    	Please correct the following errors:
	    	<ul id="errors-list">
	    	<?php foreach($errors as $key=>$value){?>
	    		<li><?php echo $value;?></li>
	    	<?php }?>
	    	</ul>
	    </div>
		<div class="control-group<?php if(isset($errors['fr_num'])){ ?> error<?php }?>">
			<label>FR #</label>
			<input type="text" name="fr_num" placeholder="FR #" value="<?php if(isset($data['fr_num'])){ echo $data['fr_num'];}?>" class="span3" maxlength="1000"/>
		</div>
		<div class="control-group<?php if(isset($errors['grade'])){ ?> error<?php }?>">
			<label>Grade</label>
			<input type="text" name="grade" placeholder="Grade" value="<?php if(isset($data['grade'])){ echo $data['grade'];}?>" class="span3" maxlength="1000"/>
		</div>
		<div class="control-group<?php if(isset($errors['rqn_bid'])){ ?> error<?php }?>">
			<label>RQN Sticker Bid</label>
			<div class="input-prepend">
				<span class="add-on">$</span>
				<input type="text" name="rqn_bid" placeholder="RQN Sticker Bid" value="<?php if(isset($data['rqn_bid'])){ echo $data['rqn_bid'];}?>" class="span2" maxlength="1000"/>
			</div>
		</div>
		<div class="control-group<?php if(isset($errors['rqn_ask'])){ ?> error<?php }?>">
			<label>RQN Sticker Ask</label>
			<div class="input-prepend">
				<span class="add-on">$</span>
				<input type="text" name="rqn_ask" placeholder="RQN Sticker Ask" value="<?php if(isset($data['rqn_ask'])){ echo $data['rqn_ask'];}?>" class="span2" maxlength="1000"/>
			</div>
		</div>
		<div class="control-group<?php if(isset($errors['email'])){ ?> error<?php }?>">
			<label>Dealer Contact</label>
			<input type="text" name="email" placeholder="Email Address" value="<?php if(isset($data['email'])){ echo $data['email'];}?>" class="span5" maxlength="1000"/>
		</div>
		<div class="control-group<?php if(isset($errors['comments'])){ ?> error<?php }?>">
			<label>Comments</label>
			<textarea name="comments" class="span5"><?=isset($data['comments']) ? $data['comments'] : ''?></textarea>
		</div>
		<button type="submit" class="btn">Edit Bid/Ask Item</button><a href="/bid_asks/" class="btn">Back</a>
	</fieldset>
</form>