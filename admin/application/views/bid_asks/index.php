<script type="text/javascript">
	function change_page(page_number){
		self.location.href = '/bid_asks/page/<? echo $opts['page_size'] . '/'?>' + page_number;
	}
</script>
<fieldset>
	<legend>Bid / Ask Items</legend>
	<div id="news_items">
		<table class="table table-hover">
		<tr>
			<th>FR #</th>
			<th>Grade</th>
			<th>RQN Sticker Bid</th>
			<th>RQN Sticker Ask</th>
			<th>Dealer Contact</th>
			<th>Status</th>
			<th>Comments</th>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
		<tr>
		<?php 
		if(isset($bid_ask_items)){
			foreach($bid_ask_items as $bid_ask_item){
				?>
				<tr>
					<td><?=$bid_ask_item['fr_num']?></td>
					<td><?=$bid_ask_item['grade']?></td>
					<td><?=money_format('%(.2n', $bid_ask_item['rqn_bid'])?></td>
					<td><?=($bid_ask_item['rqn_ask'] == 0) ? '' : money_format('%(.2n', $bid_ask_item['rqn_ask'])?></td>
					<td>
						<a href="mailto:<?=$bid_ask_item['email']?>"><?=$bid_ask_item['email']?></a>
					</td>
					<td><?=($bid_ask_item['rqn_ask']==0) ? 'Wanted' : 'Contact Dealer'?></td>
					<td><?=nl2br($bid_ask_item['comments'])?></td>
					<td><a href="/bid_asks/edit/<?php echo $bid_ask_item['id'];?>" class="btn btn-mini">Edit</a></td>
					<td>
						<a href="/bid_asks/delete/<?php echo $bid_ask_item['id'];?>" class="btn btn-mini"
							onclick="return confirm('Are you sure you want to delete this item?')">Delete</a>
					</td>
				</tr>			
			<?php } ?>
			<?php if($count > $opts['page_size']){?>
			<tr>
				<td colspan="10">
					<div class="pagination pagination-right">
					  <ul>
					    <li <?php if($opts['page_number'] == 1){?>class='disabled'<?php }?>><a href="javascript:change_page(1);">Prev</a></li>
					    <?php 
					    $end_page = ceil($count / $opts['page_size']);
					    for($i = 1; $i <= $end_page; $i++){?>
					    <li <?php if($opts['page_number'] == $i){?>class='active'<?php }?>><a href="javascript:change_page(<?php echo $i;?>);"><?php echo $i;?></a></li>
					    <?php }?>
					    <li <?php if($opts['page_number'] == $end_page){?>class='disabled'<?php }?>><a href="javascript:change_page(<?php echo $end_page;?>);">Next</a></li>
					  </ul>
					</div>
				</td>
			</tr>
			<?php }?>
		</table>
		<?php }?>
		<div>
			<a href="/bid_asks/add" class="btn pull-right">New Bid / Ask Item</a>
		</div>
		
	</div>
</fieldset>