<?php if (isset($submission['notes'])) {?>
	<script type="text/javascript">
		var notes = {};
	</script>
			<table id="order-form" class="table table-bordered">
    			<thead>
	    			<tr>
	    				<th>&nbsp;</th>
	    				<th>Reviews</th>
	    				<th>Sticker</th>
	    				<th>Line Number</th>
	    				<th>Status</th>
	    				<th>RQN Service</th>
	    				<th><?php echo get_column_name('type', $submission['region_id']);?></th>
	    				<th>Denomination</th>
	    				<th><?php echo get_column_name('catalog_number', $submission['region_id']);?></th>
	    				<th><?php echo get_column_name('pp', $submission['region_id']);?></th>
	    				<th>Serial Number</th>
	    				<th>Grading Service</th>
	    				<th>Bar Code</th>
	    				<th>Declared Value</th>
	    				<th>Service Cost</th>
	    			</tr>
    			</thead>
    			<?php foreach($submission['notes'] as $note){?>
    			<tr>
    				<td>
		    			<script type="text/javascript">
							notes['<?php echo $note['id']?>'] = <?php echo json_encode($note);?>;
		    			</script>
    				<?php 
	    				$final = false;
	    				if($note['status_id'] < 1000){
	    					$final = true;
	    					echo '&nbsp;';
	    				}
	    				elseif($note['status_id'] == 1000){
	    					$graded = false;
	    					$grades = $note['grades'];
	    					foreach($grades as $grade){
								if($current_admin->id == $grade['created_uid']){
	    							$graded = true;	
	    							break;
	    						}
	    					}
	    					if(!$graded){?>
	    						<a note_id="<?php echo $note['id']?>" href="#grade-modal" role="button" class="btn btn-mini grade-btn" data-toggle="modal">Review</a>
	    					<?php } else {?>
	    						<a class="btn btn-mini" disabled="disabled">Review</a>
	    					<?php }
    					} else {
    						echo '&nbsp;';
	    				}?>
	    				<br/>
	    				<a href="/notes/edit/<?=$note['id']?>" class="btn btn-mini btn-info">Edit</a>
    				</td>
    				<td><?php echo count($note['grades']) - ($note['status_id'] > 1000 ? 1 : 0);?></td>
	    			<td><?php echo get_stickered($note);?></td>
    				<td<?php echo check_history($note, 'sequence');?>><?php echo $note['sequence'];?></td>
    				<td><?php echo $note['status'];?></td>
    				<td<?php echo check_history($note, 'service_level_id');?>><?php echo $note['service_level']['title'];?></td>
	    			<?php echo get_note_type($note, $submission['region_id']);?>
    				<td<?php echo check_history($note, 'denomination');?>>$<?php echo money_format('%i', $note['denomination']);?></td>
    				<td<?php echo check_history($note, 'catalog_number');?>><?php echo $note['catalog_number'];?></td>
    				<td<?php echo check_history($note, 'pp');?>><?php echo $note['pp'];?></td>
    				<td<?php echo check_history($note, 'serial_number');?>><?php echo $note['serial_number'];?></td>
    				<td<?php echo check_history($note, 'grading_service_id');?>><?php echo $note['grading_service']['name'];?></td>
    				<td<?php echo check_history($note, 'barcode');?>><?php echo $note['barcode'];?></td>
      				<td<?php echo check_history($note, 'declared_value');?>>$<?php echo money_format('%i', $note['declared_value']);?></td>
      				<td<?php echo check_history($note, 'service_cost');?>><span <?php echo ($note['status_id'] == 600) ? 'style="text-decoration: line-through;"' : '';?>>$<?php echo money_format('%i', $note['service_cost']);?></span></td>
   				</tr>
	   			<?php if( $note['status_id'] >= 1000){?>
	   				<tr>
	   					<td colspan="15">
	   						<?php if(isset($note['grades']) && count($note['grades']) > 0){?>
	   						<div class="container-fluid">
	   							<h4>Reviews</h4>
	   							<?php foreach($note['grades'] as $grade){
	   								if($note['status_id'] == 1000){
	   									$this->load->view('inc/grade', $grade);
	   								}
	   								else if($grade['is_final'] >= 1){
	   									$this->load->view('inc/grade', $grade);
	   								}?>
	   							<?php }?>
	   	  					</div>
	   	  					<?php }?>
	   	  					<?php if($note['status_id'] == 1000){?>
	   						<div>
								<a note_id="<?php echo $note['id']?>" href="#grade-modal" role="button" class="btn final-grade-btn pull-right" data-toggle="modal">Final Review</a>
							</div>
							<?php }?>
	   	  				</td>
	   	  			</tr>
	   	  			<?php }?>
    			<?php }?>
    			<tr id="order-form-footer">
    				<td colspan="13">
    					<strong class="pull-right">Total Declared Value</strong>
    				</td>
    				<td>
    					$<?php echo money_format('%i', $submission['total_declared_value']);?>
    				</td>
    				<td>&nbsp;</td>
    			</tr>
    			<tr>
	    			<td colspan="14">
    					<strong class="pull-right">Sub Total</strong>
    				</td>
    				<td>
    					$<?php echo money_format('%i', $submission['sub_total']);?>
    				</td>
    			</tr>
    			<tr>
	    			<td colspan="14">
    					<strong class="pull-right">Postage</strong>
    				</td>
    				<td>
    					$<?php echo money_format('%i', $submission['postage']);?>
    				</td>
    			</tr>
    			<?php if($submission['discount'] > 0){?>
    			<tr>
	    			<td colspan="14">
    					<strong class="pull-right">Discount (%<?php echo $submission['discount']?>)</strong>
    				</td>
    				<td>
    					$<?php echo money_format('%i', $submission['discount_amount']);?>
    				</td>
    			</tr>
    			<?php }?>
    			<tr>
	    			<td colspan="14">
    					<strong class="pull-right">Total</strong>
    				</td>
    				<td>
    					$<?php echo money_format('%i', $submission['total']);?>
    				</td>
    			</tr>
    		</table>
<?php 
}
	