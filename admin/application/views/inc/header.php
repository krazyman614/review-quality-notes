<div class="navbar navbar-inverse">
	<div class="navbar-inner">
		<div class="container">
			<a class="btn btn-navbar" data-toggle="collapse"
				data-target=".nav-collapse"> <span class="icon-bar"></span> <span
				class="icon-bar"></span> <span class="icon-bar"></span>
			</a> <a class="brand" href="#">Review Quality Notes Admin</a>
			<?php if(isset($current_admin) && !empty($current_admin)){?>
			<div class="nav-collapse collapse">
				<ul class="nav">
					<li class="dropdown">
					    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
					      Submission Admin
					      <b class="caret"></b>
					    </a>
					    <ul class="dropdown-menu">
							<li><a href="/submissions">Submissions</a></li>
							<?php if($current_admin->admin_type_id <= 1){?>
							<li class="divider"></li>
							<li><a href="/grading_services">Grading Services</a></li>
							<li><a href="/note_types">Note Types</a></li>
							<li><a href="/postage">Postage</a></li>
							<li><a href="/service_levels">Service Levels</a></li>
							<li><a href="/settings/discount">Discount</a></li>
							<?php }?>
					    </ul>
					</li>
					<!--
					<li><a href="/faqs">FAQs</a></li>
					-->
					<li><a href="/news_items">News</a></li>
					<li><a href="/specials">Specials</a></li>
					<li><a href="/users">Customers</a></li>
					<li><a href="/bid_asks">Bid/Ask</a></li>
					<?php if($current_admin->admin_type_id <= 1){?>
						<li class="dropdown">
						    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
						      Reports
						      <b class="caret"></b>
						    </a>
						    <ul class="dropdown-menu">
								<li><a href="/reports/submissions">Submission Report</a></li>
								<li><a href="/reports/notes">Note Report</a></li>
								<li><a href="/reports/note_status_history">Note Status History Report</a></li>
						    </ul>
						</li>
					<?php }?>
					<?php if($current_admin->admin_type_id <= 1){?>
					<li><a href="/admins">Admins</a></li>
					<?php }?>
					<li><a href="/dealer">Directory</a></li>
					<li><a href="/home/logout">Logout</a></li>
				</ul>
			</div>
			<?php }?>
		</div>
	</div>
</div>