<script>
	$(function(){
		$('#create-form').submit(function(){
			$('#display-error').hide();
			$('#errors-list').empty();
			$('.error').removeClass('error');
			$(this).find('input').each(function(){
				var inpt = $(this);
				var name = inpt.attr('name');
				switch(name.toLowerCase()){
				case 'time_to_process':
					break;
				default:
					if($.trim(inpt.val()) == ''){
						inpt.parent().addClass('error');
						$('#errors-list').append('<li>Missing ' + name.replaceAll('_', ' ') + '</li>');
					}			
					break;
				}		
			});
			if($(this).find('.error').length == 0)
				return true;
			else{
				$('#display-error').show();
				return false;
			}
		});
	});
</script>
<form method="post" id="create-form" action="/service_levels/edit_service_level">
	<input type="hidden" name="id" value="<?php if(isset($data['id'])){ echo $data['id'];}?>"/>
	<fieldset>
		<legend>Edit Service Level</legend>
		<?php if(isset($success) && $success){?>
			<div class="alert alert-success">
				Your service level has been saved
			</div>
		<?php }?>
		<div id="display-error" class="alert alert-error"<?php if(!isset($errors)){?> style="display:none;"<?php }?>>
	    	Please correct the following errors:
	    	<ul id="errors-list">
	    	<?php foreach($errors as $key=>$value){?>
	    		<li><?php echo $value;?></li>
	    	<?php }?>
	    	</ul>
	    </div>
		<div class="control-group<?php if(isset($errors['level'])){ ?> error<?php }?>">
			<label>Level</label>
			<input type="text" name="level" placeholder="Level" value="<?php if(isset($data['level'])){ echo $data['level'];}?>" class="span2" maxlength="2"/>
		</div>
		<div class="control-group<?php if(isset($errors['title'])){ ?> error<?php }?>">
			<label>Title</label>
			<input type="text" name="title" placeholder="Title" value="<?php if(isset($data['title'])){ echo $data['title'];}?>" class="span5" maxlength="100"/>
		</div>
		<div class="control-group<?php if(isset($errors['per_note'])){ ?> error<?php }?>">
			<label>Per Note</label>
			<input type="text" name="per_note" placeholder="Per Note" value="<?php if(isset($data['per_note'])){ echo $data['per_note'];}?>" class="span2" maxlength="5"/>
		</div>
		<div class="control-group<?php if(isset($errors['time_to_process'])){ ?> error<?php }?>">
			<label>Time to Process</label>
			<input type="text" name="time_to_process" placeholder="Time to Process" value="<?php if(isset($data['time_to_process'])){ echo $data['time_to_process'];}?>" class="span5" maxlength="255"/>
		</div>
		<div class="control-group<?php if(isset($errors['status_id'])){ ?> error<?php }?>">
			<label>Status</label>
			<select name="status_id">
				<option value="1" <?php if(isset($data['status_id']) && $data['status_id'] == '1'){?>selected="selected"<?php }?>>Active</option>
				<option value="0" <?php if(isset($data['status_id']) && $data['status_id'] == '0'){?>selected="selected"<?php }?>>Inactive</option>
			</select>
		</div>
		<button type="submit" class="btn">Edit Service Level</button><a href="/service_levels/" class="btn">Back</a>
	</fieldset>
</form>