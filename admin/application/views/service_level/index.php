<script type="text/javascript">
	function change_page(page_number){
		self.location.href = '/service_levels/page/<? echo $opts['page_size'] . '/'?>' + page_number;
	}
</script>
<fieldset>
	<legend>Service Levels</legend>
	<div id="service_levels">
		<div>
			<a href="/service_levels/add" class="btn pull-right">New Service Levels</a>
		</div>
		<table class="table table-hover">
		<tr>
			<th>Level</th>
			<th>Title</th>
			<th>Per Note</th>
			<th>Time To Process</th>
			<th>Status</th>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
		<tr>
		<?php 
		if(isset($service_levels)){
			foreach($service_levels as $service_level){
				?>
				<tr>
					<td><?php echo $service_level['level'];?></td>
					<td><?php echo $service_level['title'];?></td>
					<td><?php echo $service_level['per_note'] > 0 ? '$' . money_format('%i', $service_level['per_note']) : '' ;?></td>
					<td><?php echo $service_level['time_to_process'];?></td>
					<td><?php echo ($service_level['status_id'] > 0 ? 'Active' : 'Inactive');?>
					<td><a href="/service_levels/edit/<?php echo $service_level['id'];?>" class="btn btn-mini">Edit</a></td>
					<td><a href="/service_levels/delete/<?php echo $service_level['id'];?>" onclick="return confirm('Are you sure you want to delete this?');" class="btn btn-mini">Delete</a></td>
				</tr>
			<?php } ?>
			<?php if($count > $opts['page_size']){?>
			<tr>
				<td colspan="6">
					<div class="pagination pagination-right">
					  <ul>
					    <li <?php if($opts['page_number'] == 1){?>class='disabled'<?php }?>><a href="javascript:change_page(1);">Prev</a></li>
					    <?php 
					    $end_page = ceil($count / $opts['page_size']);
					    for($i = 1; $i <= $end_page; $i++){?>
					    <li <?php if($opts['page_number'] == $i){?>class='active'<?php }?>><a href="javascript:change_page(<?php echo $i;?>);"><?php echo $i;?></a></li>
					    <?php }?>
					    <li <?php if($opts['page_number'] == $end_page){?>class='disabled'<?php }?>><a href="javascript:change_page(<?php echo $end_page;?>);">Next</a></li>
					  </ul>
					</div>
				</td>
			</tr>
			<?php }?>
		</table>
		<?php }?>
		<div>
			<a href="/service_levels/add" class="btn pull-right">New Service Levels</a>
		</div>
		
	</div>
</fieldset>