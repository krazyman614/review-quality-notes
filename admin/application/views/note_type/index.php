<script type="text/javascript">
	$(function(){
		$('#frm-search').submit(function(){
			var url = '/note_types';
			var val = $('#search-region_id').val();
			if(val != '') url += '/page/' +val;
			console.log(url);
			$('#frm-search').attr('action', url);
			return true;
		});

	});
	
	function change_page(page_number){
		var val = $('#search-region_id').val();		
		if (!val || val=='') val="null"; //fix for null values
		self.location.href =  '/note_types/page/' + val + "/"+'<? echo  $opts['page_size'] . '/'?>' + page_number  ;
	}
</script>

<fieldset>
	<legend>Note Types</legend>
	<div id="note_types">
		<div>
			<?php if(isset($regions)){?>
			<form id="frm-search" method="get">
			<div class="input-append pull-left">
				<select class="span2" id="search-region_id">
					<option value="">All</option>
		  			<?php foreach($regions as $region){?>
					<option value="<?php echo $region['id'];?>"<?php if(isset($region_id) && $region['id'] == $region_id){?> selected="selected"<?php }?>><?php echo $region['name'];?></option>
					<?php }?>
			  	</select>
		  		<input type="submit" class="btn" value="Search"/>
			</div>
			</form>
			<?php }?>
			<a href="/note_types/add" class="btn pull-right">New Note Types</a>
		</div>
		<table class="table table-hover">
		<tr>
			<th>Type</th>
			<th>Region</th>
			<th>Status</th>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
		<tr>
		<?php 
		if(isset($note_types)){
			foreach($note_types as $note_type){
				?>
				<tr>
					<td><?php echo $note_type['type'];?></td>
					<td><?php echo $note_type['region']['name'];?></td>
					<td><?php echo ($note_type['status_id'] > 0 ? 'Active' : 'Inactive');?>
					<td><a href="/note_types/edit/<?php echo $note_type['id'];?>" class="btn btn-mini">Edit</a></td>
					<td><a href="/note_types/delete/<?php echo $note_type['id'];?>" onclick="return confirm('Are you sure you want to delete this?');" class="btn btn-mini">Delete</a></td>
				</tr>
			<?php } ?>
			<?php if($count > $opts['page_size']){?>
			<tr>
				<td colspan="4">
					<div class="pagination pagination-right">
					  <ul>
					    <li <?php if($opts['page_number'] == 1){?>class='disabled'<?php }?>><a href="javascript:change_page(1);">Prev</a></li>
					    <?php 
					    $end_page = ceil($count / $opts['page_size']);
					    for($i = 1; $i <= $end_page; $i++){?>
					    <li <?php if($opts['page_number'] == $i){?>class='active'<?php }?>><a href="javascript:change_page(<?php echo $i;?>);"><?php echo $i;?></a></li>
					    <?php }?>
					    <li <?php if($opts['page_number'] == $end_page){?>class='disabled'<?php }?>><a href="javascript:change_page(<?php echo $end_page;?>);">Next</a></li>
					  </ul>
					</div>
				</td>
			</tr>
			<?php }?>
		</table>
		<?php }?>
		<div>
			<a href="/note_types/add" class="btn pull-right">New Note Types</a>
		</div>
		
	</div>
</fieldset>