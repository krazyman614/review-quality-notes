<script>
	$(function(){
		$('#create-form').submit(function(){
			$('#display-error').hide();
			$('#errors-list').empty();
			$('.error').removeClass('error');
			$(this).find('input').each(function(){
				var inpt = $(this);
				var name = inpt.attr('name');
				
				if($.trim(inpt.val()) == ''){
					inpt.parent().addClass('error');
					$('#errors-list').append('<li>Missing ' + name.replaceAll('_', ' ') + '</li>');
				}			
			});
			if($(this).find('.error').length == 0)
				return true;
			else{
				$('#display-error').show();
				return false;
			}
		});
	});
</script>
<form method="post" id="create-form" action="/note_types/edit_note_type">
	<input type="hidden" name="id" value="<?php if(isset($data['id'])){ echo $data['id'];}?>"/>
	<fieldset>
		<legend>Edit Note Type</legend>
		<?php if(isset($success) && $success){?>
			<div class="alert alert-success">
				Your note type has been saved
			</div>
		<?php }?>
		<div id="display-error" class="alert alert-error"<?php if(!isset($errors)){?> style="display:none;"<?php }?>>
	    	Please correct the following errors:
	    	<ul id="errors-list">
	    	<?php foreach($errors as $key=>$value){?>
	    		<li><?php echo $value;?></li>
	    	<?php }?>
	    	</ul>
	    </div>
		<div class="control-group<?php if(isset($errors['type'])){ ?> error<?php }?>">
			<label>Type</label>
			<input type="text" name="type" placeholder="Type" value="<?php if(isset($data['type'])){ echo $data['type'];}?>" class="span5" maxlength="100"/>
		</div>
		<?php if(isset($regions)){?>
		<div class="control-group<?php if(isset($errors['region_id'])){ ?> error<?php }?>">
			<label>Region</label>
			<select name="region_id">
				<?php foreach($regions as $region){?>		
				<option value="<?php echo $region['id'];?>" <?php if(isset($data['region_id']) && $data['region_id'] == $region['id']){?>selected="selected"<?php }?>><?php echo $region['name'];?></option>
				<?php }?>
			</select>
		</div>
		<?php }?>
		<div class="control-group<?php if(isset($errors['status_id'])){ ?> error<?php }?>">
			<label>Status</label>
			<select name="status_id">
				<option value="1" <?php if(isset($data['status_id']) && $data['status_id'] == '1'){?>selected="selected"<?php }?>>Active</option>
				<option value="0" <?php if(isset($data['status_id']) && $data['status_id'] == '0'){?>selected="selected"<?php }?>>Inactive</option>
			</select>
		</div>
		<button type="submit" class="btn">Edit Note Type</button><a href="/note_types/" title="Back to Note Types" class="btn">Back</a>
	</fieldset>
</form>