<script>
	$(function(){
		$('#create-form').submit(function(){
			$('#display-error').hide();
			$('#errors-list').empty();
			$('.error').removeClass('error');
			$(this).find('input').each(function(){
				var inpt = $(this);
				var name = inpt.attr('name');
				if($.trim(inpt.val()) == ''){
					inpt.parent().addClass('error');
					$('#errors-list').append('<li>Missing ' + name.replace('_', ' ') + '</li>');
				}			
			});
			if($(this).find('.error').length == 0)
				return true;
			else{
				$('#display-error').show();
				return false;
			}
		});
	});
</script>
<form method="post" id="create-form" action="/admins/create">
	<fieldset>
		<legend>Create Admin</legend>
		<div id="display-error" class="alert alert-error"<?php if(!isset($errors)){?> style="display:none;"<?php }?>>
	    	Please correct the following errors:
	    	<ul id="errors-list">
	    	<?php 
	    	if(isset($errors)){
		    	foreach($errors as $key=>$value){?>
	    		<li><?php echo $value;?></li>
		    	<?php }
		    }?>
	    	</ul>
	    </div>
		<div class="control-group<?php if(isset($errors['name'])){ ?> error<?php }?>">
			<label>Name</label>
			<input type="text" name="name" placeholder="Name" value="<?php if(isset($data['name'])){ echo $data['name'];}?>"/>
		</div>
		<div class="control-group<?php if(isset($errors['email'])){ ?> error<?php }?>">
			<label>Email</label>
			<input type="text" name="email" placeholder="Email" value="<?php if(isset($data['email'])){ echo $data['email'];}?>"/>
		</div>
		<div class="control-group<?php if(isset($errors['password'])){ ?> error<?php }?>">
			<label>Password</label>
			<input type="password" name="password" placeholder="Password" value="<?php if(isset($data['password'])){ echo $data['password'];}?>"/>
		</div>
		<div class="control-group<?php if(isset($errors['confirm_password'])){ ?> error<?php }?>">
			<label>Confirm Password</label>
			<input type="password" name="confirm_password" placeholder="Confirm Password" value="<?php if(isset($data['confirm_password'])){ echo $data['confirm_password'];}?>"/>
		</div>
		<div class="control-group<?php if(isset($errors['admin_type_id'])){ ?> error<?php }?>">
			<label>Type</label>
			<select name="admin_type_id">
				<option value="1" <?php if(isset($data['admin_type_id']) && $data['admin_type_id'] == '1'){?>selected="selected"<?php }?>>Sys Admin</option>
				<option value="100" <?php if(isset($data['admin_type_id']) && $data['admin_type_id'] == '100'){?>selected="selected"<?php }?>>Reviewer</option>
			</select>
		</div>
		<div class="control-group<?php if(isset($errors['status_id'])){ ?> error<?php }?>">
			<label>Status</label>
			<select name="status_id">
				<option value="1" <?php if(isset($data['status_id']) && $data['status_id'] == '1'){?>selected="selected"<?php }?>>Active</option>
				<option value="0" <?php if(isset($data['status_id']) && $data['status_id'] == '0'){?>selected="selected"<?php }?>>Inactive</option>
			</select>
		</div>
		<button type="submit" class="btn">Create Admin</button><a href="/admins/" title="Back" class="btn">Back</a>
	</fieldset>
</form>