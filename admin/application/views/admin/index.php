<script type="text/javascript">
	function change_page(page_number){
		self.location.href = '/admins/page/<? echo $opts['page_size'] . '/'?>' + page_number;
	}
</script>
<fieldset>
	<legend>Admins</legend>
	<div id="admins">
		<div>
			<a href="/admins/add" class="btn pull-right">New Admins</a>
		</div>
		<table class="table table-hover">
		<tr>
			<th>Email</th>
			<th>Name</th>
			<th>&nbsp;</th>
		<tr>
		<?php 
		if(isset($admins)){
			foreach($admins as $admin){
				?>
				<tr>
					<td><?php echo $admin['email'];?></td>
					<td><?php echo $admin['name'];?></td>
					<td><a href="/admins/edit/<?php echo $admin['id'];?>" class="btn btn-mini">Edit</a></td>
				</tr>			
			<?php } ?>
			<?php if($count > $opts['page_size']){?>
			<tr>
				<td colspan="3">
					<div class="pagination pagination-right">
					  <ul>
					    <li <?php if($opts['page_number'] == 1){?>class='disabled'<?php }?>><a href="javascript:change_page(1);">Prev</a></li>
					    <?php 
					    $end_page = ceil($count / $opts['page_size']);
					    for($i = 1; $i <= $end_page; $i++){?>
					    <li <?php if($opts['page_number'] == $i){?>class='active'<?php }?>><a href="javascript:change_page(<?php echo $i;?>);"><?php echo $i;?></a></li>
					    <?php }?>
					    <li <?php if($opts['page_number'] == $end_page){?>class='disabled'<?php }?>><a href="javascript:change_page(<?php echo $end_page;?>);">Next</a></li>
					  </ul>
					</div>
				</td>
			</tr>
			<?php }?>
		</table>
		<?php }?>
		<div>
			<a href="/admins/add" class="btn pull-right">New Admins</a>
		</div>
		
	</div>
</fieldset>