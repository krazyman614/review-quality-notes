<script>
	$(function(){
		$('#create-form').submit(function(){
			$('#display-error').hide();
			$('#errors-list').empty();
			$('.error').removeClass('error');
			$(this).find('input').each(function(){
				var inpt = $(this);
				var name = inpt.attr('name');
				if($.trim(inpt.val()) == ''){
					inpt.parent().addClass('error');
					$('#errors-list').append('<li>Missing ' + name.replace('_', ' ') + '</li>');
				}			
			});
			if($(this).find('.error').length == 0)
				return true;
			else{
				$('#display-error').show();
				return false;
			}
		});
	});
</script>
<form method="post" id="create-form" action="/grading_services/create">
	<fieldset>
		<legend>Create Grading Service</legend>
		<div id="display-error" class="alert alert-error"<?php if(!isset($errors)){?> style="display:none;"<?php }?>>
	    	Please correct the following errors:
	    	<ul id="errors-list">
	    	<?php foreach($errors as $key=>$value){?>
	    		<li><?php echo $value;?></li>
	    	<?php }?>
	    	</ul>
	    </div>
		<div class="control-group<?php if(isset($errors['name'])){ ?> error<?php }?>">
			<label>Name</label>
			<input type="text" name="name" placeholder="Name" value="<?php if(isset($data['name'])){ echo $data['name'];}?>" class="span5" maxlength="1000"/>
		</div>
		<div class="control-group<?php if(isset($errors['abbrev'])){ ?> error<?php }?>">
			<label>Abbrev</label>
			<input type="text" name="abbrev" placeholder="Abbrev" value="<?php if(isset($data['abbrev'])){ echo $data['abbrev'];}?>" class="span2" maxlength="4"/>
		</div>
		<div class="control-group<?php if(isset($errors['status_id'])){ ?> error<?php }?>">
			<label>Status</label>
			<select name="status_id">
				<option value="1" <?php if(isset($data['status_id']) && $data['status_id'] == '1'){?>selected="selected"<?php }?>>Active</option>
				<option value="0" <?php if(isset($data['status_id']) && $data['status_id'] == '0'){?>selected="selected"<?php }?>>Inactive</option>
			</select>
		</div>
		<button type="submit" class="btn">Create Grading Service</button>
	</fieldset>
</form>