<script type="text/javascript">
	function change_page(page_number){
		self.location.href = '/grading_services/page/<? echo $opts['page_size'] . '/'?>' + page_number;
	}
</script>

<fieldset>
	<legend>Grading Services</legend>
	<div id="grading_services">
		<div>
			<a href="/grading_services/add" class="btn pull-right">New Grading Services</a>
		</div>
		<table class="table table-hover">
		<tr>
			<th>Abbrev</th>
			<th>Name</th>
			<th>Status</th>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
		<tr>
		<?php 
		if(isset($grading_services)){
			foreach($grading_services as $grading_service){
				?>
				<tr>
					<td><?php echo $grading_service['abbrev'];?></td>
					<td><?php echo $grading_service['name'];?></td>
					<td><?php echo ($grading_service['status_id'] > 0 ? 'Active' : 'Inactive');?>
					<td><a href="/grading_services/edit/<?php echo $grading_service['id'];?>" class="btn btn-mini">Edit</a></td>
					<td><a href="/grading_services/delete/<?php echo $grading_service['id'];?>" onclick="return confirm('Are you sure you want to delete this?');" class="btn btn-mini">Delete</a></td>
				</tr>
			<?php } ?>
			<?php if($count > $opts['page_size']){?>
			<tr>
				<td colspan="4">
					<div class="pagination pagination-right">
					  <ul>
					    <li <?php if($opts['page_number'] == 1){?>class='disabled'<?php }?>><a href="javascript:change_page(1);">Prev</a></li>
					    <?php 
					    $end_page = ceil($count / $opts['page_size']);
					    for($i = 1; $i <= $end_page; $i++){?>
					    <li <?php if($opts['page_number'] == $i){?>class='active'<?php }?>><a href="javascript:change_page(<?php echo $i;?>);"><?php echo $i;?></a></li>
					    <?php }?>
					    <li <?php if($opts['page_number'] == $end_page){?>class='disabled'<?php }?>><a href="javascript:change_page(<?php echo $end_page;?>);">Next</a></li>
					  </ul>
					</div>
				</td>
			</tr>
			<?php }?>
		</table>
		<?php }?>
		<div>
			<a href="/grading_services/add" class="btn pull-right">New Grading Services</a>
		</div>
		
	</div>
</fieldset>