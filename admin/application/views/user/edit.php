<script>
	$(function(){
		$('#create-form').submit(function(){
			$('#display-error').hide();
			$('#errors-list').empty();
			$('.error').removeClass('error');
			$(this).find('input').each(function(){
				var inpt = $(this);
				var type = inpt.attr('type');
				switch(type){
				case 'checkbox':
					if(inpt.attr('name') === 'accepted_terms' && !inpt.is(':checked')){
						$('#errors-list').append('<li>Did not accept terms</li>');
						inpt.parent().parent().addClass('error');
					}
					break;
				default:
					var name = inpt.attr('name');
					switch(name){
					case 'address2':
					case 'alt_phone_number':
					case 'password':
					case 'confirm_password':
						break;
					default:
						if($.trim(inpt.val()) == ''){
							inpt.parent().addClass('error');
							$('#errors-list').append('<li>Missing ' + name.replace('_', ' ') + '</li>');
						}			
						break;
					}
				}
			});
			if($(this).find('.error').length == 0)
				return true;
			else{
				$('#display-error').show();
				return false;
			}
		});
	});
</script>
<form method="post" id="create-form" action="/users/edit_user">
	<input type="hidden" name="id" value="<?php if(isset($data['id'])){ echo $data['id'];}?>"/>
	<fieldset>
		<legend>Edit Customer</legend>
		<?php if(isset($success) && $success){?>
			<div class="alert alert-success">
				Your user has been updated
			</div>
		<?php }?>
		
		<div id="display-error" class="alert alert-error"<?php if(!isset($errors)){?> style="display:none;"<?php }?>>
	    	Please correct the following errors:
	    	<ul id="errors-list">
	    	<?php foreach($errors as $key=>$value){?>
	    		<li><?php echo $value;?></li>
	    	<?php }?>
	    	</ul>
	    </div>
		<div class="control-group<?php if(isset($errors['name'])){ ?> error<?php }?>">
			<label>Name</label>
			<input type="text" name="name" placeholder="Name" value="<?php if(isset($data['name'])){ echo $data['name'];}?>"/>
		</div>
		<div class="control-group<?php if(isset($errors['email'])){ ?> error<?php }?>">
			<label>Email</label>
			<input type="text" name="email" placeholder="Email" value="<?php if(isset($data['email'])){ echo $data['email'];}?>"/>
		</div>
		<div class="control-group<?php if(isset($errors['password'])){ ?> error<?php }?>">
			<label>Password</label>
			<input type="password" name="password" placeholder="Password" value="<?php if(isset($data['password'])){ echo $data['password'];}?>"/>
		</div>
		<div class="control-group<?php if(isset($errors['confirm_password'])){ ?> error<?php }?>">
			<label>Confirm Password</label>
			<input type="password" name="confirm_password" placeholder="Confirm Password" value="<?php if(isset($data['confirm_password'])){ echo $data['confirm_password'];}?>"/>
		</div>
		<div class="control-group<?php if(isset($errors['company_name'])){ ?> error<?php }?>">
			<label>Company Name</label>
			<input type="text" name="company_name" placeholder="Company Name" value="<?php if(isset($data['company_name'])){ echo $data['company_name'];}?>"/>
		</div>
		<div class="control-group<?php if(isset($errors['address']) || isset($errors['address2']) || isset($errors['city']) || isset($errors['state']) || isset($errors['postal_code']) || isset($errors['country'])){ ?> error<?php }?>">
			<label>Address</label>
			<input type="text" name="address" placeholder="Address" class="span5" value="<?php if(isset($data['address'])){ echo $data['address'];}?>"/>
			<br />
			<input type="text" name="address2" placeholder="Address Line 2" class="span5" value="<?php if(isset($data['address2'])){ echo $data['address2'];}?>"/>
			<div class="controls controls-row">
				<input type="text" name="city" placeholder="City" class="span2" value="<?php if(isset($data['city'])){ echo $data['city'];}?>"/>
				<input type="text" name="state_province" placeholder="State" maxlength="2" class="span1" value="<?php if(isset($data['state_province'])){ echo $data['state_province'];}?>"/>
				<input type="text" name="postal_code" placeholder="Postal Code" maxlength="10" class="span2" value="<?php if(isset($data['postal_code'])){ echo $data['postal_code'];}?>"/>
			</div>
			<input type="text" name="country" placeholder="Country" maxlength="2" class="span2" value="<?php if(isset($data['country'])){ echo $data['country'];}?>"/>
		</div>
		<div class="control-group<?php if(isset($errors['phone_number'])){ ?> error<?php }?>">
			<label>Phone Number</label> <input type="text" name="phone_number" placeholder="Phone Number" value="<?php if(isset($data['phone_number'])){ echo $data['phone_number'];}?>"/>
		</div>
		<div class="control-group<?php if(isset($errors['alt_phone_number'])){ ?> error<?php }?>">
			<label>Alt Phone Number</label> <input type="text" name="alt_phone_number" placeholder="Alt Phone Number" value="<?php if(isset($data['alt_phone_number'])){ echo $data['alt_phone_number'];}?>"/>
		</div>
		<div class="control-group<?php if(isset($errors['pay_by_check'])){ ?> error<?php }?>">
			<label class="checkbox">
				<input type="checkbox" name="pay_by_check" value="1" <?php if($data['pay_by_check'] === '1'){?>checked="checked"<?php }?>/>
				Can pay by check
			</label>
		</div>
		<div class="control-group<?php if(isset($errors['accepted_terms'])){ ?> error<?php }?>">
			<label class="checkbox">
				<input type="checkbox" name="accepted_terms" value="1" <?php if(isset($data['accepted_terms'])){?>checked="checked"<?php }?>/>
				Accept terms and conditions
			</label>
		</div>
		<div class="control-group<?php if(isset($errors['status_id'])){ ?> error<?php }?>">
			<label>Status</label>
			<select name="status_id">
				<option value="1" <?php if(isset($data['status_id']) && $data['status_id'] == '1'){?>selected="selected"<?php }?>>Active</option>
				<option value="0" <?php if(isset($data['status_id']) && $data['status_id'] == '0'){?>selected="selected"<?php }?>>Inactive</option>
			</select>
		</div>
		<button type="submit" class="btn">Edit Customer</button><a href="/users" title="Back" class="btn">Back</a>
	</fieldset>
</form>