<script type="text/javascript">
	function change_page(page_number){
		self.location.href = '/users/page/<?= $opts['page_size'] . '/'?>' + page_number;
	}
</script>
<fieldset>
	<legend>Customers</legend>
	<div id="users">
		<div>
			<a href="/users/add" class="btn pull-right">New Customers</a>
		</div>
		<table class="table table-hover">
		<tr>
			<th>Email</th>
			<th>Name</th>
            <th>Created</th>
			<th>&nbsp;</th>
		<tr>
		<?php 
		if(isset($users)){
			foreach($users as $user){
				?>
				<tr>
					<td><?php echo $user['email'];?></td>
					<td><?php echo $user['name'];?></td>
                    <td><?php echo date('m/d/Y', strtotime($user['created_date']))?></td>
					<td><a href="/users/edit/<?php echo $user['id'];?>" class="btn btn-mini">Edit</a></td>
                    <td>
                        <a href="/users/delete/<?php echo $user['id'];?>" class="btn btn-mini"
                           onclick="return confirm('Are you sure you want to delete this user?')">Delete</a>
                    </td>
                </tr>
			<?php } ?>
			<?php if($count > $opts['page_size']){?>
			<tr>
				<td colspan="4">
					<div class="pagination pagination-right">
					  <ul>
					    <li <?php if($opts['page_number'] == 1){?>class='disabled'<?php }?>><a href="javascript:change_page(1);">Prev</a></li>
					    <?php 
					    $end_page = ceil($count / $opts['page_size']);
					    for($i = 1; $i <= $end_page; $i++){?>
					    <li <?php if($opts['page_number'] == $i){?>class='active'<?php }?>><a href="javascript:change_page(<?php echo $i;?>);"><?php echo $i;?></a></li>
					    <?php }?>
					    <li <?php if($opts['page_number'] == $end_page){?>class='disabled'<?php }?>><a href="javascript:change_page(<?php echo $end_page;?>);">Next</a></li>
					  </ul>
					</div>
				</td>
			</tr>
			<?php }?>
		</table>
		<?php }?>
		<div>
			<a href="/users/add" class="btn pull-right">New Customers</a>
		</div>
		
	</div>
</fieldset>