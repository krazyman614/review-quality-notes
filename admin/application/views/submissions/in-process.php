<style>
.modal-grades {
    width: 900px;
    margin: -300px 0 0 -450px;
}
</style>

<fieldset>
	<legend>Order #<?php echo $submission['order_number']?></legend>
	<?php if(isset($msg) && !empty($msg)){?>
		<div class="alert">
			<?php echo $msg;?>
		</div>
<?php }?>
	<div class="container-fluid">
		<div class="row-fluid">
		  	<div class="span10">
	  			<strong>Status:</strong>
  				<?php print $submission['status'];?> - <?php echo date('D, d M Y g:i A', strtotime($submission['updated_date']));?>
	  		</div>
	    </div>
	  	<?php if(!empty($submission['fedex_account_number'])){?>
	  	<div class="row-fluid">
		  	<div class="span8">
		  		<strong>Fedex Account # :</strong>
		  		<?php print $submission['fedex_account_number'];?>
		  	</div>
		</div>
	  	<?php }?>
	    <div class="row-fluid">
		    <div class="span2">
		    	<strong>Info</strong><br/>
		    	<?php echo $submission['name'];?><br/>
		    	<?php echo $submission['company_name'];?><br/>
		    	<?php echo $submission['phone_number'];?><br/>
		    	<?php echo $submission['email'];?><br/>
		    </div>
		    <div class="span2">
		    	<strong>Shipping Address</strong><br/>
		       	<?php echo $submission['address'];?><br/>
		    	<?php if(trim($submission['address2']) != ''){ ?>
		       	<?php echo $submission['address2'];?><br/>
		       	<?php }?>
		    	<?php echo $submission['city'];?>, <?php echo $submission['state_province'];?> <?php echo $submission['postal_code'];?><br/>
		    	<?php echo $submission['country'];?><br/>
			</div>
			<?php if ($submission['payment_type_id'] == 1) {?>
			<div class="span2">
		    	<strong>Billing Address</strong><br/>
		       	<?php echo $submission['billing_address'];?><br/>
		    	<?php if(trim($submission['billing_address2']) != ''){ ?>
		    	<?php echo $submission['billing_address2'];?><br/>
		    	<?php }?>
		    	<?php echo $submission['billing_city'];?>, <?php echo $submission['billing_state_province'];?> <?php echo $submission['billing_postal_code'];?><br/>
		    	<?php echo $submission['billing_country'];?><br/>
			</div>
			<?php }?>
			<?php switch($submission['payment_type_id']){
				case 1:?>
				    <div class="span2">
				    	<strong>Credit Card</strong><br/>
				    	<?php echo $submission['card_type'];?><br/>
				    	<?php echo $submission['name_on_card'];?><br/>
				    	############<?php echo $submission['card_last_four_numbers'];?><br/>
				    	<?php echo $submission['card_exp_month'];?>/<?php echo $submission['card_exp_year'];?><br/>
				    </div>
					<?php 
					break;
				case 2: ?>
				    <div class="span2">
				    	<strong>Check</strong><br/>
				    </div>				
					<?php 
					break;
			}?>
		</div>
		<div class="row-fluid">
	    	<?php $this->load->view('inc/notes', array('submission' => $submission, 'current_admin' => $current_admin));?>
		</div>
	</div>
</fieldset>

<div id="grade-modal" class="modal hide fade modal-grades" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
    <h3 id="myModalLabel">Review Note</h3>
  </div>
  <form id="frm-grade" method="post" action="/submissions/grade/<?php echo $submission['id'];?>" style="margin:0px;">
  	<input type="hidden" name="note_id" value=""/>
  	<input type="hidden" name="final" value="0"/>
	  <div class="modal-body">
			<fieldset>
		    	<legend>Score</legend>
		    	<div id="scores" style="width:800px;"></div>
		  		<table class="table table-bordered">
		  		<tr>
		  			<th>&nbsp;</th>
		  			<th>Paper</th>
		  			<th>Color</th>
		  			<th>Printing</th>
		  			<th>Face Margins</th>
		  			<th>Face Centering</th>
		  			<th>Back Margins</th>
		  			<th>Back Centering</th>
		  			<th>Registration</th>
		  		</tr>
		  		<tr>
		  			<td><strong>Other</strong></td>
		  			<td><input type="radio" name="paper" id="paper" value="0"/></td>
		  			<td><input type="radio" name="color" id="color" value="0"/></td>
		  			<td><input type="radio" name="printing" id="printing" value="0"/></td>
		  			<td><input type="radio" name="face_margins" id="face-margins" value="0"/></td>
		  			<td><input type="radio" name="face_centering" id="face-centering" value="0"/></td>
		  			<td><input type="radio" name="back_margins" id="back-margins" value="0"/></td>
		  			<td><input type="radio" name="back_centering" id="back-centering" value="0"/></td>
		  			<td><input type="radio" name="registration" id="registration" value="0"/></td>
		  		</tr>
		  		<tr>
			  		<td><strong>Satisfactory</strong></td>
		  			<td><input type="radio" name="paper" id="paper" value="50"/></td>
		  			<td><input type="radio" name="color" id="color" value="50"/></td>
					<td><input type="radio" name="printing" id="printing" value="50"/></td>
		  			<td><input type="radio" name="face_margins" id="face-margins" value="50"/></td>
		  			<td><input type="radio" name="face_centering" id="face-centering" value="50"/></td>
		  			<td><input type="radio" name="back_margins" id="back-margins" value="50"/></td>
		  			<td><input type="radio" name="back_centering" id="back-centering" value="50"/></td>
		  			<td><input type="radio" name="registration" id="registration" value="50"/></td>
		  		</tr>
		  		<tr>
			  		<td><strong>Quality</strong>
		  			<td><input type="radio" name="paper" id="paper" value="100"/></td>
		  			<td><input type="radio" name="color" id="color" value="100"/></td>
		  			<td><input type="radio" name="printing" id="printing" value="100"/></td>
		  			<td><input type="radio" name="face_margins" id="face-margins" value="100"/></td>
		  			<td><input type="radio" name="face_centering" id="face-centering" value="100"/></td>
		  			<td><input type="radio" name="back_margins" id="back-margins" value="100"/></td>
		  			<td><input type="radio" name="back_centering" id="back-centering" value="100"/></td>
		  			<td><input type="radio" name="registration" id="registration" value="100"/></td>
		  		</tr>
			  	</table>
			</fieldset>
			<fieldset>
		    	<legend>Comments</legend>
		    	<textarea name="comments" rows="5" style="width:98%;"></textarea>
		    </fieldset>
			<fieldset id="sticker-set">
		    	<legend>Sticker</legend>
		  		<div class="controls">
			  		<label class="radio inline">
						<input type="radio" name="sticker" id="sticker" value="1"/>yes
					</label>
			  		<label class="radio inline">
						<input type="radio" name="sticker" id="sticker" value="0" checked/>no
					</label>
				</div>
				<label>
					Serial Number
					<input type="text" name="sticker_serial_number" id="sticker_serial" value="" />
				</label>
			</fieldset>
		</div>
	  <div class="modal-footer">
	  	<div class="alert alert-error"></div>
	    <button class="btn btn-primary">Save Review</button>
	  </div>
</form>
</div>


<script type="text/javascript">
	function format_grade_score(score){
		switch(parseInt(score)){
			case 0: return 'Other';
			case 50: return 'Satisfactory';
			case 100: return 'Superior';
			default: return 'Unknown';
		}
	}
    					
	$(function(){
		$('#grade-modal').find('.alert-error').hide();
		
		$('#order-form').on('click', '.grade-btn', function(){
			$('#grade-modal').find('.alert-error').hide();
			$('#scores').empty();
			$('#sticker-set').hide();
			var note = notes[$(this).attr('note_id')];
			var id = '';
			if(note){
				id = note.id;
			}
			$('#frm-grade').find('[type=radio]').attr('checked', false);
			$('#frm-grade').find('[name=comments]').val('');
			$('#frm-grade').find('[name=sticker_serial_number]').val('');
			
			$('#frm-grade').find('[name=note_id]').val(note.id);
			$('#frm-grade').find('[name=final]').val(0);
		});

		var grades = [{'id' : 0, 'name' : 'Other'}, {'id' : 50, 'name' : 'Satisfactory'}, {'id' : 100, 'name' : 'Superior'}];
		
		$('#order-form').on('click', '.final-grade-btn', function(){
			$('#grade-modal').find('.alert-error').hide();
			$('#scores').empty();
			$('#sticker-set').show();
			$('#frm-grade').find('[type=radio]').attr('checked', false);
			$('#frm-grade').find('[name=comments]').val('');
			$('#frm-grade').find('[name=sticker_serial_number]').val('');
			
			var note = notes[$(this).attr('note_id')];
			var id = '';
			if(note){
				id = note.id;
				if(note.grades && note.grades.length){
					var tbl = $('<table class="table table-bordered"></table>');
					$('#scores').append(tbl);
					tbl.append('<tr>' +
							'<th>&nbsp;</th>' +
							'<th>Paper</th>' +
							'<th>Color</th>' +
							'<th>Printing</th>' +
							'<th>Face Margins</th>' +
							'<th>Face Centering</th>' +
							'<th>Back Margins</th>' +
							'<th>Back Centring</th>' +
							'<th>Registration</th>' +
						'</tr>');

					$.each(note.grades, function(indx, grade){
						$.each(grades, function(i, g){
							tbl.append('<tr valign="top">' +
									'<td><strong>' + g.name + '</strong></td>' +
									'<td><input type="checkbox"' + (g.id == grade.paper ? ' checked' : '') + ' disabled/></td>' +
									'<td><input type="checkbox"' + (g.id == grade.color ? ' checked' : '') + ' disabled/></td>' +
									'<td><input type="checkbox"' + (g.id == grade.printing ? ' checked' : '') + ' disabled/></td>' +
									'<td><input type="checkbox"' + (g.id == grade.face_margins ? ' checked' : '') + ' disabled/></td>' +
									'<td><input type="checkbox"' + (g.id == grade.face_centering ? ' checked' : '') + ' disabled/></td>' +
									'<td><input type="checkbox"' + (g.id == grade.back_margins ? ' checked' : '') + ' disabled/></td>' +
									'<td><input type="checkbox"' + (g.id == grade.back_centering ? ' checked' : '') + ' disabled/></td>' +
									'<td><input type="checkbox"' + (g.id == grade.registration ? ' checked' : '') + ' disabled/></td>' +
								'</tr>');
						});
						tbl.append('<tr>' +
								'<td><strong>Comments</strong>' +
								'<td colspan="8">' + grade.comments + '</td>' +
							'</tr>' +
							'<tr>' +
								'<td><strong>Reviewed By</strong></td>' +
								'<td colspan="8">' +
									(grade.by ? grade.by.name : 'unknown') +
								'</td>' +
							'</tr>');
					});						
				}				
			}
			$('#frm-grade').find('[name=note_id]').val(note.id);
			$('#frm-grade').find('[name=final]').val("1");
		});

		$('#frm-grade').submit(function(){
			$('#grade-modal').find('.alert-error').hide();
			var elm = $('#frm-grade').find('[name=paper]:checked');
			if(elm.length == 0){
				$('#grade-modal').find('.alert-error').show();
				$('#grade-modal').find('.alert-error').html('Please select a paper');
				return false;
			}
			var elm = $('#frm-grade').find('[name=color]:checked');
			if(elm.length == 0){
				$('#grade-modal').find('.alert-error').show();
				$('#grade-modal').find('.alert-error').html('Please select a color');
				return false;
			}
			var elm = $('#frm-grade').find('[name=printing]:checked');
			if(elm.length == 0){
				$('#grade-modal').find('.alert-error').show();
				$('#grade-modal').find('.alert-error').html('Please select a printing');
				return false;
			}
			var elm = $('#frm-grade').find('[name=face_margins]:checked');
			if(elm.length == 0){
				$('#grade-modal').find('.alert-error').show();
				$('#grade-modal').find('.alert-error').html('Please select a face margins');
				return false;
			}
			var elm = $('#frm-grade').find('[name=face_centering]:checked');
			if(elm.length == 0){
				$('#grade-modal').find('.alert-error').show();
				$('#grade-modal').find('.alert-error').html('Please select a face centering');
				return false;
			}
			var elm = $('#frm-grade').find('[name=back_margins]:checked');
			if(elm.length == 0){
				$('#grade-modal').find('.alert-error').show();
				$('#grade-modal').find('.alert-error').html('Please select a back margins');
				return false;
			}
			var elm = $('#frm-grade').find('[name=back_centering]:checked');
			if(elm.length == 0){
				$('#grade-modal').find('.alert-error').show();
				$('#grade-modal').find('.alert-error').html('Please select a back centering');
				return false;
			}
			var elm = $('#frm-grade').find('[name=registration]:checked');
			if(elm.length == 0){
				$('#grade-modal').find('.alert-error').show();
				$('#grade-modal').find('.alert-error').html('Please select a registration');
				return false;
			}

			if($('#frm-grade').find('[name=final]').val() == '1'){
				var elm = $('#frm-grade').find('[name=sticker]:checked');
				if(elm.length == 0){
					$('#grade-modal').find('.alert-error').show();
					$('#grade-modal').find('.alert-error').html('Please select if stickered');
					return false;
				}
				
				var stker = $('#frm-grade').find('[name=sticker]:checked').val();
				var serial_number = $('#frm-grade').find('[name=sticker_serial_number]').val();
				if (stker == 1  && $.trim(serial_number)==''){
					$('#grade-modal').find('.alert-error').show();
					$('#grade-modal').find('.alert-error').html('Sticker requires Serial Number');
					return false;
				}
			}
			return true;
		});
	});
</script>