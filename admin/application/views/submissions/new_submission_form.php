<?php 
function get_pp_size($region_id){
	switch($region_id){
		case 'us': return ' maxlength="1" style="width:16px;"';
		default: return ' maxlength="10" style="width:60px;"';
	}
}


function get_type_field($region_id){
	switch($region_id){
		case 'us': return "'<td>' + get_note_types_dropdown(data && data.note_type_id ? data.note_type_id : '') + '</td>'";
		default: return "'<td><input type=\"text\" name=\"note_country[]\" value=\"' + (data && data.note_country ? data.note_country : '') + '\" maxlength=\"255\" style=\"width:120px;\"/></td>'";
	}
}

?>
<script type="text/javascript">
	var access_token = "<?php echo $access_token; ?>";

	var notes = [];
	<?php if(isset($service_levels)){?>
	var services = [
	   <?php 
	   $itt = 0;
	   foreach($service_levels as $service_level){?>
	   { id: "<?php echo $service_level['id'];?>", level: "<?php echo $service_level['level'];?>", per_note : <?php echo $service_level['per_note'];?> }<?php if($itt++ < count($service_levels) - 1){ echo ', ';}?>
	   <?php }?>
	];
	<?php }?>
	<?php if(isset($note_types)){?>
	var note_types = [
	   <?php 
	   $itt = 0;
	   foreach($note_types as $note_type){?>
	   { id: "<?php echo $note_type['id'];?>", type: "<?php echo $note_type['type'];?>" }<?php if($itt++ < count($note_types) - 1){ echo ', ';}?>
	   <?php }?>
	];
	<?php }?>
	<?php if(isset($grading_services)){?>
	var grading_services = [
	   <?php 
	   $itt = 0;
	   foreach($grading_services as $grading_service){?>
	   { id: "<?php echo $grading_service['id'];?>", type: "<?php echo $grading_service['abbrev'];?>" }<?php if($itt++ < count($grading_services) - 1){ echo ', ';}?>
	   <?php }?>
	];
	<?php }?>
	<?php if(isset($postage)){?>
	var postages = [
	   <?php 
	   $itt = 0;
	   foreach($postage as $p){?>
	   { under_value: <?php echo $p['under_value'];?>, cost: <?php echo $p['cost'];?> }<?php if($itt++ < count($postage) - 1){ echo ', ';}?>
	   <?php }?>
	];
	<?php }?>

	function get_service_level_dropdown(level){
		if(!level) level = $('#order-form').find('[name="service_level[]"]').val();
		var str = '<select name="service_level[]" style="width:50px;">';
		$.each(services, function(index, service){
			if(service.per_note > 0){
				str += '<option per_note="' + service.per_note + '" value="' + service.id + '"' + (level == service.id ? ' selected="selected"' : '') + '>' + service.level + '</option>';
			}
		});
		str += '</select>';
		return str; 
	}
	function get_note_types_dropdown(type){
		var str = '<select name="note_type[]" class="note-types" style="width:150px;">';
		$.each(note_types, function(index, note_type){
			str += '<option value="' + note_type.id + '"' + (type == note_type.id ? ' selected="selected"' : '') + '>' + note_type.type + '</option>';
		});
		str += '</select>';
		return str; 
	}
	function get_grading_service_dropdown(data){
		var id = null;
		if(data.grading_service) id = data.grading_service.id;
		else if(data.grading_service_id) id = data.grading_service_id.id;
		var str = '<select name="grading_service[]" class="grading-service" style="width:100px;">';
		$.each(grading_services, function(index, grading_service){
			str += '<option value="' + grading_service.id + '"' + (id == grading_service.id ? ' selected="selected"' : '') + '>' + grading_service.type + '</option>';
		});
		str += '</select>';
		return str; 
	}
	
	function add_row(data){
		$('#order-form-footer').before('<tr class="note">' +
			'<td><a class="btn delete-row" href="javascript:void(0)"><i class="icon-minus"></i></a></td>' +
			'<td><input type="checkbox" name="received[]" class="cbx-note" value="' + (data ? data.id : '') + '"' + (data && data.status_id == 300 ? ' checked' : '') + '/>' +
				'<input type="hidden" name="note_id[]"  value="' + (data ? data.id : '') + '"/>' +
			'</td>' +
			'<td class="line-number">' + ($('#order-form').find('tr').length - 4) + '</td>' +
			'<td>' + get_service_level_dropdown(data && data.service_level_id ? data.service_level_id : '') + '</td>' +
			<?php echo get_type_field($region['id']);?> + 
			'<td>' +
				'<div class="input-prepend" style="width:100px;">' +
			  		'<span class="add-on">$</span>' +
			  		'<input style="width:60px;" name="denomination[]" type="text"' + (data && data.denomination ? ' value="' + data.denomination + '"' : '') + '/>' +
				'</div>' +
			'</td>' +
			'<td><input name="catalog_number[]" style="width:100px;"' + (data && data.catalog_number ? ' value="' + data.catalog_number + '"' : '') + '/></td>' +
			'<td><input name="pp[]" <?php echo get_pp_size($region['id']);?>' + (data && data.pp ? ' value="' + data.pp + '"' : '') + '/></td>' +
			'<td><input name="serial_number[]" style="width:100px;"' + (data && data.serial_number ? ' value="' + data.serial_number + '"' : '') + '/></td>' +
			'<td>' + get_grading_service_dropdown(data) + '</td>' + 
			'<td><input name="barcode[]" style="width:150px;"' + (data && data.barcode ? ' value="' + data.barcode + '"' : '') + '/></td>' +
			'<td>' +
				'<div class="input-prepend control-group" style="width:100px;">' +
			  		'<span class="add-on">$</span>' +
			  		'<input style="width:60px;" name="declared_value[]" type="text"' + (data && data.declared_value ? ' value="' + data.declared_value + '"' : '') + '/>' +
				'</div>' +
			'</td>' +
			'<td>' +
				'<div class="input-prepend control-group" style="width:100px;">' +
	  				'<span class="add-on">$</span>' +
	  				'<input style="width:60px;" name="service_cost[]" disabled type="text"' + (data && data.declared_value ? ' value="' + data.declared_value + '"' : '') + '/>' +
				'</div>' +
			'</td>' +			
			'</tr>');
	}

	function format_currency(c){
		if($.trim(c) == '') return '---';
		if(typeof(c)=='string' && isNaN(c)){
			c = c.replace(/,/g, '');
			c = parseFloat(c);
			if(isNaN(c)) return "---";
		}
		var num = new Number(c);
		return num.toFixed(2);
	}

	function calculate_declared_total(){
		$('#order-form').find('[name="denomination[]"]').each(function(){
			$(this).parent().removeClass('error');
			if($.trim($(this).val()) != ''){
				var val = format_currency($(this).val());
				var val = parseFloat(val);
				if(isNaN(val)){
					$(this).parent().addClass('error');
				}
				$(this).val(format_currency($(this).val()));
			}
		});
		
		var total = 0.0;
		$('#order-form').find('[name="declared_value[]"]').each(function(){
			$(this).parent().removeClass('error');
			if($.trim($(this).val()) != ''){
				var val = format_currency($(this).val());
				var val = parseFloat(val);
				if(isNaN(val)){
					$(this).parent().addClass('error');
				}
				else {
					total += val;
				}
				$(this).val(format_currency($(this).val()));
			}
		});
		$('#order-form').find('[name="total_declared_value"]').val(format_currency(total));
	}

	function calculate_total(){
		var total = 0.0;
		var postage = -1.0;
		$('#order-form').find('[name="service_level[]"]').each(function(){
			var tr = $(this).parents('tr');
			var per_note = parseFloat($(this).find('option:selected').attr('per_note'));
			if(per_note <= 0 || total < 0){
				tr.find('[name="service_cost[]"]').val('---');
				total = -1;
			}
			else {
				total += per_note;
				tr.find('[name="service_cost[]"]').val(format_currency(per_note));
			}			
		});


		if(total >= 0){
			$('#order-form').find('[name=sub_total]').val(format_currency(total));

			var postage_total = 0.0;
			if($('#pickup').val() == 'yes' || $.trim($('#fedex_account_number').val()) != ''){
				postage = 0;
			}
			else {
				$('#order-form').find('[name="declared_value[]"]').each(function(){
					if($.trim($(this).val()) != ''){
						var val = parseFloat($(this).val());
						if(!isNaN(val)){
							postage_total += val;
						}
					}
				});
				
				for(var i = 0; i < postages.length; i++){
					if(postages[i].under_value >= postage_total){
						postage = postages[i].cost;
						break;
					}
				}
			}
		}
		else {
			$('#order-form').find('[name=sub_total]').val('---');
		}

		if(postage < 0){
			$('#order-form').find('[name="postage"]').val('---');
			$('#order-form').find('[name="total"]').val('---');
			$('.submit-order').attr('disabled', true);
		}
		else {
			$('#order-form').find('[name="postage"]').val(format_currency(postage));

			total += postage;

			$('#order-form').find('[name="total"]').val(format_currency(total));
			$('.submit-order').attr('disabled', false);
		}
	}


	$(function(){
		$("#order-form").on("blur", "input[name='barcode[]']", function() {
			var $input = $(this);
			var val = $input.val();

			if (val.length > 0) {
				$.getJSON('/notes/check_barcode_exists/' + val, function(data) {
					if (data.exists) {
						$input.css('background-color', '#FFFFE0');
						alert('That barcode already exists in the system.');
					} else {
						$input.css('background-color', '#FFFFFF');
					}
				});
			}
		});
		
		$('#create-form').submit(function(){
			$('#display-error').hide();
			$('#errors-list').empty();
			$('.error').removeClass('error');
			$(this).find('input').each(function(){
				var inpt = $(this);
				var type = inpt.attr('type');
				var name = inpt.attr('name');
				if(name && name.indexOf('[]') < 0){
					switch(name){
					case 'address2':
					case 'total':
						break;
					case 'postage':
						if($.trim(inpt.val()) == '' || $.trim(inpt.val()) == '---'){
							inpt.parent().addClass('error');
							$('#errors-list').append('<li>No Postage for order, please email</li>');
						}			
					default:
						if($.trim(inpt.val()) == ''){
							inpt.parent().addClass('error');
							$('#errors-list').append('<li>Missing ' + name.replace('_', ' ') + '</li>');
						}			
						break;
					}
				}
			});
			if($(this).find('.error').length == 0)
				return true;
			else{
				$('#display-error').show();
				return false;
			}
		});

		<?php foreach($submission['notes'] as $note){?>
			var note = <?php echo json_encode($note);?>;
			add_row(note);
			notes.push(note);
		<?php }?>
		calculate_declared_total();	
		calculate_total();

		$('#order-form').on('click', '.delete-row', function(){
			var id = $(this).parent().parent().find("input[name='note_id[]']").val();

			for (var i = notes.length - 1; i >= 0; i--) {
				if (notes[i].id == id) {
					notes.splice(i, 1);
				}
			}
			
			$(this).parent().parent().remove();
			$('.line-number').each(function(index){
				$(this).empty();
				$(this).append((index+1) + '');
			});
			if($('#order-form').find('tr').length < 24){
				$('#order-form').find('.add-row').show();
			}	
			check_disabled();
			calculate_declared_total();	
			calculate_total();		
		});
		$('#order-form').on('click', '.add-row', function(){
			add_row();
			if($('#order-form').find('tr').length >= 24){
				$('#order-form').find('.add-row').hide();
			}
			$('.mark-received').attr('disabled', $('.cbx-note').length != $('.cbx-note:checked').length);
			calculate_declared_total();	
			calculate_total();
		});
		$('#order-form').on('change', '[name="denomination[]"]', function(){
			calculate_declared_total();	
			calculate_total();
		});

		$('#order-form').on('change', '[name="declared_value[]"]', function(){
			calculate_declared_total();	
			calculate_total();
		});

		$('#order-form').on('change', '[name="service_level[]"]', function(){
			var val  = $(this).val();
			$('#order-form').find('[name="service_level[]"]').val(val);
			
			calculate_total();
		});
		$('#order-form').find('[name="service_level[]"]').first().change();

		$('#create-form').on('change', '.postage-country', function(){
			var country = $(this).val();
			console.log(country);
			$.ajax({
				url : '/postage_rates/get/' + country,
				data: {
					access_token: access_token
				},
				success: function(json){
					if(json.status.code !== 0){
						alert(json.status.message);
					}
					else if(json.postage) {
						postage = [];
						$('#postage').empty();
						var html = '<thead><tr><th>Total Value</th>';
						$.each(json.postage, function(index, p){
							html += '<th>Under $' + p.under_value_formatted + '</th>';

							postage.push({under_value: p.under_value, cost : p.cost});
						});
						html += '<th>Anything Over</th></tr></thead>';
	    				html += '<tr><td>Additional Cost</td>';
						$.each(json.postage, function(index, p){
			    			html += '<td>' + p.cost_formatted + '</td>';
						});
						html += '<th>Please email</th></tr>';
						$('#postage').append(html);	    			
					}
				}
			});
		});
	});
</script>

<fieldset style="padding-left:10px;">
	<legend>Order #<?php echo $submission['order_number']?></legend>
	<form id="frm-submission" method="post" action="">
		<div id="display-error" class="container alert alert-error"<?php if(!isset($msg) || empty($msg)){?> style="display:none;"<?php }?>>
	    	Please correct the following errors:
	    	<ul id="errors-list">
	    	<?php if(is_array($msg)){
	    		foreach($msg as $key=>$value){?>
		    		<li><?php echo $value;?></li>
		    	<?php }
	    	} else {?>
	    		<?php echo $msg;?>
	    	<?php }?>
	    	</ul>
	    </div>
		<div class="container-fluid">
			<div class="row-fluid">
			  	<div class="span10">
		  			<strong>Status:</strong>
	  				<?php print $submission['status'];?> - <?php echo date('D, d M Y g:i A', strtotime($submission['updated_date']));?>
		  		</div>
		    </div>
		  	<?php if(!empty($submission['fedex_account_number'])){?>
		  	<div class="row-fluid">
			  	<div class="span8">
			  		<strong>Fedex Account # :</strong>
			  		<?php print $submission['fedex_account_number'];?>
			  		<input type="hidden" id="fedex_account_number" value="<?php print $submission['fedex_account_number'];?>"/>
			  	</div>
			</div>
		  	<?php }?>
		    <div class="row-fluid">
			    <div class="span2">
			    	<strong>Info</strong><br/>
			    	<?php echo $submission['name'];?><br/>
			    	<?php echo $submission['company_name'];?><br/>
			    	<?php echo $submission['phone_number'];?><br/>
			    	<?php echo $submission['email'];?><br/>
			    </div>
			    <div class="span2">
			    	<strong>Shipping Address</strong><br/>
			       	<?php echo $submission['address'];?><br/>
			    	<?php if(trim($submission['address2']) != ''){ ?>
			       	<?php echo $submission['address2'];?><br/>
			       	<?php }?>
			    	<?php echo $submission['city'];?>, <?php echo $submission['state_province'];?> <?php echo $submission['postal_code'];?><br/>
			    	<?php echo $submission['country'];?><br/>
				</div>
				<?php if ($submission['payment_type_id'] == 1) {?>
				<div class="span2">
			    	<strong>Billing Address</strong><br/>
			       	<?php echo $submission['billing_address'];?><br/>
			    	<?php if(trim($submission['billing_address2']) != ''){ ?>
			    	<?php echo $submission['billing_address2'];?><br/>
			    	<?php }?>
			    	<?php echo $submission['billing_city'];?>, <?php echo $submission['billing_state_province'];?> <?php echo $submission['billing_postal_code'];?><br/>
			    	<?php echo $submission['billing_country'];?><br/>
				</div>
				<?php }?>
				<?php switch($submission['payment_type_id']){
					case 1:?>
					    <div class="span2">
					    	<strong>Credit Card</strong><br/>
					    	<?php echo $submission['card_type'];?><br/>
					    	<?php echo $submission['name_on_card'];?><br/>
					    	############<?php echo $submission['card_last_four_numbers'];?><br/>
					    	<?php echo $submission['card_exp_month'];?>/<?php echo $submission['card_exp_year'];?><br/>
					    </div>
						<?php 
						break;
					case 2: ?>
					    <div class="span2">
					    	<strong>Check</strong><br/>
					    </div>				
						<?php 
						break;
				}?>
			</div>
			<div class="row-fluid">
	    		<table id="order-form" class="table table-bordered">
	    			<thead>
		    			<tr>
		    				<th>&nbsp;</th>
		    				<th>Received</th>
		    				<th>Line Number</th>
		    				<th>RQN Service</th>
	    					<th><?php echo get_column_name('type', $region['id']);?></th>
		    				<th>Denomination</th>
		    				<th><?php echo get_column_name('catalog_number', $submission['region_id']);?></th>
		    				<th><?php echo get_column_name('pp', $submission['region_id']);?></th>
		    				<th>Serial Number</th>
		    				<th>Grading Service</th>
		    				<th>Bar Code</th>
		    				<th>Declared Value</th>
		    				<th>Service Cost</th>
		    			</tr>
	    			</thead>
	    			<tr id="order-form-footer">
	    				<td><a class="btn add-row" href="javascript:void(0)"><i class="icon-plus"></i></a></td>
	    				<td colspan="10">
	    					<strong class="pull-right">Total Declared Value</strong>
	    				</td>
	    				<td>
	    					<div class="input-prepend" style="width:100px;">
				  				<span class="add-on">$</span>
				  				<input style="width:60px;" name="total_declared_value" type="text" disabled value="<?php if(isset($submission['total_declared_value'])){ echo $submission['total_declared_value'];}?>"/>
							</div>
	    				</td>
	    				<td>&nbsp;</td>
	    			</tr>
	    			<tr>
		    			<td colspan="12">
	    					<strong class="pull-right">Sub Total</strong>
	    				</td>
	    				<td>
	    					<div class="input-prepend" style="width:100px;">
				  				<span class="add-on">$</span>
				  				<input style="width:60px;" name="sub_total" type="text" disabled value="<?php if(isset($submission['sub_total'])){ echo $submission['sub_total'];}?>"/>
							</div>
	    				</td>
	    			</tr>
	    			<tr>
		    			<td colspan="12">
	    					<strong class="pull-right">Postage</strong>
	    					<?php if($submission['postage'] <= 0 && empty($submission['fedex_account_number'])){?>
			    				<span class="pull-right" style="margin-right:15px;">
									<input type="hidden" name="pickup" id="pickup" value="yes">
									I will pick up this order
				    			</span>
				    		<?php }?>
	    				</td>
	    				<td>
	    					<div class="input-prepend" style="width:100px;">
				  				<span class="add-on">$</span>
				  				<input style="width:60px;" name="postage" type="text" disabled value="<?php if(isset($submission['postage'])){ echo $submission['postage'];}?>"/>
							</div>
	    				</td>
	    			</tr>
	    			<tr>
		    			<td colspan="12">
	    					<strong class="pull-right">Total</strong>
	    				</td>
	    				<td>
	    					<div class="input-prepend" style="width:100px;">
				  				<span class="add-on">$</span>
				  				<input style="width:60px;" name="total" type="text" disabled value="<?php if(isset($submission['total'])){ echo $submission['total'];}?>"/>
							</div>
	    				</td>
	    			</tr>
	    		</table>
	    	</div>
			<div class="row-fluid">
				<input type="submit" class="mark-update btn" value="Save"/>
	      		<a href="#mark-incomplete-comments" role="button" class="btn mark-incomplete" data-toggle="modal">Mark Incomplete</a>
	      		<input type="submit" class="mark-received btn" disabled="disabled" value="Mark Received"/>
	      	</div>
		</div>
		<div id="mark-incomplete-comments" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		  <div class="modal-header">
		    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
		    <h3 id="myModalLabel">Incomplete Comments</h3>
		  </div>
		  <div class="modal-body">
		  	<textarea style="width:98%" rows="10" name="comments"></textarea>
		  </div>
		  <div class="modal-footer">
		    <input type="submit" class="btn btn-primary" value="Mark Incomplete"/>
		  </div>
		</div>
	</form>
</fieldset>
<script type="text/javascript">

	function check_changes(){
		var change = false;
		$('#order-form').find('tr.note').each(function(indx){
			if(notes.length <= indx){
				change = true;
				return false;
			}
			/*
			var note = notes[indx]; 
			var tr = $(this);
			if(note.service_level_id != tr.find('[name="service_level[]"]').val()){
				change = true;
				return false;
			}
			else if(note.note_type_id != tr.find('[name="note_type[]"]').val()){
				change = true;
				return false;
			}
			else if(note.denomination != tr.find('[name="denomination[]"]').val()){
				change = true;
				return false;
			}
			else if(note.catalog_number != tr.find('[name="catalog_number[]"]').val()){
				change = true;
				return false;
			}
			else if(note.pp != tr.find('[name="pp[]"]').val()){
				change = true;
				return false;
			}
			else if(note.serial_number != tr.find('[name="serial_number[]"]').val()){
				change = true;
				return false;
			}
			else if(note.grading_level_id != tr.find('[name="grading_level[]"]').val()){
				change = true;
				return false;
			} 
			else if(note.barcode != tr.find('[name="barcode[]"]').val()){ //Even though changing barcode is allowed, force them to edit first
				change = true;
				return false;
			}	*/		
		});
		return change;
	}

	function check_disabled(){
		var disabled = false;

		if($('.cbx-note').length != notes.length){
			disabled = true;
		}
		else if($('.cbx-note').length != $('.cbx-note:checked').length){
			disabled = true;
		}
		else if(check_changes()){
			disabled = true;
		}
		
		$('.mark-received').attr('disabled', disabled);
	}

	$(function(){
		$('.mark-incomplete').click(function(){
			$('#frm-submission').attr('action', '/submissions/mark_incomplete/<?php echo $submission['id']?>');
		});
		$('.mark-update').click(function(){
			$('#frm-submission').attr('action', '/submissions/edit_submission/<?php echo $submission['id']?>');
		});
		$('.mark-received').click(function(){
			if(!$('.mark-received').is(':disabled')){
				$('#frm-submission').attr('action', '/submissions/mark_received/<?php echo $submission['id']?>');
			}
		});

		if($('.cbx-note').length == $('.cbx-note:checked').length){
			$('.mark-received').attr('disabled', false);
		}
		
		$('#order-form').on('change', 'input', check_disabled);
		$('#order-form').on('change', 'select', check_disabled);
		$('#order-form').on('click', '.cbx-note', check_disabled);		
	});
</script>
