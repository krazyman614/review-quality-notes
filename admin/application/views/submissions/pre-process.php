<fieldset>
	<legend>Order #<?php echo $submission['order_number']?></legend>
	<?php if(isset($msg) && !empty($msg)){?>
		<div class="alert">
			<?php echo $msg;?>
		</div>
	<?php }?>
	<form id="frm-submission" method="post" action="/submissions/mark_in_process/<?php echo $submission['id']?>">
		<div class="container-fluid">
		  <div class="container-fluid">
			<div class="row-fluid">
			  	<div class="span10">
		  			<strong>Status:</strong>
	  				<?php print $submission['status'];?> - <?php echo date('D, d M Y g:i A', strtotime($submission['updated_date']));?>
		  		</div>
		    </div>
		  	<?php if(!empty($submission['fedex_account_number'])){?>
		  	<div class="row-fluid">
			  	<div class="span8">
			  		<strong>Fedex Account # :</strong>
			  		<?php print $submission['fedex_account_number'];?>
			  	</div>
			</div>
		  	<?php }?>
		    <div class="row-fluid">
			    <div class="span2">
			    	<strong>Info</strong><br/>
			    	<?php echo $submission['name'];?><br/>
			    	<?php echo $submission['company_name'];?><br/>
			    	<?php echo $submission['phone_number'];?><br/>
			    	<?php echo $submission['email'];?><br/>
			    </div>
			    <div class="span2">
			    	<strong>Shipping Address</strong><br/>
			       	<?php echo $submission['address'];?><br/>
			    	<?php if(trim($submission['address2']) != ''){ ?>
			       	<?php echo $submission['address2'];?><br/>
			       	<?php }?>
			    	<?php echo $submission['city'];?>, <?php echo $submission['state_province'];?> <?php echo $submission['postal_code'];?><br/>
			    	<?php echo $submission['country'];?><br/>
				</div>
				<?php if ($submission['payment_type_id'] == 1) {?>
				<div class="span2">
			    	<strong>Billing Address</strong><br/>
			       	<?php echo $submission['billing_address'];?><br/>
			    	<?php if(trim($submission['billing_address2']) != ''){ ?>
			    	<?php echo $submission['billing_address2'];?><br/>
			    	<?php }?>
			    	<?php echo $submission['billing_city'];?>, <?php echo $submission['billing_state_province'];?> <?php echo $submission['billing_postal_code'];?><br/>
			    	<?php echo $submission['billing_country'];?><br/>
				</div>
				<?php }?>
				<?php switch($submission['payment_type_id']){
					case 1:?>
					    <div class="span2">
					    	<strong>Credit Card</strong><br/>
					    	<?php echo $submission['card_type'];?><br/>
					    	<?php echo $submission['name_on_card'];?><br/>
					    	############<?php echo $submission['card_last_four_numbers'];?><br/>
					    	<?php echo $submission['card_exp_month'];?>/<?php echo $submission['card_exp_year'];?><br/>
					    </div>
						<?php 
						break;
					case 2: ?>
					    <div class="span2">
					    	<strong>Check</strong><br/>
					    </div>				
						<?php 
						break;
				}?>
			</div>
			<div class="row-fluid">
	    		<?php $this->load->view('inc/notes', array('submission' => $submission, 'current_admin' => $current_admin));?>
			</div>
			<div class="row-fluid">
				<input type="submit" class="btn" value="Mark In Process"/>
			</div>
		   </div>
		</div>
	</form>
</fieldset>