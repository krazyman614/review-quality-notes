
<form method="post" class="form-horizontal" action="/home/login">
	<div id="display-error" class="alert alert-error"<?php if(!isset($errors)){?> style="display:none;"<?php }?>>
    	Please correct the following errors:
    	<ul id="errors-list">
    	<?php 
    	if(isset($errors)){
	    	if(is_array($errors)){
		    	foreach($errors as $key=>$value){?>
		    		<li><?php echo $value;?></li>
		    	<?php }
	    	}
	    	else {?>
	    		<li><?php echo $errors?></li>
	    	<?php }
	    }?>
    	</ul>
    </div>
	<div class="control-group<?php if(isset($errors['email'])){ echo ' error';}?>">
		<label class="control-label" for="inputEmail">Email</label>
		<div class="controls">
			<input type="text" name="email" placeholder="Email" value="<?php if(isset($email)){ echo $email; }?>" autofocus/>
		</div>
	</div>
	<div class="control-group<?php if(isset($errors['email'])){ echo ' error';}?>">
		<label class="control-label" for="inputPassword">Password</label>
		<div class="controls">
			<input type="password" name="password" id="inputPassword" placeholder="Password" value="<?php if(isset($password)){ echo $password; }?>"/>
		</div>
	</div>
	<div class="control-group">
		<div class="controls">
			<label class="checkbox"> 
				<input type="checkbox" name="remember_me" value="yes"/>Remember me
			</label>
			<button type="submit" class="btn">Sign in</button>
		</div>
	</div>
</form>