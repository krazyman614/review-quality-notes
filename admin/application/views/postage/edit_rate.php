<script>
	$(function(){
		$('#create-form').submit(function(){
			$('#display-error').hide();
			$('#errors-list').empty();
			$('.error').removeClass('error');
			$(this).find('input').each(function(){
				var inpt = $(this);
				var name = inpt.attr('name');
				if($.trim(inpt.val()) == ''){
					inpt.parent().addClass('error');
					$('#errors-list').append('<li>Missing ' + name.replace('_', ' ') + '</li>');
				}
			});
			if($(this).find('.error').length == 0)
				return true;
			else{
				$('#display-error').show();
				return false;
			}
		});
	});
</script>
<form method="post" id="create-form" action="/postage/post_edit_rate/<?php echo $postage_country['id'];?>">
	<input type="hidden" name="id" value="<?php if(isset($data['id'])){ echo $data['id'];}?>"/>	
	<fieldset>
		<legend>Edit Postage Rate</legend>
		<?php if(isset($success) && $success){?>
			<div class="alert alert-success">
				Your postage rate has been saved
			</div>
		<?php }?>
		<div id="display-error" class="alert alert-error"<?php if(!isset($errors)){?> style="display:none;"<?php }?>>
	    	Please correct the following errors:
	    	<ul id="errors-list">
	    	<?php foreach($errors as $key=>$value){?>
	    		<li><?php echo $value;?></li>
	    	<?php }?>
	    	</ul>
	    </div>
		<div class="control-group<?php if(isset($errors['name'])){ ?> error<?php }?>">
			<label>Name</label>
			<input type="text" name="name" placeholder="Name" value="<?php if(isset($data['name'])){ echo $data['name'];}?>" class="span5" maxlength="100"/>
		</div>
		<div class="input-prepend input-append control-group<?php if(isset($errors['under_value'])){ ?> error<?php }?>">
			<label>Under Value</label>
			<span class="add-on">$</span>
			<input type="text" name="under_value" placeholder="Under Value" value="<?php if(isset($data['under_value'])){ echo $data['under_value'];}?>" class="span2" maxlength="5"/>
			<span class="add-on">.00</span>
		</div>
		<div class="input-prepend input-append control-group<?php if(isset($errors['cost'])){ ?> error<?php }?>">
			<label>Cost</label>
			<span class="add-on">$</span>
			<input type="text" name="cost" placeholder="Cost" value="<?php if(isset($data['cost'])){ echo $data['cost'];}?>" class="span2" maxlength="5"/>
			<span class="add-on">.00</span>
		</div>
		<div class="control-group<?php if(isset($errors['status_id'])){ ?> error<?php }?>">
			<label>Status</label>
			<select name="status_id">
				<option value="1" <?php if(isset($data['status_id']) && $data['status_id'] == '1'){?>selected="selected"<?php }?>>Active</option>
				<option value="0" <?php if(isset($data['status_id']) && $data['status_id'] == '0'){?>selected="selected"<?php }?>>Inactive</option>
			</select>
		</div>
		<button type="submit" class="btn">Edit Postage Rate</button><a href="/postage/rates/<?php echo $postage_country['id'];?>" title="Back to Postages" class="btn">Back</a>
	</fieldset>
</form>