<script type="text/javascript">
	function change_page(page_number){
		self.location.href = '/postage/rates_page/<?php echo $postage_country['id'];?>/<? echo $opts['page_size'] . '/'?>' + page_number;
	}
</script>
<fieldset>
	<legend>Postage Rates <?php if(isset($postage_country)){?> - <?php echo $postage_country['name'];?><?php }?></legend>
	<div id="rates">
		<div>
			<a href="/postage/add_rate/<?php echo $postage_country['id'];?>" class="btn pull-right">New Postage Rate</a>
		</div>
		<table class="table table-hover">
		<tr>
			<th>Name</th>
			<th>Under Value</th>
			<th>Cost</th>
			<th>Status</th>
			<th>&nbsp;</th>
		<tr>
		<?php 
		if(isset($rates)){
			foreach($rates as $rate){
				?>
				<tr>
					<td><?php echo $rate['name'];?></td>
					<td>$<?php echo money_format('%i',$rate['under_value']);?></td>
					<td>$<?php echo money_format('%i',$rate['cost']);?></td>
					<td><?php echo ($rate['status_id'] > 0 ? 'Active' : 'Inactive');?>
					<td><a href="/postage/edit_rate/<?php echo $postage_country['id'];?>/<?php echo $rate['id'];?>" class="btn btn-mini">Edit</a></td>
				</tr>			
			<?php } ?>
			<?php if($count > $opts['page_size']){?>
			<tr>
				<td colspan="5">
					<div class="pagination pagination-right">
					  <ul>
					    <li <?php if($opts['page_number'] == 1){?>class='disabled'<?php }?>><a href="javascript:change_page(1);">Prev</a></li>
					    <?php 
					    $end_page = ceil($count / $opts['page_size']);
					    for($i = 1; $i <= $end_page; $i++){?>
					    <li <?php if($opts['page_number'] == $i){?>class='active'<?php }?>><a href="javascript:change_page(<?php echo $i;?>);"><?php echo $i;?></a></li>
					    <?php }?>
					    <li <?php if($opts['page_number'] == $end_page){?>class='disabled'<?php }?>><a href="javascript:change_page(<?php echo $end_page;?>);">Next</a></li>
					  </ul>
					</div>
				</td>
			</tr>
			<?php }?>
		</table>
		<?php }?>
		<div>
			<a href="/postage/add_rate/<?php echo $postage_country['id'];?>" class="btn pull-right">New Postage Rate</a>
		</div>
		
	</div>
</fieldset>