<script>
	$(function(){
		$('#create-form').submit(function(){
			$('#display-error').hide();
			$('#errors-list').empty();
			$('.error').removeClass('error');
			$(this).find('input').each(function(){
				var inpt = $(this);
				var name = inpt.attr('name');
				if($.trim(inpt.val()) == ''){
					inpt.parent().addClass('error');
					$('#errors-list').append('<li>Missing ' + name.replace('_', ' ') + '</li>');
				}
			});
			if($(this).find('.error').length == 0)
				return true;
			else{
				$('#display-error').show();
				return false;
			}
		});
	});
</script>
<form method="post" id="create-form" action="/postage/create">
	<fieldset>
		<legend>Create Postage Country</legend>
		<div id="display-error" class="alert alert-error"<?php if(!isset($errors)){?> style="display:none;"<?php }?>>
	    	Please correct the following errors:
	    	<ul id="errors-list">
	    	<?php foreach($errors as $key=>$value){?>
	    		<li><?php echo $value;?></li>
	    	<?php }?>
	    	</ul>
	    </div>
	    
		<div class="control-group<?php if(isset($errors['full_name'])){ ?> error<?php }?>">
			<label>Full Name</label>
			<input type="text" name="full_name" placeholder="Full Name" value="<?php if(isset($data['full_name'])){ echo $data['full_name'];}?>" class="span5" maxlength="255"/>
		</div>
		<div class="control-group<?php if(isset($errors['name'])){ ?> error<?php }?>">
			<label>Abbrev</label>
			<input type="text" name="name" placeholder="Abbrev" value="<?php if(isset($data['name'])){ echo $data['name'];}?>" class="span1" maxlength="4"/>
		</div>
		<?php if(isset($regions)){?>
		<div class="control-group<?php if(isset($errors['region_id'])){ ?> error<?php }?>">
			<label>Region</label>
			<select name="region_id">
				<?php foreach($regions as $region){?>		
				<option value="<?php echo $region['id'];?>" <?php if(isset($data['region_id']) && $data['region_id'] == $region['id']){?>selected="selected"<?php }?>><?php echo $region['name'];?></option>
				<?php }?>
			</select>
		</div>
		<?php }?>
		<div class="control-group<?php if(isset($errors['status_id'])){ ?> error<?php }?>">
			<label>Status</label>
			<select name="status_id">
				<option value="1" <?php if(isset($data['status_id']) && $data['status_id'] == '1'){?>selected="selected"<?php }?>>Active</option>
				<option value="0" <?php if(isset($data['status_id']) && $data['status_id'] == '0'){?>selected="selected"<?php }?>>Inactive</option>
			</select>
		</div>
		<button type="submit" class="btn">Create Postage Country</button><a href="/postage/" title="Back to Postages" class="btn">Back</a>
	</fieldset>
</form>