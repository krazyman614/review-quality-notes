<script type="text/javascript">
	function change_page(page_number){
		self.location.href = '/postage/page/<? echo $opts['page_size'] . '/'?>' + page_number;
	}
</script>
<fieldset>
	<legend>Postage</legend>
	<div id="postages">
		<div>
			<a href="/postage/add" class="btn pull-right">New Postage</a>
		</div>
		<table class="table table-hover">
		<tr>
			<th>Name</th>
			<th>Abbrev</th>
			<th>Status</th>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
		<tr>
		<?php 
		if(isset($postages)){
			foreach($postages as $postage){
				?>
				<tr>
					<td><?php echo $postage['full_name'];?></td>
					<td><?php echo $postage['name'];?></td>
					<td><?php echo ($postage['status_id'] > 0 ? 'Active' : 'Inactive');?>
					<td><a href="/postage/edit/<?php echo $postage['id'];?>" class="btn btn-mini">Edit</a></td>
					<td><a href="/postage/rates/<?php echo $postage['id'];?>" class="btn btn-mini">Rates</a></td>
					<td><a href="/postage/delete/<?php echo $postage['id'];?>" onclick="return confirm('Are you sure you want to delete this?');" class="btn btn-mini">Delete</a></td>
				</tr>			
			<?php } ?>
			<?php if($count > $opts['page_size']){?>
			<tr>
				<td colspan="5">
					<div class="pagination pagination-right">
					  <ul>
					    <li <?php if($opts['page_number'] == 1){?>class='disabled'<?php }?>><a href="javascript:change_page(1);">Prev</a></li>
					    <?php 
					    $end_page = ceil($count / $opts['page_size']);
					    for($i = 1; $i <= $end_page; $i++){?>
					    <li <?php if($opts['page_number'] == $i){?>class='active'<?php }?>><a href="javascript:change_page(<?php echo $i;?>);"><?php echo $i;?></a></li>
					    <?php }?>
					    <li <?php if($opts['page_number'] == $end_page){?>class='disabled'<?php }?>><a href="javascript:change_page(<?php echo $end_page;?>);">Next</a></li>
					  </ul>
					</div>
				</td>
			</tr>
			<?php }?>
		</table>
		<?php }?>
		<div>
			<a href="/postage/add" class="btn pull-right">New Postage</a>
		</div>
		
	</div>
</fieldset>