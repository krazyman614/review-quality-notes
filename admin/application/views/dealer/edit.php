<?php 
$data = array();
$data = mysql_fetch_array(mysql_query("select * from directory where id='".addslashes($id)."'"));

?>
<form method="post" id="create-form" action="/dealer/save">
	<input type="hidden" name="id" value="<?=$id?>"/>
	<fieldset>
		<legend>Edit Dealer</legend>
		<?php if(isset($success) && $success){?>
			<div class="alert alert-success">
				Your special has been updated
			</div>
		<?php }?>
		
		<div class="control-group">
			<label>Name</label>
			<input type="text" name="name" value="<?php if(isset($data['name'])){ echo $data['name'];}?>" class="span5" maxlength="100"/>
		</div>
		<div class="control-group">
			<label>Business</label>
			<input type="text" name="business" value="<?php if(isset($data['business'])){ echo $data['business'];}?>" class="span5" maxlength="100"/>
		</div>
		<div class="control-group">
			<label>Address</label>
			<input type="text" name="address"  value="<?php if(isset($data['address'])){ echo $data['address'];}?>" class="span5" maxlength="100"/>
		</div>
		<div class="control-group">
			<label>City</label>
			<input type="text" name="city" value="<?php if(isset($data['city'])){ echo $data['city'];}?>" class="span5" maxlength="100"/>
		</div>
		<div class="control-group">
			<label>State</label>
			<input type="text" name="state" value="<?php if(isset($data['state'])){ echo $data['state'];}?>" class="span5" maxlength="100"/>
		</div>
		<div class="control-group">
			<label>Zip</label>
			<input type="text" name="zip" value="<?php if(isset($data['zip'])){ echo $data['zip'];}?>" class="span5" maxlength="100"/>
		</div>
		<div class="control-group">
			<label>Work Phone</label>
			<input type="text" name="work_phone" value="<?php if(isset($data['work_phone'])){ echo $data['work_phone'];}?>" class="span5" maxlength="100"/>
		</div>
		<div class="control-group">
			<label>Cell Phone</label>
			<input type="text" name="phone" value="<?php if(isset($data['phone'])){ echo $data['phone'];}?>" class="span5" maxlength="100"/>
		</div>
		<div class="control-group">
			<label>Fax</label>
			<input type="text" name="fax" value="<?php if(isset($data['fax'])){ echo $data['fax'];}?>" class="span5" maxlength="100"/>
		</div>
		<div class="control-group">
			<label>Website</label>
			<input type="text" name="website"  value="<?php if(isset($data['website'])){ echo $data['website'];}?>" class="span5" maxlength="100"/>
		</div>
		<div class="control-group">
			<label>Email</label>
			<input type="text" name="email"  value="<?php if(isset($data['email'])){ echo $data['email'];}?>" class="span5" maxlength="100"/>
		</div>
		<div class="control-group">
			<label>What they sell/buy</label>
			<textarea name='sell' rows='10' cols='150'><?php if(isset($data['sell'])){ echo $data['sell'];}?></textarea>
		</div>
		<button type="submit" class="btn">Edit Dealer</button><a href="/dealers/" class="btn">Back</a>
	</fieldset>
</form>