<script>
	$(function(){
		$('#create-form').submit(function(){
			//Error Display
			$('#display-error').hide();
			$('#errors-list').empty();
			$('.error').removeClass('error');
			$(this).find('input').each(function(){
				var inpt = $(this);
				var name = inpt.attr('name');
				if($.trim(inpt.val()) == ''){
					inpt.parent().addClass('error');
					$('#errors-list').append('<li>Missing ' + name.replace('_', ' ') + '</li>');
				}			
			});
			if($(this).find('.error').length == 0)
				return true;
			else{
				$('#display-error').show();
				return false; //don't submit to server
			}
		});

		$('.date').datepicker({
			format: 'mm/dd/yyyy'
		});
	});
</script>
<form method="post" id="create-form" action="/specials/create">
	<fieldset>
		<legend>Create Special</legend>
		<div id="display-error" class="alert alert-error"<?php if(!isset($errors)){?> style="display:none;"<?php }?>>
	    	Please correct the following errors:
	    	<ul id="errors-list">
	    	<?php foreach($errors as $key=>$value){?>
	    		<li><?php echo $value;?></li>
	    	<?php }?>
	    	</ul>
	    </div>
		<div class="control-group<?php if(isset($errors['title'])){ ?> error<?php }?>">
			<label>Title</label>
			<input type="text" name="title" placeholder="Title" value="<?php if(isset($data['title'])){ echo $data['title'];}?>" class="span5" maxlength="1000"/>
		</div>
		<div class="control-group<?php if(isset($errors['expires_date'])){ ?> error<?php }?>">
			<label>Expiration Date</label>
			<div class="input-append date" data-date="<?php echo date('m/d/Y', isset($data['expires_date']) ? $data['expires_date'] : time());?>" data-date-format="dd/mm/yyyy">
			  	<input class="span2" size="16" type="text" value="<?php echo date('m/d/Y', isset($data['date']) ? $data['expires_date'] : time());?>" name="expires_date"/>
  				<span class="add-on"><i class="icon-th"></i></span>
			</div>
		</div>
		<div class="control-group<?php if(isset($errors['description'])){ ?> error<?php }?>">
			<label>Description</label>
			<textarea name="description" rows="5" class="span5"><?php if(isset($data['description'])){ echo $data['description'];}?></textarea>
		</div>
		<div class="control-group<?php if(isset($errors['status_id'])){ ?> error<?php }?>">
			<label>Status</label>
			<select name="status_id">
				<option value="1" <?php if(isset($data['status_id']) && $data['status_id'] == '1'){?>selected="selected"<?php }?>>Active</option>
				<option value="0" <?php if(isset($data['status_id']) && $data['status_id'] == '0'){?>selected="selected"<?php }?>>Inactive</option>
			</select>
		</div>
		<button type="submit" class="btn">Create Special</button><a href="/specials/" class="btn">Back</a>
	</fieldset>
</form>