<script type="text/javascript">
	function change_page(page_number){
		self.location.href = '/specials/page/<? echo $opts['page_size'] . '/'?>' + page_number;
	}
</script>
<fieldset>
	<legend>Specials</legend>
	<div id="specials">
		<table class="table table-hover">
		<tr>
			<th>Expiration Date</th>
			<th>Title</th>
			<th>Status</th>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
		<tr>
		<?php 
		if(isset($specials)){
			foreach($specials as $special){
				?>
				<tr>
					<td><?php echo date("F j, Y", $special['expires_date']);?></td>
					<td><?php echo $special['title'];?></td>
					<td><?php echo ($special['status_id'] > 0 ? 'Active' : 'Inactive');?></td>
					<td><a href="/specials/edit/<?php echo $special['id'];?>" class="btn btn-mini">Edit</a></td>
					<td><a href="/specials/delete/<?php echo $special['id'];?>" class="btn btn-mini">Delete</a></td>
				</tr>			
			<?php } ?>
			<?php if($count > $opts['page_size']){?>
			<tr>
				<td colspan="5">
					<div class="pagination pagination-right">
					  <ul>
					    <li <?php if($opts['page_number'] == 1){?>class='disabled'<?php }?>><a href="javascript:change_page(1);">Prev</a></li>
					    <?php 
					    $end_page = ceil($count / $opts['page_size']);
					    for($i = 1; $i <= $end_page; $i++){?>
					    <li <?php if($opts['page_number'] == $i){?>class='active'<?php }?>><a href="javascript:change_page(<?php echo $i;?>);"><?php echo $i;?></a></li>
					    <?php }?>
					    <li <?php if($opts['page_number'] == $end_page){?>class='disabled'<?php }?>><a href="javascript:change_page(<?php echo $end_page;?>);">Next</a></li>
					  </ul>
					</div>
				</td>
			</tr>
			<?php }?>
		</table>
		<?php }?>
		<div>
			<a href="/specials/add" class="btn pull-right">New Special</a>
		</div>
		
	</div>
</fieldset>